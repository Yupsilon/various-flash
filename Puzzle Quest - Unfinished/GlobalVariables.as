﻿package  {
	import flash.text.*;
	
	public class GlobalVariables {

		static public var xk_MAX_MANA_TYPES:Number=6;
		static public var GEM_RED:Number=0;
		static public var GEM_BLUE:Number=1;
		static public var GEM_GREEN:Number=2;
		static public var GEM_YELLOW:Number=3;
		static public var GEM_SKULL:Number=4;
		static public var GEM_PURPLE:Number=5;
		
		static public var stageWidth=750;
		static public var stageHeight=400;
		static public var OverlayWidth=150;
		
		static public var AiIdleTime=20;
		static public var IdleTime=25;
		
		static public var Difficulty=[1,1.2,1.25];
		
		static public var MaxArmySpells=3;
		static public var MaxArmyStack=4;
		static public var MaxLevel=100;
		static public var HPBalancer=6;
		static public var ABalancer=4;
		static public var DBalancer=4;
		static public var SBalancer=4;
		static public var Protection=2;
		static public var ManaRegenChance=10;
		static public var ArmyPassiveTresspass=12;
		static public var LossPenalty=0.25;

		static public var GemSize:Number=40;
		static public var GemSpeed:Number=15;
		static public var ChancePurple:Number=60;
		static public var MapMoveSpeed:Number=10;		
		
		static public var TextPLAYERNAME:TextFormat= new TextFormat();		
		TextPLAYERNAME.font="Uncial ATT";
		TextPLAYERNAME.color=0xFFFFFF;
		TextPLAYERNAME.size=15;
		TextPLAYERNAME.align="center";		
		static public var TextARMYNAME:TextFormat= new TextFormat();		
		TextARMYNAME.font="Arial";
		TextARMYNAME.color=0xFFFFFF;
		TextARMYNAME.size=15;
		TextARMYNAME.align="left";		
		static public var TextARMYUI:TextFormat= new TextFormat();		
		TextARMYUI.font="Arial";
		TextARMYUI.color=0xFFFFFF;
		TextARMYUI.size=10;
		TextARMYUI.align="left";		
		static public var TextLIFEBAR:TextFormat= new TextFormat();		
		TextLIFEBAR.font="Arial";
		TextLIFEBAR.color=0xFFFFFF;
		TextLIFEBAR.size=10;
		TextLIFEBAR.align="center";
		static public var TextINGAMEMENU:TextFormat= new TextFormat();		
		TextINGAMEMENU.font="Arial";
		TextINGAMEMENU.color=0xFFFFFF;
		TextINGAMEMENU.size=16;
		TextINGAMEMENU.align="center";		
		static public var TextPOPUPEFX:TextFormat= new TextFormat();		
		TextPOPUPEFX.font="Uncial ATT";
		TextPOPUPEFX.color=0xFFFFFF;
		TextPOPUPEFX.size=15;
		TextPOPUPEFX.align="center";		
		static public var TextPOPUPEFXBG:TextFormat= new TextFormat();		
		TextPOPUPEFXBG.font="Uncial ATT";
		TextPOPUPEFXBG.color=0x000000;
		TextPOPUPEFXBG.size=15;
		TextPOPUPEFXBG.align="center";	
		static public var TextENDSCREEN:TextFormat= new TextFormat();		
		TextENDSCREEN.font="Arial";
		TextENDSCREEN.color=0xFFFFFF;
		TextENDSCREEN.size=16;
		TextENDSCREEN.align="left";		

		static public var textHUBWORLD:TextFormat= new TextFormat();		
		textHUBWORLD.font="Arial";
		textHUBWORLD.color=0xFFFFFF;
		textHUBWORLD.size=10;
		textHUBWORLD.align="center";		
		
		static public var TextBARRACKS:TextFormat= new TextFormat();		
		TextBARRACKS.font="Arial";
		TextBARRACKS.color=0xFFFFFF;
		TextBARRACKS.size=13;
		TextBARRACKS.align="left";	
		
		static public var textMESSAGE:TextFormat= new TextFormat();		
		textMESSAGE.font="Arial";
		textMESSAGE.color=0x000000;
		textMESSAGE.size=10;
		textMESSAGE.align="center";
		
		static public var textMAINMENU:TextFormat= new TextFormat();		
		textMAINMENU.font="Arial";
		textMAINMENU.color=0xFFFFFF;
		textMAINMENU.size=10;
		textMAINMENU.align="center";		
		
	}
	
}
