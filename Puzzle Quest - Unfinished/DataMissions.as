﻿package  {
	
	public class DataMissions {
		
		public static function GetDifficulty(game:HubWorldHandler,Modify:Boolean=true):Number
		{
			var Zim=MapData.GetCombinedPartyLevel(game);
			if (Zim==0)
			{
				return 0.6*game.Memory["Difficulty"];
			}
			else if (Zim<50)
			{
				return 0.9*game.Memory["Difficulty"];
			}
			else if (Zim>250)
			{
				return 1.1*game.Memory["Difficulty"];
			}
			else 
			{
				return 1*game.Memory["Difficulty"];
			}
		}

		public static function MakeNewGame(game:HubWorldHandler,MissionData:Array) {
			
			var NewGame:MainGame=new MainGame();			
					
			var enemyarmies:Array=MissionData[4][0];
			var alliedarmies:Array=MissionData[4][1];
			
			NewGame.init(game);
			
			NewGame.Memory["Difficulty"]=GetDifficulty(game);
			
			NewGame.Memory["DataItemArmies"]=new Array();
			NewGame.Memory["DataItemArmies"][0]=new Array();
			
			NewGame.Memory["PlayerNames"]=new Array();
			NewGame.Memory["PlayerNames"][0]=Language.PlayerName;
			NewGame.Memory["PlayerNames"][1]=MissionData[4][3];
			
			NewGame.Memory["Rewards"]=MissionData[4][4];
			
			for (var Zim=0; Zim<game.Memory["Data"].Armies.length; Zim++)
			{
				game.Memory["Data"].Armies[Zim].realID=Zim;
				NewGame.Memory["DataItemArmies"][0].push(game.Memory["Data"].Armies[Zim]);
			}
			
			var BonusLevel=0;
			if (alliedarmies.length>0)
			{
				for (Zim=0; Zim<alliedarmies.length; Zim++)
				{
					if (NewGame.Memory["DataItemArmies"][0].length<GlobalVariables.MaxArmyStack)
					{
						var ARMY:DataItemArmy=DataItemArmy.MakeNewArmy(alliedarmies[Zim]);
						ARMY.realID=-1;
						NewGame.Memory["DataItemArmies"][0].push(ARMY);
						BonusLevel+=ARMY.Level;
					}
				}
			}
			NewGame.Memory["DataItemArmies"][1]=new Array();
						
			if (MissionData[1]=="Battle")
			{
				for (Zim=0; Zim<enemyarmies.length; Zim++)
				{
					if (NewGame.Memory["DataItemArmies"][1].length<=GlobalVariables.MaxArmyStack)
					{
						var Level=enemyarmies[Zim][1];
						var PartyLevel=BonusLevel+MapData.GetCombinedPartyLevel(game);
						if (Level!=null)
						{
							Level=Math.ceil(enemyarmies[Zim][1]*Math.max(1,PartyLevel/MissionData[4][2]));
						}
						else
						{
							Level=Math.max(PartyLevel/enemyarmies.length,MissionData[4][2]/enemyarmies.length);
						}
						Level=Math.round(Level*(1+NewGame.Memory["Difficulty"])/2*(1+(alliedarmies.length-enemyarmies.length)/20))
						var temparmydata:Array=[enemyarmies[Zim][0],Level,enemyarmies[Zim][2],enemyarmies[Zim][3],enemyarmies[Zim][4]]
						NewGame.Memory["DataItemArmies"][1].push(DataItemArmy.MakeNewArmy(temparmydata));
					}
				}
			}
			else if (MissionData[1]=="RandomBattle")
			{
				var Armies = Math.round(Math.random()*(enemyarmies[0]-Math.max(enemyarmies[1],1)))+Math.max(enemyarmies[1],1);
				for (Zim=0; Zim<Armies; Zim++)
				{
					if (NewGame.Memory["DataItemArmies"][1].length<=GlobalVariables.MaxArmyStack)
					{
						Level=enemyarmies[1];
						if (Level!=null)
						{
							Level=Math.max(Math.max((BonusLevel+MapData.GetCombinedPartyLevel(game))/Armies,MissionData[4][2]/Armies));
						}
						Level=Math.round(Level*(1+NewGame.Memory["Difficulty"])/2*(1+(alliedarmies.length-enemyarmies.length)/20))
						temparmydata=[enemyarmies[2][Math.round(Math.random()*(enemyarmies[2].length-1))],Level,null,null,null]						
						NewGame.Memory["DataItemArmies"][1].push(DataItemArmy.MakeNewArmy(temparmydata));
					}
				}
			}
			
			game.stage.addChild(NewGame);		
			NewGame.fin();			
		}
		
		public static function TakeGold(game:HubWorldHandler,Amount:Number,Forced:Boolean):Boolean
		{	
			if (Forced==false)
			{
				if (game.Memory["Data"].PlayerData[1]>=Amount)
				{
					game.Memory["Data"].PlayerData[1]=Math.max(0,game.Memory["Data"].PlayerData[1]-Amount);
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				game.Memory["Data"].PlayerData[1]=Math.max(0,game.Memory["Data"].PlayerData[1]-Amount);
				return true;
			}
		}
	}
	
}
