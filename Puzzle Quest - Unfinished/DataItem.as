﻿package  {
	
	public class DataItem {

		public static var Stat_Life:Number=0;
		public static var Stat_Attack:Number=1;
		public static var Stat_Armor:Number=2;
		public static var Stat_Special:Number=3;
		
		public static var Passives:Array = ["attackasmagic","freedefend","bonusarmordefense","bonusspecialdefense","retaliateondeath","bonusatkonlowhp",
											"bonusdefonlowhp","bonusspconlowhp","vampirism","spellvamp","retaliate","regenerate",
											"selfstunafterspell","selfstunafterattack","selfsilenceafterspell","BonusMorale","execute",
											"highatklowdefense","highspclowdefense","highatklowspec","highdeflowatk","highspclowatk","highdeflowspc",
											"heavyhitter","stopdamageover1","invulnerable","unbeliever","immunity","horde","loner","bonusmgonatk","bedeviled",
											"blunt","cursed","fearfull","poisontouch","poisonclaw","fireatk","destroyrandomgem","crit","plaguetouch",
											"Nomorale","Degeneration","Attackslooselife",
											"GenerateRed","GenerateBlue","GenerateGreen","GenerateYellow","GenerateRandom",
											"HabitatRed","HabitatBlue","HabitatGreen","HabitatYellow",
											"Synergy",
											]
		
		

		public static function PrepareTheArmies(game:MainGame)
		{
			game.Memory["PlayerLife"]=[0,0];
			for (var Player=0; Player<game.Memory["DataItemArmies"].length; Player++)
			{
				game.Memory["DisplayItemArmies"][Player]=new Array();
				for (var Zim=0; Zim<game.Memory["DataItemArmies"][Player].length; Zim++)
				{
					var Army:DisplayItemArmy=new DisplayItemArmy;
					Army.Name=game.Memory["DataItemArmies"][Player][Zim].Name;					
					Army.MaxLife=Math.max(1,game.Memory["DataItemArmies"][Player][Zim].MaxLife-game.Memory["DataItemArmies"][Player][Zim].Deaths);
					if (game.Memory["HWH"].Memory["Difficulty"]>1)
					{						
						Army.MaxLife=Math.max(1,game.Memory["DataItemArmies"][Player][Zim].MaxLife-game.Memory["DataItemArmies"][Player][Zim].Deaths);
					}
					Army.Life=Army.MaxLife//Math.min(game.Memory["DataItemArmies"][Player][Zim].Life,game.Memory["DataItemArmies"][Player][Zim].MaxLife);
					Army.Type=game.Memory["DataItemArmies"][Player][Zim].Type;
					Army.Attack=game.Memory["DataItemArmies"][Player][Zim].Attack;
					Army.Special=game.Memory["DataItemArmies"][Player][Zim].Special;
					Army.Abilities=game.Memory["DataItemArmies"][Player][Zim].Abilities;
					Army.Armor=game.Memory["DataItemArmies"][Player][Zim].Armor;
					Army.Passive=game.Memory["DataItemArmies"][Player][Zim].Passive;
					Army.Level=game.Memory["DataItemArmies"][Player][Zim].Level;
					Army.Defender=false;
					Army.PlayerOwner=Player;
					Army.Modifiers=new Array();
					Army.ID=Zim;
					Army.realID=game.Memory["DataItemArmies"][Player][Zim].realID;
					game.Memory["DisplayItemArmies"][Player][Zim]=Army;
					
					game.Memory["Display"]["PlayerLife"][Player][Zim]=Army.Life;
					game.Memory["Display"]["PlayerLifeUpdateCheck"][Player][Zim]=Army.Life;
					
					game.Memory["PlayerLife"][Player]+=Army.Life;
				}
			}
		}		
		
		public static function GivePlayerArmy(game:HubWorldHandler,Army:DataItemArmy)
		{
			if (game.Memory["Data"].Armies.length<GlobalVariables.MaxArmyStack)
			{
				game.Memory["Data"].Armies.push(Army);
			}
			else
			{
				game.Memory["Data"].BenchedArmies.push(Army);
			}
		}
		
		public static function PurchaseArmy(game:HubWorldHandler,Army:DataItemArmy,Multiplier:Number):Boolean
		{
			trace(Multiplier,game.Memory["Data"].PlayerData[1],Army.GetMyGoldCost(Multiplier));
			if (game.Memory["Data"].PlayerData[1]>Army.GetMyGoldCost(Multiplier))
			{
				GivePlayerArmy(game,Army);
				game.Memory["Data"].PlayerData[1]=game.Memory["Data"].PlayerData[1]-Army.GetMyGoldCost(Multiplier)
				return true;
			}
			else
			{
				return false;
			}
		}
		
	}	
}
/*<?xml version="1.0"?>
<TextLibrary>

   <Text tag="[ITEM_I1AC_NAME]">Assassin&apos;s Cloak</Text>
   <Text tag="[ITEM_I1AC_POWER]">Creates a Skull for every 3 Gold you receive when matching Gold coins</Text>
   <Text tag="[ITEM_I1AH_NAME]">Antharg&apos;s Crown</Text>
   <Text tag="[ITEM_I1AH_POWER]">Doubles all green and blue Mana earned</Text>
   <Text tag="[ITEM_I1AR_NAME]">Antharg&apos;s Robe</Text>
   <Text tag="[ITEM_I1AR_POWER]">Halves all incoming damage greater than 1 point (rounding down)</Text>
   <Text tag="[ITEM_I1AS_NAME]">Antharg&apos;s Blade</Text>
   <Text tag="[ITEM_I1AS_POWER]">Inflicts Disease for 8 turns and doubles damage on every hit</Text>
   <Text tag="[ITEM_I1AT_NAME]">Tome of Plague</Text>
   <Text tag="[ITEM_I1AT_POWER]">Inflicts your enemy with Disease for 8 turns every time anybody casts a spell</Text>
   <Text tag="[ITEM_I1BC_NAME]">Bloodstone Cloak</Text>
   <Text tag="[ITEM_I1BC_POWER]">Adds +1 to Blue and Green Mana for every 3 Red Mana you gain</Text>
   <Text tag="[ITEM_I1BH_NAME]">Bloodstone Helm</Text>
   <Text tag="[ITEM_I1BH_POWER]">Adds +1 to Blue and Green Mana for every 3 Red Mana you gain</Text>
   <Text tag="[ITEM_I1BN_NAME]">Runic Banner</Text>
   <Text tag="[ITEM_I1BN_POWER]">Adds +2 to all Mana and Life Points each turn if Blue and Yellow Mana are both 16+</Text>
   <Text tag="[ITEM_I1BP_NAME]">Banner of Purity</Text>
   <Text tag="[ITEM_I1BP_POWER]">Does 2 points of damage to evil creatures whenever they cast a spell</Text>
   <Text tag="[ITEM_I1BS_NAME]">Boots of Silence</Text>
   <Text tag="[ITEM_I1BS_POWER]">Adds +1 to Blue Mana for every 3 Gold you receive</Text>
   <Text tag="[ITEM_I1BT_NAME]">Blade of Terror</Text>
   <Text tag="[ITEM_I1BT_POWER]">Gives a 40% chance to inflict Fear for 3 turns whenever you do 3 or more damage</Text>
   <Text tag="[ITEM_I1CR_NAME]">Chameleon Ring</Text>
   <Text tag="[ITEM_I1CR_POWER]">You become Hidden for 3 turns whenever you match 4 or 5 gems</Text>
   <Text tag="[ITEM_I1DB_NAME]">Darkblade</Text>
   <Text tag="[ITEM_I1DB_POWER]">Adds +1 to damage for every 2 Skulls in play whenever you do 3 or more damage</Text>
   <Text tag="[ITEM_I1DS_NAME]">Dragonslayer</Text>
   <Text tag="[ITEM_I1DS_POWER]">Adds +5 to damage against Dragons whenever you do 3 or more damage</Text>
   <Text tag="[ITEM_I1DW_NAME]">Dwarven Sigils</Text>
   <Text tag="[ITEM_I1DW_POWER]">Adds +1 to damage when matching Skulls</Text>
   <Text tag="[ITEM_I1ER_NAME]">Elcor&apos;s Ring</Text>
   <Text tag="[ITEM_I1ER_POWER]">Removes all status effects and slightly increases all resistances every time you match 4 or 5 gems</Text>
   <Text tag="[ITEM_I1GC_NAME]">Ghoul Claw</Text>
   <Text tag="[ITEM_I1GC_POWER]">Drains an enemy&apos;s Green and Blue Mana by 1 point for every 2 damage done and gives it to you</Text>
   <Text tag="[ITEM_I1GH_NAME]">Ghoulstone</Text>
   <Text tag="[ITEM_I1GH_POWER]">Adds +10% to all Resistances when fighting a Ghoul or Ur-Ghoul</Text>
   <Text tag="[ITEM_I1GS_NAME]">Griffon Shield</Text>
   <Text tag="[ITEM_I1GS_POWER]">Adds +3 to damage whenever you do 3 or more damage if your mount is a Griffon</Text>
   <Text tag="[ITEM_I1HC_NAME]">Harpy Helm</Text>
   <Text tag="[ITEM_I1HC_POWER]">Gives a 50% chance to inflict Terror for 2 turns whenever you do 3 or more damage</Text>
   <Text tag="[ITEM_I1HQ_NAME]">Witch&apos;s Claw</Text>
   <Text tag="[ITEM_I1HQ_POWER]">Each point of damage you do drains 1 point of Yellow Mana from the enemy and gives it to you</Text>
   <Text tag="[ITEM_I1LG_NAME]">Lifeward Gem</Text>
   <Text tag="[ITEM_I1LG_POWER]">Adds +4 to Life Points at the start of your turn if Life Points are 20 or lower</Text>
   <Text tag="[ITEM_I1NC_NAME]">Night Cloak</Text>
   <Text tag="[ITEM_I1NC_POWER]">Creates a random Purple Star at the start of each of your turns</Text>
   <Text tag="[ITEM_I1NS_NAME]">Nova Shield</Text>
   <Text tag="[ITEM_I1NS_POWER]">Blinds an opponent for 3 turns if they do 15+ damage in one strike</Text>
   <Text tag="[ITEM_I1PC_NAME]">Plague Cloak</Text>
   <Text tag="[ITEM_I1PC_POWER]">Adds +1 to your Life Points every turn for each Disease on an enemy</Text>
   <Text tag="[ITEM_I1PK_NAME]">Plague Knife</Text>
   <Text tag="[ITEM_I1PK_POWER]">Inflicts Disease on an enemy for 5 turns when doing 4 or more points of damage</Text>
   <Text tag="[ITEM_I1PS_NAME]">Plague Skin</Text>
   <Text tag="[ITEM_I1PS_POWER]">Inflicts an attacker with Disease for 5 turns whenever you receive 5 or more points of damage</Text>
   <Text tag="[ITEM_I1RB_NAME]">Runic Blade</Text>
   <Text tag="[ITEM_I1RB_POWER]">Adds +1 damage for every 8 Blue and 8 Green Mana you have whenever you do 5 or more damage</Text>
   <Text tag="[ITEM_I1RC_NAME]">Runic Cloak</Text>
   <Text tag="[ITEM_I1RC_POWER]">Adds +2 to all Mana and Life Points each turn if your Yellow and Green Mana are both 16+</Text>
   <Text tag="[ITEM_I1RH_NAME]">Runic Helm</Text>
   <Text tag="[ITEM_I1RH_POWER]">Adds +2 to all Mana and Life Points each turn if your Red and Yellow Mana are both 16+</Text>
   <Text tag="[ITEM_I1RM_NAME]">Ratmaster Helm</Text>
   <Text tag="[ITEM_I1RM_POWER]">Adds +10 to Cunning if your mount is a Giant Rat</Text>
   <Text tag="[ITEM_I1SA_NAME]">Staff of Abjuration</Text>
   <Text tag="[ITEM_I1SA_POWER]">Adds +1% to all Resistances for each 5 levels of your enemy</Text>
   <Text tag="[ITEM_I1SH_NAME]">Stormhammer</Text>
   <Text tag="[ITEM_I1SH_POWER]">Destroys a random column of gems whenever you match 4 or 5 gems</Text>
   <Text tag="[ITEM_I1SM_NAME]">Stormhelm</Text>
   <Text tag="[ITEM_I1SM_POWER]">Destroys a random column of gems whenever you receive 4 or more Experience</Text>
   <Text tag="[ITEM_I1SR_NAME]">Stormarmor</Text>
   <Text tag="[ITEM_I1SR_POWER]">Destroys a random column of gems whenever you receive 4 or more Gold</Text>
   <Text tag="[ITEM_I1SS_NAME]">Stormshield</Text>
   <Text tag="[ITEM_I1SS_POWER]">Destroys a random column of gems whenever you gain 6 or more Yellow Mana</Text>
   <Text tag="[ITEM_I1ST_NAME]">Stiletto</Text>
   <Text tag="[ITEM_I1ST_POWER]">Reduces your enemy&apos;s Mana reserves by 1 for every 3 points of damage you do</Text>
   <Text tag="[ITEM_I1TA_NAME]">Talisman of Araveine</Text>
   <Text tag="[ITEM_I1TA_POWER]">Adds half your Blue Mana to Water Resistance</Text>
   <Text tag="[ITEM_I1TB_NAME]">Thief&apos;s Boots</Text>
   <Text tag="[ITEM_I1TB_POWER]">Adds +8 to Blue Mana whenever an enemy casts a spell</Text>
   <Text tag="[ITEM_I1TC_NAME]">Thief&apos;s Cowl</Text>
   <Text tag="[ITEM_I1TC_POWER]">Doubles all damage you do while Hidden</Text>
   <Text tag="[ITEM_I1TE_NAME]">Sharp Teeth</Text>
   <Text tag="[ITEM_I1TE_POWER]">Adds +2 to all damage done when matching Skulls</Text>
   <Text tag="[ITEM_I1TH_NAME]">Tome of Healing</Text>
   <Text tag="[ITEM_I1TH_POWER]">Adds +3 to Life Points at the start of your turn</Text>
   <Text tag="[ITEM_I1UR_NAME]">Urgol Charm</Text>
   <Text tag="[ITEM_I1UR_POWER]">Adds +4 to Blue and Green Mana whenever an enemy casts a spell</Text>
   
   <Text tag="[ITEM_I2BA_NAME]">Bow of Accuracy</Text>
   <Text tag="[ITEM_I2BA_POWER]">If you have 9+ yellow mana at the start of your turn, your opponent takes 1 point of damage.</Text>
   <Text tag="[ITEM_I2CS_NAME]">Cloak of Sorrows</Text>
   <Text tag="[ITEM_I2CS_POWER]">Has a 50% chance on each of your turns to reduce the duration of all status effects on your and your opponent by one round.</Text>
   <Text tag="[ITEM_I2CT_NAME]">Choking Terror</Text>
   <Text tag="[ITEM_I2CT_POWER]">Inflicts terror on your opponent for five turns whenever you match four or five gems.</Text>
   <Text tag="[ITEM_I2DB_NAME]">Dagger of Binding</Text>
   <Text tag="[ITEM_I2DB_POWER]">Has a 20% chance to cause your opponent to miss a turn whenever you deal three or more damage.</Text>
   <Text tag="[ITEM_I2DC_NAME]">Dark Elf&apos;s Crossbow</Text>
   <Text tag="[ITEM_I2DC_POWER]">Poisons your opponent for two turns whenever you deal three or more damage.</Text>
   <Text tag="[ITEM_I2EW_NAME]">Earthring of the Warlock</Text>
   <Text tag="[ITEM_I2EW_POWER]">For every three Green mana you gain your enemy loses one Yellow mana.</Text>
   <Text tag="[ITEM_I2FB_NAME]">Flowing Body</Text>
   <Text tag="[ITEM_I2FB_POWER]">Washes away the impurities of poisons and disease whenever you gain blue mana.</Text>
   <Text tag="[ITEM_I2GF_NAME]">Granite Face</Text>
   <Text tag="[ITEM_I2GF_POWER]">Protects against 3 points of damage whenever you recieve 3+ points of damage, as long as you have at least 16 Green mana.</Text>
   <Text tag="[ITEM_I2HH_NAME]">Hat of the Highwayman</Text>
   <Text tag="[ITEM_I2HH_POWER]">Steals one gold from your opponent each of your turns.</Text>
   <Text tag="[ITEM_I2SV_NAME]">Shielding Vortex</Text>
   <Text tag="[ITEM_I2SV_POWER]">Any attack on you has a chance equal to your yellow mana to be reduced to one damage.</Text>
   <Text tag="[ITEM_I2TL_NAME]">Time Lyre</Text>
   <Text tag="[ITEM_I2TL_POWER]">On each of your turns, has a 50% chance to extend the duration of all status effects on you and your opponent by one round.</Text>
   <Text tag="[ITEM_I2WW_NAME]">Waterring of the Warlock</Text>
   <Text tag="[ITEM_I2WW_POWER]">For every three blue mana you gain, your opponent loses 1 red mana.</Text>

</TextLibrary>
*/