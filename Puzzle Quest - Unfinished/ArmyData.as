﻿package  {
	
	public class ArmyData {

		public static var Display=[Portrait_City,Portrait_Skeleton,Portrait_Bat,Portrait_Spider,Portrait_Goblin,Portrait_Rat,Portrait_Wolf,
								   Portrait_FrostWolf,Portrait_Wasp,Portrait_Zombie,Portrait_Sandworm,Portrait_Scorpion,Portrait_Imp,
								   Portrait_Troll,Portrait_Thief,Portrait_Orc,Portrait_OrcWarrior,Portrait_OrcWolfrider,								   
								   Portrait_DarkDwarf,Portrait_GreyDwarf,Portrait_Harpy,Portrait_Iceguard,Portrait_Guardsman,Portrait_Ogre,
								   Portrait_DarkArcher,Portrait_Apprentince,Portrait_OgreMage,Portrait_Minotaur,Portrait_FireElemental,
								   Portrait_Necromancer,Portrait_Sprite,Portrait_HillGiant,Portrait_Catapult,Portrait_Centaur,
								   Portrait_Doomknight,Portrait_Griffon,Portrait_Liche,Portrait_Knight,Portrait_IronGolem,
								   Portrait_Spiderpriestess,Portrait_HumanWarrior,Portrait_Wyvern,Portrait_Druid,Portrait_OgreWarrior,
								   Portrait_RedAxe,Portrait_Tauron,Portrait_FireGiant,Portrait_FrostGiant,Portrait_Arkliche,Portrait_DragonBlue,
								   Portrait_DragonGreen,Portrait_DragonRed,Portrait_DragonUndead,Portrait_Hydra,Portrait_Minogoth,Portrait_ElfQueen,
								   //TBD
								   Portrait_Pegasus,Portrait_PlaguePriest,Portrait_Antharg,Portrait_Ghoul,Portrait_PlagueWitch,Portrait_PlagueTaster,
								   Portrait_Bard,Portrait_Demoness
								   ]
		
		public static var Army_Castle=0;
		public static var Army_Skeleton=1;//5-10
		public static var Army_Bat=2;//6
		public static var Army_Spider=3;
		public static var Army_Goblin=4;//5-10
		public static var Army_Rat=5;//5-11
		public static var Army_Wolf=6;//5
		public static var Army_FrostWolf=7;//20
		public static var Army_Wasp=8;//5
		public static var Army_Zombie=9;//4-10
		public static var Army_SandWorm=10;//9
		public static var Army_Scorpion=11;//5
		public static var Army_Imp=12;
		public static var Army_Troll=13;//10-15
		public static var Army_Orc=15;//6-12
		public static var Army_Thief=14;//5-10
		public static var Army_OrcWarrior=16;//tribe1
		public static var Army_OrcWolfrider=17;//tribe2
		public static var Army_DarkDwarf=18;//5-10
		public static var Army_GreyDwarf=19;//drarfcapital
		public static var Army_Harpy=20;//5
		public static var Army_Iceguard=21;//UNUSED
		public static var Army_Guard=22;//empire
		public static var Army_Ogre=23;//ogrearena
		public static var Army_DarkArcher=24;//dkelf
		public static var Army_Apprentince=25;//empire
		public static var Army_OgreMage=26;//ogre camp
		public static var Army_Minotaur=27;//5-10
		public static var Army_FireElemental=28;//demongate
		public static var Army_Necromancer=29;//mortuus
		public static var Army_Sprite=30;//fey forest
		public static var Army_HillGiant=31;//6-12
		public static var Army_Catapult=32;//dwarf capital
		public static var Army_Centaur=33;//6-12
		public static var Army_DoomKnight=34;//garledun
		public static var Army_Griffon=35;//dwarf city
		public static var Army_Liche=36;//mortuus
		public static var Army_Knight=37;//siria
		public static var Army_IronGolem=38;//10-15
		public static var Army_Spiderpriestess=39;//DKelf
		public static var Army_HumanWarrior=40;//5
		public static var Army_Wyvern=41;
		public static var Army_Druid=42;
		public static var Army_OgreWarrior=43;
		public static var Army_RedAxe=44;
		public static var Army_Tauron=45;
		public static var Army_FireGiant=46;
		public static var Army_FrostGiant=47;		
		public static var Army_ArchLiche=48;
		public static var Army_DragonBlue=49;
		public static var Army_DragonRed=50;
		public static var Army_DragonGreen=51;
		public static var Army_DragonUndead=52;
		public static var Army_DragonHydra=53;
		public static var Army_MinoGoth=54;
		public static var Army_ElfQueen=55;

		public static var BaseStats:Array=[6,1,0,1,1];
		public static var ArmyStats:Array=[// - -= CASTLE =- -
							 [//StatsPerLevel
							  [5,5,5,5],//Life, Attack, Armor, Special
							  //Abilities
							  [[2,101,-1],//Ability ID, level required, Requires stat?
							   ],
							   //passives
							   [["immunity"],["immunity"],["immunity"]]
							 ],
							 [// - - - -= SKELETON =- - - - 1
							  [10,3,2,1],
							  [//Ability ID, level required, Requires stat?
							   [SpellData.cSpellEatSkull,0,-1],
							   [SpellData.cSpellScare,15,-1],
							   [SpellData.cSpellGraveDigger,4,-1],
							   [SpellData.cSpellUnholyRage,25,0],
							   [SpellData.cSpellShieldBash,8,-1],
							   [SpellData.cSpellRaiseDead,5,-1],
							   [SpellData.cSpellStealManaBlue,12,1],	   
							   [SpellData.cSpellDrainSkulls,30,-1],		   
							   [SpellData.cSpellHex,10,2],				
							   [SpellData.cSpellRaiseDead,25,-1],		
							   ],
							  [["vampirism"],	
							   ["immunity"],
							   ["cursed"]]
							 ],
							 [// - - - -= BAT =- - - - 2
							  [8,5,2,1],
							  [[SpellData.cSpellSwoop,10,-1],
							   [SpellData.cSpellEcho,0,-1],
							   [SpellData.cSpellHowl,15,-1],							   
							   [SpellData.cSpellTapLife,5,0],
							   [SpellData.cSpellEatStar,20,-1],								   
							   [SpellData.cSpellSwarmCall,12,-1],		
							   [SpellData.cSpellSwarmStrike,25,-1],	
							   [SpellData.cSpellWindBlast,5,2],							   
							   [SpellData.cSpellPlague,20,1],  
							   ],
							  [["vampirism"],	
							   ["plaguetouch"],
							   ["bonusmgonatk"]]
							 ],
							 [// - - - -= SPIDER =- - - - 3
							  [7,3,4,3],
							  [[SpellData.cSpellVenom,0,-1],
							   [SpellData.cSpellWeb,12,-1],
							   [SpellData.cSpellSwarmStrike,13,-1],
							   [SpellData.cSpellScratch,4,-1],
							   [SpellData.cSpellSwiftness,20,0],
							   [SpellData.cSpellChannelYellow,25,1],
							   [SpellData.cSpellRainPoison,15,2],
							   [SpellData.cSpellInjectPoison,8,-1],
							   ],
							  [["horde"],	
							   ["highdeflowatk"],
							   ["selfstunafterspell"]]
							 ],
							 [// - - - -= GOBLIN =- - - - 4
							  [7,3,4,1],
							  [[SpellData.cSpellSneak,7,-1],
							   [SpellData.cSpellScavenge,0,-1],			
							   [SpellData.cSpellIgnite,13,-1],	
							   [SpellData.cSpellThrowStone,20,-1],
							   [SpellData.cSpellShieldBash,16,0],
							   [SpellData.cSpellScare,15,1],  
							   [SpellData.cSpellZap,16,2],	   							   
							   [SpellData.cSpellReplenish,24,2],			
							   ],
							  [["HabitatRed"],	
							   ["freedefend"],
							   ["selfsilenceafterspell"]]
							 ],
							 [// - - - -= RAT =- - - - 5
							  [8,4,1,3],
							  [[SpellData.cSpellScavenge,9,-1],
							   [SpellData.cSpellScratch,0,-1],
							   [SpellData.cSpellRevenge,18,-1],
							   [SpellData.cSpellSandAttack,15,-1],
							   [SpellData.cSpellRabidBite,5,0],
							   [SpellData.cSpellBurrow,24,-1],
							   [SpellData.cSpellChannelGreen,20,1],
							   [SpellData.cSpellPlague,5,2],
							   [SpellData.cSpellAnthragsKiss,25,2],						
							   ],
							  [["plaguetouch"],	
							   ["destroyrandomgem"],
							   ["HabitatYellow"]]
							 ],
							 [// - - - -= WOLF =- - - - 6
							  [11,2,2,2],
							  [[SpellData.cSpellHowl,0,-1],
							   [SpellData.cSpellRabidBite,8,-1],
							   [SpellData.cSpellSavageRoar,25,-1],
							   [SpellData.cSpellWound,4,-1],
							   [SpellData.cSpellSwarmCall,15,-1],
							   [SpellData.cSpellBloodBlade,20,0],						   
							   [SpellData.cSpellTaunt,10,1],
							   [SpellData.cSpellEatStar,5,-1], 
							   [SpellData.cSpellSandAttack,5,2],
							   ],
							  [["fearfull"],	
							   ["retaliate"],
							   ["bonusmgonatk"]]
							 ],
							 [// - - - -= ICEWOLF =- - - - 7
							  [10,2,2.5,2],
							  [[SpellData.cSpellIceFloe,15,-1],	
							   [SpellData.cSpellHybernate,0,-1],					
							   [SpellData.cSpellUnholyRage,20,-1],
							   [SpellData.cSpellWound,10,-1],
							   [SpellData.cSpellHowl,5,0],
							   [SpellData.cSpellStealManaBlue,15,1],
							   [SpellData.cSpellIceBlast,25,-1],
							   [SpellData.cSpellCurse,5,2],
							   ],
							  [["heavyhitter"],	
							   ["retaliate"],
							   ["cursed","HabitatBlue"]]
							 ],
							 [// - - - -= WASP =- - - - 8
							  [6,5,1.25,3],
							  [[SpellData.cSpellVenom,0,-1],
							   [SpellData.cSpellSwarmStrike,25,-1],
							   [SpellData.cSpellSwoop,5,0],
							   [SpellData.cSpellInjectPoison,12,-1],
							   [SpellData.cSpellSwarmCall,15,-1],
							   [SpellData.cSpellGust,4,-1],	
							   [SpellData.cSpellSwarmCall,11,1],
							   [SpellData.cSpellWindBlast,17,2],	
							   ],
							  [["poisonclaw"],	
							   ["retaliateondeath"],
							   ["horde"]]
							 ],
							 [// - - - -= ZOMBIE =- - - - 9
							  [12,1,3,2],
							  [[SpellData.cSpellRegenerate,14,-1],
							   [SpellData.cSpellEatSkull,0,-1],
							   [SpellData.cSpellRabidBite,5,-1],
							   [SpellData.cSpellEatStar,17,0],		
							   [SpellData.cSpellMeatShield,8,1],
							   [SpellData.cSpellSleep,12,-1],
							   [SpellData.cSpellBlight,10,2],	
							   [SpellData.cSpellPlagueExplosion,25,2],
							   [SpellData.cSpellPlagueBlade,20,-1],
							   ],
							  [["immunity"],	
							   ["stopdamageover1"],
							   ["plaguetouch"]]
							 ],
							 [// - - - -= WORM =- - - - 10
							  [8,2,2,4],
							  [[SpellData.cSpellBurrow,7,-1],
							   [SpellData.cSpellGraveDigger,13,-1],							   
							   [SpellData.cSpellScavenge,0,-1],
							   [SpellData.cSpellScratch,10,-1],
							   [SpellData.cSpellDig,20,0],		   
							   [SpellData.cSpellRegenerate,0,1],
							   [SpellData.cSpellTaunt,17,-1],
							   [SpellData.cSpellEatEarth,19,2],							   
							   ],
							  [["loner"],	
							   ["regenerate","invulnerable"],
							   ["destroyrandomgem"]]
							 ],
							 [// - - - -= SCORPION =- - - - 11
							  [9,4,4,3],
							  [[SpellData.cSpellInjectPoison,0,-1],				
							   [SpellData.cSpellBurrow,10,-1],
							   [SpellData.cSpellSwiftness,20,-1],
							   [SpellData.cSpellPoisonBlade,15,-1],
							   [SpellData.cSpellSneak,5,0],
							   [SpellData.cSpellSandAttack,8,-1],
							   [SpellData.cSpellDig,12,-1],			
							   [SpellData.cSpellToxic,17,1],
							   [SpellData.cSpellStoneSkin,17,2],
							   ],
							  [["highatklowspec","highatklowdefense"],	
							   ["highdeflowatk","highdeflowspc"],
							   ["selfsilenceafterspell"]]
							 ],
							 [// - - - -= IMP =- - - - 12
							  [5,6,2,6],
							  [[SpellData.cSpellTrickMana,0,-1],
							   [SpellData.cSpellIgnite,20,-1],		
							   [SpellData.cSpellCurse,5,-1],
							   [SpellData.cSpellFireBall,10,-1],
							   [SpellData.cSpellSwiftness,25,-1],
							   [SpellData.cSpellIgnite,15,0],	
							   [SpellData.cSpellDecieve,15,1],	
							   [SpellData.cSpellGust,5,7],
							   [SpellData.cSpellDecieve,11,2],	
							   ],
							  [["attackasmagic","HabitatRed"],	
							   ["stopdamageover1"],
							   ["attackasmagic"]]
							 ],
							  [// - - - -= TROLL =- - - - 13
							  [13,2,2,1],
							  [[SpellData.cSpellRegenerate,0,-1],
							   [SpellData.cSpellThrowStone,8,-1],
							   [SpellData.cSpellHybernate,15,-1],
							   [SpellData.cSpellSkullBash,10,-1],
							   [SpellData.cSpellStoneSkin,20,-1],
							   [SpellData.cSpellUnholyRage,4,0],
							   [SpellData.cSpellStoneForm,4,1],
							   [SpellData.cSpellDrinkAle,25,-1],
							   [SpellData.cSpellLesserHeal,4,2],							   
							   ],
							  [["heavyhitter"],	
							   ["regenerate","HabitatGreen"],
							   ["selfsilenceafterspell","HabitatBlue"]]
							 ],
							 [// - - - -= THIEF =- - - - 14
							  [9,3,3,2],
							  [
							   [SpellData.cSpellBackStab,0,-1],
							   [SpellData.cSpellSleep,20,-1],
							   [SpellData.cSpellEatFire,15,0],
							   [SpellData.cSpellScratch,13,-1],
							   [SpellData.cSpellSwiftness,15,1],
							   [SpellData.cSpellSilence,5,2],
							   [SpellData.cSpellTrickMana,25,-1],
							   [SpellData.cSpellDecieve,8,-1],
							   ],
							  [["crit"],	
							   ["bonusarmordefense"],
							   ["execute"]]
							 ],
							 [// - - - -= ORC =- - - - 15
							  [10,3.25,2,1],
							  [[SpellData.cSpellUnholyRage,0,-1],
							   [SpellData.cSpellFireBall,20,-1],
							   [SpellData.cSpellSkullBash,10,-1],
							   [SpellData.cSpellRevenge,15,0],
							   [SpellData.cSpellTaunt,5,-1],
							   [SpellData.cSpellShieldBash,15,1],
							   [SpellData.cSpellCurse,25,-1],
							   [SpellData.cSpellLesserHeal,15,2],
							   ],
							  [["bonusatkonlowhp"],	
							   ["stopdamageover1"],
							   ["selfstunafterspell"]]
							 ],
							  [// - - - -= ORC ELITE =- - - - 16
							  [12,4,3,1],
							  [[SpellData.cSpellHeavyBlow,0,-1],
							   [SpellData.cSpellShieldBash,25,-1],
							   [SpellData.cSpellGemSmash,5,-1],
							   [SpellData.cSpellTaunt,15,-1],
							   [SpellData.cSpellRevenge,20,0],
							   [SpellData.cSpellTowerShield,20,1],
							   [SpellData.cSpellFireBlast,10,-1],
							   [SpellData.cSpellRainFire,20,2],
							   [SpellData.cSpellChannelWhite,30,-1],
							   ],
							  [["heavyhitter"],	
							   ["freedefend"],
							   ["highspclowatk"]]
							 ],
							  [// - - - -= ORC WOLFRIDER =- - - - 17
							  [10.75,4,2,3],
							  [[SpellData.cSpellSwarmCall,0,-1],
							   [SpellData.cSpellSwiftness,10,-1],
							   [SpellData.cSpellWound,20,-1],
							   [SpellData.cSpellSavageRoar,25,-1],
							   [SpellData.cSpellIgnite,15,0],
							   [SpellData.cSpellHowl,15,1],
							   [SpellData.cSpellRevenge,25,-1],
							   [SpellData.cSpellPlagueBlade,15,2],
							   [SpellData.cSpellFireBlast,5,-1],
							   ],
							  [["horde"],	
							   ["retaliate"],
							   ["highatklowspc"]]
							 ],
							  [// - - - -= DARKDWARF =- - - - 18
							  [10,2,3.25,5],
							  [[SpellData.cSpellGemSmash,0,-1],
							   [SpellData.cSpellFireBall,5,-1],	
							   [SpellData.cSpellFireBomb,20,-1], 
							   [SpellData.cSpellBloodRite,10,-1],
							   [SpellData.cSpellIgnite,15,0],				
							   [SpellData.cSpellRegenerate,15,-1],
							   [SpellData.cSpellMeatShield,25,1],
							   [SpellData.cSpellCurse,15,2],  
							   [SpellData.cSpellCleansingFlame,30,-1], 
							   ],
							  [["bonusatkonlowhp"],	
							   ["bonusdefonlowhp"],
							   ["bonusspconlowhp"]]
							 ],
							  [// - - - -= DWARF =- - - - 19
							  [13,2,4,2.25],
							  [[SpellData.cSpellBombardment,5,-1],
							   [SpellData.cSpellEatEarth,15,-1],
							   [SpellData.cSpellStrength,10,-1],
							   [SpellData.cSpellDrinkAle,0,-1],
							   [SpellData.cSpellThrowAxe,25,-1],
							   [SpellData.cSpellThrowBoulder,20,0],
							   [SpellData.cSpellStoneSkin,20,1],
							   [SpellData.cSpellEarthElly,20,2],
							   [SpellData.cSpellChannelGreen,30,-1],
							   ],
							  [["blunt"],	
							   ["freedefend"],
							   ["bonusspecialdefense"]]
							 ],
							  [// - - - -= HARPY =- - - - 20
							  [8,5,2,5],
							  [[SpellData.cSpellScare,0,-1],
							   [SpellData.cSpellWindBlast,16,-1],	
							   [SpellData.cSpellNecroPlague,24,-1],
							   [SpellData.cSpellPlagueExplosion,15,-1],
							   [SpellData.cSpellAnthragsKiss,30,-1],
							   [SpellData.cSpellSwoop,10,0], 
							   [SpellData.cSpellEatStar,10,1],  
							   [SpellData.cSpellGust,10,2],
							   [SpellData.cSpellCurse,6,-1],	
							   ],
							  [["heavyhitter","HabitatYellow"],	
							   ["unbeliever"],
							   ["attackasmagic","HabitatYellow"]]
							 ],
							  [// - - - -= HIELF =- - - - 21 UNUSED
							  [10,3,2,4],
							  [[SpellData.cSpellShieldBash,0,-1],
							   [SpellData.cSpellTowerShield,15,-1],
							   [SpellData.cSpellRest,10,-1],
							   [SpellData.cSpellTaunt,5,0],
							   [SpellData.cSpellBless,5,2],
							   [SpellData.cSpellShieldCharge,25,-1],
							   [SpellData.cSpellIronWall,30,1],
							   ],
							  [["crit"],	
							   ["retaliate"],
							   ["bonusmgonatk","HabitatBlue"]]
							 ],
							 [// - - - -= GUARD =- - - - 22
							  [12,3,3,2],
							  [[SpellData.cSpellShieldBash,0,-1],
							   [SpellData.cSpellTowerShield,15,-1],
							   [SpellData.cSpellRest,10,-1],
							   [SpellData.cSpellTaunt,5,0],
							   [SpellData.cSpellBless,5,2],
							   [SpellData.cSpellShieldCharge,25,-1],
							   [SpellData.cSpellIronWall,30,1],
							   ],
							  [["freedefend"],	
							   ["bonusarmordefense"],
							   ["bonusspecialdefense"]]
							 ],
							 [// - - - -= OGRE =- - - - 23
							  [12,6,1,1],
							  [[SpellData.cSpellSkullBash,0,-1],
							   [SpellData.cSpellGemSmash,5,-1],
							   [SpellData.cSpellRest,10,-1],
							   [SpellData.cSpellBloodRite,12,0],
							   [SpellData.cSpellUltimate,16,-1],
							   [SpellData.cSpellShieldCharge,20,-1],
							   [SpellData.cSpellMeatShield,20,1],
							   [SpellData.cSpellThrowStone,12,2],
							   ],
							  [["blunt"],	
							   ["selfstunafterattack"],
							   ["attackasmagic"]]
							 ],
							 [// - - - -= DKELF =- - - - 24
							  [7,6,3,6],
							  [[SpellData.cSpellInjectPoison,0,-1],
							   [SpellData.cSpellPlagueBlade,25,-1],
							   [SpellData.cSpellClearSkulls,30,-1],
							   [SpellData.cSpellRaiseDead,5,-1],
							   [SpellData.cSpellPoisonExplosion,15,-1],
							   [SpellData.cSpellSwiftness,10,0],
							   [SpellData.cSpellTrickShot,10,1],
							   [SpellData.cSpellDarkBolt,10,2],
							   ],
							  [["crit","HabitatRed"],	
							   ["unbeliever"],
							   ["heavyhitter"]]
							 ],
							 [// - - - -= MAGE =- - - - 25
							  [8,2,3,6],
							  [[SpellData.cSpellFireBlast,10,0],
							   [SpellData.cSpellIceBlast,10,2],
							   [SpellData.cSpellWindBlast,10,1],
							   [SpellData.cSpellReplenish,5,1],
							   [SpellData.cSpellBless,5,-1],
							   [SpellData.cSpellEntangle,0,-1],
							   [SpellData.cSpellChannel,15,-1],
							   [SpellData.cSpellSandStorm,20,-1],
							   [SpellData.cSpellGreaterHeal,25,1],
							   ],
							  [["execute"],	
							   ["attackasmagic"],
							   ["GenerateRandom"]]
							 ],
							 [// - - - -= OGREMAGE =- - - - 26
							  [12,1,2,4],
							  [[SpellData.cSpellBloodRite,30,-1],
							   [SpellData.cSpellZap,0,-1],
							   [SpellData.cSpellCurse,15,-1],
							   [SpellData.cSpellFireBlast,5,-1],
							   [SpellData.cSpellWaterAvatar,30,-1],
							   [SpellData.cSpellFireElly,25,-1],
							   [SpellData.cSpellRegenerate,10,0],
							   [SpellData.cSpellSilence,20,1],
							   [SpellData.cSpellRainFire,20,2],
							   ],
							  [["highdeflowatk"],	
							   ["invulnerable"],
							   ["selfstunafterspell"]]
							 ],
							 [// - - - -= MINOTAUR =- - - - 27
							  [12,6,1,1],
							  [[SpellData.cSpellScare,0,-1],
							   [SpellData.cSpellDrainSkulls,15,-1],
							   [SpellData.cSpellEatFire,20,-1],
							   [SpellData.cSpellRevenge,10,-1],
							   [SpellData.cSpellScratch,5,-1],
							   [SpellData.cSpellBerserker,10,0],
							   [SpellData.cSpellDrinkAle,15,1],
							   [SpellData.cSpellZap,9,2],
							   [SpellData.cSpellAvalanche,25,2],
							   ],
							  [["blunt"],	
							   ["selfstunafterattack"],
							   ["selfstunafterspell"]]
							 ],
							 [// - - - -= FIREELLY =- - - - 28
							  [10,4,2.25,1.25],
							  [[SpellData.cSpellFireBall,0,-1],
							   [SpellData.cSpellEatFire,25,-1],
							   [SpellData.cSpellFireBlast,15,-1],
							   [SpellData.cSpellFireAvatar,10,-1],
							   [SpellData.cSpellChannelRed,5,-1],
							   [SpellData.cSpellBerserker,18,0],
							   [SpellData.cSpellWallOfFire,13,1],
							   [SpellData.cSpellIgnite,15,2],
							   ],
							  [["fireatk"],	
							   ["highatklowdefense"],
							   ["highspclowatk"]]
							 ],
							 [// - - - -= NECRO =- - - - 29
							  [5,7,2,3],
							  [[SpellData.cSpellCurse,0,-1],
							   [SpellData.cSpellDarkBolt,5,-1],
							   [SpellData.cSpellRaiseDead,10,-1],
							   [SpellData.cSpellDrainSkulls,20,-1],
							   [SpellData.cSpellStealManaBlue,25,-1],
							   [SpellData.cSpellStealLife,15,2],
							   [SpellData.cSpellPlague,15,0],
							   [SpellData.cSpellEatSkull,15,1],
							   ],
							  [["selfstunafterattack"],	
							   ["bonusdefonlowhp"],
							   ["attackasmagic"]]
							 ],
							 [// - - - -= SPRITE =- - - - 30
							  [3,7,1,7],
							  [
							   [SpellData.cSpellGrow,25,-1],
							   [SpellData.cSpellSleep,0,-1],
							   [SpellData.cSpellTrickShot,10,-1],
							   [SpellData.cSpellNightmare,20,-1],
							   [SpellData.cSpellEntangle,15,1],
							   [SpellData.cSpellZap,15,2],
							   [SpellData.cSpellSwiftness,15,0],
							   [SpellData.cSpellSleepPowder,30,2],
							   ],
							  [["crit","retaliateondeath"],	
							   ["blunt"],
							   ["GenerateRed","GenerateBlue","GenerateGreen","GenerateYellow"]]
							 ],
							 [// - - - -= GIANT =- - - - 31
							  [12,2,2,4],
							  [[SpellData.cSpellGemSmash,10,-1],
							   [SpellData.cSpellStoneSkin,15,-1],
							   [SpellData.cSpellChannelGreen,25,-1],
							   [SpellData.cSpellThrowBoulder,20,2],
							   [SpellData.cSpellMeatShield,5,1],
							   [SpellData.cSpellSkullBash,0,-1],
							   [SpellData.cSpellHeavyBlow,20,0],
							   [SpellData.cSpellSandAttack,22,-1],
							   ],
							  [["freedefend"],	
							   ["bonusarmordefense"],
							   ["bonusspecialdefense"]]
							 ],
							 [// - - - -= CATAPULT =- - - - 32
							  [13,4,2,4],
							  [[SpellData.cSpellBombardment,10,-1],
							   [SpellData.cSpellSiege,15,-1],
							   [SpellData.cSpellIronWall,20,-1],
							   [SpellData.cSpellEarthElly,0,0],
							   [SpellData.cSpellGoblinToss,0,1],
							   [SpellData.cSpellFireBall,0,2],
							   [SpellData.cSpellChannelWhite,30,-1],
							   ],
							  [["fireatk"],	
							   ["selfstunafterattack"],
							   ["selfstunafterspell"]]
							 ],	
							 [// - - - -= CENTAUR =- - - - 33
							  [12,5,3,2],
							  [[SpellData.cSpellCharge,0,-1],
							   [SpellData.cSpellBloodBlade,10,-1],
							   [SpellData.cSpellGodlyStrength,25,-1],
							   [SpellData.cSpellScare,5,-1],
							   [SpellData.cSpellSwiftness,20,0],
							   [SpellData.cSpellEarthAvatar,15,1],
							   [SpellData.cSpellWindAvatar,15,2],
							   ],
							  [["blunt","HabitatYellow"],	
							   ["freedefend"],
							   ["attackasmagic"]]
							 ],		
							 [// - - - -= DEATHKNIGHT =- - - - 34
							  [14,5,2,2],
							  [[SpellData.cSpellEatSkull,10,-1],
							   [SpellData.cSpellScare,30,-1],
							   [SpellData.cSpellShieldBash,0,-1],
							   [SpellData.cSpellRaiseDead,5,-1],
							   [SpellData.cSpellShieldBash,20,-1],
							   [SpellData.cSpellUnholyRage,15,0],
							   [SpellData.cSpellTowerShield,25,1],
							   [SpellData.cSpellClearSkulls,25,2],
							   ],
							  [["fearfull"],	
							   ["invulnerable"],
							   ["spellvamp"]]
							 ],		
							 [// - - - -= GRIFFON =- - - - 35
							  [12,4,3,2],
							  [[SpellData.cSpellWindAvatar,24,-1],
							   [SpellData.cSpellEcho,4,-1],
							   [SpellData.cSpellHybernate,10,-1],
							   [SpellData.cSpellScratch,0,-1],
							   [SpellData.cSpellTaunt,16,-1],
							   [SpellData.cSpellSwoop,14,0],
							   [SpellData.cSpellSavageRoar,18,1],
							   [SpellData.cSpellWindElly,8,2],
							   ],
							  [["heavyhitter"],	
							   ["retaliate"],
							   ["HabitatYellow"]]
							 ],		
							 [// - - - -= LICHE =- - - - 36
							  [7.8,3,3,6],
							  [[SpellData.cSpellChannelRed,30,-1],
							   [SpellData.cSpellChannelBlue,35,-1],
							   [SpellData.cSpellCurse,0,-1],
							   [SpellData.cSpellIceBlast,25,-1],
							   [SpellData.cSpellDarkBolt,5,-1],
							   [SpellData.cSpellWindElly,10,-1],
							   [SpellData.cSpellAnthragsKiss,15,0],
							   [SpellData.cSpellRaiseDead,20,1],
							   [SpellData.cSpellDrainSkulls,20,2],					   
							   ],
							  [["spellvamp"],	
							   ["spellvamp"],
							   ["bonusspconlowhp"]]
							 ],		
							 [// - - - -= KNIGHT =- - - - 37
							  [11,3,4,3],
							  [[SpellData.cSpellBloodBlade,20,-1],
							   [SpellData.cSpellIronWall,5,-1],
							   [SpellData.cSpellHeavyBlow,0,-1],
							   [SpellData.cSpellTowerShield,15,-1],
							   [SpellData.cSpellBless,10,2],
							   [SpellData.cSpellCharge,10,1],
							   [SpellData.cSpellChannel,10,0],
							   ],
							  [["unbeliever"],	
							   ["freedefend"],
							   ["BonusMorale"]]
							 ],		
							 [// - - - -= GOLEM =- - - - 38
							  [8.75,2,6,4],
							  [[SpellData.cSpellReinforce,0,-1],
							   [SpellData.cSpellBodySlam,10,-1],
							   [SpellData.cSpellIronFist,10,-1],
							   [SpellData.cSpellFireBomb,20,-1],
							   [SpellData.cSpellRevenge,25,-1],
							   [SpellData.cSpellBurrow,5,-1],
							   [SpellData.cSpellIronWall,15,1],
							   [SpellData.cSpellBombardment,15,0],
							   ],
							  [["selfstunafterattack"],	
							   ["bonusarmordefense"],
							   ["GenerateRandom"]]
							 ],		
							 [// - - - -= SPIDERPRIESTESS =- - - - 39
							  [10,3,3,6.2],
							  [[SpellData.cSpellInjectPoison,0,-1],
							   [SpellData.cSpellSwarmAttack,10,-1],
							   [SpellData.cSpellSwarmDeffend,10,-1],
							   [SpellData.cSpellSwarmStrike,25,-1],
							   [SpellData.cSpellThunder,5,-1],
							   [SpellData.cSpellPoisonExplosion,30,-1],
							   [SpellData.cSpellRainBlood,15,0],
							   [SpellData.cSpellWeb,15,1],
							   [SpellData.cSpellUltimate,15,2],
							   ],
							  [["heavyhitter"],	
							   ["unbeliever"],
							   ["bonusmgonatk"]]
							 ],		
							 [// - - - -= WARRIOR =- - - - 40
							  [11,4,4,3],
							  [[SpellData.cSpellUnholyRage,25,-1],
							   [SpellData.cSpellChannelWhite,0,-1],
							   [SpellData.cSpellThrowAxe,5,-1],
							   [SpellData.cSpellCharge,9,-1],
							   [SpellData.cSpellRest,20,-1],
							   [SpellData.cSpellBloodBlade,13,-1],
							   [SpellData.cSpellSandStorm,35,-1],
							   [SpellData.cSpellRupture,16,0],
							   [SpellData.cSpellEarthAvatar,17,1],
							   [SpellData.cSpellTrickMana,15,2],
							   ],
							  [["execute"],	
							   ["selfstunafterattack"],
							   ["crit"]]
							 ],		
							 [// - - - -= WYVERN =- - - - 41 UNUSED
							  [10,4,3,3],
							  [],
							  [["poisonclaw"],	
							   ["poisontouch"],
							   ["attackasmagic"]]
							 ],		
							 [// - - - -= DRUID =- - - - 42 UNUSED
							  [12,1,4,4.25],
							  [],
							  [["bonusmgonatk"],	
							   ["blunt"],
							   ["bonusspecialdefense"]]
							 ],		
							 [// - - - -= OGREWARRIOR =- - - - 43 UNUSED
							  [14,7,2,2],
							  [],
							  [["selfstunafterattack"],	
							   ["freedefend"],
							   ["ttackasmagic"]]
							 ],		
							 [// - - - -= REDAXE =- - - - 44 UNUSED
							  [14,7,2,2],
							  [],
							  [["heavyhitter"],	
							   ["horde"],
							   ["bonusmgonatk"]]
							 ],		
							 [// - - - -= TAURON =- - - - 45 UNUSED
							  [11,2.6,3.33,2],
							  [],
							  [["bonusatkonlowhp"],	
							   ["bonusarmordefense"],
							   ["bonusspecialdefense"]]
							 ],		
							 [// - - - -= FIREGIANT =- - - - 46 UNUSED
							  [16,7,2,2],
							  [],
							  [["HabitatRed"],	
							   ["retaliate"],
							   ["attackasmagic"]]
							 ],	
							 [// - - - -= ICE =- - - - 47
							  [16,3.45,6,2],
							  [],
							  [["Moveeverysercondturn"],	
							   ["immunity"],
							   ["HabitatBlue"]]
							 ],									 
							 [// - - - -= ARCHLICHE =- - - - 48
							  [15,3,3,7],
							  [],
							  [["Nomorale"],	
							   ["Nomorale"],
							   ["Nomorale"]]
							 ],	
							 [// - - - -= BLUEDRAGON =- - - - 49
							  [15,4,5,4],
							  [],
							  [["Moveeverysercondturn"],	
							   ["HabitatBlue"],
							   ["GenerateBlue"]]
							 ],	
							 [// - - - -= REDDRAGON =- - - - 50
							  [15,5,4,4],
							  [],
							  [["HabitatRed"],	
							   ["Moveeverysercondturn"],
							   ["GenerateRed"]]
							 ],	
							 [// - - - -= GREENDRAGON =- - - - 51
							  [15,4,4,5],
							  [],
							  [["Moveeverysercondturn"],	
							   ["HabitatGreen"],
							   ["GenerateGreen"]]
							 ],	
							 [// - - - -= UNDEADDRAGON =- - - - 52
							  [16,6,6,6],
							  [],
							  [["Degeneration"],	
							   ["Nomorale"],
							   ["Attackslooselife"]]
							 ],	
							 [// - - - -= HYDRA =- - - - 53
							  [15,3,7,3],
							  [],
							  [["heavyhitter"],	
							   ["regenerate"],
							   ["HabitatGreen"]]
							 ],	
							 [// - - - -= UNDEADOTAUR =- - - - 54
							  [15,7,3,3],
							  [],
							  [["Degeneration","Attackslooselife"],	
							   ["Degeneration","Attackslooselife"],
							   ["Degeneration","Attackslooselife"]]
							 ],	
							 [// - - - -= ICEQUEEN =- - - - 55
							  [10,4,4,4],
							  [[SpellData.cSpellReplenish,25,-1],
							   [SpellData.cSpellInnerFire,15,-1],
							   [SpellData.cSpellCleansingFlame,5,-1],
							   [SpellData.cSpellHex,0,-1],
							   [SpellData.cSpellChannelRed,10,0],
							   [SpellData.cSpellChannelBlue,10,1],
							   [SpellData.cSpellChannelYellow,10,2],
							   [SpellData.cSpellFireElly,20,0],
							   [SpellData.cSpellWaterElly,20,1],
							   [SpellData.cSpellWindElly,20,2],
							   ],
							  [["GenerateRed","HabitatRed"],	
							   ["GenerateBlue","HabitatBlue"],
							   ["GenerateYellow","HabitatYellow"]]
							 ]	
		]

	}
	
}
