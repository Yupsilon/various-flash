﻿package {
	
	import flash.events.*;
	import flash.display.*;
	import flash.geom.*;
	import flash.text.*;
	
	 public class MainMenus 
	 {

public static function DrawMainMenu(game:HubWorldHandler,intro:Boolean=true):void 
{		
	game.Memory["Buttons"]=new Array();
	
	//game.Memory["MM"]=m;
	
	game.Memory["MM"]=new MovieClip();
	game.Memory["Stage"].addChild(game.Memory["MM"]);
	game.Memory["MM"].x=0;
	game.Memory["MM"].y=0;	
	
	game.Memory["MM"].Delete=false;
	
	var BGO:MainMenuBG=new MainMenuBG();
	game.Memory["MM"].addChild(BGO);
	BGO.width=GlobalVariables.stageWidth;
	BGO.height=GlobalVariables.stageHeight;
	BGO.x=GlobalVariables.stageWidth/2;
	BGO.y=GlobalVariables.stageHeight/2;
	game.Memory["Buttons"].push(BGO)
	
	for (var Zim=0; Zim<3; Zim++)
	{
		var SaveData=GameData.loadData(Zim);
		
	var BTNstart:BTNFirstMenu=new BTNFirstMenu();
	game.Memory["MM"].addChild(BTNstart);
	BTNstart.width=400;
	BTNstart.height=80;
	BTNstart.x=400;
	BTNstart.y=75+90*Zim;
	BTNstart.data="newgame";
	BTNstart.saveslot=Zim;
	game.Memory["Buttons"].push(BTNstart)
	
	
	var DIAGTEXT:TextField=new TextField()	
		game.Memory["MM"].addChild(DIAGTEXT);
			DIAGTEXT.text="";
		if (SaveData==null)
		{
			DIAGTEXT.appendText(Language.MainMenuText[0]);
			if (Zim==1)
			{
				DIAGTEXT.appendText("+");
			}
			if (Zim==2)
			{
				DIAGTEXT.appendText("++");
			}
		}
		else
		{			
			DIAGTEXT.appendText(SaveData.Name)
			DIAGTEXT.appendText("\n"+Language.MainMenuText[1]+SaveData.GetCompletionPercent()+"%")
		}
		
		DIAGTEXT.wordWrap=true;	
		DIAGTEXT.width=BTNstart.width;	
		DIAGTEXT.height=BTNstart.height;
		DIAGTEXT.x=BTNstart.x-BTNstart.width/2;
		DIAGTEXT.y=BTNstart.y-3*BTNstart.height/8;
		DIAGTEXT.setTextFormat(GlobalVariables.textMAINMENU);
		BTNstart.T=DIAGTEXT;	
	}				
	
	var BTNmute:BTNFirstMenu=new BTNFirstMenu();
	game.Memory["MM"].addChild(BTNmute);
	BTNmute.width=250;
	BTNmute.height=40;
	BTNmute.x=400;
	BTNmute.y=335;
	BTNmute.data="Delete";
	game.Memory["Buttons"].push(BTNmute)
	
	var BTNTXT:TextField=new TextField()	
		game.Memory["MM"].addChild(BTNTXT);
		
		BTNTXT.text=Language.MainMenuText[2];
		
		
		BTNTXT.wordWrap=true;	
		BTNTXT.width=BTNmute.width;	
		BTNTXT.height=BTNmute.height;
		BTNTXT.x=BTNmute.x-BTNmute.width/2;
		BTNTXT.y=BTNmute.y-BTNmute.height/2+10;
		BTNTXT.setTextFormat(GlobalVariables.textMAINMENU);
		BTNstart.T=BTNTXT;	
		
		
							var MuteBTN=new MuteButton();
							game.Memory["MM"].addChild(MuteBTN)
							MuteBTN.x=700;
							MuteBTN.y=375;
							MuteBTN.width=50;
							MuteBTN.height=25;
							MuteBTN.data="mute";
							game.Memory["Buttons"].push(MuteBTN);
							MuteBTN.gotoAndStop(1);
	
	if (intro==true)
	{
	var BTNIntro:BTNmenu=new BTNmenu();
	game.Memory["MM"].addChild(BTNIntro);
	BTNIntro.width=GlobalVariables.stageWidth;
	BTNIntro.height=GlobalVariables.stageHeight;
	BTNIntro.x=GlobalVariables.stageWidth/2;
	BTNIntro.y=GlobalVariables.stageHeight/2;
	BTNIntro.data="perish";
	
	game.Memory["Buttons"].push(BTNIntro)
	}
}

public static function HandleButtons(game:HubWorldHandler,event:MouseEvent)
{			
	var T=game.Memory["GameTime"].GetTime()
	
	var X=event.stageX
	var Y=event.stageY
	var erase=false;
	
	for (var b=game.Memory["Buttons"].length-1; b>=0; b--)
	{	
		if (game.Memory["Buttons"][b]!=null && T>=game.Memory["GameTime"].ClickTimer)
		{
			if (X>=game.Memory["Buttons"][b].x-game.Memory["Buttons"][b].width/2 && X<=game.Memory["Buttons"][b].x+game.Memory["Buttons"][b].width/2)
			{
				if (Y>=game.Memory["Buttons"][b].y-game.Memory["Buttons"][b].height/2 && Y<=game.Memory["Buttons"][b].y+game.Memory["Buttons"][b].height/2)
				{			
					if (game.Memory["Buttons"][b].data=="perish" && game.Memory["Buttons"][b].alpha>0)
						{
							game.Memory["GameTime"].ClickTimer=T+2;
							game.Memory["Buttons"][b].alpha=0;			
							break;
						}
						else if (game.Memory["Buttons"][b].data=="newgame")
						{
							if (game.Memory["MM"].Delete==false)
							{
								var Game=game.Memory["Buttons"][b].saveslot;
								EraseButtons(game);	
								game.StartGame(Game);							
								break;
							}
							else if (game.Memory["MM"].Delete==true)
							{
								GameData.eraseSave(game.Memory["Buttons"][b].saveslot)
								game.Memory["GameTime"].ClickTimer=T+2;		
								EraseButtons(game);	
								DrawMainMenu(game,false)
								game.Memory["MM"].Delete=false;
								break;
							}
						}
						else if (game.Memory["Buttons"][b].data=="Delete")
						{
							game.Memory["MM"].Delete=(game.Memory["MM"].Delete==false);
							game.Memory["GameTime"].ClickTimer=T+2;
							break;
						}		
						else if (game.Memory["Buttons"][b].data=="mute")
						{
							game.Memory["SoundManager"].MuteSounds();
							game.Memory["GameTime"].ClickTimer=T+2;
							break;
						}					
					}
				}
			}
		}
	
	if (erase==true)
	{
		EraseButtons(game);
	}
}



public static function EraseButtons(game:HubWorldHandler) 
{
	while (game.Memory["MM"].numChildren > 0){ game.Memory["MM"].removeChildAt(0); }
	game.Memory["MM"].parent.removeChild(game.Memory["MM"]);
	game.Memory["Buttons"].length=0;
	game.Memory["MM"]=null;
	
	/*if (this.parent!=null)
	{
		this.parent.removeChild(this)
	}*/
}



 }

 
}