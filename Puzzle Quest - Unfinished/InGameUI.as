﻿package  {
	import flash.text.*;
	import flash.display.MovieClip;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	
	public class InGameUI {				

		public static function DrawTheUI(game:MainGame) {											
		
			game.Memory["Buttons"]=new Array();
			game.Memory["LayerLayer"]=new MovieClip();
			game.Memory["LayerLayer"].x=0;
			game.Memory["LayerLayer"].y=0;
			game.parent.addChild(game.Memory["LayerLayer"]);			
			game.Memory["BGLayer"]=new MovieClip();
			game.Memory["BGLayer"].x=0;
			game.Memory["BGLayer"].y=0;
			game.Memory["LayerLayer"].addChild(game.Memory["BGLayer"]);			
			game.Memory["GemLayer"]=new MovieClip();
			game.Memory["GemLayer"].x=(GlobalVariables.stageWidth-(game.Memory["BoardSize"]-1)*GlobalVariables.GemSize)/2;
			game.Memory["GemLayer"].y=GlobalVariables.stageHeight*0.4/2;
			game.Memory["LayerLayer"].addChild(game.Memory["GemLayer"]);			
			game.Memory["UiLayer"]=new MovieClip();
			game.Memory["UiLayer"].x=0;
			game.Memory["UiLayer"].y=0;
			game.Memory["LayerLayer"].addChild(game.Memory["UiLayer"]);			
			game.Memory["EffectLayer"]=new MovieClip();
			game.Memory["EffectLayer"].x=0;
			game.Memory["EffectLayer"].y=0;
			game.Memory["LayerLayer"].addChild(game.Memory["EffectLayer"]);
			
			game.Memory["InfoUI"]=new Array();
			
			var BG:background = new background();
			BG.width=GlobalVariables.stageWidth;
			BG.height=GlobalVariables.stageHeight;
			BG.x=BG.width/2;
			BG.y=BG.height/2;
			BG.gotoAndStop(Math.ceil(Math.random()*BG.totalFrames))
			game.Memory["BGLayer"].addChild(BG);
			
			for (var Y=1; Y<game.Memory["BoardSize"]; Y++)
			{
				for (var X=1; X<game.Memory["BoardSize"]; X++)
				{	
					var GemBG:CellBG = new CellBG();
					GemBG.x=X*GlobalVariables.GemSize-GlobalVariables.GemSize/2;
					GemBG.y=Y*GlobalVariables.GemSize-GlobalVariables.GemSize/2;
					GemBG.width=GlobalVariables.GemSize;
					GemBG.height=GlobalVariables.GemSize;
					GemBG.alpha=0.8;
					
					game.Memory["GemLayer"].addChild(GemBG);
				}
			}
						
			PlayerUI(game,0,0,1);
			PlayerUI(game,GlobalVariables.stageWidth-(GlobalVariables.stageWidth-GlobalVariables.stageHeight)*1.2/2,1,-1);
			
			var PopUpMenu=new InGameMenu();
			PopUpMenu.x=GlobalVariables.stageWidth/2
			PopUpMenu.y=GlobalVariables.stageHeight+126
			PopUpMenu.data="MainMenu";
			PopUpMenu.dy=PopUpMenu.y
			game.Memory["Buttons"].push(PopUpMenu);
			game.Memory["LayerLayer"].addChild(PopUpMenu);
			
			var PopUpMenuBTN=new MuteButton();	
			PopUpMenuBTN.active=true;
			PopUpMenuBTN.width=200;
			PopUpMenuBTN.height=32;
			PopUpMenuBTN.x=PopUpMenu.x;
			PopUpMenuBTN.y=PopUpMenu.y-142;
			PopUpMenuBTN.data="PopUpMenu";
			PopUpMenuBTN.alpha=0;
			game.Memory["LayerLayer"].addChild(PopUpMenuBTN);
			game.Memory["Buttons"].push(PopUpMenuBTN);
						
			var DIAGTEXT:TextField=new TextField()	
			PopUpMenu.addChild(DIAGTEXT);
			DIAGTEXT.text=Language.MMText[0];
			DIAGTEXT.wordWrap=true;	
			DIAGTEXT.width=100;	
			DIAGTEXT.height=100;
			DIAGTEXT.x=-50;
			DIAGTEXT.y=-154;
			DIAGTEXT.setTextFormat(GlobalVariables.TextINGAMEMENU);
			
			var Functions=["mute","endturn","surrender","quit"]
			for (var Zim=0; Zim<Functions.length; Zim++)
			{
				var MenuBTNa=new MainMenuButton();	
				MenuBTNa.active=true;
				MenuBTNa.x=0;
				MenuBTNa.y=-102+37*Zim;
				MenuBTNa.width=180;
				MenuBTNa.height=38;
				MenuBTNa.data=Functions[Zim];
				PopUpMenu.addChild(MenuBTNa);
				game.Memory["Buttons"].push(MenuBTNa);
						
				var MyText:TextField=new TextField()	
				PopUpMenu.addChild(MyText);
				MyText.text=Language.MMText[1+Zim];
				MyText.wordWrap=true;	
				MyText.width=100;	
				MyText.height=100;
				MyText.x=-50;
				MyText.y=MenuBTNa.y-12;
				MyText.setTextFormat(GlobalVariables.TextINGAMEMENU);
			}
		}
		
		public static function PlayerUI(game:MainGame,X:Number,Player:Number,F:Number) {		
			
			
			var UIb:SideUI = new SideUI();
			UIb.width=(GlobalVariables.stageWidth-GlobalVariables.stageHeight)*1.2/2;
			UIb.scaleX*=F;
			UIb.height=GlobalVariables.stageHeight;
			UIb.x=X+UIb.width/2;
			UIb.y=UIb.height/2;
			game.Memory["BGLayer"].addChild(UIb);
			UIb.data="UI";
			UIb.Owner=Player;
			game.Memory["Buttons"].push(UIb);
			UIb.ManaDisplays=new Array();
			
			var Spinner:MovieClip;
			
			if (Player==0)
			{
				Spinner = new GreenSpin();
			}
			else
			{
				Spinner = new RedSpin();
			}
			
			Spinner.x=UIb.x+75*F;
			Spinner.y=20;
			Spinner.alpha=0;
			game.Memory["BGLayer"].addChild(Spinner);
			game.Memory["Player"+Player+"Turn"]=Spinner;
						
			var DIAGTEXT:TextField=new TextField()	
			game.Memory["UiLayer"].addChild(DIAGTEXT);
			DIAGTEXT.text=game.Memory["PlayerNames"][Player];
			DIAGTEXT.wordWrap=true;	
			DIAGTEXT.width=UIb.width;	
			DIAGTEXT.height=UIb.height;
			DIAGTEXT.x=X;
			DIAGTEXT.y=UIb.y-UIb.height/2+12;
			DIAGTEXT.setTextFormat(GlobalVariables.TextPLAYERNAME);		
			
			for (var Zim=0; Zim<GlobalVariables.xk_MAX_MANA_TYPES; Zim++)
			{
				var GemB:GemSymbol = new GemSymbol();
				GemB.width=20;
				GemB.height=20;
				GemB.x=X+20+30*Zim+GemB.width/2;
				GemB.y=50;
				GemB.gotoAndStop(Zim+1);
				game.Memory["UiLayer"].addChild(GemB);
				
				//GemB.UIdata="Gem"+(Zim+1)
				//game.Memory["InfoUI"].push(GemB)
				
				var GemT:TextField=new TextField()	
				game.Memory["UiLayer"].addChild(GemT);
				GemT.text=game.Memory["Display"]["PlayerMana"][Player][Zim];	
				GemT.width=20;	
				GemT.height=20;
				GemT.x=GemB.x-GemB.width/2+5;
				GemT.y=GemB.y+10;
				GemT.setTextFormat(GlobalVariables.TextARMYNAME);
				UIb.ManaDisplays[Zim]=GemT;
			}
			
			for (Zim=0; Zim<game.Memory["DisplayItemArmies"][Player].length; Zim++)
			{
				var Army:MovieClip = new MovieClip();
				Army.x=X+UIb.width/2;
				Army.y=130+45*Zim;
				Army.ox=Army.x;
				Army.dx=Army.x;
				Army.dy=Army.y;
				Army.selected=false;
				Army.data="Army";
				Army.Owner=Player;
				Army.Army=Zim;
				Army.F=F;
				Army.active=true;
				game.Memory["UiLayer"].addChild(Army);
				game.Memory["Buttons"].push(Army);
				
				Army.BG= new ArmyBGbutton();
				Army.BG.width=UIb.width*0.975;
				Army.BG.height=45;
				Army.BG.x=0;
				Army.BG.y=0;
				Army.addChild(Army.BG);
				Army.BG.gotoAndStop(Zim);
				Army.UIdata="Army"
				game.Memory["InfoUI"].push(Army)
								
				var Display = new Bitmap(new ArmyData.Display[game.Memory["DisplayItemArmies"][Player][Zim].Type](null, null));				
				Display.scaleY=44/Display.height;
				Display.scaleX=Display.scaleY*F;
				Display.x=0-UIb.width*0.975/2*F+F*10;
				Display.y=0-Display.height/2-2;
				Army.addChild(Display);
				Army.ArmyData=Display;								
								
				var ArmyT:TextField=new TextField()	
				Army.addChild(ArmyT);
				ArmyT.text=game.Memory["DisplayItemArmies"][Player][Zim].Name;	
				ArmyT.width=180;	
				ArmyT.height=30;
				ArmyT.x=0-80/2-10+25*F;
				ArmyT.y=0-23;
				ArmyT.setTextFormat(GlobalVariables.TextARMYNAME);
				Army.ArmyData=ArmyT;
				
				var Life_Bar:LifeBar = new LifeBar()
				Life_Bar.width=80;
				Life_Bar.height=15;			
				Life_Bar.F=F;				
				Life_Bar.x=Life_Bar.width/2+43*F-43;
				Life_Bar.y=Army.BG.height/2-Life_Bar.height*3.6/5;
				Army.addChild(Life_Bar);
				Army.LifeBar=Life_Bar;
				
				var Life_BG:LifeBar = new LifeBar()
				Life_BG.width=Life_Bar.width;
				Life_BG.height=Life_Bar.height;				
				Life_BG.x=Life_Bar.x;
				Life_BG.y=Life_Bar.y;
				Life_BG.alpha=0.5;
				Army.addChild(Life_BG);
								
				var Life_T:TextField=new TextField()	
				Army.addChild(Life_T);
				Life_T.text=game.Memory["DisplayItemArmies"][Player][Zim].Life+"/"+game.Memory["DisplayItemArmies"][Player][Zim].MaxLife;	
				Life_T.width=80;	
				Life_T.height=20;
				Life_T.x=43*F-43;
				Life_T.y=Life_Bar.y-Life_Bar.height/2;
				Life_T.setTextFormat(GlobalVariables.TextLIFEBAR);
				Army.LifeText=Life_T;				
				
				var bDefend:ShieldIcon = new ShieldIcon();
				bDefend.width=34;
				bDefend.height=40;
				bDefend.x=Army.BG.width/2*F-10*F;
				bDefend.y=0;
				bDefend.alpha=0;
				bDefend.gotoAndStop(Zim+1);
				Army.addChild(bDefend);				
				Army.Shield=bDefend;
			}
			
				var ArmyUI:MovieClip = new MovieClip()			
				ArmyUI.x=X+30;
				ArmyUI.y=160;
				game.Memory["UiLayer"].addChild(ArmyUI);				
				
				UIb.Icons=new Array();
				
				var IconA = new UIInfoBTN()
				IconA.width=25;
				IconA.height=25;				
				IconA.x=15;
				IconA.y=13;
				IconA.alpha=0;
				IconA.gotoAndStop(1);
				ArmyUI.addChild(IconA);
				IconA.UIdata="Attack";
				game.Memory["InfoUI"].push(IconA)
								
				var TextA:TextField=new TextField()	
				ArmyUI.addChild(TextA);
				TextA.text="25";	
				TextA.width=80;	
				TextA.height=20;
				TextA.x=30;
				TextA.y=5;
				TextA.alpha=0;
				TextA.setTextFormat(GlobalVariables.TextARMYUI);
				
				var IconB = new UIInfoBTN()
				IconB.width=25;
				IconB.height=25;				
				IconB.x=70;
				IconB.y=13;
				IconB.alpha=0;
				IconB.gotoAndStop(2);
				ArmyUI.addChild(IconB);	
				IconB.UIdata="Armor"
				game.Memory["InfoUI"].push(IconB)
								
				var TextB:TextField=new TextField()	
				ArmyUI.addChild(TextB);
				TextB.text="25";	
				TextB.width=80;	
				TextB.height=20;
				TextB.x=85;
				TextB.y=5;
				TextB.alpha=0;
				TextB.setTextFormat(GlobalVariables.TextARMYUI);
				
				var IconC = new UIInfoBTN()
				IconC.width=25;
				IconC.height=25;				
				IconC.x=125;
				IconC.y=13;
				IconC.alpha=0;
				IconC.gotoAndStop(3);
				ArmyUI.addChild(IconC);		
								
				var TextC:TextField=new TextField()	
				ArmyUI.addChild(TextC);
				TextC.text="25";	
				TextC.width=80;	
				TextC.height=20;
				TextC.x=140;
				TextC.y=5;
				TextC.alpha=0;
				TextC.setTextFormat(GlobalVariables.TextARMYUI);
				IconC.UIdata="Special"
				game.Memory["InfoUI"].push(IconC)
								
				UIb.Icons=[IconA,IconB,IconC];
				UIb.TextA=TextA;
				UIb.TextB=TextB;
				UIb.TextC=TextC;

				if (Player==0)
				{
					var AttackBTN = new MovieClip()
					//AttackBTN.width=40;
					//AttackBTN.height=40;		
					AttackBTN.x=UIb.width*3/4-6;
					AttackBTN.y=210;	
					AttackBTN.dx=AttackBTN.x;
					AttackBTN.dy=AttackBTN.y;
					AttackBTN.alpha=0;
					AttackBTN.Owner=Player;
					//AttackBTN.gotoAndStop(1);
					AttackBTN.data="Attack";
					game.Memory["UiLayer"].addChild(AttackBTN);	
					game.Memory["Buttons"].push(AttackBTN);
					AttackBTN.UIdata="AttackSpell"
					game.Memory["InfoUI"].push(AttackBTN)
					
					var ButtonBG= new ArmySpellbutton();
					ButtonBG.width=UIb.width/2-14;
					ButtonBG.height=40;
					ButtonBG.x=0;
					ButtonBG.y=0;
					AttackBTN.addChild(ButtonBG);
					AttackBTN.BG=ButtonBG;
					
					var ButtonText:TextField=new TextField()	
					AttackBTN.addChild(ButtonText);
					ButtonText.text=Language.SpellText[SpellData.cSpellAttack][0];	
					ButtonText.width=80;	
					ButtonText.height=20;
					ButtonText.x=0-ButtonBG.width/2+10;
					ButtonText.y=0-ButtonBG.height/2+1;
					ButtonText.alpha=1;
					ButtonText.setTextFormat(GlobalVariables.TextARMYUI);			
					AttackBTN.Text=ButtonText;
					
					AttackBTN.ManaDisplays=new Array();
					for (var X=4; X<GlobalVariables.xk_MAX_MANA_TYPES; X++)
					{
						var GemA:GemSymbol = new GemSymbol();
						GemA.width=15;
						GemA.height=15;
						GemA.x=0-ButtonBG.width/2+14+30*(X-4);
						GemA.y=10;
						GemA.gotoAndStop(X+1);
						AttackBTN.addChild(GemA);
						
						var Gemt:TextField=new TextField()	
						AttackBTN.addChild(Gemt);
						Gemt.text=String(Math.round(SpellData.SpellBook[SpellData.cSpellAttack][1][X]));	
						Gemt.width=20;	
						Gemt.height=20;
						Gemt.x=0-ButtonBG.width/2+20+30*(X-4);
						Gemt.y=0;
						Gemt.setTextFormat(GlobalVariables.TextARMYUI);
						AttackBTN.ManaDisplays[X]=Gemt;
						
					}
					
					var DefenderBTN = new MovieClip();
					//DefenderBTN.width=40;
					//DefenderBTN.height=40;				
					DefenderBTN.x=UIb.width*1/4+6;
					DefenderBTN.y=210;
					DefenderBTN.dx=DefenderBTN.x;
					DefenderBTN.dy=DefenderBTN.y;
					DefenderBTN.alpha=0;
					//DefenderBTN.gotoAndStop(2);
					DefenderBTN.Owner=Player;
					DefenderBTN.data="Defend";
					game.Memory["UiLayer"].addChild(DefenderBTN);	
					game.Memory["Buttons"].push(DefenderBTN);
					DefenderBTN.UIdata="DefenderSpell"
					game.Memory["InfoUI"].push(DefenderBTN)
					
					var ButtonBG= new ArmySpellbutton();
					ButtonBG.width=UIb.width/2-14;
					ButtonBG.height=40;
					ButtonBG.x=0;
					ButtonBG.y=0;
					DefenderBTN.addChild(ButtonBG);
					DefenderBTN.BG=ButtonBG;
								
					var ButtonText:TextField=new TextField()	
					DefenderBTN.addChild(ButtonText);
					ButtonText.text=Language.SpellText[SpellData.cSpellDefender][0];;	
					ButtonText.width=80;	
					ButtonText.height=20;
					ButtonText.x=0-ButtonBG.width/2+10;
					ButtonText.y=0-ButtonBG.height/2+1;
					ButtonText.alpha=1;
					ButtonText.setTextFormat(GlobalVariables.TextARMYUI);			
					DefenderBTN.Text=ButtonText;
					
					DefenderBTN.ManaDisplays=new Array();
					for (var X=4; X<GlobalVariables.xk_MAX_MANA_TYPES; X++)
					{
						var GemA:GemSymbol = new GemSymbol();
						GemA.width=15;
						GemA.height=15;
						GemA.x=0-ButtonBG.width/2+14+30*(X-4);
						GemA.y=10;
						GemA.gotoAndStop(X+1);
						DefenderBTN.addChild(GemA);
						
						var Gemt:TextField=new TextField()	
						DefenderBTN.addChild(Gemt);
						Gemt.text=String(Math.round(SpellData.SpellBook[SpellData.cSpellDefender][1][X]));	
						Gemt.width=20;	
						Gemt.height=20;
						Gemt.x=0-ButtonBG.width/2+20+30*(X-4);
						Gemt.y=0;
						Gemt.setTextFormat(GlobalVariables.TextARMYUI);
						DefenderBTN.ManaDisplays[X]=Gemt;
						
					}
				}
				
				for (Zim=0; Zim<3;Zim++)
				{
					
					var SpellB= new MovieClip();
					SpellB.x=UIb.x;
					SpellB.y=265+48*Zim;
					game.Memory["UiLayer"].addChild(SpellB);	
					game.Memory["Buttons"].push(SpellB);
					SpellB.Spell=Zim;
					SpellB.data="Spell";
					SpellB.Owner=Player;
					SpellB.active=false;
					SpellB.alpha=0;			
					SpellB.UIdata="SpellData";
					game.Memory["InfoUI"].push(SpellB)
						
					game.Memory["SpellUI"+Player+","+Zim]=SpellB;
					
					var SpellBG= new ArmySpellbutton();
					SpellBG.width=UIb.width*0.9;
					SpellBG.height=46;
					SpellBG.x=0;
					SpellBG.y=0;
					SpellB.addChild(SpellBG);
					SpellB.BG=SpellBG;
								
					var tempText:TextField=new TextField()	
					SpellB.addChild(tempText);
					tempText.text="ASD";	
					tempText.width=80;	
					tempText.height=20;
					tempText.x=0-SpellBG.width/2+10;
					tempText.y=0-SpellBG.height/2+1;
					tempText.alpha=1;
					tempText.setTextFormat(GlobalVariables.TextARMYUI);			
					SpellB.Text=tempText;
					
					SpellB.ManaDisplays=new Array();
					for (var X=0; X<GlobalVariables.xk_MAX_MANA_TYPES; X++)
					{
						var GemB:GemSymbol = new GemSymbol();
						GemB.width=15;
						GemB.height=15;
						GemB.x=0-SpellBG.width/2+14+30*X;
						GemB.y=10;
						GemB.gotoAndStop(X+1);
						SpellB.addChild(GemB);
						
						var GemT:TextField=new TextField()	
						SpellB.addChild(GemT);
						GemT.text="0";	
						GemT.width=20;	
						GemT.height=20;
						GemT.x=0-SpellBG.width/2+20+30*X;
						GemT.y=0;
						GemT.setTextFormat(GlobalVariables.TextARMYUI);
						SpellB.ManaDisplays[X]=GemT;
						
					}
				}
		}
		
		public static function DrawAbilityUI(game:MainGame,Player:Number,nArmy:Number)
		{
			if (Player==0)
								{
									var Army:DisplayItemArmy=game.Memory["DisplayItemArmies"][Player][nArmy]
									for (var Zim=0; Zim<Army.Abilities.length; Zim++)
									{
										var BTN=game.Memory["SpellUI"+Player+","+Zim];
										if (BTN!=null)
										{
											BTN.active=true;
											BTN.alpha=100;
											BTN.army=nArmy;
											BTN.SpellData=Army.Abilities[Zim];						
											BTN.Text.text=Language.SpellText[Army.Abilities[Zim]][0];
											BTN.Text.setTextFormat(GlobalVariables.TextARMYUI)
											
											for (var Gir=0; Gir<BTN.ManaDisplays.length; Gir++)
											{
												BTN.ManaDisplays[Gir].text=Math.round(SpellData.SpellBook[Army.Abilities[Zim]][1][Gir]);
												BTN.ManaDisplays[Gir].setTextFormat(GlobalVariables.TextARMYUI)
											}						
										}
									}
								}
								else 
								{
									var Army:DisplayItemArmy=game.Memory["DisplayItemArmies"][Player][nArmy]
									for (var Zim=0; Zim<Army.Abilities.length; Zim++)
									{
										var BTN=game.Memory["SpellUI"+Player+","+Zim];
										if (BTN!=null)
										{
											BTN.active=true;
											BTN.data="SpellEnemy";
											BTN.alpha=100;
											BTN.SpellData=Army.Abilities[Zim];				
											BTN.Text.text=Language.SpellText[Army.Abilities[Zim]][0];
											BTN.Text.setTextFormat(GlobalVariables.TextARMYUI)
											
											for (var Gir=0; Gir<BTN.ManaDisplays.length; Gir++)
											{
												BTN.ManaDisplays[Gir].text=Math.round(SpellData.SpellBook[Army.Abilities[Zim]][1][Gir]);
												BTN.ManaDisplays[Gir].setTextFormat(GlobalVariables.TextARMYUI)
											}
											BTN.BG.gotoAndStop(3)
										}
									}
								}
		}
	}
	
}
