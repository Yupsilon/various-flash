﻿package  {
	import flash.display.MovieClip;
	import flash.text.TextField;
	
	public class GemManager {

		public static function DrawTheBoard(game:MainGame) {
									
			game.Memory["Board"]=new Array();
						
			for (var Y=0; Y<game.Memory["BoardSize"]; Y++)
			{
				var Arrey=new Array();
				for (var X=0; X<game.Memory["BoardSize"]; X++)
				{
					Arrey.push(null);
				}
				game.Memory["Board"].push(Arrey);
				
			}	
									
			MakeFreshBoard(game);
		}
		
		public static function MakeFreshBoard(game:MainGame) {
			
			ClearAllGems(game);					
							
			for (var Y=1; Y<game.Memory["BoardSize"]; Y++)
			{
				for (var X=1; X<game.Memory["BoardSize"]; X++)
				{					
							var Colors:Array=new Array();
							for (var C=1; C<=GlobalVariables.xk_MAX_MANA_TYPES; C++)
							{
								if (C!=game.Memory["PrevGemColor"] && (C!=GlobalVariables.GEM_PURPLE+1 || (Math.random()*100)<GlobalVariables.ChancePurple))
								{
									Colors.push(C);
								}
							}
							var RND=Math.round(Math.random()*(Colors.length-1));
							var truecolors=Colors[RND];
								
							var Check = 1;
							
							var Neighbors:Array=[GetGemAt(game,X,Y+1),
											GetGemAt(game,X,Y-1),
											GetGemAt(game,X+1,Y),
											GetGemAt(game,X-1,Y)];
							
							for (C=0; C<Neighbors.length; C++)
							{
								if (Neighbors[C]!=null)
								{
									if (Neighbors[C].Type==truecolors) 
									{
										Check+=1
									}
									else 
									{
										Colors.splice(Neighbors[C].Type,1)
									}
								}
							}	
							if (Check>=2) {								
								truecolors=Colors[Math.round(Math.max(0,Math.random()*Colors.length-1))];
						 	}
							MakeNewGem(game,X,Y,truecolors);
					}
				}	
		}	
		
		public static function MakeNewGem(game:MainGame,X:Number,Y:Number,Type:Number) {
			
			if (X>0 && Y<game.Memory["Board"].length)
			{
				if (Type==-1)
				{
					var Col=-1;
					var Colors:Array=new Array();
					if (Y<=0)
					{
						var Zim=GetHighestGem(game,X);
						if (Zim!=null)
						{
							Col=Zim.Type;
						}
					}
					for (var C=1; C<=GlobalVariables.xk_MAX_MANA_TYPES; C++)
					{
						if (C!=game.Memory["PrevGemColor"] && (C!=GlobalVariables.GEM_PURPLE+1 || (Math.random()*100)<GlobalVariables.ChancePurple))
						{
							Colors.push(C);
						}
					}
					var RND=Math.round(Math.random()*(Colors.length-1));
					Type=Colors[RND];
					game.Memory["PrevGemColor"]=Type;
				}
						
				var Gem:DataItemGem = new DataItemGem();
				Gem.X=X;			
				Gem.Y=Y;
				game.Memory["Board"][Y][X]=Gem;
				Gem.Type=Type;
				Gem.Moving=false;
				Gem.dead=false;
				Gem.CanDrop=false;
				Gem.Name="g_x"+Gem.X+"_y"+Gem.Y;
				
				game.Memory[Gem.Name]=Gem;			
				Gem.x=Gem.X*GlobalVariables.GemSize;
				Gem.y=(0-1)*GlobalVariables.GemSize;
				
				Gem.Clip=new GemSymbol();
				Gem.Clip.width=GlobalVariables.GemSize;
				Gem.Clip.height=GlobalVariables.GemSize;
				Gem.Clip.alpha=0;
				Gem.Clip.x=Gem.x-GlobalVariables.GemSize/2;
				Gem.Clip.y=Gem.y-GlobalVariables.GemSize/2;
				Gem.Clip.gotoAndStop(Type);
				game.Memory["GemLayer"].addChild(Gem.Clip);
				
				game.Memory["BoardMana"][Gem.Type-1]+=1;
							
				/*Gem.Text=new TextField()	
				game.Memory["GemLayer"].addChild(Gem.Text);
				Gem.Text.text=Gem.X+","+Gem.Y;
				Gem.Text.wordWrap=true;	
				Gem.Text.width=Gem.Clip.width;	
				Gem.Text.height=Gem.Clip.height;
				Gem.Text.x=Gem.Clip.x;
				Gem.Text.y=Gem.Clip.y;
				Gem.Text.setTextFormat(GlobalVariables.TextPLAYERUI);*/
				
				game.Memory["GemLayer"].addChild(Gem.Clip);			
				
				game.Memory["Gems"].push(Gem);	
			}			
		}		
		
		public static function ChangeGemColor(game:MainGame,Gem:DataItemGem,Type:Number) {
				game.Memory["BoardMana"][Gem.Type-1]-=1;					
				SpecialEffects.MakeGemExplosion(game.Memory["HWH"],game.Memory["EffectLayer"],Gem.Type,Gem.Clip);
				Gem.Type=Type+1;
				Gem.Clip.gotoAndStop(Type+1);
				game.Memory["BoardMana"][Gem.Type-1]+=1;
				game.Memory["BoardUpdate"]=false;
				SpecialEffects.MakeGemExplosion(game.Memory["HWH"],game.Memory["EffectLayer"],Gem.Type,Gem.Clip);
		}	
		
		public static function ClearAllGems(game:MainGame) {
			
			for (var Zim=0; Zim<game.Memory["Gems"].length; Zim++)
			{
				if (game.Memory["Gems"][Zim]!=null)
				{
					DestroyGem(game,game.Memory["Gems"][Zim].X,game.Memory["Gems"][Zim].Y,game.Memory["Gems"][Zim])
				}
			}
		}
		
		public static function GetHighestGem(game:MainGame,X:Number)
		{
			var Gem=null;
			for (var Zim=0; Zim<game.Memory["Gems"].length; Zim++)
			{
				if (Gem==null || (game.Memory["Gems"][Zim]!=null && Gem.X==X && game.Memory["Gems"][Zim].Y<Gem.Y))
				{
					Gem=game.Memory["Gems"][Zim];
				}
			}
			return Gem;
		}
		
		public static function HandleGems(game:MainGame)
		{
			game.Memory["GemsMoving"]=false;
			var Clear=false;
			for each (var Gem in game.Memory["Gems"])
			{
				//var Gem=game.Memory["Gems"][Zim]
				if (Gem!=null)
				{														
					if (Gem.dead==false)
					{
						if (Gem.x!=Gem.X*GlobalVariables.GemSize || Gem.y!=Gem.Y*GlobalVariables.GemSize)
						{
							Gem.Moving=true;
							game.Memory["GemsMoving"]=true;
							var Speed=GlobalVariables.GemSpeed;
							if (Gem.CanDrop==true)
							{
								Speed*=2;
							}
							
							if (Gem.x>Gem.X*GlobalVariables.GemSize)
							{
								Gem.x=Math.max(Gem.x-Speed,Gem.X*GlobalVariables.GemSize);
							}
							else if (Gem.x<Gem.X*GlobalVariables.GemSize)
							{
								Gem.x=Math.min(Gem.x+Speed,Gem.X*GlobalVariables.GemSize);
							}
							
							
							if (Gem.y>Gem.Y*GlobalVariables.GemSize)
							{
								Gem.y=Math.max(Gem.y-Speed,Gem.Y*GlobalVariables.GemSize);
							}
							else if (Gem.y<Gem.Y*GlobalVariables.GemSize)
							{
								Gem.y=Math.min(Gem.y+Speed,Gem.Y*GlobalVariables.GemSize);
							}				
							
						}	
						else 
						{
							Gem.Name="g_x"+Gem.X+"_y"+Gem.Y;
							if (Gem.Moving==true && (game.Memory[Gem.Name]==null || game.Memory[Gem.Name]==Gem))
							{
							game.Memory[Gem.Name]=Gem;
							Gem.Moving=false;
							}
						}
						
						DropGem(game,Gem);
						
						Gem.Clip.x=Gem.x-GlobalVariables.GemSize/2;
						Gem.Clip.y=Gem.y-GlobalVariables.GemSize/2;
																							
						if (Gem.Y>0)
						{
							Gem.Clip.alpha=Math.min(Gem.Clip.alpha+0.2,1);
						}					
					}
					else
					{
						DestroyGem(game,0,0,Gem)
						Clear=true;
					}
				}
				else
				{
					game.Memory["Gems"].splice(game.Memory["Gems"].indexOf(Gem),1);
				}
			}
			
			if (game.Memory["Selection"]!=undefined)
			{
				if (game.Memory["Selection"].active==true)
				{
					game.Memory["Selection"].alpha=Math.min(game.Memory["Selection"].alpha+0.2,1)
					game.Memory["Selection"].rotation=game.Memory["Selection"].rotation+14;
				}
				else
				{
					game.Memory["Selection"].alpha=Math.max(game.Memory["Selection"].alpha-0.2,0)
				}
			}
			else
			{
				game.Memory["Selection"]=new GemSelection();
				game.Memory["Selection"].alpha=0;
				game.Memory["Selection"].active=false;
				game.Memory["Selection"].width=GlobalVariables.GemSize*1.5;
				game.Memory["Selection"].height=GlobalVariables.GemSize*1.5;
				game.Memory["UiLayer"].addChild(game.Memory["Selection"]);
			}
			
			if (game.Memory["BoardUpdate"]==false && game.Memory["GemsMoving"]==false)
			{
				UpdateBoard(game,true);
			}
			else if (game.Memory["GemsMoving"]==true)
			{
				game.Memory["BoardUpdate"]=false;
			}
			else
			{
				HandleMana.BalanceMana(game);	
			}
		}
		
		public static function DropGem(game:MainGame,Gem:Object)
		{			
			
			var GemT=GetGemAt(game,Gem.X,Gem.Y+1);
			if (Gem.Moving==false && GemT==null && game.Memory["Board"][Gem.Y+1]!=null)
								{									
									game.Memory["Board"][Gem.Y][Gem.X]=null
									game.Memory["Board"][Gem.Y+1][Gem.X]=Gem																		
																		
									game.Memory[Gem.Name]=undefined;
									Gem.Y=Gem.Y+1;						
									Gem.Name="g_x"+Gem.X+"_y"+Gem.Y;
									Gem.CanDrop=true
									game.Memory["GemsMoving"]=true;
								}
								else
								{
									Gem.CanDrop=false;
								}
		}		
		
		public static function DestroyGem(game:MainGame,X:Number,Y:Number,Gem=null,Replace=true) {
			
			if (Gem==null){ 
				Gem=GetGemAt(game,X,Y)
			}
			else
			{
				X=Gem.X
				Y=Gem.Y
			}
			if (Gem!=null && Gem!=undefined)
			{
				for (var Zim=0; Zim<game.Memory["Gems"].length; Zim++)
				{
					var GemT=game.Memory["Gems"][Zim]
					if (GemT!=null && GemT.X==X && GemT.Y==Y && GemT.dead==false)
					{
						Gem=GemT
					}
				}
			}			
			
			if (Gem!=null && Gem!=undefined)
			{							
				
				game.Memory["BoardMana"][Gem.Type-1]-=1;
				if (Gem.Clip!=null)
				{
					SpecialEffects.MakeGemExplosion(game.Memory["HWH"],game.Memory["EffectLayer"],Gem.Type,Gem.Clip);
					Gem.Clip.parent.removeChild(Gem.Clip);
					Gem.Clip=null;
				}
				game.Memory[Gem.Name]=undefined;
				game.Memory["Board"][Gem.Y][Gem.X]=null;
				Gem.Type=-1;
				Gem.dead=true;
				Gem.X=0;
				Gem.Y=0;
				
				if (Replace==true)
				{
					MakeNewGem(game,X,0,-1);
				}
			}
		}
		
		public static function GetGemAt(game:MainGame,X:Number,Y:Number)
		{
			if (game.Memory["Board"][Y]==null)
			{
				return null;
			}
			return game.Memory["Board"][Y][X];
		}
		
		public static function GetGem(game:MainGame,X:Number,Y:Number)
		{			
			return game.Memory["g_x"+X+"_y"+Y];
		}
		
		public static function PlayerMoveGems(game:MainGame,Player:Number,GemA:Object,GemB:Object)
		{			
			if (GemA!=null && GemB!=null)
			{
				SwapGems(game,GemA,GemB);								
				HandleMana.RemoveMana(game,Player,1,GlobalVariables.GEM_PURPLE);
			}
		}
		
		public static function SwapGems(game:MainGame,GemA:Object,GemB:Object)
		{
			var X=GemA.X;
			var Y=GemA.Y;
							
			GemA.X=GemB.X;
			GemB.X=X;
							
			GemA.Y=GemB.Y;
			GemB.Y=Y;
							
			game.Memory["Board"][GemA.Y][GemA.X]=GemA;
			game.Memory["Board"][GemB.Y][GemB.X]=GemB;
							
			GemA.Name="g_x"+GemA.X+"_y"+GemA.Y;
			GemB.Name="g_x"+GemB.X+"_y"+GemB.Y;
																		
			game.Memory[GemA.Name]=GemA;
			game.Memory[GemB.Name]=GemB;	
			
			game.Memory["BoardUpdate"]=false;
		}
		
		public static function UpdateBoard(game:MainGame,Update:Boolean)
		{			
			var TempBoard:Array=new Array();
			for (var Y=0; Y<game.Memory["Board"].length; Y++)
			{				
				var Gir=new Array();
				for (var X=0; X<game.Memory["Board"][Y].length; X++)
				{
					var Gem=game.Memory["Board"][Y][X];
					if (Gem!=null && Gem.Moving==false && Gem.CanDrop==false && Gem.dead==false)
					{
						Gir.push(Gem.Type);
					}
				}
				TempBoard.push(Gir);
			}		
			
						
			for (var Zim=0; Zim<=game.Memory["Board"].length; Zim++)
			{				
				if (game.Memory["Board"][Zim]!=null)
				{
					var Neighbors:Array=new Array();						
					var prevINT=-1
					for (Gir=0; Gir<=game.Memory["Board"][Zim].length; Gir++)
					{
						Gem=GetGemAt(game,Gir,Zim)
						if (Gem!=null && Gem!=undefined)
						{
							var xINT=Gem.Type;	
							
							if (xINT==prevINT)
							{
									Neighbors.push(Gem);							
									if (Gir==game.Memory["Board"][Zim].length-1)
									{
										if (Neighbors.length>=3)
										{
											for (var Zet=0; Zet<Neighbors.length; Zet++)
											{
												Neighbors[Zet].dead=true;
												SpecialEffects.MakeGemManaParticles(game.Memory["HWH"],game.Memory["EffectLayer"],game.Memory["PlayerTurn"],Neighbors[Zet].Type,Neighbors[Zet].Clip,(Neighbors.length-2)/Neighbors.length);
											}
											HandleMana.ResolveMana(game,Neighbors.length,prevINT);							
										}
									}
							}
							else
							{
								if (Neighbors.length>=3)
								{
									for (Zet=0; Zet<Neighbors.length; Zet++)
									{
										Neighbors[Zet].dead=true;
										SpecialEffects.MakeGemManaParticles(game.Memory["HWH"],game.Memory["EffectLayer"],game.Memory["PlayerTurn"],Neighbors[Zet].Type,Neighbors[Zet].Clip,(Neighbors.length-2)/Neighbors.length);
									}
									HandleMana.ResolveMana(game,Neighbors.length,prevINT);							
								}
								Neighbors=new Array();	
								Neighbors.push(Gem)	
							}
							prevINT=xINT;
							
						}
					}
					
					Neighbors=new Array();						
					prevINT=-1
					
					for (Gir=0; Gir<=game.Memory["Board"][Zim].length; Gir++)
					{
						Gem=GetGemAt(game,Zim,Gir)
						if (Gem!=null && Gem!=undefined)
						{
							xINT=Gem.Type;	
							
							if (xINT==prevINT)
							{
									Neighbors.push(Gem);							
									if (Gir==game.Memory["Board"][Zim].length-1)
									{
										if (Neighbors.length>=3)
										{
											for (Zet=0; Zet<Neighbors.length; Zet++)
											{
												Neighbors[Zet].dead=true;
												SpecialEffects.MakeGemManaParticles(game.Memory["HWH"],game.Memory["EffectLayer"],game.Memory["PlayerTurn"],Neighbors[Zet].Type,Neighbors[Zet].Clip,(Neighbors.length-2)/Neighbors.length);
											}
											HandleMana.ResolveMana(game,Neighbors.length,prevINT);							
										}
									}
							}
							else
							{
								if (Neighbors.length>=3)
								{
									for (Zet=0; Zet<Neighbors.length; Zet++)
									{
										Neighbors[Zet].dead=true;
										SpecialEffects.MakeGemManaParticles(game.Memory["HWH"],game.Memory["EffectLayer"],game.Memory["PlayerTurn"],Neighbors[Zet].Type,Neighbors[Zet].Clip,(Neighbors.length-2)/Neighbors.length);
									}
									HandleMana.ResolveMana(game,Neighbors.length,prevINT);							
								}
								Neighbors=new Array();	
								Neighbors.push(Gem)	
							}
							prevINT=xINT;
							
						}
					}
				}
			}		
		
			/*for (var Zim=0; Zim<game.Memory["Gems"].length; Zim++)
			{
				var Gem=game.Memory["Gems"][Zim]
				if (Gem!=null && Gem.Moving==false && Gem.Type!=0 && Gem.CanDrop==false && Gem.dead==false)
				{
						var Xneighbors=new Array();
						var Yneighbors=new Array();
						
						for (var Zet=1; Zet<5; Zet++)
						{
							var Neighbor=GetGemAt(game,Gem.X+Zet,Gem.Y)
							if (Neighbor!=null)
							{
								if (Neighbor.Type==Gem.Type && Neighbor.Moving==false && Neighbor.YUsed==false)
								{
									Yneighbors.push(Neighbor);
								}
								else
								{
									break;
								}
							}
							else
							{
								break;
							}
						}
						
						for (Zet=1; Zet<5; Zet++)
						{
							Neighbor=GetGemAt(game,Gem.X-Zet,Gem.Y)
							if (Neighbor!=null)
							{
								if (Neighbor.Type==Gem.Type && Neighbor.Moving==false && Neighbor.YUsed==false)
								{
									Yneighbors.push(Neighbor);
								}
								else
								{
									break;
								}
							}
						}
						
						for (Zet=1; Zet<5; Zet++)
						{
							Neighbor=GetGemAt(game,Gem.X,Gem.Y+Zet)
							if (Neighbor!=null)
							{
								if (Neighbor.Type==Gem.Type && Neighbor.Moving==false && Neighbor.XUsed==false)
								{
									Xneighbors.push(Neighbor);
								}
								else
								{
									break;
								}
							}
						}
						
						for (Zet=1; Zet<5; Zet++)
						{
							Neighbor=GetGemAt(game,Gem.X,Gem.Y-Zet)
							if (Neighbor!=null)
							{
								if (Neighbor.Type==Gem.Type && Neighbor.Moving==false && Neighbor.XUsed==false)
								{
									Xneighbors.push(Neighbor);
								}
								else
								{
									break;
								}
							}
						}
						
						if (Xneighbors.length>=2)
						{
							for (Zet=0; Zet<Xneighbors.length; Zet++)
							{
								Xneighbors[Zet].dead=true;
								Xneighbors[Zet].XUsed=true;
								game.Memory["Board"][Xneighbors[Zet].Y][Xneighbors[Zet].X]=null;
							}
						}
						
						if (Yneighbors.length>=2)
						{
							for (Zet=0; Zet<Yneighbors.length; Zet++)
							{
								Yneighbors[Zet].dead=true;
								Xneighbors[Zet].YUsed=true;
								game.Memory["Board"][Yneighbors[Zet].Y][Yneighbors[Zet].X]=null;
							}
						}
						if (Yneighbors.length>=2 || Xneighbors.length>=2)
						{
								Gem.dead=true;
								game.Memory["Board"][Gem.Y][Gem.X]=null;
								HandleMana.ResolveMana(game,Xneighbors.length,Gem);
								HandleMana.ResolveMana(game,Yneighbors.length,Gem);
						}
				}
				else if (Gem!=null && Gem.Type==-1)
				{
					game.Memory["Gems"][Zim]=null;
				}
			}
			for (Zim=0; Zim<game.Memory["Gems"].length; Zim++)
			{
				Gem=game.Memory["Gems"][Zim]
				if (Gem!=null && Gem.dead==true)
				{
					DestroyGem(game,0,0,Gem);
				}			
			}*/
		game.Memory["BoardUpdate"]=Update;
		game.Memory["GemsMoving"]=true;
		}
	}	
}
