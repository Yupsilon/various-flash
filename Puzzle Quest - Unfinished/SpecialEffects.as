﻿package 
{

	public class SpecialEffects
	{
		import flash.text.*;
		import flash.display.*;
		import flash.geom.*;
		import flash.filters.*;

		public static var EffectsProjectiles:Array = [
		//Classname, Initial Width/Height, Animation Data
		[
		SpecialEffectWarAxeProjectile,
		[80,80],
		[
		["Birth","Death","MoveSpeed"],
		[1,10,50]
		]
		],
		[
		SpecialEffectGemSparkle,
		[2,2],
		[
		["Birth","Death","MoveSpeed"],
		[6,11,20]
		]
		],
		[
		SpecialEffectStonethrow,
		[90,90],
		[
		["Birth","Death","MoveSpeed"],
		[6,16,30]
		]
		],
		[
		SpecialEffectDarkBolt,
		[40,40],
		[
		["Birth","Death","MoveSpeed"],
		[5,16,30]
		]
		],
		]
		public static var EffectsChannels:Array = [
		//Classname, Initial Width/Height, Animation Data
		[
		SpecialEffectWarAxeChannel,
		[80,80],
		[
		["Birth","Death"],
		[6,8]
		]
		],
		[
		SpecialEffectRedChannel,
		[50,50],
		[
		["Birth","Death"],
		[4,6]
		]
		],
		[
		SpecialEffectBlueChannel,
		[50,50],
		[
		["Birth","Death"],
		[4,6]
		]
		],
		[
		SpecialEffectGreenChannel,
		[50,50],
		[
		["Birth","Death"],
		[4,6]
		]
		],
		[
		SpecialEffectYellowChannel,
		[50,50],
		[
		["Birth","Death"],
		[4,6]
		]
		],
		[
		SpecialEffectNeutralChannel,
		[50,50],
		[
		["Birth","Death"],
		[4,6]
		]
		],
		]
		public static var EffectsExplosions:Array = [
		//Classname, Initial Width/Height, Animation Data 
		[
		SpecialEffectGemPop,//0
		[50,50],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectElectricity,//1
		[50,50],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectExplosion,//2
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectHeal,//3
		[50,50],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectPoison,//4
		[50,50],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectFireBlast,//5
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectBash,//6
		[25,25],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectIceStar,//7
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectGust,//8
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectRage,//9
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectSound,//10
		[60,60],
		[
		["Death"],
		[1]
		]
		],
		[
		CastEffectFire,//11
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		CastEffectWater,//12
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		CastEffectEarth,//13
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		CastEffectWind,//14
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		CastEffectHeal,//15
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectFireElemental,//16
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectWaterElemental,//17
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectEarthElemental,//18
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectWindElemental,//19
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectEarthBlast,//20
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectFireBall,//21
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		[
		SpecialEffectIceExplosion,//22
		[10,10],
		[
		["Death"],
		[1]
		]
		],
		]

		public static function MakeSpecialEffectChannel(game:HubWorldHandler,EFFECTTARGET:DisplayItemArmy,PARENT:Object,TYPE:Number,ROTATION:Number=0,SCALE:Number=1):MovieClip
		{
			var X = 0;
			var Y = 0;

			var Facing = 1;
			if (EFFECTTARGET.PlayerOwner == 1)
			{
				Facing = -1;
			}

			var EFKT=new EffectsChannels[TYPE][0]();
			PARENT.addChild(EFKT);
			EFKT.target = InGameButtons.GetArmyDisplay(game.Memory["game"],EFFECTTARGET.PlayerOwner,EFFECTTARGET.ID);
			if (EFKT.target != null)
			{
				EFKT.x = EFKT.target.x;
				EFKT.y = EFKT.target.y;
			}
			else
			{
				EFKT.x = -100;
				EFKT.y = -100;
			}
			EFKT.spin = ROTATION;
			EFKT.width = EffectsChannels[TYPE][1][0] * SCALE;
			EFKT.height = EffectsChannels[TYPE][1][1] * SCALE;
			EFKT.animation = "Birth";
			EFKT.F=Facing;
			EFKT.T = "Channel";
			EFKT.animationdata = EffectsChannels[TYPE][2]
			EFKT.gotoAndStop(1);
			game.Memory["Effects"].push(EFKT);
			return EFKT;
		}

		public static function MakeSpecialEffectExplosionOnArmy(game:MainGame,TYPE:Number,SCALE:Number,Army:DisplayItemArmy)
		{

			var ArmyDisplay = InGameButtons.GetArmyDisplay(game,Army.PlayerOwner,Army.ID);

			if (ArmyDisplay!=null)
			{
				MakeSpecialEffectExplosion(game.Memory["HWH"],game.Memory["EffectLayer"],TYPE,SCALE,ArmyDisplay.x+ArmyDisplay.parent.x,ArmyDisplay.y+ArmyDisplay.parent.y);
			}
		}

		public static function MakeSpecialEffectExplosion(game:HubWorldHandler,PARENT:Object,TYPE:Number,SCALE:Number,X:Number,Y:Number):MovieClip
		{
			var EFKT=new EffectsExplosions[TYPE][0]();
			PARENT.addChild(EFKT);
			EFKT.x = X;
			EFKT.y = Y;
			EFKT.width = EffectsExplosions[TYPE][1][0] * SCALE;
			EFKT.height = EffectsExplosions[TYPE][1][1] * SCALE;
			EFKT.animation = "Death";
			EFKT.T = "Explode";
			EFKT.animationdata = EffectsExplosions[TYPE][2]
			EFKT.gotoAndStop(1);
			game.Memory["Effects"].push(EFKT);
			return EFKT;
		}

		public static function GenerateProjectileArmyArmy(game:HubWorldHandler,PARENT:Object,TYPE:Number,ArmyShooter:DisplayItemArmy,ArmyTarget:DisplayItemArmy,Damage:Number,SCALE:Number)
		{
			var Caster = InGameButtons.GetArmyDisplay(game.Memory["game"],ArmyShooter.PlayerOwner,ArmyShooter.ID);
			var Target = InGameButtons.GetArmyDisplay(game.Memory["game"],ArmyTarget.PlayerOwner,ArmyTarget.ID);

			var Facing = 1;
			if (ArmyShooter.PlayerOwner == 1)
			{
				Facing = -1;
			}

			if (Caster!=null && Target!=null)
			{
				//trace("F "+Facing);
				var EFKT=new EffectsProjectiles[TYPE][0]();
				PARENT.addChild(EFKT);
				EFKT.x = Caster.x + Caster.parent.x - 80 * Facing;
				EFKT.y = Caster.y + Caster.parent.y;
				EFKT.width = EffectsProjectiles[TYPE][1][0] * SCALE;
				EFKT.height = EffectsProjectiles[TYPE][1][1] * SCALE;
				EFKT.scaleX *=  Facing;
				EFKT.animation = "Birth";
				EFKT.T = "Projectile";
				EFKT.damagedata = [Damage,ArmyTarget.PlayerOwner,ArmyTarget.ID]
				EFKT.animationdata = EffectsProjectiles[TYPE][2]
				EFKT.dx = Target.x + Target.parent.x;
				EFKT.dy = Target.y + Target.parent.y;

				EFKT.damage = Damage;
				EFKT.gotoAndStop(1);
				game.Memory["Effects"].push(EFKT);
			}

		}

		public static function GenerateParticleProjectileArmyArmy(game:HubWorldHandler,PARENT:Object,TYPE:Number,EXPLODETYPE:Number,ArmyShooter:DisplayItemArmy,ArmyTarget:DisplayItemArmy,Damage:Number,Interval:Number,SCALE:Number)
		{
			var Caster = InGameButtons.GetArmyDisplay(game.Memory["game"],ArmyShooter.PlayerOwner,ArmyShooter.ID,true);
			var Target = InGameButtons.GetArmyDisplay(game.Memory["game"],ArmyTarget.PlayerOwner,ArmyTarget.ID,true);

			var Facing = 1;
			if (ArmyShooter.PlayerOwner == 1)
			{
				Facing = -1;
			}

			if (Caster!=null && Target!=null)
			{

				var EFKT=new MovieClip();
				PARENT.addChild(EFKT);
				EFKT.x = Caster.x + Caster.parent.x - 80;
				EFKT.y = Caster.y + Caster.parent.y;
				EFKT.scale = SCALE;
				EFKT.T = "Particle Projectile";
				EFKT.animation = "Stand";
				EFKT.damagedata = [Damage,ArmyTarget.PlayerOwner,ArmyTarget.ID]
				EFKT.particle = TYPE;
				EFKT.dx = Target.x + Target.parent.x;
				EFKT.dy = Target.y + Target.parent.y;
				EFKT.tT = 0;
				EFKT.tM = Interval;
				EFKT.tEX = EXPLODETYPE;
				EFKT.gotoAndStop(1);
				game.Memory["Effects"].push(EFKT);
				EFKT.damage = Damage;
				EFKT.gotoAndStop(1);
				game.Memory["Effects"].push(EFKT);
			}
		}

		public static function MakeGemManaParticles(game:HubWorldHandler,PARENT:Object,PLAYER:Number,COLOR:Number,CASTER:Object,MANA:Number)
		{
			var X = 10;
			if (PLAYER==1)
			{
				X=GlobalVariables.stageWidth-(GlobalVariables.stageWidth-GlobalVariables.stageHeight)/2-25;
			}
			for (var Zim=0; Zim<10; Zim++)
			{
				if (PLAYER>=0)
				{
					var tX = Math.random() * GlobalVariables.GemSize - GlobalVariables.GemSize / 2;
					var tY = Math.random() * GlobalVariables.GemSize - GlobalVariables.GemSize / 2;
					var KOL = new ColorTransform();
					var Colorz = [0x000000,0xFF0000,0x18D4ED,0x55FF55,0xFFFF55,0xCCCCCC,0x9900FF]
					KOL.color = Colorz[COLOR]					
					
					var Pro:MovieClip = MakeSpecialEffectProjectile(game,PARENT,1,1 - Math.random() * 0.9,CASTER.x + CASTER.parent.x + tX,CASTER.y + CASTER.parent.y + tY,X + 30 * COLOR - 30 + 20,50,1);
					Pro.transform.colorTransform = KOL;
					Pro.alpha = 0.8;
					if (MANA!=0)
					{
						if (MANA==0.3333333333333333)
						{
							MANA = 0.3333333333333334;
						}
						Pro.manadata = [PLAYER,MANA,COLOR - 1]
						MANA = 0;
					}
				}
			}
		}

		public static function MakeGemExplosion(game:HubWorldHandler,PARENT:Object,COLOR:Number,CASTER:Object)
		{
			for (var Zim=0; Zim<10; Zim++)
			{
				var tX = Math.random() * GlobalVariables.GemSize - GlobalVariables.GemSize / 2;
				var tY = Math.random() * GlobalVariables.GemSize - GlobalVariables.GemSize / 2;

				var Exp = MakeSpecialEffectExplosion(game,PARENT,0,0.4 + 0.4 * Math.random(),CASTER.x + CASTER.parent.x + tX,CASTER.y + CASTER.parent.y + tY);
				var KOL = new ColorTransform();
				var Colorz = [0x000000,0xFF0000,0x18D4ED,0x55FF55,0xFFFF55,0xCCCCCC,0x9900FF]
				KOL.color = Colorz[COLOR]
				Exp.transform.colorTransform = KOL;
				Exp.rotation = Math.random() * 360;
				Exp.alpha=0.8
			}
		}

		public static function MakeManaParticles(game:HubWorldHandler,COLOR:Number,OWNER:Number)
		{
			var oX = 10;
			if (OWNER==1)
			{
				oX+=GlobalVariables.stageWidth-(GlobalVariables.stageWidth-GlobalVariables.stageHeight)*1.2/2;
			}
			for (var Zim=0; Zim<10; Zim++)
			{
				var X=oX+30*COLOR+20+Math.sin(36*Zim)*10;
				var Y=50+Math.cos(36*Zim)*10;
				var Exp = MakeSpecialEffectExplosion(game,game.Memory["game"].Memory["EffectLayer"],0,0.4,X,Y);
				var KOL = new ColorTransform();
				var Colorz = [0xFF0000,0x18D4ED,0x55FF55,0xFFFF55,0xCCCCCC,0x9900FF]
				KOL.color = Colorz[COLOR]
				Exp.transform.colorTransform = KOL;
				Exp.rotation = Math.random() * 360;
				Exp.alpha = 0.7 * Math.random() + 0.3;
			}
		}

		public static function MakeSpecialEffectProjectile(game:HubWorldHandler,PARENT:Object,TYPE:Number,SCALE:Number,X:Number,Y:Number,tX:Number,tY:Number,Facing:Number):MovieClip
		{

			var EFKT=new EffectsProjectiles[TYPE][0]();
			PARENT.addChild(EFKT);
			EFKT.x = X;
			EFKT.y = Y;
			EFKT.width = EffectsProjectiles[TYPE][1][0] * SCALE * Facing;
			EFKT.height = EffectsProjectiles[TYPE][1][1] * SCALE;
			EFKT.animation = "Birth";
			EFKT.T = "Projectile";
			EFKT.animationdata = EffectsProjectiles[TYPE][2]
			EFKT.dx = tX;
			EFKT.dy = tY;
			EFKT.gotoAndStop(1);
			game.Memory["Effects"].push(EFKT);
			return EFKT;
		}

		public static function MakeSpecialEffectParticleProjectile(game:HubWorldHandler,PARENT:Object,TYPE:Number,SCALE:Number,X:Number,Y:Number,tX:Number,tY:Number,Interval:Number):MovieClip
		{
			var EFKT=new MovieClip();
			PARENT.addChild(EFKT);
			EFKT.x = X;
			EFKT.y = Y;
			EFKT.scale = SCALE;
			EFKT.T = "Particle Projectile";
			EFKT.animation = "Stand";
			EFKT.particle = TYPE;
			EFKT.dx = tX;
			EFKT.dy = tY;
			EFKT.tT = 0;
			EFKT.tM = Interval;
			EFKT.gotoAndStop(1);
			game.Memory["Effects"].push(EFKT);
			return EFKT;
		}

		public static function HandleSpecialEffects(game:HubWorldHandler)
		{
			for each (var Zim in game.Memory["Effects"])
			{
				if (Zim!=null)
				{
					Zim.gotoAndStop(Zim.currentFrame+1);
					if (Zim.T == "Channel" && Zim.target != null)
					{
						Zim.x = Zim.target.x - 80*Zim.F;
						Zim.y = Zim.target.y;
					}
					else if (Zim.T=="Channel" && Zim.target==null)
					{
						Zim.x = -100;
						Zim.y = -100;
					}
					if (Zim.animation == "Birth")
					{
						if (Zim.animationdata[1][Zim.animationdata[0].indexOf("Birth")] <= Zim.currentFrame)
						{
							Zim.animation = "Stand";
						}
					}
					else if (Zim.animation=="Stand")
					{
						if (Zim.spin > 0 || Zim.spin < 0)
						{
							Zim.rotation +=  Zim.spin;
						}


						if (Zim.T == "Effect Text")
						{
							Zim.tT++;
							Zim.y = Zim.y - 1 / 2;
							if (Zim.tT >= Zim.tM)
							{
								Zim.animation = "Death";
							}
						}
						else if (Zim.T!="Particle Projectile" && Zim.animationdata[1][Zim.animationdata[0].indexOf("Death")]<=Zim.currentFrame)
						{
							if (Zim.T == "Channel" || Zim.T == "Projectile")
							{
								Zim.gotoAndStop(Zim.animationdata[1][Zim.animationdata[0].indexOf("Birth")]);
							}
							else
							{
								Zim.animation = "Death";
							}
						}
						if (Zim.T == "Projectile" || Zim.T == "Particle Projectile")
						{
							if (Zim.y != Zim.dy || Zim.x != Zim.dx)
							{

								var ang=Math.atan2((Zim.dy-Zim.y),(Zim.dx-Zim.x));
								var MoveSpeed = 8;
								if (Zim.animationdata != null)
								{
									MoveSpeed = Zim.animationdata[1][Zim.animationdata[0].indexOf("MoveSpeed")]
								}

								if (Zim.y < Zim.dy)
								{
									Zim.y = Math.min(Zim.dy,Zim.y + MoveSpeed * Math.sin(ang));
								}
								if (Zim.y > Zim.dy)
								{
									Zim.y = Math.max(Zim.dy,Zim.y + MoveSpeed * Math.sin(ang));
								}

								if (Zim.x < Zim.dx)
								{
									Zim.x = Math.min(Zim.dx,Zim.x + MoveSpeed * Math.cos(ang));
								}
								if (Zim.x > Zim.dx)
								{
									Zim.x = Math.max(Zim.dx,Zim.x + MoveSpeed * Math.cos(ang));
								}
							}
							else
							{
								Zim.animation="Death";
							}
						}
						if (Zim.T == "Particle Projectile")
						{
							Zim.tT++;
							if (Zim.tT >= Zim.tM)
							{
								Zim.tT = 0;
								MakeSpecialEffectExplosion(game,Zim.parent,Zim.particle,Zim.scale,Zim.x,Zim.y);
							}
						}
					}
					else if (Zim.animation=="Death")
					{
						if (Zim.spin > 0 || Zim.spin < 0)
						{
							Zim.rotation +=  Zim.spin;
						}
						if (Zim.tEX!=null)
						{
							MakeSpecialEffectExplosion(game,Zim.parent,Zim.tEX,1,Zim.x,Zim.y)
							Zim.tEX=null;
						}
						if (Zim.T != "Effect Text" && Zim.T != "Particle Projectile" && Zim.animationdata[1][Zim.animationdata[0].indexOf("Death")] >= Zim.currentFrame)
						{
							Zim.gotoAndStop(Zim.animationdata[1][Zim.animationdata[0].indexOf("Death")]);
						}
						else if (Zim.T=="Particle Projectile" || Zim.currentFrame==Zim.totalFrames || Zim.T=="Effect Text")
						{
							Zim.alpha -=  0.1;
							if (Zim.alpha <= 0)
							{
								Destroy(game,Zim);
							}
							if (Zim.T == "Effect Text")
							{
								Zim.y--;
							}														
						}
						if (Zim.damagedata != null && Zim.damagedata != undefined)
						{
							game.Memory["game"].Memory["Display"]["PlayerLife"][Zim.damagedata[1]][Zim.damagedata[2]] -=  Zim.damagedata[0]
							Zim.damagedata = null;
						}
						if (Zim.manadata != null && Zim.manadata != undefined)
						{
							HandleMana.UpdateManaAt(game.Memory["game"],Zim.manadata[0],Zim.manadata[1],Zim.manadata[2]);
							Zim.manadata = null;
						}
					}
				}
			}
		}

		public static function Destroy(game:HubWorldHandler,Zim)
		{
			if (Zim.parent != null)
			{
				Zim.parent.removeChild(Zim);
			}
			game.Memory["Effects"].splice(game.Memory["Effects"].indexOf(Zim),1);
		}

		public static function AddSpecialEffectText(game:HubWorldHandler,PARENT:MovieClip,X:Number,Y:Number,TEXT:String,dY:Number):MovieClip
		{
			var DIAGTEXTY:TextField= new TextField();
			DIAGTEXTY.width = 120;
			DIAGTEXTY.text = TEXT;
			DIAGTEXTY.wordWrap = true;
			DIAGTEXTY.setTextFormat(GlobalVariables.TextPOPUPEFXBG);
			DIAGTEXTY.x = 0 - DIAGTEXTY.width / 2 - 1;
			DIAGTEXTY.y = 0 - 6;

			var EFKTW=new MovieClip();
			PARENT.addChild(EFKTW);
			EFKTW.x = X;
			EFKTW.y = Y - dY;
			EFKTW.dx = X;
			EFKTW.dy = Y - dY;
			EFKTW.alpha = 1;
			EFKTW.T = "Effect Text";
			EFKTW.animation = "Stand";
			game.Memory["Effects"].push(EFKTW);
			EFKTW.tT = 0;
			EFKTW.tM = 80;
			EFKTW.filters = [new BlurFilter(3,3)]
			EFKTW.addChild(DIAGTEXTY);

			var DIAGTEXTER= new TextField();
			DIAGTEXTER.width = 120;
			DIAGTEXTER.text = TEXT;
			DIAGTEXTER.wordWrap = true;
			DIAGTEXTER.setTextFormat(GlobalVariables.TextPOPUPEFXBG);
			DIAGTEXTER.x = 0 - DIAGTEXTER.width / 2 - 1;
			DIAGTEXTER.y = 0 - 6;

			var EFKTW=new MovieClip();
			PARENT.addChild(EFKTW);
			EFKTW.x = X;
			EFKTW.y = Y - dY;
			EFKTW.dx = X;
			EFKTW.dy = Y - dY;
			EFKTW.alpha = 1;
			EFKTW.T = "Effect Text";
			EFKTW.animation = "Stand";
			game.Memory["Effects"].push(EFKTW);
			EFKTW.tT = 0;
			EFKTW.tM = 80;
			EFKTW.filters = [new BlurFilter(3,3)]
			EFKTW.addChild(DIAGTEXTER);

			var DIAGTEXT:TextField= new TextField();
			DIAGTEXT.text = TEXT;
			DIAGTEXT.width = 120;
			DIAGTEXT.wordWrap = true;
			DIAGTEXT.setTextFormat(GlobalVariables.TextPOPUPEFX);
			DIAGTEXT.x = 0 - DIAGTEXT.width / 2;
			DIAGTEXT.y = 0 - 5;

			var EFKT=new MovieClip();
			PARENT.addChild(EFKT);
			EFKT.x = X;
			EFKT.y = Y - dY;
			EFKT.dx = X;
			EFKT.dy = Y - dY;
			EFKT.alpha = 1;
			EFKT.T = "Effect Text";
			EFKT.animation = "Stand";
			game.Memory["Effects"].push(EFKT);
			EFKT.tT = 0;
			EFKT.tM = 80;
			EFKT.BG = EFKTW;
			EFKT.addChild(DIAGTEXT);

			return EFKT;
		}

		public static function MakeSpecialEffectTextOnArmy(game:MainGame,TEXT:String,Army:DisplayItemArmy,Y:Number):MovieClip
		{
			var ArmyDisplay = InGameButtons.GetArmyDisplay(game,Army.PlayerOwner,Army.ID);

			if (ArmyDisplay==null)
			{
				return null;
			}
			else
			{
				return AddSpecialEffectText(game.Memory["HWH"],game.Memory["EffectLayer"],ArmyDisplay.x+ArmyDisplay.parent.x,ArmyDisplay.y+ArmyDisplay.parent.y,TEXT,Y);
			}
		}
	}

}