﻿package  {
	import flash.display.*;
	import flash.geom.*;
	import flash.events.*;	
	import flash.utils.*;
	import flash.text.TextField;
	
	public class HubWorld {

		public static function Draw(game:HubWorldHandler) {
			
			
			var HubWorld:MovieClip=new MovieClip();
			game.Memory["Stage"].addChild(HubWorld);
			HubWorld.x=0;
			HubWorld.y=0;	
			game.Memory["HubWorld"]=HubWorld;			
			
			var Karte:MovieClip=new MovieClip();
			Karte.x=0;
			Karte.y=0;
			HubWorld.addChild(Karte);
			game.Memory["HubWorld"]["WorldMap"]=Karte;
			
			var World= new Bitmap(new WorldMap(1, 1));
			Karte.addChild(World);
			World.width=MapData.MapWidth+2;
			World.height=MapData.MapHeight+2;
			World.x=0//Karte.width/2;
			World.y=0//Karte.height/2;
			
			var SideOverlay:HubWorldSideUI=new HubWorldSideUI();
			HubWorld.addChild(SideOverlay);
			SideOverlay.width=250;
			SideOverlay.height=GlobalVariables.stageHeight;
			SideOverlay.x=GlobalVariables.stageWidth-SideOverlay.width/2;
			SideOverlay.y=GlobalVariables.stageHeight/2;
			
			game.Memory["HubWorld"]["WorldMap"]["Locations"]=new Array();
			
			for (var Zim=0; Zim<MapData.MapLocationData.length; Zim++)
			{
				var Location=MapData.MapLocationData[Zim];
				
				var This=new Location[5]();
				This.x=Math.min(MapData.MapWidth,Math.max(Location[2],0))				
				This.y=Math.min(MapData.MapHeight,Math.max(Location[3],0))	
				This.scaleX*=3/4
				This.scaleY*=3/4
				This.ID=Zim
				This.Picture=Location[4]
				This.Conditions=Location[6]
				This.Quests=Location[7]
				game.Memory["HubWorld"]["WorldMap"]["Locations"].push(This);
				Karte.addChild(This);
				
				UpdateLocation(game,Zim);
			}
			ChangeLocation(game,game.Memory["Data"].PlayerData[0]);
		}

		public static function ChangeLocation(game,Location:Number)
		{
			game.Memory["Data"].PlayerData[0]=Location;
			CenterMap(game,game.Memory["HubWorld"]["WorldMap"]["Locations"][Location].x,game.Memory["HubWorld"]["WorldMap"]["Locations"][Location].y)						
			DrawCityOverlay(game);
		}

		public static function DrawCityOverlay(game:HubWorldHandler)
		{
			game.Memory["Buttons"]=new Array();
			
			var Zim=game.Memory["HubWorld"]["WorldMap"]["Locations"][game.Memory["Data"].PlayerData[0]];
			var HubWorld=Zim.parent.parent;
			
			if (HubWorld.Overlay!=null)
			{
			HubWorld.removeChild(HubWorld.Overlay);
			}
			
			var Overlay=new MovieClip();
			game.Memory["HubWorld"].addChild(Overlay)
			HubWorld.Overlay=Overlay;
			
			if (Zim!=null)
			{
				var Pic=new Zim.Picture();
				Overlay.addChild(Pic)
				Pic.x=GlobalVariables.stageWidth-125;
				Pic.y=65;
				Pic.width=85;
				Pic.height=85;
				
				var tempText:TextField=new TextField()	
				Overlay.addChild(tempText);
				tempText.text=MapData.MapLocationData[Zim.ID][0]+"\n"+MapData.MapLocationData[Zim.ID][1]
				tempText.width=200;	
				tempText.height=60;
				tempText.wordWrap=true;	
				tempText.x=GlobalVariables.stageWidth-125-tempText.width/2;
				tempText.y=125;
				tempText.alpha=1;
				tempText.setTextFormat(GlobalVariables.textHUBWORLD);					
				
				
							var GoBTN=new HubWorldUIBTN();
							Overlay.addChild(GoBTN)
							GoBTN.x=GlobalVariables.stageWidth-125;
							GoBTN.y=210;
							GoBTN.width=180;
							GoBTN.height=25;
							GoBTN.data="GoTo"
							game.Memory["Buttons"].push(GoBTN);
				
							var BTNtext:TextField=new TextField()	
							Overlay.addChild(BTNtext);
							BTNtext.text="Center"
							BTNtext.width=200;	
							tempText.height=60;
							BTNtext.wordWrap=true;	
							BTNtext.x=GoBTN.x-BTNtext.width/2;
							BTNtext.y=GoBTN.y-GoBTN.height/2;
							BTNtext.setTextFormat(GlobalVariables.textHUBWORLD);	
			
					var xINT=1;
				for (var Gir=0; Gir<Zim.Quests.length; Gir++)
				{
					if (xINT>5)
					{
						break;
					}
					var Quest=Zim.Quests[Gir];
										
						if (MapData.DoIMeetConditions(game,Quest[2]) && 
							(MapData.GetCombinedPartyLevel(game)>Quest[3][0] || Quest[3][0]==-1) && 
							(MapData.GetCombinedPartyLevel(game)<Quest[3][1] || Quest[3][1]==-1) && 
							!((Quest[1]=="Battle" || Quest[1]=="RandomBattle") && Quest[4].length==0 && game.Memory["Data"].Armies.length==0))
						{
							var HubBTN=new HubWorldUIBTN();
							Overlay.addChild(HubBTN)
							HubBTN.x=GlobalVariables.stageWidth-125;
							HubBTN.y=210+xINT*27;
							HubBTN.width=180;
							HubBTN.height=25;
							HubBTN.data="Action";
							HubBTN.action=Quest;
							xINT++;
							game.Memory["Buttons"].push(HubBTN);
				
							var BTNText:TextField=new TextField()	
							Overlay.addChild(BTNText);
							BTNText.text=Quest[0]
							BTNText.width=200;	
							BTNText.height=60;
							BTNText.wordWrap=true;	
							BTNText.x=HubBTN.x-BTNText.width/2;
							BTNText.y=HubBTN.y-HubBTN.height/2;
							BTNText.setTextFormat(GlobalVariables.textHUBWORLD);	
						}
				}		
				
				var Actions=["mute","save","quit"];
				for (Gir=0; Gir<3; Gir++)
				{
					
							var BTNsmall=new MuteButton();
							Overlay.addChild(BTNsmall)
							BTNsmall.x=GlobalVariables.stageWidth-250+60+65*Gir;
							BTNsmall.y=375;
							BTNsmall.width=30;
							BTNsmall.height=30;
							BTNsmall.gotoAndStop(Gir+1);
							BTNsmall.data=Actions[Gir];
							game.Memory["Buttons"].push(BTNsmall);
				}		
			}
		}

		public static function CenterMap(game:HubWorldHandler,X:Number,Y:Number)
		{
			game.Memory["HubWorld"]["WorldMap"].x=Math.max(0-MapData.MapWidth+(GlobalVariables.stageWidth-250),Math.min((GlobalVariables.stageWidth-250)/2-X,0))
			game.Memory["HubWorld"]["WorldMap"].y=Math.max(0-MapData.MapHeight+(GlobalVariables.stageHeight),Math.min((GlobalVariables.stageHeight)/2-Y,0))			
		}

		public static function UpdateLocation(game:HubWorldHandler,ID:Number)
		{
			var Zim=game.Memory["HubWorld"]["WorldMap"]["Locations"][ID];
			if (Zim!=null)
			{				
				if (MapData.DoIMeetConditions(game,Zim.Conditions))
				{
					Zim.alpha=1;
					Zim.active=true;
				}
				else
				{
					Zim.alpha=0;
					Zim.active=false;
				}
			}
		}
		
		public static function MoveTheMap(game:HubWorldHandler,X:Number,Y:Number)
		{
			game.Memory["HubWorld"]["WorldMap"].x=Math.max(0-MapData.MapWidth+(GlobalVariables.stageWidth-250),
								Math.min(game.Memory["HubWorld"]["WorldMap"].x+X*GlobalVariables.MapMoveSpeed,0))
			game.Memory["HubWorld"]["WorldMap"].y=Math.max(0-MapData.MapHeight+(GlobalVariables.stageHeight),
								Math.min(game.Memory["HubWorld"]["WorldMap"].y+Y*GlobalVariables.MapMoveSpeed,0))			
		}
		
		public static function Destroy(game)
		{
			game.Memory["Stage"].removeEventListener(Event.ENTER_FRAME,game.CameraBorders);
			game.Memory["HubWorld"].numChildren=0;
			game.Memory["HubWorld"].parent.removeChild(game.Memory["HubWorld"]);
			game.Memory["HubWorld"]["WorldMap"]["Locations"]=null;
			game.map=null;
		}		
		
		public static function GetMouseLocation(game:HubWorldHandler,event:MouseEvent):Number
		{
			var Nummer=-1;
			var InDist=-1;
			for (var Zim=0; Zim<game.Memory["HubWorld"]["WorldMap"]["Locations"].length; Zim++)
			{
				var X=event.stageX-game.Memory["HubWorld"]["WorldMap"].x;
				var Y=event.stageY-game.Memory["HubWorld"]["WorldMap"].y;
				var Dist=Math.sqrt((game.Memory["HubWorld"]["WorldMap"]["Locations"][Zim].x-X)*(game.Memory["HubWorld"]["WorldMap"]["Locations"][Zim].x-X)+
								   (game.Memory["HubWorld"]["WorldMap"]["Locations"][Zim].y-Y)*(game.Memory["HubWorld"]["WorldMap"]["Locations"][Zim].y-Y));
								   if ((Dist<InDist || InDist==-1) && Dist<game.Memory["HubWorld"]["WorldMap"]["Locations"][Zim].width)
								   {
									   Nummer=Zim;
									   InDist=Dist;
								   }
			}
			return Nummer;
		}
		
		public static function UpdateVictoryScreen(game:HubWorldHandler) {
			if (game.Memory["HubWorld"]!=null && game.Memory["HubWorld"]["InfoUI"]!=null && game.Memory["HubWorld"]["InfoUI"].done==false)
			{
				var T=game.Memory["GameTime"].GetTime();
				
				trace(game.Memory["HubWorld"]["InfoUI"].TextTBA.length>0 , game.Memory["HubWorld"]["InfoUI"].updateT<T)
												
				if (game.Memory["HubWorld"]["InfoUI"].currentGOLD<game.Memory["HubWorld"]["InfoUI"].targetGOLD)
				{					
					game.Memory["HubWorld"]["InfoUI"].currentGOLD=Math.round(Math.max(1,Math.min(game.Memory["HubWorld"]["InfoUI"].currentGOLD+game.Memory["HubWorld"]["InfoUI"].targetGOLD/25,game.Memory["HubWorld"]["InfoUI"].targetGOLD)));
					game.Memory["HubWorld"]["InfoUI"].GOLDtext.text=game.Memory["HubWorld"]["InfoUI"].currentGOLD
					game.Memory["HubWorld"]["InfoUI"].GOLDtext.setTextFormat(GlobalVariables.TextPOPUPEFX)		
				}
				else if (game.Memory["HubWorld"]["InfoUI"].currentXP<game.Memory["HubWorld"]["InfoUI"].targetXP)
				{					
					game.Memory["HubWorld"]["InfoUI"].currentXP=Math.round(Math.max(1,Math.min(game.Memory["HubWorld"]["InfoUI"].currentXP+game.Memory["HubWorld"]["InfoUI"].targetXP/25,game.Memory["HubWorld"]["InfoUI"].targetXP)));
					game.Memory["HubWorld"]["InfoUI"].XPtext.text=game.Memory["HubWorld"]["InfoUI"].currentXP
					game.Memory["HubWorld"]["InfoUI"].XPtext.setTextFormat(GlobalVariables.TextPOPUPEFX)		
				}
				else if (game.Memory["HubWorld"]["InfoUI"].TextTBA.length>0 && game.Memory["HubWorld"]["InfoUI"].updateT<T)
				{
					game.Memory["HubWorld"]["InfoUI"].endTXT.text=game.Memory["HubWorld"]["InfoUI"].endTXT.text+game.Memory["HubWorld"]["InfoUI"].TextTBA[0]
					game.Memory["HubWorld"]["InfoUI"].TextTBA.splice(0,1);
					game.Memory["HubWorld"]["InfoUI"].endTXT.setTextFormat(GlobalVariables.TextLIFEBAR)
					game.Memory["HubWorld"]["InfoUI"].updateT=T+10;
				}
				else
				{
					game.Memory["HubWorld"]["InfoUI"].done=true
				}
			}
		}
		
		public static function UiButtons(game:HubWorldHandler,event:MouseEvent)
		{
			var T=game.Memory["GameTime"].GetTime()
			
			if (game.Memory["HubWorld"]["InfoUI"]!=null)
			{
				if (game.Memory["HubWorld"]["InfoUI"].done==true)
				{
					game.Memory["HubWorld"]["InfoUI"].text.parent.removeChild(game.Memory["HubWorld"]["InfoUI"].text);
					game.Memory["HubWorld"]["InfoUI"].GOLDtext.parent.removeChild(game.Memory["HubWorld"]["InfoUI"].GOLDtext);
					game.Memory["HubWorld"]["InfoUI"].XPtext.parent.removeChild(game.Memory["HubWorld"]["InfoUI"].XPtext);
					game.Memory["HubWorld"]["InfoUI"].endTXT.parent.removeChild(game.Memory["HubWorld"]["InfoUI"].endTXT);
					game.Memory["HubWorld"]["InfoUI"].parent.removeChild(game.Memory["HubWorld"]["InfoUI"]);
					game.Memory["HubWorld"]["InfoUI"]=null;
					game.Memory["GameTime"].ClickTimer=T+2;
				}
				else  if (game.Memory["HubWorld"]["InfoUI"].done==false)
				{
					game.Memory["HubWorld"]["InfoUI"].GOLDtext.text=game.Memory["HubWorld"]["InfoUI"].targetGOLD
					game.Memory["HubWorld"]["InfoUI"].GOLDtext.setTextFormat(GlobalVariables.TextPOPUPEFX)					
					game.Memory["HubWorld"]["InfoUI"].XPtext.text=Math.round(game.Memory["HubWorld"]["InfoUI"].targetXP)
					game.Memory["HubWorld"]["InfoUI"].XPtext.setTextFormat(GlobalVariables.TextPOPUPEFX)
					game.Memory["HubWorld"]["InfoUI"].endTXT.text=game.Memory["HubWorld"]["InfoUI"].endTXT.text+game.Memory["HubWorld"]["InfoUI"].TextTBA
					game.Memory["HubWorld"]["InfoUI"].endTXT.setTextFormat(GlobalVariables.TextLIFEBAR)
					game.Memory["HubWorld"]["InfoUI"].done=true
				}
				else
				{
					game.Memory["HubWorld"]["InfoUI"].text.parent.removeChild(game.Memory["HubWorld"]["InfoUI"].text);
					game.Memory["HubWorld"]["InfoUI"].parent.removeChild(game.Memory["HubWorld"]["InfoUI"]);
					game.Memory["HubWorld"]["InfoUI"]=null;
					game.Memory["GameTime"].ClickTimer=T+2;
				}
			}
				else
				{
		if (event.stageX<GlobalVariables.stageWidth-250 && game.Memory["HubWorld"]["ShopUI"]==null)
		{
			var LOC=GetMouseLocation(game,event)
			if (LOC!=-1)
			{
				ChangeLocation(game,LOC);
			}
			game.Memory["GameTime"].ClickTimer=T+2;
		}
		else 
		{
		var X=event.stageX
		var Y=event.stageY
			for (var b=0; b<game.Memory["Buttons"].length; b++)
			{	
				if (game.Memory["Buttons"][b]!=null && game.Memory["Buttons"][b].parent!=null && T>=game.Memory["GameTime"].ClickTimer)
				{
					if (X>=game.Memory["Buttons"][b].x+game.Memory["Buttons"][b].parent.x-game.Memory["Buttons"][b].width/2 && X<=game.Memory["Buttons"][b].x+game.Memory["Buttons"][b].parent.x+game.Memory["Buttons"][b].width/2)
					{
						if (Y>=game.Memory["Buttons"][b].y+game.Memory["Buttons"][b].parent.y-game.Memory["Buttons"][b].height/2 && Y<=game.Memory["Buttons"][b].y+game.Memory["Buttons"][b].parent.y+game.Memory["Buttons"][b].height/2)
						{			
							if (game.Memory["Buttons"][b].data=="save")
							{
								game.Memory["Data"].saveData();
								game.Memory["GameTime"].ClickTimer=T+2;
								DisplayInfoScreen(game,Language.VictoryScreen[9]);
								break;
							}			
							else if (game.Memory["Buttons"][b].data=="mute")
							{
								game.Memory["SoundManager"].MuteSounds();
								game.Memory["GameTime"].ClickTimer=T+2;
								break;
							}		
							else if (game.Memory["Buttons"][b].data=="quit")
							{
								game.Memory["Data"].saveData();
								Close(game);
								MainMenus.DrawMainMenu(game,false);
								game.Memory["GameTime"].ClickTimer=T+2;
								break;
							}		
							else if (game.Memory["Buttons"][b].data=="GoTo")
							{
								ClearShopScreen(game);
								CenterMap(game,game.Memory["HubWorld"]["WorldMap"]["Locations"][game.Memory["Data"].PlayerData[0]].x,game.Memory["HubWorld"]["WorldMap"]["Locations"][game.Memory["Data"].PlayerData[0]].y);
								game.Memory["GameTime"].ClickTimer=T+2;
								break;
							}			
							else if (game.Memory["Buttons"][b].data=="Action")
							{
								ClearShopScreen(game);
								if (game.Memory["Buttons"][b].action[1]=="Battle" || game.Memory["Buttons"][b].action[1]=="RandomBattle")
								{
									Close(game);
									DataMissions.MakeNewGame(game,game.Memory["Buttons"][b].action);
								}
								else if (game.Memory["Buttons"][b].action[1]=="Barracks")
								{
									Close(game);
									DataBarracks.DrawBarracksMenu(game);
								}
								else if (game.Memory["Buttons"][b].action[1]=="UnitStore")
								{
									DisplayShopScreen(game,game.Memory["Buttons"][b].action[4]);
								}
								else if (game.Memory["Buttons"][b].action[1]=="Info")
								{
										DisplayInfoScreen(game,game.Memory["Buttons"][b].action[4]);
								}
								else if (game.Memory["Buttons"][b].action[1]=="Pay")
								{
										DisplayPayScreen(game,game.Memory["Buttons"][b].action[4]);
								}
								game.Memory["GameTime"].ClickTimer=T+2;
								break;
							}		
							else if (game.Memory["Buttons"][b].data=="purchaseshop")
							{
								if (DataItem.PurchaseArmy(game,game.Memory["Buttons"][b].father.army,game.Memory["Buttons"][b].Multiplier)==true)
								{
									game.Memory["Buttons"][b].parent.removeChild(game.Memory["Buttons"][b])
								}
								game.Memory["GameTime"].ClickTimer=T+2;
								break;
							}	
							else if (game.Memory["Buttons"][b].data=="closeshop")
							{
								ClearShopScreen(game)
								game.Memory["GameTime"].ClickTimer=T+2;
								break;
							}		
								else if (game.Memory["Buttons"][b].data=="payconditions")
								{
									if (DataMissions.TakeGold(game,game.Memory["Buttons"][b].cost,false)==true)
									{
										game.Memory["Data"].updateProgress(game.Memory["Buttons"][b].conditions)
										Draw(game);
										ClearShopScreen(game)
										game.Memory["GameTime"].ClickTimer=T+2;
									}
									break;
								}												
							}
						}
					}
				}
			}
				}
		}
		
		public static function Close(game:HubWorldHandler)
		{
			game.Memory["HubWorld"].parent.removeChild(game.Memory["HubWorld"]);	
			game.Memory["HubWorld"]=null;
		}
		
		public static function CameraBorders(game:HubWorldHandler,X:Number,Y:Number)
		{
			if (game.Memory["HubWorld"]["ShopUI"]==null)
			{
				if (X<MapData.Border)
				{
					MoveTheMap(game,1,0)
				}
				else if (X>GlobalVariables.stageWidth-MapData.Border-250 && X<GlobalVariables.stageWidth-250)
				{
					MoveTheMap(game,-1,0)
				}
				if (Y<MapData.Border && X<GlobalVariables.stageWidth-250)
				{
					MoveTheMap(game,0,1)
				}
				else if (Y>GlobalVariables.stageHeight-MapData.Border && X<GlobalVariables.stageWidth-250)
				{
					MoveTheMap(game,0,-1)
				}
			}
		}
		
		public static function DisplayVictoryScreen(game:HubWorldHandler,RewardData:Array,ArmyData:Array)
		{
					var Zim:InfoOverlay=new InfoOverlay()
					game.Memory["HubWorld"].addChild(Zim);
					Zim.x=(GlobalVariables.stageWidth-250)/2;
					Zim.y=GlobalVariables.stageHeight/2;
					Zim.width=(GlobalVariables.stageWidth-250)*0.65;
					Zim.height=(GlobalVariables.stageHeight)*0.7;					
					game.Memory["HubWorld"]["InfoUI"]=Zim;
					
					var Level=MapData.GetCombinedPartyLevel(game);
					
					Zim.targetXP=Level*10*RewardData[1];
					Zim.targetGOLD=RewardData[0];
					Zim.currentXP=0;
					Zim.currentGOLD=0;
					Zim.updateT=0;
					Zim.done=false;
					
					var DIAGTEXT:TextField=new TextField()	
					game.Memory["HubWorld"].addChild(DIAGTEXT);
					DIAGTEXT.text=Language.VictoryScreen[0]+Language.VictoryScreen[1]+Language.VictoryScreen[2];
					DIAGTEXT.wordWrap=true;	
					var format=GlobalVariables.TextENDSCREEN;
					DIAGTEXT.setTextFormat(format);
					DIAGTEXT.width=Zim.width*0.9;	
					DIAGTEXT.height=Zim.height*0.9;					
					DIAGTEXT.x=Zim.x-DIAGTEXT.width/2;
					DIAGTEXT.y=Zim.y-DIAGTEXT.height/2;
					Zim.text=DIAGTEXT;
					
					var TEXTA:TextField=new TextField()	
					game.Memory["HubWorld"].addChild(TEXTA);
					TEXTA.text=Zim.currentGOLD;
					TEXTA.wordWrap=true;	
					var format=GlobalVariables.TextPOPUPEFX;
					TEXTA.setTextFormat(format);
					TEXTA.width=Zim.width*0.9;	
					TEXTA.height=Zim.height*0.9;					
					TEXTA.x=Zim.x-TEXTA.width/2+65;
					TEXTA.y=Zim.y-TEXTA.height/2+75;
					Zim.GOLDtext=TEXTA;
					
					var TEXTB:TextField=new TextField()	
					game.Memory["HubWorld"].addChild(TEXTB);
					TEXTB.text=Zim.currentGOLD;
					TEXTB.wordWrap=true;	
					var format=GlobalVariables.TextPOPUPEFX;
					TEXTB.setTextFormat(format);
					TEXTB.width=Zim.width*0.9;	
					TEXTB.height=Zim.height*0.9;					
					TEXTB.x=TEXTA.x;
					TEXTB.y=TEXTA.y+30;
					Zim.XPtext=TEXTB;
			
					/*var Zim:UIScroll=new UIScroll()
					game.Memory["HubWorld"].addChild(Zim);
					Zim.x=(GlobalVariables.stageWidth-250)/2;
					Zim.y=GlobalVariables.stageHeight/2;
					Zim.width=(GlobalVariables.stageWidth-250)*0.8;
					Zim.height=(GlobalVariables.stageHeight)*0.8;
					
					game.Memory["HubWorld"]["InfoUI"]=Zim;
					
										
					var DIAGTEXT:TextField=new TextField()	
					game.Memory["HubWorld"].addChild(DIAGTEXT);
					DIAGTEXT.text=Language.VictoryScreen[0]+Language.VictoryScreen[1]+RewardData[0]+Language.VictoryScreen[2]+"\n";*/
					
					Zim.TextTBA=new Array();
					
					for (var Gir=0; Gir<ArmyData.length; Gir++)
					{
						if (ArmyData[Gir]==1)
						{
							//DIAGTEXT.text=DIAGTEXT.text+"\n"+game.Memory["Data"].Armies[Gir].Name+Language.VictoryScreen[3];
							Zim.TextTBA.push("\n"+game.Memory["Data"].Armies[Gir].Name+Language.VictoryScreen[3])
						}
						if (ArmyData[Gir]==2)
						{
							//DIAGTEXT.text=DIAGTEXT.text+"\n"+game.Memory["Data"].Armies[Gir].Name+Language.VictoryScreen[4];
							Zim.TextTBA.push("\n"+game.Memory["Data"].Armies[Gir].Name+Language.VictoryScreen[4])
						}
					}		
					
					if (RewardData[2]!=null)
					{
						if (getQualifiedClassName(RewardData[2][2])=="String")
						{
							//DIAGTEXT.text=DIAGTEXT.text+"\n"+RewardData[2][2]+Language.VictoryScreen[5];
							Zim.TextTBA.push("\n"+RewardData[2][2]+Language.VictoryScreen[5])
						}
						else 
						{
							//DIAGTEXT.text=DIAGTEXT.text+"\n"+Language.VictoryScreen[10]+Language.VictoryScreen[5];
							Zim.TextTBA.push("\n"+Language.VictoryScreen[10]+Language.VictoryScreen[5])
						}
					}
					/*DIAGTEXT.wordWrap=true;	
					var format=GlobalVariables.textMESSAGE;
					DIAGTEXT.setTextFormat(format);
					DIAGTEXT.width=Zim.width*0.9;	
					DIAGTEXT.height=Zim.height*0.9;					
					DIAGTEXT.x=(GlobalVariables.stageWidth-250)/2-DIAGTEXT.width/2;
					DIAGTEXT.y=GlobalVariables.stageHeight*0.55-Zim.height/2+30;
					Zim.text=DIAGTEXT;*/
					
					var TXTD:TextField=new TextField()	
					game.Memory["HubWorld"].addChild(TXTD);
					TXTD.text="";
					TXTD.wordWrap=true;	
					var format=GlobalVariables.TextINGAMEMENU;
					TXTD.setTextFormat(format);
					TXTD.width=Zim.width*0.9;	
					TXTD.height=Zim.height*0.9;					
					TXTD.x=DIAGTEXT.x;
					TXTD.y=DIAGTEXT.y+135;
					Zim.endTXT=TXTD;
				}
		
		public static function DisplayDefeatScreen(game:HubWorldHandler,RewardData:Array)
		{
					var Zim:UIScroll=new UIScroll()
					game.Memory["HubWorld"].addChild(Zim);
					Zim.x=(GlobalVariables.stageWidth-250)/2;
					Zim.y=GlobalVariables.stageHeight/2;
					Zim.width=(GlobalVariables.stageWidth-250)*0.8;
					Zim.height=(GlobalVariables.stageHeight)*0.8;
					
					game.Memory["HubWorld"]["InfoUI"]=Zim;
					
										
					var DIAGTEXT:TextField=new TextField()	
					game.Memory["HubWorld"].addChild(DIAGTEXT);
					DIAGTEXT.text=Language.VictoryScreen[6]+Language.VictoryScreen[7]+Math.round(RewardData[0]*GlobalVariables.LossPenalty)+Language.VictoryScreen[8];			
					DIAGTEXT.wordWrap=true;	
					var format=GlobalVariables.textMESSAGE;
					DIAGTEXT.setTextFormat(format);
					DIAGTEXT.width=Zim.width*0.9;	
					DIAGTEXT.height=Zim.height*0.9;					
					DIAGTEXT.x=(GlobalVariables.stageWidth-250)/2-DIAGTEXT.width/2;
					DIAGTEXT.y=GlobalVariables.stageHeight*0.55-Zim.height/2+30;
					Zim.text=DIAGTEXT;
				}
				
				
		
		public static function DisplayInfoScreen(game:HubWorldHandler,Text:String)
		{
					var Zim:UIScroll=new UIScroll()
					game.Memory["HubWorld"].addChild(Zim);
					Zim.x=(GlobalVariables.stageWidth-250)/2;
					Zim.y=GlobalVariables.stageHeight/2;
					Zim.width=(GlobalVariables.stageWidth-250)*0.8;
					Zim.height=(GlobalVariables.stageHeight)*0.8;
					
					game.Memory["HubWorld"]["InfoUI"]=Zim;
					
										
					var DIAGTEXT:TextField=new TextField()	
					game.Memory["HubWorld"].addChild(DIAGTEXT);
					DIAGTEXT.text=Text;			
					DIAGTEXT.wordWrap=true;	
					var format=GlobalVariables.textMESSAGE;
					DIAGTEXT.setTextFormat(format);
					DIAGTEXT.width=Zim.width*0.9;	
					DIAGTEXT.height=Zim.height*0.9;					
					DIAGTEXT.x=(GlobalVariables.stageWidth-250)/2-DIAGTEXT.width/2;
					DIAGTEXT.y=GlobalVariables.stageHeight*0.55-Zim.height/2+100;
					Zim.text=DIAGTEXT;
				}
				
				
		
		public static function DisplayPayScreen(game:HubWorldHandler,Data:Array)
		{
					var Zim:MovieClip=new MovieClip()
					game.Memory["HubWorld"].addChild(Zim);
					Zim.x=(GlobalVariables.stageWidth-250)/2;
					Zim.y=GlobalVariables.stageHeight/2;					
					game.Memory["HubWorld"]["ShopUI"]=Zim;		
										
					var UIBG:UIScroll=new UIScroll()
					Zim.addChild(UIBG);
					UIBG.x=0;
					UIBG.y=0;
					UIBG.width=(GlobalVariables.stageWidth-250)*0.8;
					UIBG.height=(GlobalVariables.stageHeight)*0.8;			
										
					var DIAGTEXT:TextField=new TextField()	
					Zim.addChild(DIAGTEXT);
					DIAGTEXT.text=Data[0]+"\n\n"+Language.BarracksText[12]+game.Memory["Data"].PlayerData[1]+Language.BarracksText[13];;			
					DIAGTEXT.wordWrap=true;	
					var format=GlobalVariables.textMESSAGE;
					DIAGTEXT.setTextFormat(format);
					DIAGTEXT.width=Zim.width*0.9;	
					DIAGTEXT.height=Zim.height*0.9;					
					DIAGTEXT.x=0-DIAGTEXT.width/2;
					DIAGTEXT.y=0-UIBG.height/2+50;
					Zim.text=DIAGTEXT;
					
					if (game.Memory["Data"].PlayerData[1]>=Data[1])
					{
					var BuyB:ShopButtonBuy=new ShopButtonBuy()
					game.Memory["HubWorld"]["ShopUI"].addChild(BuyB);
					BuyB.x=-50;					
					BuyB.y=90;
					BuyB.width=20;
					BuyB.height=20;
					BuyB.cost=Data[1];
					BuyB.data="payconditions";
					BuyB.conditions=Data[2];
					BuyB.father=Zim;
					game.Memory["Buttons"].push(BuyB);
					}
					
					var CloseB:ShopButtonClose=new ShopButtonClose()
					game.Memory["HubWorld"]["ShopUI"].addChild(CloseB);
					CloseB.x=50;
					CloseB.y=90;
					CloseB.data="closeshop";
					game.Memory["Buttons"].push(CloseB);
					CloseB.width=20;
					CloseB.height=20;
				}
		
		public static function DisplayShopScreen(game:HubWorldHandler,Data:Array)
		{
			ClearShopScreen(game);
			
			var FatherClip:MovieClip=new MovieClip()
					game.Memory["HubWorld"].addChild(FatherClip);
					FatherClip.x=(GlobalVariables.stageWidth-250)/2;
					FatherClip.y=GlobalVariables.stageHeight/2;
					game.Memory["HubWorld"]["ShopUI"]=FatherClip;
					
					var Zim:ShopBG=new ShopBG()
					game.Memory["HubWorld"]["ShopUI"].addChild(Zim);
					Zim.x=0;
					Zim.y=0;
					Zim.width=(GlobalVariables.stageWidth-250)*0.7;
					Zim.height=(GlobalVariables.stageHeight)*0.7;
					
					for (var i=0; i<Math.min(Data[0].length,2); i++)
					{
						var Batten:HubWorldUIBTN=new HubWorldUIBTN();
						game.Memory["HubWorld"]["ShopUI"].addChild(Batten);
						Batten.width=(GlobalVariables.stageWidth-250)*0.55;
						Batten.height=75;
						Batten.x=0;
						Batten.y=80*i-65;
						
						Batten.army=DataItemArmy.MakeNewArmy([Data[0][i],Math.round(Data[1][0]+Math.random()*(Data[1][1]-Data[1][0])),Data[3],null,null]);
					
						var ArmyIcon=new Bitmap(new ArmyData.Display[Data[0][i]](null, null));
						game.Memory["HubWorld"]["ShopUI"].addChild(ArmyIcon);
						ArmyIcon.scaleY=(Batten.height-10)/ArmyIcon.height;
						ArmyIcon.scaleX=ArmyIcon.scaleY;
						ArmyIcon.x=0-((Batten.width-10)/2);
						ArmyIcon.y=Batten.y-ArmyIcon.height/2;					
										
						var TEXTY=Batten.army.Name+Language.BarracksText[0]+Batten.army.Level+" "+Batten.army.GetMyName()+"\nA-"+Math.round(Batten.army.Attack)+" D-"+Math.round(Batten.army.Armor)+" M-"+Math.round(Batten.army.Special)+"\n"+Language.BarracksText[10]+Language.PassiveText[DataItem.Passives.indexOf(Batten.army.Passive)][0];
										
						var DIAGTEXT:TextField=new TextField()	
						game.Memory["HubWorld"]["ShopUI"].addChild(DIAGTEXT);
						DIAGTEXT.text=TEXTY;			
						DIAGTEXT.wordWrap=true;	
						DIAGTEXT.setTextFormat(GlobalVariables.TextARMYUI);
						DIAGTEXT.width=Batten.width*0.9;	
						DIAGTEXT.height=Batten.height*0.9;					
						DIAGTEXT.x=Batten.x-50;
						DIAGTEXT.y=Batten.y-30;
						Batten.text=DIAGTEXT;
						
						var DIAGTEXTTwo:TextField=new TextField()	
						game.Memory["HubWorld"]["ShopUI"].addChild(DIAGTEXTTwo);
						DIAGTEXTTwo.text=Language.BarracksText[11]+(Batten.army.GetMyGoldCost(Data[2]));			
						DIAGTEXTTwo.wordWrap=true;	
						DIAGTEXTTwo.setTextFormat(GlobalVariables.TextLIFEBAR);					
						DIAGTEXTTwo.width=Batten.width*0.9;	
						DIAGTEXTTwo.height=Batten.height*0.9;					
						DIAGTEXTTwo.x=Batten.x+100-DIAGTEXTTwo.width/2;
						DIAGTEXTTwo.y=Batten.y-30;
						Batten.texttwo=DIAGTEXTTwo;
					
						var BuyB:ShopButtonBuy=new ShopButtonBuy()
						game.Memory["HubWorld"]["ShopUI"].addChild(BuyB);
						BuyB.x=Batten.x+100;
						BuyB.y=Batten.y;
						BuyB.width=20;
						BuyB.height=20;
						BuyB.data="purchaseshop";
						BuyB.father=Batten;
						BuyB.Multiplier=Data[2];
						game.Memory["Buttons"].push(BuyB);
					}
						
					var DIAGTEXTGOLD:TextField=new TextField()	
					game.Memory["HubWorld"]["ShopUI"].addChild(DIAGTEXTGOLD);
					DIAGTEXTGOLD.text=Language.BarracksText[12]+game.Memory["Data"].PlayerData[1]+Language.BarracksText[13];			
					DIAGTEXTGOLD.wordWrap=true;	
					DIAGTEXTGOLD.setTextFormat(GlobalVariables.TextLIFEBAR);					
					DIAGTEXTGOLD.width=Batten.width*0.9;	
					DIAGTEXTGOLD.height=Batten.height*0.9;					
					DIAGTEXTGOLD.x=Batten.x-DIAGTEXTGOLD.width/2;
					DIAGTEXTGOLD.y=Batten.y+45;
					Batten.texttwo=DIAGTEXTGOLD;
					
					var CloseB:ShopButtonClose=new ShopButtonClose()
					game.Memory["HubWorld"]["ShopUI"].addChild(CloseB);
					CloseB.x=0;
					CloseB.y=100;
					CloseB.data="closeshop";
					game.Memory["Buttons"].push(CloseB);
					CloseB.width=20;
					CloseB.height=20;
										
				}
				
				public static function ClearShopScreen(game:HubWorldHandler)
				{
					if (game.Memory["HubWorld"]["ShopUI"]!=null)
					{
						game.Memory["HubWorld"]["ShopUI"].parent.removeChild(game.Memory["HubWorld"]["ShopUI"]);
						game.Memory["HubWorld"]["ShopUI"]=null;
					}
				}
				
	}
	
}
