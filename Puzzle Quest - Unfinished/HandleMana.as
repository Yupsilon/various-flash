﻿package  {
	
	public class HandleMana {

		public static function ResolveMana(game:MainGame,Nr:Number,GemType:Number)
		{				
			if (game.Memory["PlayerTurn"]>=0)
			{
				if (Nr==3)
				{
					AddMana(game,game.Memory["PlayerTurn"],1,GemType-1,false);
				}			
				else if (Nr==4)
				{
					AddMana(game,game.Memory["PlayerTurn"],2,GemType-1,false);
				}		
				else if (Nr>=5)
				{
					AddMana(game,game.Memory["PlayerTurn"],2,GemType-1,false);
					AddMana(game,game.Memory["PlayerTurn"],1,GlobalVariables.GEM_PURPLE,true);
				}
			}
		}
		
		public static function BalanceMana(game:MainGame)
		{
			if (GetMana(game,0,GlobalVariables.GEM_PURPLE)==0 &&
				GetMana(game,1,GlobalVariables.GEM_PURPLE)==0)
				{					
					if (game.Memory["PlayerTurn"]==0)					
					{
						var CompetentArmies=0;
						for (var Zim=0; Zim<game.Memory["DisplayItemArmies"][1].length; Zim++)
						{
							CompetentArmies+=game.Memory["DisplayItemArmies"][1][Zim].GetMorale(game);
						}
						AddMana(game,1,CompetentArmies,GlobalVariables.GEM_PURPLE,true);
						game.Memory["PlayerTurn"]=1;
					}
					else if (game.Memory["PlayerTurn"]==1)					
					{
						CompetentArmies=0;
						for (Zim=0; Zim<game.Memory["DisplayItemArmies"][0].length; Zim++)
						{
							CompetentArmies+=game.Memory["DisplayItemArmies"][0][Zim].GetMorale(game);
						}
						AddMana(game,0,CompetentArmies,GlobalVariables.GEM_PURPLE,true);
						game.Memory["PlayerTurn"]=0;
					}
					else if (game.Memory["PlayerTurn"]==-1)
					{
						game.Memory["PlayerTurn"]=1;
					}
					game.ForwardTurn();
				}
				else if (GetMana(game,0,GlobalVariables.GEM_PURPLE)==0)
				{
						game.Memory["PlayerTurn"]=1;
				}
				else if (GetMana(game,1,GlobalVariables.GEM_PURPLE)==0)
				{
						game.Memory["PlayerTurn"]=0;
				}
				
		}

		public static function AddMana(game:MainGame,Player:Number,Mana:Number,ManaType:Number,Update:Boolean) {
			game.Memory["PlayerMana"][Player][ManaType]+=Mana;
			if (Update==true)
			{
				UpdateManaAt(game,Player,Mana,ManaType,true);
			}
		}
				
		public static function DoIHaveEnoughMana(game:MainGame,PlayerOwner:Number,ManaCost:Array,TakeIfSuccess:Boolean=true):Boolean {
			
			var Mana=true;
			for (var Zim=0; Zim<GlobalVariables.xk_MAX_MANA_TYPES; Zim++)
			{
				if (game.Memory["PlayerMana"][PlayerOwner][Zim]<ManaCost[Zim])
				{
					Mana=false;
					if (PlayerOwner==0)
					{
						DrawInfoOverlay.DisplayErrorOverlay(game,12);
					}
					break;
				}
			}
			if (Mana==true && TakeIfSuccess==true)
			{
				for (Zim=0; Zim<GlobalVariables.xk_MAX_MANA_TYPES; Zim++)
				{
					RemoveMana(game,PlayerOwner,ManaCost[Zim],Zim);
				}
			}
			return Mana;
		}		
		
		public static function GetMana(game:MainGame,Player:Number,ManaType:Number):Number {
			return game.Memory["PlayerMana"][Player][ManaType];
		}
		
		public static function RemoveMana(game:MainGame,Player:Number,Mana:Number,ManaType:Number) {
			game.Memory["PlayerMana"][Player][ManaType]=Math.max(0,game.Memory["PlayerMana"][Player][ManaType]-Mana);
			UpdateManaAt(game,Player,0-Mana,ManaType);
		}
		
		public static function UpdateManaAt(game:MainGame,Player:Number,Mana:Number,ManaType:Number,Particles:Boolean=false) {
			game.Memory["Display"]["PlayerMana"][Player][ManaType]=Math.max(0,game.Memory["Display"]["PlayerMana"][Player][ManaType]+Mana);
			if (Particles==true)
			{
				SpecialEffects.MakeManaParticles(game.Memory["HWH"],ManaType,Player);
			}
		}

	}
	
}
