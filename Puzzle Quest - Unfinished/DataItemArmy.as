﻿package  {	
	import flash.utils.*;
	
	public class DataItemArmy {

		public var Name:String;
		public var Type:Number;
		public var Life:Number;
		public var MaxLife:Number;
		public var Level:Number;
		public var Special:Number;
		public var Attack:Number;
		public var Abilities:Array;
		public var Armor:Number;
		public var LevelUpStats:Array;
		public var XP:Number=0;
		public var LevelUpXP:Number=0;
		public var Passive:String;
		public var Deaths:Number=0;
		public var realID:Number;
		
		public function Wrap():Array
		{
			return [Name,Type,Life,MaxLife,Level,Special,Attack,Abilities,Armor,LevelUpStats,Passive,Deaths,XP,LevelUpXP]
		}		
		
		public static function UnWrap(ArmyData:Array):DataItemArmy
		{
			var DIA:DataItemArmy=new DataItemArmy();
			
			DIA.Name=ArmyData[0];
			DIA.Type=ArmyData[1];
			DIA.Life=ArmyData[3];
			DIA.MaxLife=ArmyData[3];
			DIA.Level=ArmyData[4];
			DIA.Special=ArmyData[5];
			DIA.Attack=ArmyData[6];
			DIA.Abilities=ArmyData[7];
			DIA.Armor=ArmyData[8];
			DIA.LevelUpStats=ArmyData[9];
			DIA.Passive=ArmyData[10];
			DIA.Deaths=ArmyData[11];
			DIA.XP=ArmyData[12];
			DIA.LevelUpXP=ArmyData[13];
			
			return DIA;
		}
		
		public function CalculateLevelXP(Level:Number):Number
		{
			var Multiplier =(LevelUpStats[0]+LevelUpStats[1]+LevelUpStats[2]+LevelUpStats[3])/25;
			
			//Multiplier=(Multiplier+Multiplier*Multiplier)/2;
			
			return (3+5*Level*Level/3-5*(Level-2)*(Level-2)/3)*Multiplier;
		}
		
		public function GetAbilities():Array
		{
			var Temp:Array=new Array();
						
						
						for (var Zim=0; Zim<ArmyData.ArmyStats[Type][1].length; Zim++)
						{
							var Ability=ArmyData.ArmyStats[Type][1][Zim];
							
						
							if (Abilities.indexOf(Ability[0])==-1 &&
								Ability[1]<=Level &&
								(Ability[2]==-1 || 
								 (Ability[2]==GetMyType())))
								 {
										Temp.push(Ability[0]);
								 }
								
						}
						return Temp;
		}
		
		public function AddXP(XadditionP:Number,XPLevel:Number):Number
		{
			var Data=0;
			
			var OldLevel=Level;
			
			XP+=XadditionP*XPLevel*10//*XPLevel/Level;
			while (XP>LevelUpXP && Level<GlobalVariables.MaxLevel)
			{
				LevelUpXP+=CalculateLevelXP(Level);
				Level++;
				Data=1;
				MaxLife+=LevelUpStats[DataItem.Stat_Life]/GlobalVariables.HPBalancer;
				Attack+=LevelUpStats[DataItem.Stat_Attack]/GlobalVariables.ABalancer;
				Special+=LevelUpStats[DataItem.Stat_Special]/GlobalVariables.SBalancer;
				Armor+=LevelUpStats[DataItem.Stat_Armor]/GlobalVariables.DBalancer;
			}
			
			for (var Zim=0; Zim<ArmyData.ArmyStats[Type][1].length; Zim++)
			{
				var Ability=ArmyData.ArmyStats[Type][1][Zim];
				
				if (Ability[1]>OldLevel && 
					Ability[1]<=Level && 
					(Ability[2]==-1 || 
					(Ability[2]==0 && GetMyType()==0) ||
					(Ability[2]==1 && GetMyType()==1)||
					(Ability[2]==2 && GetMyType()==2)))
					{
						Data=2;
						if (Abilities.length<GlobalVariables.MaxArmySpells)
						{
							Abilities.push(Ability[0]);
						}
					}
			}
			
			return Data;
		}
		
		public function GetMyName():String
		{						
						return Language.ArmyNames[Type][GetMyType()];;
		}
		
		public function GetMyGoldCost(Multiplier:Number):Number
		{
			return Math.ceil((LevelUpStats[0]+LevelUpStats[1]+LevelUpStats[2]+LevelUpStats[3])/25*Level*10*Multiplier);
		}
		
		public function GetMyType():Number
		{
			var ATK=LevelUpStats[DataItem.Stat_Attack]-ArmyData.ArmyStats[Type][0][DataItem.Stat_Attack]
						var DEF=LevelUpStats[DataItem.Stat_Armor]-ArmyData.ArmyStats[Type][0][DataItem.Stat_Armor]
						var MAG=LevelUpStats[DataItem.Stat_Special]-ArmyData.ArmyStats[Type][0][DataItem.Stat_Special]
						
						var Type=0;
						if (DEF>ATK && DEF>MAG)
						{
							Type=1;
						}						
						else if (MAG>DEF && MAG>ATK)
						{
							Type=2;
						}
						return Type;
		}
		
		public static function MakeNewArmy(ArrayArmyStats:Array):DataItemArmy
		{			
					var Army:DataItemArmy=new DataItemArmy();
					
					Army.Type=ArrayArmyStats[0];
					Army.Level=ArrayArmyStats[1];
					Army.LevelUpStats=new Array();
					for (var Zim=0; Zim<ArmyData.ArmyStats[Army.Type][0].length; Zim++)
					{
						Army.LevelUpStats.push(ArmyData.ArmyStats[Army.Type][0][Zim]);
					}
					
					if (ArrayArmyStats[3]!=null)
					{
						for (Zim=0; Zim<ArrayArmyStats[3].length; Zim++)
						{
							Army.LevelUpStats[Zim]=ArrayArmyStats[3][Zim];
						}
					}
					for (Zim=1; Zim<Army.LevelUpStats.length; Zim++)
					{
						Army.LevelUpStats[Zim]=Math.max(1,Math.random()*ArmyData.BaseStats[4]+Army.LevelUpStats[Zim]);
					}
					
					Army.MaxLife=ArmyData.BaseStats[DataItem.Stat_Life]+Army.LevelUpStats[DataItem.Stat_Life]*Army.Level/GlobalVariables.HPBalancer;
					Army.Special=ArmyData.BaseStats[DataItem.Stat_Special]+Army.LevelUpStats[DataItem.Stat_Special]*Army.Level/GlobalVariables.SBalancer;
					Army.Attack=ArmyData.BaseStats[DataItem.Stat_Attack]+Army.LevelUpStats[DataItem.Stat_Attack]*Army.Level/GlobalVariables.ABalancer;
					Army.Armor=ArmyData.BaseStats[DataItem.Stat_Armor]+Army.LevelUpStats[DataItem.Stat_Armor]*Army.Level/GlobalVariables.DBalancer;
					
					if (ArrayArmyStats[2]!=null)
					{
						if (getQualifiedClassName(ArrayArmyStats[2])=="String")
						{
							Army.Name=ArrayArmyStats[2];
						}
						else
						{
							Army.Name=""
							for each (var Varr in ArrayArmyStats[2])
							{
								Army.Name+=Varr[Math.round(Math.random()*(Varr.length-1))];
							}
						}
					}
					else
					{
						Army.Name=Army.GetMyName();
					}
					if (ArrayArmyStats[4]!=null)
					{
						Army.Abilities=ArrayArmyStats[4];
					}
					else
					{
						var TempAbilities=ArmyData.ArmyStats[Army.Type][1];
						
						Army.Abilities=new Array();
						
						
						for (Zim=0; Zim<TempAbilities.length; Zim++)
						{
							var Ability=TempAbilities[Zim];
							
						
							if (Army.Abilities.indexOf(Ability[0])==-1 &&
								Ability[1]<=Army.Level &&
								(Ability[2]==-1 || 
								 (Ability[2]==0 && Army.GetMyType()==0) ||
								 (Ability[2]==2 && Army.GetMyType()==1)||
								 (Ability[2]==1 && Army.GetMyType()==2)))
								 {
									 if (Army.Abilities.length<GlobalVariables.MaxArmySpells)
									 {
										 Army.Abilities.push(Ability[0]);
									 }
									 else if (Math.random()*100>=40)
									 {
										 Army.Abilities[Math.round(Math.random()*(Army.Abilities.length-1))]=Ability[0];
									 }
								 }
								
						}
					}
					
					if (DataItem.Passives.indexOf(ArrayArmyStats[5])!=-1)
					{
						Army.Passive=ArrayArmyStats[5];
					}
					else if (Army.GetMyType()==0)
					{
						Army.Passive=ArmyData.ArmyStats[Army.Type][2][0][Math.round(Math.random()*(ArmyData.ArmyStats[Army.Type][2][0].length-1))];
					}	
					else if (Army.GetMyType()==1)
					{
						Army.Passive=ArmyData.ArmyStats[Army.Type][2][1][Math.round(Math.random()*(ArmyData.ArmyStats[Army.Type][2][1].length-1))];
					}						
					else if (Army.GetMyType()==2)
					{
						Army.Passive=ArmyData.ArmyStats[Army.Type][2][2][Math.round(Math.random()*(ArmyData.ArmyStats[Army.Type][2][2].length-1))];
					}
										
					Army.XP=0;
					Army.LevelUpXP=Army.CalculateLevelXP(Army.Level);
					
					return Army;
		}
	}
}
