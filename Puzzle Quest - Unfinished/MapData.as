﻿package  {
	
	public class MapData {

		public static var MapWidth:Number = 1200;
		public static var MapHeight:Number = 1200;			
		public static var Border=35
		
		public static var StartLocation:Number = 0;
				
		public static var ProgressCompletion=[0,3,2,2,1,1,1,1,1,1,2,2,1,1,1,1,1,2,3];//17
		
		public static var MapLocationData:Array = 
		[
		 	[
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Black Burg","A lonely tower in Etheria.",
				577,28,asd,IconG,
				//Objectives needed
				[[1,0,">="]],	
				//Quests Awarded
				[
				 	[
						 "Defend the Keep",
						 "Battle",
						 [[1,0,"="]],//variable requirements
						 [-1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  	[
						   		ArmyData.Army_Knight,//Army Type
								8,//Level
								"Lord Deathbane",//Name
								null,//Level Up Stats
								[SpellData.cSpellBless,SpellData.cSpellCharge,SpellData.cSpellRest]//Abilities
							],
						  	[
						   		ArmyData.Army_Guard,//Army Type
								5,//Level
								"Paladin",//Name
								null,//Level Up Stats
								[SpellData.cSpellRest,SpellData.cSpellTowerShield]
							],
						  	[
						   		ArmyData.Army_Guard,//Army Type
								5,//Level
								"Paladin",//Name
								null,//Level Up Stats
								[SpellData.cSpellRest,SpellData.cSpellTowerShield]
							],
						  	[
						   		ArmyData.Army_Guard,//Army Type
								5,//Level
								"Paladin",//Name
								null,//Level Up Stats
								[SpellData.cSpellRest,SpellData.cSpellTowerShield]
							],
						  	[
						   		ArmyData.Army_Guard,//Army Type
								5,//Level
								"Paladin",//Name
								null,//Level Up Stats
								[SpellData.cSpellRest,SpellData.cSpellTowerShield]
							]
						   ],
						  //Temp Allied armies (if available)
						  [						   
						  	[
						   		0,//Army Type
								10,//Level
								"Black Burg",//Name
								[25,5,3,5],//Level Up Stats
								[SpellData.cSpellUltimate,SpellData.cSpellPlague,SpellData.cSpellRainFire]//Abilities
							],   
						  	[
						   		ArmyData.Army_Skeleton,//Army Type
								5,//Level
								"Blackguard",//Name
								[22,3,7,1],//Level Up Stats
								[SpellData.cSpellEatSkull,SpellData.cSpellShieldBash]//Abilities
							],   
						  	[
						   		ArmyData.Army_Skeleton,//Army Type
								5,//Level
								"Blackguard",//Name
								[22,3,7,1],//Level Up Stats
								[SpellData.cSpellEatSkull,SpellData.cSpellScare]//Abilities
							]
						   ],
						  //MinLevel
						  25,
						  //EnemyName
						  "Deathbane's Army",
						  //Reward
						  [200,0,null,//gold,xp,army,conditions
						   [[1,1,"="]]
						  ],
						 ],
					],
				 	[
						 "Orc Invaders!",
						 "RandomBattle",
						 [],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		4,//Max Armies
						  		2,//Min Armies
								[ArmyData.Army_Goblin,ArmyData.Army_Goblin,ArmyData.Army_Orc],//Army Types
								0
						   ],						  
						  [  
						  	[
						   		0,//Army Type
								5,//Level
								"Fort Walls",//Name
								null,//Level Up Stats
								[SpellData.cSpellReinforce]//Abilities
							],   
						  ],
						  5,
						  "Invaders",
						  [75,25,null,null]
						 ],
					],
				 	[
						 "Barracks",
						 "Barracks",
						 [],//variable requirements
						 [1,-1],//level requirements: Min, Max						 			
					 ],	
				 	[
						 "Hit Job",
						 "Battle",
						 [[1,0,">"]],//variable requirements
						 [-1,1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  	[
						   		ArmyData.Army_Knight,//Army Type
								10,//Level
								null,//Name
								null,//Level Up Stats
								[SpellData.cSpellUltimate,SpellData.cSpellCharge]//Abilities
							],
						   ],
						  //Temp Allied armies (if available)
						  [						  
						  	[
						   		ArmyData.Army_Skeleton,//Army Type
								5,//Level
								"Blackguard",//Name
								[22,3,7,1],//Level Up Stats
								[SpellData.cSpellGraveDigger,SpellData.cSpellShieldBash]//Abilities
							],   
						  	[
						   		ArmyData.Army_Skeleton,//Army Type
								5,//Level
								"Blackguard",//Name
								[22,3,7,1],//Level Up Stats
								[SpellData.cSpellEatSkull,SpellData.cSpellScare]//Abilities
							]
						   ],
						  //MinLevel
						  5,
						  //EnemyName
						  "The Target",
						  //Reward
						  [125,0,null,null],
						 ]					  
					 ],	 
					 [
						 "Hire Skeleton",
						 "UnitStore",
						 [[1,0,"!="]],//variable requirements
						 [-1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						   		[ArmyData.Army_Skeleton],//Army Types
								[5,10],//Levels
								1,//MultiplierCost	
								UnitNaming.GenericEvil//Naming Sets
						 ]						  
					 ],
				]
			 ],
			 [
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Tower of Bone","Description",
				647,52.5,asd,IconA,
				//Objectives needed
				[[1,0,">"]],	
				//Quests Awarded
				[
				 	[
						 "Hire Armies",
						 "UnitStore",
						 [[1,0,"!="],[2,1,"<="]],//variable requirements
						 [-1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						   		[ArmyData.Army_Zombie],//Army Types
								[4,10],//Levels
								2,//MultiplierCost	
								UnitNaming.GenericEvil//Naming Sets
						 ]						  
					 ],
				 	[
						 "Hire Armies",
						 "UnitStore",
						 [[1,0,"!="],[2,1,">"]],//variable requirements
						 [-1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						   		[ArmyData.Army_Zombie,ArmyData.Army_Rat],//Army Types
								[5,11],//Levels
								2,//MultiplierCost	
								UnitNaming.GenericEvil//Naming Sets
						 ]						  
					 ], 
				 	[
						 "Rat Problem",
						 "RandomBattle",
						 [],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		2,//Max Armies
						  		1,//Min Armies
								[ArmyData.Army_Rat],//Army Types
								0
						   ],						  
						  [     
						  ],
						  20,
						  "Vermin",
						  [25,55,null,[[2,1,"+"]]]
						 ],
					],
				 ]
			 ],	
			 [
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Durbul","Description",
				430,40,asd,IconC,
				//Objectives needed
				[[1,0,">"]],	
				//Quests Awarded
				[
				 [
						 "Against The Wilderness",
						 "RandomBattle",
						 [],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		3,//Max Armies
						  		1,//Min Armies
								[ArmyData.Army_FrostGiant,ArmyData.Army_FrostWolf,ArmyData.Army_Wolf,ArmyData.Army_Spider,ArmyData.Army_FrostWolf,ArmyData.Army_Wolf,ArmyData.Army_Spider,ArmyData.Army_FrostWolf,ArmyData.Army_Wolf,ArmyData.Army_Spider,ArmyData.Army_FrostWolf,ArmyData.Army_Wolf,ArmyData.Army_Spider],//Army Types
								0
						   ],						  
						  [     
						  ],
						  30,
						  "Wilderness",
						  [15,75,null,[[3,1,"+"]]]],
					],										 
				 	[
						 "Hunt a Frost Wolf",
						 "Battle",
						 [[3,1,">"]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_FrostWolf,//Army Type
								null,//Level
								null,//Name
								[22,3,7,1],//Level Up Stats
								[SpellData.cSpellHybernate,SpellData.cSpellHowl,SpellData.cSpellWound]//Abilities
							], 
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  50,
						  //EnemyName
						  "Wilderness",
						  //Reward
						  [0,25,[ArmyData.Army_FrostWolf,//Army Type
								5,//Level
								UnitNaming.Beast,//Name
								null,//Level Up Stats
								null//Abilities
							],null]	//Gold,XP
						 ]						  
					 ],
				 ]
			 ],				
			 [//4
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Castle Mortuus","Description",
				500,80,asd,IconB,
				//Objectives needed
				[[1,0,">"]],	
				//Quests Awarded
				[
				  [
						 "Objective",
						 "Info",
						 [[1,1,"="]],
						 [-1,-1],
						 "Objective:\nAssemble a large army\nand take back Castle Mortuus."
				  ],
				 	[
						 "Barracks",
						 "Barracks",
						 [[1,1,">"]],//variable requirements
						 [1,-1],//level requirements: Min, Max						 			
					 ],	
				 	[
						 "Garrison",
						 "UnitStore",
						 [[1,1,">"]],//variable requirements
						 [100,-1],//level requirements: Min, Max
						 //MissionData
						 [
						   		[ArmyData.Army_Skeleton,ArmyData.Army_Necromancer],//Army Types
								[15,20],//Levels
								2,//MultiplierCost	
								UnitNaming.GenericEvil//Naming Sets
						 ]						  
					 ],
				 	[
						 "Garrison",
						 "UnitStore",
						 [[1,1,">"]],//variable requirements
						 [-1,100],//level requirements: Min, Max
						 //MissionData
						 [
						   		[ArmyData.Army_Skeleton],//Army Types
								[15,20],//Levels
								2,//MultiplierCost	
								UnitNaming.GenericEvil//Naming Sets
						 ]						  
					 ],
				  [
						 "Take Back Castle Mortuus",
						 "Battle",
						 [[1,1,"="]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  	[
						   		ArmyData.Army_Castle,//Army Type
								20,//Level
								"Castle Gates",//Name
								[15,0,10,0],//Level Up Stats
								[SpellData.cSpellReinforce]//Abilities
							],
						  	[
						   		ArmyData.Army_ElfQueen,//Army Type
								20,//Level
								"Lady Thera",//Name
								[8,3,3,6],//Level Up Stats
								[SpellData.cSpellBless,SpellData.cSpellHex,SpellData.cSpellLesserHeal]
							],
						  	[
						   		ArmyData.Army_Guard,//Army Type
								15,//Level
								"Guard",//Name
								null,//Level Up Stats
								[SpellData.cSpellShieldBash,SpellData.cSpellIronWall]
							],
						  	[
						   		ArmyData.Army_Guard,//Army Type
								15,//Level
								"Guard",//Name
								null,//Level Up Stats
								[SpellData.cSpellShieldBash,SpellData.cSpellIronWall]
							],
						   ],
						  //Temp Allied armies (if available)
						  [		
						   ],
						  //MinLevel
						  60,
						  //EnemyName
						  "Castle Defense",
						  //Reward
						  [400,100,null,//gold,xp,army,conditions
						   [[1,2,"="]]
						  ],
						],
					],
				   [
						 "Train In Castle Mortuus",
						 "RandomBattle",
						 [[1,1,">"]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		4,//Max Armies
						  		1,//Min Armies
								[ArmyData.Army_Skeleton,ArmyData.Army_Zombie,ArmyData.Army_Skeleton,ArmyData.Army_Zombie,ArmyData.Army_Skeleton,ArmyData.Army_Zombie,ArmyData.Army_Skeleton,ArmyData.Army_Zombie,ArmyData.Army_Skeleton,ArmyData.Army_Zombie,ArmyData.Army_Skeleton,ArmyData.Army_Zombie,ArmyData.Army_Skeleton,ArmyData.Army_Zombie,ArmyData.Army_Necromancer,ArmyData.Army_DoomKnight],//Army Types
								0
						   ],						  
						  [     
						  ],
						  30,
						  "Training!",
						  [0,100,null,null],
						  ]
					],	
				]
			  ],		
			 [//5
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Nurg","Description",
				840,75,asd,IconC,
				//Objectives needed
				[[1,1,">"]],	
				//Quests Awarded
				[
				   [
						 "Goblin Arena",
						 "RandomBattle",
						 [],//variable requirements
						 [20,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		4,//Max Armies
						  		1,//Min Armies
								[ArmyData.Army_Goblin,ArmyData.Army_Goblin,ArmyData.Army_Goblin,ArmyData.Army_Rat,ArmyData.Army_Troll],//Army Types
								0
						   ],						  
						  [     
						  ],
						  30,
						  "Goblins!",
						  [50,50,null,[[4,1,"+"]]],
					],								 
				 	[
						 "Arena Champions",
						 "Battle",
						 [[4,1,">"],[5,0,"="]],//variable requirements
						 [60,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Goblin,//Army Type
								20,//Level
								"Fierce",//Name
								[6,2,3,4],//Level Up Stats
								[SpellData.cSpellZap,SpellData.cSpellIgnite,SpellData.cSpellReplenish]//Abilities
							],				  					  
						  	[
						   		ArmyData.Army_Goblin,//Army Type
								20,//Level
								"Swift",//Name
								[7,5,3,1],//Level Up Stats
								[SpellData.cSpellSneak]//Abilities
							], 	  					  
						  	[
						   		ArmyData.Army_Troll,//Army Type
								20,//Level
								"Stout",//Name
								[14,1,4,1],//Level Up Stats
								[SpellData.cSpellDrinkAle,SpellData.cSpellSkullBash]//Abilities
							], 
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  60,
						  //EnemyName
						  "Arena Champions",
						  //Reward
						  [0,150,null,[[5,1,"="]]]	//Gold,XP
						 ]						  
					 ],								 
				 	[
						 "Battle the King",
						 "Battle",
						 [[4,1,">"],[6,0,"="]],//variable requirements
						 [90,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Troll,//Army Type
								50,//Level
								"The Goblin King",//Name
								[22,3,7,1],//Level Up Stats
								[SpellData.cSpellStoneForm,SpellData.cSpellSkullBash,SpellData.cSpellUnholyRage]//Abilities
							], 				  					  
						  	[
						   		ArmyData.Army_Goblin,//Army Type
								40,//Level
								"The Royal Jester",//Name
								[6,2,3,4],//Level Up Stats
								[SpellData.cSpellZap,SpellData.cSpellLesserHeal]//Abilities
							],		
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  90,
						  //EnemyName
						  "Royal Court",
						  //Reward
						  [0,200,null,[6,1,"="]]	//Gold,XP
						 ]						  
					 ],
					 [
						 "Visit the Slavers",
						 "UnitStore",
						 [[5,1,"="],[6,0,"="]],//variable requirements
						 [-1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						   		[ArmyData.Army_Goblin],//Army Types
								[5,10],//Levels
								3,//MultiplierCost	
								UnitNaming.GenericEvil//Naming Sets
						 ]						  
					 ],
					 [
						 "Visit the Slavers",
						 "UnitStore",
						 [[5,1,"="],[6,1,"="]],//variable requirements
						 [-1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						   		[ArmyData.Army_Goblin,ArmyData.Army_Troll],//Army Types
								[10,15],//Levels
								2,//MultiplierCost	
								UnitNaming.GenericEvil//Naming Sets
						 ]						  
					 ],
					 		 
		 			]
				 ]
			  ],
			 [
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Black Forest","Description",
				600,200,asd,IconD,
				//Objectives needed
				[[9,0,">"]],	
				//Quests Awarded
				[
				 [
						 "Search the Forest",
						 "RandomBattle",
						 [],//variable requirements
						 [20,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		3,//Max Armies
						  		1,//Min Armies
								[ArmyData.Army_Wolf,ArmyData.Army_Wolf,ArmyData.Army_Wolf,ArmyData.Army_Spider,ArmyData.Army_Wasp],//Army Types
								0
						   ],						  
						  [     
						  ],
						  40,
						  "Wilderness",
						  [75,120,null,[9,1,"+"]],
						  ]
					],						 
				 	[
						 "Tame a Wolf",
						 "Battle",
						 [[9,1,">"]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Wolf,//Army Type
								null,//Level
								null,//Name
								[22,3,7,1],//Level Up Stats
								[SpellData.cSpellEatStar,SpellData.cSpellHowl,SpellData.cSpellSandAttack]//Abilities
							], 
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  60,
						  //EnemyName
						  "Wilderness",
						  //Reward
						  [0,125,[
						   		ArmyData.Army_Wolf,//Army Type
								5,//Level
								UnitNaming.Beast,//Name
								null,//Level Up Stats
								null//Abilities
							],null]	//Gold,XP
						 ]						  
					 ],
				 ]
			  ],
			[//8
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Darkwater Cave","Description",
				700,150,asd,IconI,
				//Objectives needed
				[[1,0,">"]],	
				//Quests Awarded
				[
				   [
						 "Into the Darkness",
						 "RandomBattle",
						 [],//variable requirements
						 [20,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		3,//Max Armies
						  		1,//Min Armies
								[ArmyData.Army_Goblin,ArmyData.Army_SandWorm,ArmyData.Army_Bat,ArmyData.Army_Bat,ArmyData.Army_Bat,ArmyData.Army_Bat,ArmyData.Army_SandWorm,ArmyData.Army_Spider],//Army Types
								0
						   ],						  
						  [     
						  ],
						  30,
						  "Cave Beasts",
						  [50,50,null,null],
						  ]
					],				 
				 	[
						 "Capture a Bat",
						 "Battle",
						 [[7,0,"="]],//variable requirements
						 [75,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Bat,//Army Type
								null,//Level
								null,//Name
								null,//Level Up Stats
								[SpellData.cSpellSwoop,SpellData.cSpellTapLife,SpellData.cSpellEatStar]//Abilities
							], 
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  75,
						  //EnemyName
						  "Cave Beasts",
						  //Reward
						  [0,125,[ArmyData.Army_Bat,6,null,null,null],null]	//Gold,XP
						 ]						  
					 ],		
				 ]
			  ],
			 [
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Minotaur Arena","Description",
				850,170,asd,IconK,
				//Objectives needed
				[[1,1,">"]],	
				//Quests Awarded
				[
				  [
						 "Entry Fee",
						 "Pay",
						 [[10,0,"="]],
						 [90,-1],
						 [
						  "Objective:\nAssemble a large army\nand take back Castle Mortuus.",
						  200,
						  [[10,1,"="]]
						  ]
				  ], 
				 	[
						 "Wargmaster",
						 "Battle",
						 [[10,1,"="]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Tauron,//Army Type
								null,//Level
								"Wargmaster",//Name
								[12,2,5,2],//Level Up Stats
								[SpellData.cSpellChannelRed]//Abilities
							], 		  					  
						  	[
						   		ArmyData.Army_Wolf,//Army Type
								null,//Level
								"Warg",//Name
								[10,5,3,1],//Level Up Stats
								[SpellData.cSpellHowl]//Abilities
							],	  					  
						  	[
						   		ArmyData.Army_Wolf,//Army Type
								null,//Level
								"Warg",//Name
								[10,5,3,1],//Level Up Stats
								[SpellData.cSpellHowl]//Abilities
							], 
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  90,
						  //EnemyName
						  "Champions",
						  //Reward
						  [100,125,null,[10,2,"="]]	//Gold,XP
						 ]						  
					 ],
					 [
						 "Pyromaniac",
						 "Battle",
						 [[10,2,"="]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Minotaur,//Army Type
								null,//Level
								"Pyro",//Name
								[12,1,4,4],//Level Up Stats
								[SpellData.cSpellChannelRed,SpellData.cSpellRainFire,SpellData.cSpellEatFire]//Abilities
							], 		
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  100,
						  //EnemyName
						  "Champions",
						  //Reward
						  [150,125,null,[10,3,"="]]	//Gold,XP
						 ]						  
					 ],
					 [
						 "Rabble Rousers",
						 "Battle",
						 [[10,3,"="]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_RedAxe,//Army Type
								null,//Level
								"Brewmaster",//Name
								[12,3,3,3],//Level Up Stats
								[SpellData.cSpellChannelBlue]//Abilities
							], 					  					  
						  	[
						   		ArmyData.Army_Minotaur,//Army Type
								null,//Level
								"Wrestler",//Name
								[12,3,5,3],//Level Up Stats
								[SpellData.cSpellDrinkAle]//Abilities
							], 			  					  
						  	[
						   		ArmyData.Army_Minotaur,//Army Type
								null,//Level
								"Wrestler",//Name
								[12,3,5,3],//Level Up Stats
								[SpellData.cSpellDrinkAle]//Abilities
							], 		
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  110,
						  //Champions
						  "Champion",
						  //Reward
						  [200,125,null,[10,4,"="]]	//Gold,XP
						 ]						  
					 ],
					 [
						 "The Champion",
						 "Battle",
						 [[10,4,"="]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_MinoGoth,//Army Type
								null,//Level
								"Undead-taur",//Name
								[14,3,3,3],//Level Up Stats
								[SpellData.cSpellDrinkAle,SpellData.cSpellStealLife,SpellData.cSpellEatSkull]//Abilities
							], 	 		
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  120,
						  //EnemyName
						  "Champions",
						  //Reward
						  [400,125,null,[10,5,"="]]	//Gold,XP
						 ]						  
					 ],	
					 [
						 "Hire Minotaurs",
						 "UnitStore",
						 [[10,6,"="]],//variable requirements
						 [-1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						   		[ArmyData.Army_Minotaur],//Army Types
								[5,10],//Levels
								1,//MultiplierCost	
								UnitNaming.GenericEvil//Naming Sets
						 ]						  
					 ],
				 ]
			  ],
			  [
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Gharedlun","Description",
				500,220,asd,IconF,
				//Objectives needed
				[[1,1,">"]],	
				//Quests Awarded
				[
				   [
						 "Train In Gharedlun",
						 "RandomBattle",
						 [[1,2,">"]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		4,//Max Armies
						  		2,//Min Armies
								[ArmyData.Army_Skeleton,ArmyData.Army_Zombie,ArmyData.Army_Necromancer,ArmyData.Army_DoomKnight],//Army Types
								0
						   ],						  
						  [     
						  ],
						  80,
						  "Training!",
						  [0,200,null,null],
						  ]
					],
					 [
						 "Assault on Gharedlun",
						 "Battle",
						 [[11,1,"="],[12,1,"="]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_ArchLiche,//Army Type
								null,//Level
								"Lord Gwendelon",//Name
								[14,2,4,2],//Level Up Stats
								[SpellData.cSpellNecroPlague,SpellData.cSpellRaiseDead]//Abilities
							], 					  					  
						  	[
						   		ArmyData.Army_DoomKnight,//Army Type
								null,//Level
								"Undead Paladin",//Name
								[11,2,3,4],//Level Up Stats
								[SpellData.cSpellLesserHeal]//Abilities
							], 					  					  
						  	[
						   		ArmyData.Army_DoomKnight,//Army Type
								null,//Level
								"Undead Paladin",//Name
								[11,2,3,4],//Level Up Stats
								[SpellData.cSpellLesserHeal]//Abilities
							], 		 	 		
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  150,
						  //EnemyName
						  "Gharedlun Defense",
						  //Reward
						  [400,125,null,[1,3,"="]]	//Gold,XP
						 ]						  
					 ],
					 [
						 "Thieves' Guild",
						 "UnitStore",
						 [],//variable requirements
						 [100,-1],//level requirements: Min, Max
						 //MissionData
						 [
						   		[ArmyData.Army_Thief],//Army Types
								[5,10],//Levels
								1,//MultiplierCost	
								UnitNaming.GenericEvil//Naming Sets
						 ]						  
					 ],
				 ]
			  ],
			  [
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Harvest Fields","Description",
				520,270,asd,IconC,
				//Objectives needed
				[[1,1,">"]],	
				//Quests Awarded
				[
					 [
						 "Burn the Fields",
						 "Battle",
						 [[11,0,"="]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Guard,//Army Type
								null,//Level
								"Conscript",//Name
								[13,1,3,3],//Level Up Stats
								[SpellData.cSpellBless,SpellData.cSpellIronWall]//Abilities
							], 			  					  
						  	[
						   		ArmyData.Army_Wolf,//Army Type
								null,//Level
								"Guard Dog",//Name
								[11,3,2,1],//Level Up Stats
								[SpellData.cSpellHowl,SpellData.cSpellSwiftness]//Abilities
							], 		  					  
						  	[
						   		ArmyData.Army_Wolf,//Army Type
								null,//Level
								"Guard Dog",//Name
								[11,3,2,1],//Level Up Stats
								[SpellData.cSpellHowl,SpellData.cSpellSwiftness]//Abilities
							], 	 		
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  140,
						  //EnemyName
						  "Peasants",
						  //Reward
						  [400,125,null,[11,1,"="]]	//Gold,XP
						 ]						  
					 ],	
					 [
						 "Thief!",
						 "Battle",
						 [],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Thief,//Army Type
								110,//Level
								null,//Name
								null,//Level Up Stats
								null//Abilities
							], 		 	 		
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  100,
						  //EnemyName
						  "Thief!",
						  //Reward
						  [400,125,null,null]	//Gold,XP
						 ]						  
					 ],
				 ]
			  ],
			  [
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Morlok","Description",
				600,240,asd,IconA,
				//Objectives needed
				[[1,1,">"]],	
				//Quests Awarded
				[
					 [
						 "Troll Under The Bridge",
						 "Battle",
						 [],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Troll,//Army Type
								110,//Level
								null,//Name
								null,//Level Up Stats
								null//Abilities
							], 		 	 		
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  100,
						  //EnemyName
						  "The Troll",
						  //Reward
						  [400,125,null,null]	//Gold,XP
						 ]						  
					 ],	
					 [
						 "Bash the Bridge",
						 "Battle",
						 [[12,0,"="]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Guard,//Army Type
								null,//Level
								"Bridge Guard",//Name
								[14,1,4,1],//Level Up Stats
								[SpellData.cSpellTaunt,SpellData.cSpellBless]//Abilities
							], 			  					  
						  	[
						   		ArmyData.Army_Guard,//Army Type
								null,//Level
								"Bridge Guard",//Name
								[12,4,1,3],//Level Up Stats
								[SpellData.cSpellRest,SpellData.cSpellTowerShield]//Abilities
							], 		 	 		
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  140,
						  //EnemyName
						  "Guards",
						  //Reward
						  [400,125,null,[12,1,"="]]	//Gold,XP
						 ]						  
					 ],	
				 ]
			  ],
			 [
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Gaping Maw of Doom","Description",
				380,250,asd,IconI,
				//Objectives needed
				[[1,1,">"]],	
				//Quests Awarded
				[						 
				 	[
						 "Battle the Guardian",
						 "Battle",
						 [[8,0,"="]],//variable requirements
						 [50,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_SandWorm,//Army Type
								50,//Level
								"Deathworm Head",//Name
								null,//Level Up Stats
								[SpellData.cSpellScavenge],//Abilities
								"Synergy" //Passive
							], 				  					  
						  	[
						   		ArmyData.Army_SandWorm,//Army Type
								50,//Level
								"Deathworm Head",//Name
								null,//Level Up Stats
								[SpellData.cSpellChannelGreen],//Abilities
								"Synergy" //Passive
							], 
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  80,
						  //EnemyName
						  "Two Headed Deathworm",
						  //Reward
						  [200,225,null,[8,1,"="]]	//Gold,XP
						 ]						  
					 ],		 
				 	[
						 "Capture a Worm",
						 "Battle",
						 [[8,1,"="]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_SandWorm,//Army Type
								null,//Level
								null,//Name
								null,//Level Up Stats
								[SpellData.cSpellBurrow,SpellData.cSpellEatEarth,SpellData.cSpellRegenerate]//Abilities
							], 
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  80,
						  //EnemyName
						  "Cave Guardian",
						  //Reward
						  [0,125,[ArmyData.Army_SandWorm,9,null,null,null],null]	//Gold,XP
						 ]						  
					 ],
				 ]
			  ],	
			 [ 
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Giant Tribe","Description",
				880,250,asd,IconH,
				//Objectives needed
				[[1,1,">"]],	
				//Quests Awarded
				[
				 [
						 "War against the Giants",
						 "RandomBattle",
						 [],//variable requirements
						 [80,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		3,//Max Armies
						  		1,//Min Armies
								[ArmyData.Army_HillGiant],//Army Types
								0
						   ],						  
						  [     
						  ],
						  100,
						  "Bloodmaws",
						  [50,50,null,[15,1,"+"]],
						  ]
					],				 
				 	[
						 "Attack the Tyrant",
						 "Battle",
						 [[15,2,">"],[16,0,"="]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_FireGiant,//Army Type
								null,//Level
								"Aiur the Firely",//Name
								null,//Level Up Stats
								[SpellData.cSpellFireBlast,SpellData.cSpellIgnite]//Abilities
							],	  					  
						  	[
						   		ArmyData.Army_FrostGiant,//Army Type
								null,//Level
								"Durr the Cold",//Name
								null,//Level Up Stats
								[SpellData.cSpellStoneSkin,SpellData.cSpellDrinkAle,SpellData.cSpellIceFloe]//Abilities
							], 
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  60,
						  //EnemyName
						  "Wilderness",
						  //Reward
						  [0,125,null,[16,1,"="]]	//Gold,XP
						 ]						  
					 ],
					 [
						 "Hill Giant Mercenaries",
						 "UnitStore",
						 [[16,1,"="]],//variable requirements
						 [-1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						   		[ArmyData.Army_HillGiant],//Army Types
								[6,12],//Levels
								2,//MultiplierCost	
								UnitNaming.GenericEvil//Naming Sets
						 ]						  
					 ],	
				 ]
			  ],
			 
			[
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Bloodmaw Tribe","Description",
				700,280,asd,IconC,
				//Objectives needed
				[[1,1,">"]],	
				//Quests Awarded
				[
				   [
						 "Battle Orcs",
						 "RandomBattle",
						 [],//variable requirements
						 [20,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		3,//Max Armies
						  		2,//Min Armies
								[ArmyData.Army_Goblin,ArmyData.Army_Orc,ArmyData.Army_Orc,ArmyData.Army_Orc,ArmyData.Army_Wolf],//Army Types
								0
						   ],						  
						  [     
						  ],
						  60,
						  "Bloodmaws",
						  [50,50,null,[4,1,"+"]],
						  ]
					],						 
				 	[
						 "Challange the Chief",
						 "Battle",
						 [[7,0,"="]],//variable requirements
						 [100,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_OrcWarrior,//Army Type
								null,//Level
								"Chief Garond",//Name
								null,//Level Up Stats
								[SpellData.cSpellHeavyBlow,SpellData.cSpellChannelRed,SpellData.cSpellRainFire]//Abilities
							], 
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  100,
						  //EnemyName
						  "Bloodmaws",
						  //Reward
						  [0,125,null,[7,1,"="]]	//Gold,XP
						 ]						  
					 ],		
					 [
						 "Hire Orcs",
						 "UnitStore",
						 [[7,1,"="]],//variable requirements
						 [-1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						   		[ArmyData.Army_Orc],//Army Types
								[6,12],//Levels
								1,//MultiplierCost	
								UnitNaming.GenericEvil//Naming Sets
						 ]						  
					 ],
				 ]
			  ],	
			 [ 
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Centaur Camp","Description",
				800,350,asd,IconN,
				//Objectives needed
				[[1,1,">"]],	
				//Quests Awarded
				[
				   [
						 "Assault the Centaurs",
						 "RandomBattle",
						 [],//variable requirements
						 [80,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		4,//Max Armies
						  		2,//Min Armies
								[ArmyData.Army_Centaur],//Army Types
								0//MinLevel
						   ],						  
						  [     
						  ],
						  100,
						  "Bloodmaws",
						  [50,50,null,[13,1,"+"]],
						  ]
					],						 
				 	[
						 "Challenge the Chief",
						 "Battle",
						 [[13,2,">"],[14,0,"="]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Centaur,//Army Type
								null,//Level
								"Centaur Chieftain",//Name
								[12,3,5,1],//Level Up Stats
								[SpellData.cSpellChannelWhite,SpellData.cSpellSavageRoar,SpellData.cSpellTriadOfSkulls]//Abilities
							], 		  					  
						  	[
						   		ArmyData.Army_Centaur,//Army Type
								null,//Level
								"Centaur Guard",//Name
								[10,5,3,1],//Level Up Stats
								[]//Abilities
							],  	  					  
						  	[
						   		ArmyData.Army_Centaur,//Army Type
								null,//Level
								"Centaur Guard",//Name
								[10,5,3,1],//Level Up Stats
								[]//Abilities
							],  	  					  
						  	[
						   		ArmyData.Army_Centaur,//Army Type
								null,//Level
								"Centaur Guard",//Name
								[10,5,3,1],//Level Up Stats
								[]//Abilities
							],   	
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  60,
						  //EnemyName
						  "Wilderness",
						  //Reward
						  [0,125,null,[14,1,"="]]	//Gold,XP
						 ]						  
					 ],
					 [
						 "Hire Centaurs",
						 "UnitStore",
						 [[14,1,"="]],//variable requirements
						 [-1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						   		[ArmyData.Army_Centaur],//Army Types
								[5,10],//Levels
								3,//MultiplierCost	
								UnitNaming.GenericEvil//Naming Sets
						 ]						  
					 ],	
				 ]
			  ],	
			 [ 
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Ddwarf Capital","Description",
				270,350,asd,IconJ,
				//Objectives needed
				[[1,1,">"]],	
				//Quests Awarded
				[					 
				 	[
						 "Assault the Demon Army",
						 "Battle",
						 [[17,0,"="]],//variable requirements
						 [100,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Imp,//Army Type
								null,//Level
								null,//Name
								null,//Level Up Stats
								[SpellData.cSpellTrickMana,SpellData.cSpellCurse]//Abilities
							], 	 		  					  
						  	[
						   		ArmyData.Army_Imp,//Army Type
								null,//Level
								null,//Name
								null,//Level Up Stats
								[SpellData.cSpellTrickMana,SpellData.cSpellFireBall]//Abilities
							], 	 		  					  
						  	[
						   		ArmyData.Army_Imp,//Army Type
								null,//Level
								null,//Name
								null,//Level Up Stats
								[SpellData.cSpellTrickMana,SpellData.cSpellGust]//Abilities
							], 	 	
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  135,
						  //EnemyName
						  "Wilderness",
						  //Reward
						  [0,125,null,[15,1,"="]]	//Gold,XP
						 ]						  
					 ],
					 [
						 "Dark Foundry",
						 "UnitStore",
						 [[15,0,">"],[17,4,"<"]],//variable requirements
						 [-1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						   		[ArmyData.Army_DarkDwarf],//Army Types
								[5,10],//Levels
								3,//MultiplierCost	
								UnitNaming.GenericEvil//Naming Sets
						 ]						  
					 ],	
					 [
						 "Dark Foundry",
						 "UnitStore",
						 [[17,4,">="]],//variable requirements
						 [-1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						   		[ArmyData.Army_DarkDwarf,ArmyData.Army_IronGolem],//Army Types
								[10,15],//Levels
								3,//MultiplierCost	
								UnitNaming.GenericEvil//Naming Sets
						 ]						  
					 ],	
				   [
						 "Workers Revolt",
						 "RandomBattle",
						 [[15,0,">"]],//variable requirements
						 [100,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		4,//Max Armies
						  		3,//Min Armies
								[ArmyData.Army_DarkDwarf],//Army Types
								0
						   ],						  
						  [],
						  120,
						  "Workers",
						  [50,50,null,null],
						  ]
					],
				 ]
			  ],
			 [ 
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Ddwarf City ruins","Description",
				200,410,asd,IconO,
				//Objectives needed
				[[17,0,">"]],	
				//Quests Awarded
				[
				 	[
						 "Firebat Swarm",
						 "Battle",
						 [[17,1,"="]],//variable requirements
						 [100,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Bat,//Army Type
								null,//Level
								"Firebat",//Name
								[10,4,4,2],//Level Up Stats
								[SpellData.cSpellFireAvatar,SpellData.cSpellFireBall,SpellData.cSpellEatFire],//Abilities
								"GenerateRed"
							], 	 		  					  
						  	[
						   		ArmyData.Army_Bat,//Army Type
								null,//Level
								"Firebat",//Name
								[10,4,4,2],//Level Up Stats
								[SpellData.cSpellFireAvatar,SpellData.cSpellFireBall,SpellData.cSpellEatFire],//Abilities
								"GenerateRed"
							], 	 		  					  
						  	[
						   		ArmyData.Army_Bat,//Army Type
								null,//Level
								"Firebat",//Name
								[10,4,4,2],//Level Up Stats
								[SpellData.cSpellFireAvatar,SpellData.cSpellFireBall,SpellData.cSpellEatFire],//Abilities
								"GenerateRed"
							], 	 	
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  140,
						  //EnemyName
						  "Wilderness",
						  //Reward
						  [0,125,null,[17,2,"="]]	//Gold,XP
						 ]						  
					 ],
					 [
						 "Attack Firebats",
						 "Battle",
						 [[17,1,">"]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Bat,//Army Type
								null,//Level
								"Firebat",//Name
								[10,4,4,2],//Level Up Stats
								[SpellData.cSpellFireAvatar,SpellData.cSpellFireBall,SpellData.cSpellEatFire],//Abilities
								"GenerateRed"
							], 	 		  					  
						  	[
						   		ArmyData.Army_Bat,//Army Type
								null,//Level
								"Firebat",//Name
								[10,4,4,2],//Level Up Stats
								[SpellData.cSpellFireAvatar,SpellData.cSpellFireBall,SpellData.cSpellEatFire],//Abilities
								"GenerateRed"
							], 	 		  					  
						  	[
						   		ArmyData.Army_Bat,//Army Type
								null,//Level
								"Firebat",//Name
								[10,4,4,2],//Level Up Stats
								[SpellData.cSpellFireAvatar,SpellData.cSpellFireBall,SpellData.cSpellEatFire],//Abilities
								"GenerateRed"
							], 	 	
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  140,
						  //EnemyName
						  "Wilderness",
						  //Reward
						  [0,125,null,null]	//Gold,XP
						 ]						  
					 ],
				 ]
			  ],
			 [ 
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Ddwarf Tomb","Description",
				185,240,asd,IconP,
				//Objectives needed
				[[17,1,">"]],	
				//Quests Awarded
				[
					 [
						 "Enter the Temple",
						 "Battle",
						 [[17,2,"="]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_IronGolem,//Army Type
								null,//Level
								"Collosus",//Name
								[10,2,6,4],//Level Up Stats
								[SpellData.cSpellFireBomb,SpellData.cSpellReinforce,SpellData.cSpellBodySlam],//Abilities
							], 	 		 	
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  145,
						  //EnemyName
						  "The Guardian",
						  //Reward
						  [0,125,null,[17,3,"="]]	//Gold,XP
						 ]						  
					 ],
					 [
						 "Confront the Witches",
						 "Battle",
						 [[17,3,"="]],//variable requirements
						 [1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Spiderpriestess,//Army Type
								null,//Level
								"Demoness",//Name
								null,//Level Up Stats
								[SpellData.cSpellFireBall,SpellData.cSpellRainFire],//Abilities
								"Synergy"
							], 			  					  
						  	[
						   		ArmyData.Army_Spiderpriestess,//Army Type
								null,//Level
								"Demoness",//Name
								null,//Level Up Stats
								[SpellData.cSpellChannelRed,SpellData.cSpellEatFire],//Abilities
								"Synergy"
							], 	 		 	
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  160,
						  //EnemyName
						  "Boss",
						  //Reward
						  [0,125,null,[17,4,"="]]	//Gold,XP
						 ]						  
					 ],
				 ]
			  ],
			 [ 
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Barb Camp","Description",
				75,600,asd,IconJ,
				//Objectives needed
				[[1,2,">"]],	
				//Quests Awarded
				[
				  [
						 "Monster Hunter",
						 "Info",
						 [[18,2,"<"]],
						 [-1,-1],
						 "Hunt Monsters."
				  ],
					 [
						 "Hire Warrior",
						 "UnitStore",
						 [[14,1,"="]],//variable requirements
						 [-1,-1],//level requirements: Min, Max
						 //MissionData
						 [
						   		[ArmyData.Army_HumanWarrior],//Army Types
								[5,10],//Levels
								3,//MultiplierCost	
								UnitNaming.KnightFemale//Naming Sets
						 ]						  
					 ],	
					 [
						 "Hydra",
						 "Battle",
						 [[18,3,"="]],//variable requirements
						 [170,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_DragonHydra,//Army Type
								null,//Level
								"Hydra Body",//Name
								[12,0,6,3],//Level Up Stats
								[SpellData.cSpellTriadOfSkulls,SpellData.cSpellChannelRed,SpellData.cSpellChannelWhite],//Abilities
								"Synergy"
							],					  					  
						  	[
						   		ArmyData.Army_SandWorm,//Army Type
								null,//Level
								"Hydra Head",//Name
								[8,5,3,0],//Level Up Stats
								[],//Abilities
								"Synergy"
							], 				  					  
						  	[
						   		ArmyData.Army_SandWorm,//Army Type
								null,//Level
								"Hydra Head",//Name
								[8,5,3,0],//Level Up Stats
								[],//Abilities
								"Synergy"
							], 				  					  
						  	[
						   		ArmyData.Army_SandWorm,//Army Type
								null,//Level
								"Hydra Head",//Name
								[8,5,3,0],//Level Up Stats
								[],//Abilities
								"Synergy"
							], 			 	
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  180,
						  //EnemyName
						  "Boss",
						  //Reward
						  [0,125,null,[18,4,"="]]	//Gold,XP
						 ]						  
					 ],
				 ]
			  ],
			 [ 
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Wasp Cave","Description",
				180,680,asd,IconI,
				//Objectives needed
				[[18,0,">"]],	
				//Quests Awarded
				[	
				   [
						 "Into the Nest",
						 "RandomBattle",
						 [],//variable requirements
						 [140,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		4,//Max Armies
						  		2,//Min Armies
								[ArmyData.Army_Wasp],//Army Types
								0
						   ],						  
						  [],
						  160,
						  "Workers",
						  [50,50,null,null],
						  ]
					],
				   [
						 "Capture Wasp",
						 "RandomBattle",
						 [[18,3,">"]],//variable requirements
						 [175,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		4,//Max Armies
						  		2,//Min Armies
								[ArmyData.Army_Wasp],//Army Types
								0
						   ],						  
						  [],
						  175,
						  "Workers",
						  [50,50,[ArmyData.Army_Wasp,//Army Type
								5,//Level
								UnitNaming.Beast,//Name
								null,//Level Up Stats
								null//Abilities
							],null],
						  ]
					],
					 [
						 "Wasp Queen",
						 "Battle",
						 [[18,1,"="]],//variable requirements
						 [150,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Wasp,//Army Type
								170,//Level
								"Wasp Queen",//Name
								[10,5,2,3],//Level Up Stats
								[SpellData.cSpellSandStorm,SpellData.cSpellSwarmAttack,SpellData.cSpellSwarmDeffend,SpellData.cSpellThunder],//Abilities
							],					  					  
						  	[
						   		ArmyData.Army_Wasp,//Army Type
								170,//Level
								"Drone",//Name
								[1,0,1,0],//Level Up Stats
								[],//Abilities
								"HabitatYellow"
							],				  					  
						  	[
						   		ArmyData.Army_Wasp,//Army Type
								170,//Level
								"Drone",//Name
								[1,0,1,0],//Level Up Stats
								[],//Abilities
								"HabitatYellow"
							],				  					  
						  	[
						   		ArmyData.Army_Wasp,//Army Type
								170,//Level
								"Drone",//Name
								[1,0,1,0],//Level Up Stats
								[],//Abilities
								"HabitatYellow"
							], 						 	
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  170,
						  //EnemyName
						  "Boss",
						  //Reward
						  [0,125,null,[18,2,"="]]	//Gold,XP
						 ]						  
					 ],
				 ]
			  ],
			 [ 
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Harpy Desert Shrine","Description",
				100,380,asd,IconL,
				//Objectives needed
				[[18,1,">"]],	
				//Quests Awarded
				[
				   [
						 "Harpy Carrion",
						 "RandomBattle",
						 [],//variable requirements
						 [140,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		4,//Max Armies
						  		2,//Min Armies
								[ArmyData.Army_Harpy],//Army Types
								0
						   ],						  
						  [],
						  160,
						  "Workers",
						  [50,50,null,null],
						  ]
					],
				   [
						 "Capture Harpy",
						 "RandomBattle",
						 [[18,3,">"]],//variable requirements
						 [175,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		4,//Max Armies
						  		2,//Min Armies
								[ArmyData.Army_Harpy],//Army Types
								0
						   ],						  
						  [],
						  175,
						  "Workers",
						  [50,50,[ArmyData.Army_Harpy,//Army Type
								5,//Level
								UnitNaming.Beast,//Name
								null,//Level Up Stats
								null//Abilities
							],null],
						  ]
					],
					 [
						 "Crone Sisters",
						 "Battle",
						 [[18,2,"="]],//variable requirements
						 [150,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Harpy,//Army Type
								null,//Level
								"Deino",//Name
								null,//Level Up Stats
								[SpellData.cSpellNecroPlague,SpellData.cSpellWindBlast],//Abilities
							], 			  					  
						  	[
						   		ArmyData.Army_Harpy,//Army Type
								null,//Level
								"Enyo",//Name
								null,//Level Up Stats
								[SpellData.cSpellEatStar,SpellData.cSpellCurse],//Abilities
							], 		  					  
						  	[
						   		ArmyData.Army_Harpy,//Army Type
								null,//Level
								"Pemphredo",//Name
								null,//Level Up Stats
								[SpellData.cSpellGust,SpellData.cSpellScare],//Abilities
							], 	 		 	
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  170,
						  //EnemyName
						  "Boss",
						  //Reward
						  [0,125,null,[18,3,"="]]	//Gold,XP
						 ]						  
					 ],
				]
			  ],
			 [ 
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Scorpion Cave","Description",
				80,750,asd,IconD,
				//Objectives needed
				[[1,2,">"]],	
				//Quests Awarded
				[
				   [
						 "Scorpions!",
						 "RandomBattle",
						 [],//variable requirements
						 [140,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		4,//Max Armies
						  		2,//Min Armies
								[ArmyData.Army_Scorpion],//Army Types
								0
						   ],						  
						  [],
						  160,
						  "Workers",
						  [50,50,null,null],
						  ]
					],
				   [
						 "Capture Scorpion",
						 "RandomBattle",
						 [[18,3,">"]],//variable requirements
						 [175,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		4,//Max Armies
						  		2,//Min Armies
								[ArmyData.Army_Scorpion],//Army Types
								0
						   ],						  
						  [],
						  175,
						  "Workers",
						  [50,50,[ArmyData.Army_Scorpion,//Army Type
								5,//Level
								UnitNaming.Beast,//Name
								null,//Level Up Stats
								null//Abilities
							],null],
						  ]
					],
					 [
						 "King Scorpion",
						 "Battle",
						 [[18,0,"="]],//variable requirements
						 [150,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Scorpion,//Army Type
								null,//Level
								"King Scorpion",//Name
								null,//Level Up Stats
								[SpellData.cSpellInjectPoison,SpellData.cSpellToxic,SpellData.cSpellChannelGreen],//Abilities
							], 			 		 	
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  170,
						  //EnemyName
						  "Boss",
						  //Reward
						  [0,125,null,[18,1,"="]]	//Gold,XP
						 ]						  
					 ],
				]
			  ],	
			 [ 
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Fey Forest","Description",
				240,500,asd,IconQ,
				//Objectives needed
				[[1,2,">"]],	
				//Quests Awarded
				[
				 [
						 "Deeper into the Woods",
						 "RandomBattle",
						 [],//variable requirements
						 [100,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [
						  		4,//Max Armies
						  		3,//Min Armies
								[ArmyData.Army_Sprite,ArmyData.Army_Sprite,ArmyData.Army_Sprite,ArmyData.Army_HillGiant,ArmyData.Army_Centaur,ArmyData.Army_Centaur,ArmyData.Army_Sprite,ArmyData.Army_Sprite],//Army Types
								0
						   ],						  
						  [     
						  ],
						  120,
						  "Cave Beasts",
						  [50,50,null,null],
						  ]
					],				 
				 	[
						 "Capture a Sprite",
						 "Battle",
						 [[7,0,"="]],//variable requirements
						 [175,-1],//level requirements: Min, Max
						 //MissionData
						 [
						  //Enemy Armies
						   [						  					  
						  	[
						   		ArmyData.Army_Sprite,//Army Type
								null,//Level
								null,//Name
								null,//Level Up Stats
								[SpellData.cSpellSleep,SpellData.cSpellNightmare,SpellData.cSpellTrickShot]//Abilities
							], 				  					  
						  	[
						   		ArmyData.Army_Sprite,//Army Type
								null,//Level
								null,//Name
								null,//Level Up Stats
								[SpellData.cSpellSleep,SpellData.cSpellNightmare,SpellData.cSpellTrickShot]//Abilities
							], 				  					  
						  	[
						   		ArmyData.Army_Sprite,//Army Type
								null,//Level
								null,//Name
								null,//Level Up Stats
								[SpellData.cSpellSleep,SpellData.cSpellNightmare,SpellData.cSpellTrickShot]//Abilities
							], 				  					  
						  	[
						   		ArmyData.Army_Sprite,//Army Type
								null,//Level
								null,//Name
								null,//Level Up Stats
								[SpellData.cSpellSleep,SpellData.cSpellNightmare,SpellData.cSpellTrickShot]//Abilities
							], 
						   ],
						  //Temp Allied armies (if available)
						  [],
						  //MinLevel
						  190,
						  //EnemyName
						  "Cave Beasts",
						  //Reward
						  [0,125,[ArmyData.Army_Sprite,6,null,null,null],null]	//Gold,XP
						 ]						  
					 ],	
				 ]
			  ],	
			 [ 
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Port","Description",
				300,900,asd,IconM,
				//Objectives needed
				[],	
				//Quests Awarded
				[
				 ]
			  ],	
			 [ 
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Sunder Isle","Description",
				500,1000,asd,IconL,
				//Objectives needed
				[],	
				//Quests Awarded
				[
				 ]
			  ],	
			 [ 
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Selentia","Description",
				650,900,asd,IconJ,
				//Objectives needed
				[],	
				//Quests Awarded
				[
				 ]
			  ],	
			 [
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Minotaur Shrine","Description",
				950,145,asd,IconL,
				//Objectives needed
				[],	
				//Quests Awarded
				[
				 ]
			  ],
			 [
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Dragon Fortress","Description",
				1030,300,asd,IconM,
				//Objectives needed
				[],	
				//Quests Awarded
				[
				 ]
			  ],
			 [
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Ogre Arena","Description",
				1000,430,asd,IconK,
				//Objectives needed
				[],	
				//Quests Awarded
				[
				 ]
			  ],	
			 [
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Orc Tribe 1","Description",
				1100,520,asd,IconH,
				//Objectives needed
				[],	
				//Quests Awarded
				[
				 ]
			  ],	
			 [
			 //Name, X, Y, Picture, MiniMapIcon
			 	"Orc Tribe 2","Description",
				1160,680,asd,IconC,
				//Objectives needed
				[],	
				//Quests Awarded
				[
				 ]
			  ],	
		 ];
		 
		public static function GetCombinedPartyLevel(game:HubWorldHandler)
		{
			var Total=0;
			for (var Zim=0; Zim<game.Memory["Data"].Armies.length; Zim++)
			{
				if (game.Memory["Data"].Armies[Zim]!=null)
				{
				Total+=game.Memory["Data"].Armies[Zim].Level;
				}
			}
			return Total;
		}
		 		 
		public static function DoIMeetConditions(game:HubWorldHandler,Conditions:Array):Boolean
		{
			var Met=0;
			var Req=0;
			for (var Gir=0; Gir<Conditions.length; Gir++)
				{
					Req++;
					var Condition=Conditions[Gir][0];
					var Value=Conditions[Gir][1];
					var Type=Conditions[Gir][2];
						if (game.Memory["Data"].ProgressMade[Condition]!=null && (
						(game.Memory["Data"].ProgressMade[Condition]>Value && Type==">") ||
						(game.Memory["Data"].ProgressMade[Condition]>=Value && Type==">=") ||
						(game.Memory["Data"].ProgressMade[Condition]<Value && Type=="<") ||
						(game.Memory["Data"].ProgressMade[Condition]<=Value && Type=="<=") ||
						(game.Memory["Data"].ProgressMade[Condition]==Value && Type=="=") ||
						(game.Memory["Data"].ProgressMade[Condition]!=Value && Type=="!=") ||
						(game.Memory["Data"].ProgressMade[Condition]==game.Memory["Data"].ProgressMade[Value] && Type=="==")))
						{
							Met++;
						}
				}
				
				
				if (Met>=Req)
				{
					return true;
				}
				else {
					return false;
				}
		}

	}
	
}
