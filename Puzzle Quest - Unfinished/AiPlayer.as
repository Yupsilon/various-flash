﻿package  {
	import flash.filters.DisplacementMapFilter;
	
	public class AiPlayer {
		
		public static var PossibleMoves=[[[1,0],[3,0],[[2,0],[3,0]]],//1101
										  [[0,1],[0,3],[[0,2],[0,3]]],
										  
										  [[2,0],[3,0],[[0,0],[1,0]]],//1011
										  [[0,2],[0,3],[[0,0],[0,1]]],
										  
										  [[0,1],[1,2],[[0,2],[1,2]]],//001
										  [[1,0],[2,1],[[2,0],[2,1]]],//110										  
										  
										  [[1,1],[1,2],[[0,0],[1,0]]],//100
										  [[1,1],[2,1],[[0,0],[0,1]]],//011										  
										  
										  [[1,1],[2,0],[[1,1],[1,0]]],//001
										  [[1,1],[0,2],[[1,1],[0,1]]],//110										  
										  
										  [[1,1],[0,2],[0,3],[[0,1],[1,1]]],//0100
										  [[1,1],[2,0],[3,0],[[1,0],[1,1]]],//1011										  
										  
										  [[0,1],[1,2],[0,3],[[0,2],[1,2]]],//0010
										  [[1,0],[2,1],[0,3],[[2,0],[2,1]]],//1101										  
										  
										  [[0,1],[1,2],[0,3],[0,4],[[0,2],[1,2]]],//00100
										  [[1,0],[2,1],[3,0],[4,0],[[2,0],[2,1]]],//11011										  
										 ]
		
		public static var SpellTypes=[	
										//Attack Spells
										 	[SpellData.cSpellIronFist,SpellData.cSpellBodySlam,SpellData.cSpellWound,SpellData.cSpellShieldCharge,SpellData.cSpellBackStab,SpellData.cSpellHeavyBlow,SpellData.cSpellScratch,SpellData.cSpellRabidBite,SpellData.cSpellVenom,SpellData.cSpellSwoop,SpellData.cSpellShieldBash,SpellData.cSpellAttack],
										//Single Army Spells
											[SpellData.cSpellHex,SpellData.cSpellBloodBlade,SpellData.cSpellPoisonBlade,SpellData.cSpellPlagueBlade,SpellData.cSpellAnthragsKiss,SpellData.cSpellSleep,SpellData.cSpellSandAttack,SpellData.cSpellFreeze,SpellData.cSpellSwarmStrike,SpellData.cSpellStealLife,SpellData.cSpellTapLife,SpellData.cSpellThrowAxe,SpellData.cSpellUltimate,SpellData.cSpellWindElly,SpellData.cSpellEarthElly,SpellData.cSpellWaterElly,SpellData.cSpellFireElly,SpellData.cSpellDarkBolt,SpellData.cSpellTrickShot,SpellData.cSpellEntangle,SpellData.cSpellSilence,SpellData.cSpellCurse,SpellData.cSpellDecieve,SpellData.cSpellThrowBoulder,SpellData.cSpellThrowStone,SpellData.cSpellSkullBash,SpellData.cSpellFireBall,SpellData.cSpellGust,SpellData.cSpellIceFloe,SpellData.cSpellTaunt,SpellData.cSpellWeb,SpellData.cSpellInjectPoison,SpellData.cSpellEcho,SpellData.cSpellScare,SpellData.cSpellWindBlast,SpellData.cSpellBombardment,SpellData.cSpellIceBlast,SpellData.cSpellFireBlast],
										//Friendlies
											[SpellData.cSpellLesserHeal,SpellData.cSpellGreaterHeal,SpellData.cSpellInnerFire,SpellData.cSpellBless],
										//OffensiveSelfBuffs
											[SpellData.cSpellEatStar,SpellData.cSpellWindAvatar,SpellData.cSpellWaterAvatar,SpellData.cSpellFireAvatar,SpellData.cSpellSwarmAttack,SpellData.cSpellReinforce,SpellData.cSpellBerserker,SpellData.cSpellGodlyStrength,SpellData.cSpellStrength,SpellData.cSpellBloodRite,SpellData.cSpellHowl,SpellData.cSpellRevenge,SpellData.cSpellSneak,SpellData.cSpellUnholyRage,SpellData.cSpellSwarmCall],
										//DefensiveSelfBuffs
											[SpellData.cSpellStoneForm,SpellData.cSpellHybernate,SpellData.cSpellIronWall,SpellData.cSpellIronWall,SpellData.cSpellEarthAvatar,SpellData.cSpellSwarmDeffend,SpellData.cSpellTowerShield,SpellData.cSpellStoneSkin,SpellData.cSpellBurrow,SpellData.cSpellMeatShield],
										//DefensiveSelfHeals
											[SpellData.cSpellWallOfFire,SpellData.cSpellEatSkull,SpellData.cSpellRest,SpellData.cSpellDrinkAle,SpellData.cSpellBurrow,SpellData.cSpellRegenerate],
										//NoTarget
											[SpellData.cSpellClearSkulls,SpellData.cSpellBurrial,SpellData.cSpellGraveDigger,SpellData.cSpellGoblinToss,SpellData.cSpellSiege,SpellData.cSpellRaiseDead,SpellData.cSpellChannelYellow,SpellData.cSpellChannelGreen,SpellData.cSpellChannelBlue,SpellData.cSpellChannelRed,SpellData.cSpellChannel,SpellData.cSpellEatFire,SpellData.cSpellEatEarth,SpellData.cSpellTrickMana,SpellData.cSpellDig,SpellData.cSpellIgnite,SpellData.cSpellScavenge,SpellData.cSpellDrainSkulls,SpellData.cSpellStealManaBlue,SpellData.cSpellPoisonExplosion,SpellData.cSpellPlagueExplosion,SpellData.cSpellAvalanche,SpellData.cSpellRainFire,SpellData.cSpellBlight,SpellData.cSpellPlague,SpellData.cSpellReplenish,SpellData.cSpellRainPoison,SpellData.cSpellChannelWhite,SpellData.cSpellSleepPowder,SpellData.cSpellTriadOfSkulls],
										//AnyGem
											[SpellData.cSpellCharge,SpellData.cSpellGemSmash,SpellData.cSpellFireBomb,SpellData.cSpellThunder],
										//Others
											[SpellData.cSpellNightmare,SpellData.cSpellSavageRoar,SpellData.cSpellCleansingFlame,SpellData.cSpellEatSkull,SpellData.cSpellZap,SpellData.cSpellSwiftness,SpellData.cSpellEatStar,SpellData.cSpellNecroPlague,SpellData.cSpellRupture,SpellData.cSpellToxic]
										 ]
										 

		public static function HandleTurn(game:MainGame) {
			if (game.Memory["AiPlayer"]==null)
			{
				Initialize(game);
			}
			else if (CanIMove(game)==true)
			{
				NeedIDefend(game);
				if (Math.random()*100<100*game.Memory["Difficulty"])
				{
					if (ShouldICastSpells(game)==false && NeedIAttack(game)==false)
					{
						AnalyzeBoard(game);
						if (GetNextPossibleMatch(game)==false)
						{					
							DoRandomMove(game);
						}
					}
				}
				else
				{
						AnalyzeBoard(game);
						if (GetRandomMatch(game)==false)
						{
							DoRandomMove(game);
						}
				}
				game.Memory["AiPlayer"]["IdleTime"]=game.Memory["HWH"].Memory["GameTime"].GetTime()+GlobalVariables.AiIdleTime;
			}
		}
		
		public static function CountMyArmies(game:MainGame):Number {
			
			var Gir=0;
			for each (var Zim:DisplayItemArmy in game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]])
			{
				if (Zim.AmIDead(game)==false)
				{
					Gir+=1;
				}
			}
			return Gir;
		}
		
		public static function NeedIDefend(game:MainGame) {
			
			if (CountMyArmies(game)>1 && HandleMana.DoIHaveEnoughMana(game,game.Memory["AiPlayer"]["MyPlayer"],SpellData.SpellBook[SpellData.cSpellDefender][1],false))
			{				
				var Value=[0,-1]
				for (var Zim=0; Zim<game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]].length; Zim++)
				{
					var Army=game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim]
					if (Army.AmIDead(game)==false && Army.CanAct(game)==true)
					{
						
						if (Army.Defender==true)
						{
							Value=null;
							break;
						}
					}
				}
				if (Value!=null)
				{
					for (var Zim=0; Zim<game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]].length; Zim++)
					{
						var Army=game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim]
						if (Army.AmIDead(game)==false && Army.CanAct(game)==true)
						{
							if (Army.Defender==false)						
							{
								var Data=Army.Life*Army.GetDefense(game)/Army.GetAttack(game);
								if (Data>Value[0])
								{
									Value=[Data,Zim];
								}
							}
						}
					}
				}
				if (Value!=null && Value[1]>=0)
				{
					AiCastSpell(game,Value[1],SpellData.cSpellDefender)
				}
			}
		}
		
		public static function NeedIAttack(game:MainGame):Boolean {
			
			if (game.Memory["PlayerMana"][game.Memory["AiPlayer"]["MyPlayer"]][GlobalVariables.GEM_SKULL]>0 && 
				game.Memory["PlayerMana"][game.Memory["AiPlayer"]["MyPlayer"]][GlobalVariables.GEM_PURPLE]>0)
				{
					var Value=[0,-1]
					for (var Zim=0; Zim<game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]].length; Zim++)
					{
						var Army:DisplayItemArmy=game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim]
						if (Army.AmIDead(game)==false && Army.CanAct(game)==true)
						{							
								var Data=Army.GetAttack(game);
								if (Data>Value[0])
								{
									Value=[Data,Zim];
								}							
						}
					}
					if (Value!=null && Value[1]>=0)
					{
						if (game.Memory["PlayerMana"][game.Memory["AiPlayer"]["MyPlayer"]][GlobalVariables.GEM_SKULL]>1 && 
							game.Memory["PlayerMana"][game.Memory["AiPlayer"]["MyPlayer"]][GlobalVariables.GEM_PURPLE]>1 &&
							HandleMana.DoIHaveEnoughMana(game,game.Memory["AiPlayer"]["MyPlayer"],SpellData.SpellBook[SpellData.cSpellSwiftness][1],false) &&
							game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Value[1]].Abilities.indexOf(SpellData.cSpellSwiftness)!=-1)
							{
								AiCastSpell(game,Value[1],SpellData.cSpellSwiftness)
								AiCastSpell(game,Value[1],SpellData.cSpellAttack)
							}
						AiCastSpell(game,Value[1],SpellData.cSpellAttack)
						return true;
					}
					else
					{
						return false;
					}
				}
			else
			{
				return false;
			}
		}
		
		public static function ShouldICastSpells(game:MainGame):Boolean {
			
			for (var Zim=0; Zim<game.Memory["AiPlayer"]["AvailableSpells"].length; Zim++)
			{
				var RND=Math.round(Math.random()*(game.Memory["AiPlayer"]["AvailableSpells"].length-1));
				var NR=game.Memory["AiPlayer"]["AvailableSpells"][Zim];
				game.Memory["AiPlayer"]["AvailableSpells"][Zim]=game.Memory["AiPlayer"]["AvailableSpells"][RND];
				game.Memory["AiPlayer"]["AvailableSpells"][RND]=NR;				
			}
			for (var Zim=0; Zim<game.Memory["AiPlayer"]["AvailableSpells"].length; Zim++)
			{
				if (HandleMana.DoIHaveEnoughMana(game,game.Memory["AiPlayer"]["MyPlayer"],SpellData.SpellBook[game.Memory["AiPlayer"]["AvailableSpells"][Zim]][1],false)==true &&
					(SpellTypes[2].indexOf(game.Memory["AiPlayer"]["AvailableSpells"][Zim])==-1 || game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]].length>1) && 
					(SpellTypes[3].indexOf(game.Memory["AiPlayer"]["AvailableSpells"][Zim])==-1 || HandleMana.GetMana(game,game.Memory["AiPlayer"]["MyPlayer"],GlobalVariables.GEM_SKULL)>=1) 
					&& game.Memory["AiPlayer"]["LastSpellCast"]!=game.Memory["AiPlayer"]["AvailableSpells"][Zim])
					{
						var CapableArmies=new Array()
						for (var Gir=0; Gir<game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]].length; Gir++)
							{
								var Army:DisplayItemArmy=game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Gir];
								for (var ICS=0; ICS<Army.Abilities.length; ICS++)
								{
									if (Army.Abilities[ICS]==game.Memory["AiPlayer"]["AvailableSpells"][Zim] && 
										Army.CanCast(game)==true &&
										(SpellTypes[5].indexOf(game.Memory["AiPlayer"]["AvailableSpells"][Zim])==-1 || 
										 Army.Life<Army.MaxLife/2))
									{
										CapableArmies.push(Army);
									}
								}
							}
							var Army:DisplayItemArmy=CapableArmies[0];
							if (CapableArmies.length>1 && 
								(game.Memory["AiPlayer"]["SpellsCastThisTurn"]==null || 
								 game.Memory["AiPlayer"]["SpellsCastThisTurn"].indexOf(game.Memory["AiPlayer"]["AvailableSpells"][Zim])==null))
							{
								for (Gir=0; Gir<CapableArmies.length; Gir++)
								{
									if (SpellTypes[0].indexOf(game.Memory["AiPlayer"]["AvailableSpells"][Zim])!=-1)
									{
										if (CapableArmies[Gir].Attack>Army.Attack)
										{
											Army=CapableArmies[Gir];
										}
									}
									else if (SpellTypes[4].indexOf(game.Memory["AiPlayer"]["AvailableSpells"][Zim])==-1)
									{
										if (CapableArmies[Gir].Special>Army.Special)
										{
											Army=CapableArmies[Gir];
										}
									}
									else
									{
										if (Math.random()*10>8)
										{
											Army=CapableArmies[Gir];
										}
									}
								}
							}
							if (Army!=null)
							{													
								if (game.Memory["AiPlayer"]["SpellsCastThisTurn"]==null)
								{
									game.Memory["AiPlayer"]["SpellsCastThisTurn"]=new Array();
								}
								game.Memory["AiPlayer"]["SpellsCastThisTurn"].push(game.Memory["AiPlayer"]["AvailableSpells"][Zim])
								
								AiPrepareSpell(game,Army.ID,game.Memory["AiPlayer"]["AvailableSpells"][Zim]);
								game.Memory["AiPlayer"]["LastSpellCast"]=game.Memory["AiPlayer"]["AvailableSpells"][Zim];
								return true;
							}
					}
			}
			return false;
		}
		
		public static function AiPrepareSpell(game:MainGame,Army:Number,Spell:Number) {
			
			var Cast=false;
			var TargetArmy=null;
			if (SpellTypes[8].indexOf(Spell)!=-1)
			{
				if (Spell==SpellData.cSpellSavageRoar && HandleMana.GetMana(game,game.Memory["AiPlayer"]["MyPlayer"],GlobalVariables.GEM_SKULL)>2 &&
					CountMyArmies(game)>2 && CountMyArmies(game)<=HandleMana.GetMana(game,game.Memory["AiPlayer"]["MyPlayer"],GlobalVariables.GEM_SKULL))
				{
					Cast=true;
				}
				else if (Spell==SpellData.cSpellCleansingFlame)
				{
					for (var Zim=0; Zim<game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]].length; Zim++)
					{
						if (game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim].AmIDead()==false &&
							game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim].ID!=Army &&
							game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim].Life>game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Army].GetSpecial()+1 &&
							game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim].Life<game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim].MaxLife/2)
						{
							if (TargetArmy==null ||
								Math.random()*10>5)
								{
									TargetArmy=game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim];
								}
						}
					}
				}
				else if (Spell==SpellData.cSpellZap)
				{					
					for (var Zim=0; Zim<game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]].length; Zim++)
					{
						if (game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim].AmIDead()==false &&
							game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim].ID!=Army &&
							game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim].Life<game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim].MaxLife/2)
						{
							if (TargetArmy==null ||
								game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim].Life<game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][TargetArmy].Life)
								{
									TargetArmy=game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim];
								}
						}
					}
					Cast=true
				}
				else if (Spell==SpellData.cSpellNecroPlague)
				{
					for (var Zim=0; Zim<game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]].length; Zim++)
					{
						if (game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim].AmIDead()==false && 
							game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim].HasModifier(Language.datModifierDisease,game.Memory["CurrentTurn"])==true)
						{
							if (TargetArmy==null ||
								Math.random()*10>8)
								{
									TargetArmy=game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim];
								}
						}
					}
				}
				else if (Spell==SpellData.cSpellNightmare)
				{
					for (var Zim=0; Zim<game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]].length; Zim++)
					{
						if (game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim].AmIDead()==false && 
							game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim].HasModifier(Language.datModifierSleep,game.Memory["CurrentTurn"])==true)
						{
							if (TargetArmy==null ||
								Math.random()*10>8)
								{
									TargetArmy=game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim];
								}
						}
					}
				}
				else if (Spell==SpellData.cSpellRupture)
				{
					for (var Zim=0; Zim<game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]].length; Zim++)
					{
						if (game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim].AmIDead()==false && 
							game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim].HasModifier(Language.datModifierBleed,game.Memory["CurrentTurn"])==true)
						{
							if (TargetArmy==null ||
								Math.random()*10>8)
								{
									TargetArmy=game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim];
								}
						}
					}
				}
				else if (Spell==SpellData.cSpellToxic)
				{
					for (var Zim=0; Zim<game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]].length; Zim++)
					{
						if (game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim].AmIDead()==false && 
							game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim].HasModifier(Language.datModifierPoison,game.Memory["CurrentTurn"])==true)
						{
							if (TargetArmy==null ||
								Math.random()*10>8)
								{
									TargetArmy=game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim];
								}
						}
					}
				}
			}
			else
			{
				Cast=true;
			}
			if (Cast==true)
			{
				AiCastSpell(game,Army,Spell,TargetArmy);
			}
		}
		
		public static function AiCastSpell(game:MainGame,Army:Number,Spell:Number,TargetArmy:DisplayItemArmy=null) {
			
			var SpellInfo=SpellData.SpellBook[Spell];
			cSpellEffect.PrepareSpell(game,Spell,game.Memory["AiPlayer"]["MyPlayer"],Army);			
			
			if (SpellInfo[0]==cSpellEffect.targeting_BasicAttack || SpellInfo[0]==cSpellEffect.targeting_UnitEnemy)
			{
				if (TargetArmy==null)
				{
					for (var Zim=0; Zim<game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]].length; Zim++)
					{
						if (cSpellEffect.AmIValidTarget(game,game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim]))
						{
							if (TargetArmy==null ||
								((TargetArmy.Life*TargetArmy.GetAttack(game)/TargetArmy.GetDefense(game))>(game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim].Life*game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim].GetAttack(game)/game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim].GetDefense(game)) && 
								SpellInfo[0]==cSpellEffect.targeting_BasicAttack) || 
								((TargetArmy.Life*TargetArmy.GetAttack(game)>(game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim].Life*game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim].GetAttack(game)) && SpellInfo[0]!=cSpellEffect.targeting_BasicAttack) ))
								{
									TargetArmy=game.Memory["DisplayItemArmies"][1-game.Memory["AiPlayer"]["MyPlayer"]][Zim];
								}
						}
					}
				}
				cSpellEffect.CastSpellOnArmyTarget(game,TargetArmy);
			}
			else if (SpellInfo[0]==cSpellEffect.targeting_UnitFriendly && CountMyArmies(game)>1)
			{
				if (TargetArmy==null)
				{
					for (var Zim=0; Zim<game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]].length; Zim++)
					{
						if (cSpellEffect.AmIValidTarget(game,game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim]) &&
							game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim].ID!=Army)
						{
							if (TargetArmy==null ||
								Math.random()*10>8)
								{
									TargetArmy=game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]][Zim];
								}
						}
					}
				}
				cSpellEffect.CastSpellOnArmyTarget(game,TargetArmy);
			}
			else if (SpellInfo[0]==cSpellEffect.targeting_GemAny)
			{
				var GemArray=SpellData.PickRandomNrOfGems(game.Memory["Gems"],1,[GlobalVariables.GEM_RED,GlobalVariables.GEM_BLUE,GlobalVariables.GEM_GREEN,GlobalVariables.GEM_YELLOW,GlobalVariables.GEM_PURPLE,GlobalVariables.GEM_SKULL])
				cSpellEffect.CastSpellOnGemTarget(game,game.Memory["Gems"][GemArray[0]]);
			}
			else if (SpellInfo[0]==cSpellEffect.targeting_GemRed)
			{
				var GemArray=SpellData.PickRandomNrOfGems(game.Memory["Gems"],1,[GlobalVariables.GEM_RED])
				cSpellEffect.CastSpellOnGemTarget(game,game.Memory["Gems"][GemArray[0]]);
			}
			else if (SpellInfo[0]==cSpellEffect.targeting_GemBlue)
			{
				var GemArray=SpellData.PickRandomNrOfGems(game.Memory["Gems"],1,[GlobalVariables.GEM_BLUE])
				cSpellEffect.CastSpellOnGemTarget(game,game.Memory["Gems"][GemArray[0]]);
			}
			else if (SpellInfo[0]==cSpellEffect.targeting_GemGreen)
			{
				var GemArray=SpellData.PickRandomNrOfGems(game.Memory["Gems"],1,[GlobalVariables.GEM_GREEN])
				cSpellEffect.CastSpellOnGemTarget(game,game.Memory["Gems"][GemArray[0]]);
			}
			else if (SpellInfo[0]==cSpellEffect.targeting_GemYellow)
			{
				var GemArray=SpellData.PickRandomNrOfGems(game.Memory["Gems"],1,[GlobalVariables.GEM_YELLOW])
				cSpellEffect.CastSpellOnGemTarget(game,game.Memory["Gems"][GemArray[0]]);
			}
			else if (SpellInfo[0]==cSpellEffect.targeting_GemWhite)
			{
				var GemArray=SpellData.PickRandomNrOfGems(game.Memory["Gems"],1,[GlobalVariables.GEM_SKULL])
				cSpellEffect.CastSpellOnGemTarget(game,game.Memory["Gems"][GemArray[0]]);
			}
			else if (SpellInfo[0]==cSpellEffect.targeting_GemPurple)
			{
				var GemArray=SpellData.PickRandomNrOfGems(game.Memory["Gems"],1,[GlobalVariables.GEM_PURPLE])
				cSpellEffect.CastSpellOnGemTarget(game,game.Memory["Gems"][GemArray[0]]);
			}	
			
		}
		
		public static function CanIMove(game:MainGame):Boolean {
			return (game.Memory["AiPlayer"]["IdleTime"]<game.Memory["HWH"].Memory["GameTime"].GetTime() && game.Memory["PlayerMana"][game.Memory["AiPlayer"]["MyPlayer"]][GlobalVariables.GEM_PURPLE]>0 && game.Memory["AiPlayer"]["MyPlayer"]==game.Memory["PlayerTurn"] && game.Memory["GemsMoving"]==false)
		}
		
		public static function GetIdleTime(game:MainGame):Boolean {
			return (game.Memory["PlayerMana"][game.Memory["AiPlayer"]["MyPlayer"]][GlobalVariables.GEM_PURPLE]>0 && game.Memory["AiPlayer"]["MyPlayer"]==game.Memory["PlayerTurn"] && game.Memory["GemsMoving"]==false)
		}
		
		public static function Initialize(game:MainGame) {
			game.Memory["AiPlayer"]=new Function();
			game.Memory["AiPlayer"]["MyPlayer"]=1;
			game.Memory["AiPlayer"]["IdleTime"]=0;
			game.Memory["AiPlayer"]["ManaPriorities"]=[0,0,0,0,0,0];
			game.Memory["AiPlayer"]["AvailableSpells"]=new Array();
			for each (var Army:DisplayItemArmy in game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]])
			{
				for each (var Power in Army.Abilities)
				{
					for (var Zim=0; Zim<SpellData.SpellBook[Power][1].length-1; Zim++)
					{
						game.Memory["AiPlayer"]["ManaPriorities"][Zim]+=SpellData.SpellBook[Power][1][Zim];
					}
					game.Memory["AiPlayer"]["AvailableSpells"].push(Power);
				}
			}
			game.Memory["AiPlayer"]["ManaPriorities"][4]=60/game.Memory["DisplayItemArmies"][game.Memory["AiPlayer"]["MyPlayer"]].length;
			game.Memory["AiPlayer"]["ManaPriorities"][5]=9999;
			game.Memory["AiPlayer"]["LastSpellCast"]=-1;
		}
		
		public static function GetAiHighestPriority(game:MainGame,IncludePurple:Boolean=true):Number {
			
			var CurrentCosts=new Array();
			var OrderedCosts=new Array();
			var Max=game.Memory["AiPlayer"]["ManaPriorities"].length;
			if (IncludePurple==true)
			{
				Max=GlobalVariables.GEM_PURPLE;
			}
			
			for (var iCost=0; iCost<Max; iCost++)
				{
					CurrentCosts.push(game.Memory["AiPlayer"]["ManaPriorities"][iCost]/Math.max(1,game.Memory["PlayerMana"][game.Memory["AiPlayer"]["MyPlayer"]][iCost]));
					OrderedCosts.push(game.Memory["AiPlayer"]["ManaPriorities"][iCost]/Math.max(1,game.Memory["PlayerMana"][game.Memory["AiPlayer"]["MyPlayer"]][iCost]));
				}
			OrderedCosts.sort(Array.NUMERIC);
			
			return CurrentCosts.indexOf(OrderedCosts[OrderedCosts.length-1]);
		}
		
		public static function GetNextPossibleMatch(game:MainGame):Boolean {
			
			var CurrentCosts=new Array();
			var OrderedCosts=new Array();
			for (var iCost=0; iCost<game.Memory["AiPlayer"]["ManaPriorities"].length; iCost++)
				{
					CurrentCosts.push(game.Memory["AiPlayer"]["ManaPriorities"][iCost]/Math.max(1,game.Memory["PlayerMana"][game.Memory["AiPlayer"]["MyPlayer"]][iCost]));
					OrderedCosts.push(game.Memory["AiPlayer"]["ManaPriorities"][iCost]/Math.max(1,game.Memory["PlayerMana"][game.Memory["AiPlayer"]["MyPlayer"]][iCost]));
				}
			OrderedCosts.sort(Array.NUMERIC);						
			
			var Move=[-1,-1];
			
			for (var Gir=OrderedCosts.length-1; Gir>=0; Gir--)
			{			
				if (Move[0]==-1)
				{
					for each (var Zim in game.Memory["AiPlayer"]["AvailableMoves"])
					{
						if (Zim[0]==CurrentCosts.indexOf(OrderedCosts[Gir]))
						{
							if ((Zim[1]>Move[1]) || (Zim[1]==Move[1] && Math.random()*3<1))
							{
								Move=Zim;
							}
						}
						else if (Zim[1]==5 && Move[1]!=5)
						{
								Move=Zim;
						}
					}
				}
			}
			
			if (Move!=null && Move[0]!=-1)
			{
				GemManager.PlayerMoveGems(game,game.Memory["AiPlayer"]["MyPlayer"],Move[2],Move[3]);
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public static function GetRandomMatch(game:MainGame):Boolean {
						
			var Move=game.Memory["AiPlayer"]["AvailableMoves"][Math.round(Math.random()*(game.Memory["AiPlayer"]["AvailableMoves"].length-1))];			
			
			if (Move!=null)
			{
				GemManager.PlayerMoveGems(game,game.Memory["AiPlayer"]["MyPlayer"],Move[2],Move[3]);
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public static function DoRandomMove(game:MainGame) {
			if (Math.random()*2>1)
			{
				var X=Math.round(Math.random()*(game.Memory["BoardSize"]-1))
				var Y=Math.round(Math.random()*(game.Memory["BoardSize"]-2))
				GemManager.PlayerMoveGems(game,game.Memory["AiPlayer"]["MyPlayer"],GemManager.GetGemAt(game,X,Y),GemManager.GetGemAt(game,X,Y+1));
			}
			else
			{
				var X=Math.round(Math.random()*(game.Memory["BoardSize"]-2))
				var Y=Math.round(Math.random()*(game.Memory["BoardSize"]-1))
				GemManager.PlayerMoveGems(game,game.Memory["AiPlayer"]["MyPlayer"],GemManager.GetGemAt(game,X,Y),GemManager.GetGemAt(game,X+1,Y));
			}
		}
		
		public static function AnalyzeBoard(game:MainGame) {
			game.Memory["AiPlayer"]["AvailableMoves"]=new Array();
			for (var Y=0; Y<=game.Memory["BoardSize"]; Y++)
			{
				for (var X=0; X<=game.Memory["BoardSize"]; X++)
				{
					var MyGem=GemManager.GetGemAt(game,X,Y);
					if (MyGem!=null)
					{
						var Ary:Array=[[MyGem],[MyGem],[MyGem],[MyGem]];
						var Coords:Array=[[1,1],[1,-1],[-1,1],[-1,-1]];
						for (var Gir=0; Gir<Coords.length; Gir++)
						{
							for (var Zim=0; Zim<PossibleMoves.length; Zim++)
							{
								for (var ABC=0; ABC<PossibleMoves[Zim].length-1; ABC++)
								{								
									var iX=X+Coords[Gir][0]*PossibleMoves[Zim][ABC][0];
									var iY=Y+Coords[Gir][1]*PossibleMoves[Zim][ABC][1];
									var NewGem=GemManager.GetGemAt(game,iX,iY);
									if (Ary[Gir]!=null && NewGem!=null && NewGem.Type==MyGem.Type)
									{
										Ary[Gir].push(NewGem)
										if (Ary[Gir].length==PossibleMoves[Zim].length)
										{
											var GemA=GemManager.GetGemAt(game,X+Coords[Gir][0]*PossibleMoves[Zim][PossibleMoves[Zim].length-1][0][0],Y+Coords[Gir][1]*PossibleMoves[Zim][PossibleMoves[Zim].length-1][0][1]);
											var GemB=GemManager.GetGemAt(game,X+Coords[Gir][0]*PossibleMoves[Zim][PossibleMoves[Zim].length-1][1][0],Y+Coords[Gir][1]*PossibleMoves[Zim][PossibleMoves[Zim].length-1][1][1]);
											if (GemA!=null && GemB!=null)
											{
												var Numbah=Ary[Gir].length;
												game.Memory["AiPlayer"]["AvailableMoves"].push([MyGem.Type,Numbah,GemA,GemB]);
											}
											Ary[Gir]=[MyGem];
										}
									}								
									else
									{
										Ary[Gir]=[MyGem];
										break;
									}
								}
							}
						}
					}
				}
			}			
		}
	}	
}
