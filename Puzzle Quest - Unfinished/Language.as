﻿package  {
	
	public class Language {
		
		public static var SpellText:Array = [
							  	["Attack",
								 "Deals damage to a target, and applies certain effects depending on the army's innate ability."],
								["Block",//1
								 "Enemies will have to attack this unit first."],
								["Fire Blast",//2
								 "Shoot a devastating blast of fire at the target unit, setting it alight."],
								["Ice Blast",//3
								 "Shoot a devastating blast of ice at the target unit."],
								["Bombardment",//4
								 "Hurls a enormous boulder at the target unit, dealing damage and stunning it."],
								["Wind Blast",//5
								 "Shoot a devastating blast of wind at the target unit."],
								["Eat Skull",//6
								 "Eats the target skull and heals a portion of it's own life."],
								["Scare",//7
								 "Target unit becomes scared, not providing morale for the enemy player."],
								["Gravenly Touch",//8
								 "Takes Blue Mana from the enemy, and gives it to you."],
								["Unholy Rage",//9
								 "Gain bonus Battle at the cost of life."],
								["Shield Bash",//10
								 "Stun the target unit for 1 turn, and deal damage based on your Armor."],
								["Cannibalize",//11
								 "Drain skulls from the enemy player."],
								["Swoop",//12
								 "A powerful air attack that leaves the caster weak after it is performed."],
								["Echo",//13
								 "Shoots a wave of sound that damages the target unit."],
								["Overwhelm",//14
								 "Summons a swarm of creatures that give bonus battle to the caster depending on your morale."],
								["Venom",//15
								 "Deals physical damage and has a chance to inflict venom on the target."],
								["Inject Poison",//16
								 "Injects a powerful poison in the target unit."],
								["Web",//17
								 "Traps the target in a sticky substance, preventing it from moving."],
								["Rain Poison",//18
								 "Inflicts poison on all enemy units, and has a chance to inflict a strong poison upon them."],
								["Sneak",//19
								 "This unit becomes hidden, gaining bonus damage but losing the bonus if it gets damaged."],
								["Scavange",//20
								 "Destroys random gems, scavanging anything it finds as bonus Battle."],
								 ["Ignite",//21
								 "Transforms yellow and green gems into red gems."],
								["Replenish",//22
								 "Heals a small amount of hp on all friendly units."],
								["Zap",//23
								 "Damages an enemy unit or heals an allied one."],
								["Rabid Bite",//24
								 "A powerful bite that inflicts disease on the target unit, reducing it's armor."],
								["Plague",//25
								 "Inflicts disease on enemy units, reducing their armor."],
								["Revenge",//26
								 "Gain bonus Battle Skill the lower the Life of this unit."],
								["Swiftness",//27
								 "Allows this unit to attack one extra time this turn."],
								["Tackle",//28
								 "A weak attack that has a chance to deal double damage."],
								["Howl",//29
								 "This units gains bonus attack."],
								["Taunt",//30
								 "Infuriates the target unit, preventing it from casting spells."],
								["Ice Floe",//31
								 "Launches a ice projecile at the target unit."],
								["Gust",//32
								 "Launches a wind projecile at the target unit."],
								["Meat Shield",//33
								 "Gains bonus armor, but reduces it's battle skill."],
								["Regenerate",//34
								 "The unit gains health regeneration, but reduces it's battel skill."],
								["Eclipse",//35
								 "Eat a morale star, gaininga small bonus armor and battle for a long duration."],
								["Blight",//36
								 "Inflicts poison and disease on all enemy units, at the cost of a huge portion of this unit's life."],
								["Burrow",//37
								 "This unit submerges underground, gaining huge armor and magic, but being unable to act for the duration."],
								["Exoskeleton",//38
								 "Destroy random gems and gain armor for each gem destroyed."],
								["Pickpocket",//39
								 "Steals a random type of mana from the enemy and gives it to you."],
								["Fireball",//40
								 "Launches a fire projecile at the target unit."],
								["Skull Bash",//41
								 "A weak attack that stuns it's target."],
								["Dwarven Ale",//42
								 "Drinks a strong ale that heals a large amount of health."],
								["Stone Skin",//43
								 "This unit gains a large bonus in armor."],
								["Rain of Fire",//44
								 "Damages all enemy units and sets them on fire."],
								["Overpower",//45
								 "Deals a huge amount of damage to the target unit, but weakens this unit afterwards."],
								["Shield Wall",//46
								 "Gains a huge armor bonus and goes into block stance."],
								["Backstab",//47
								 "A cheap, weak attack."],
								["Devour Fire",//48
								 "Destroys 3 random red gems and gives them as mana to you."],
								["Gem Smash",//49
								 "Destroys a gem. You gain no effect from the gem destroyed."],
								["Throw Stone",//50
								 "Launches a stone at the target unit, stunning them for a short duration"],
								["Hurl Bowlder",//51
								 "Launches a large boulder at the target unit, stunning the target for a short duration."],
								["Devour Earth",//52
								 "Destroys 3 random green gems and gives them as mana to you."],
								["Decieve",//53
								 "Causes the target unit to deal it's attack damage to itself."],
								["Blood Rite",//54
								 "This unit gains bonus battle and armor for 3 turns, but bleeds for the duration."],
								["Strength",//55
								 "Gains bonus battle and armor for 3 turns."],
								["Godly Strength",//56
								 "Gains a large bonus of battle and armor for 3 turns."],
								["Rest",//57
								 "The caster falls asleep, regenerating a large portion of hitpoints over 2 turns."],
								["Curse",//58
								 "Applies a curse on the target unit, reducing their battle skill."],
								["Silence",//59
								 "Applies silence on the target unit, preventing it from casting spells."],
								["Entangle",//60
								 "Roots entangle the target unit, stunning it for a long duration."],
								["Haste",//61
								 "Grants 1 morale for this turn."],
								["Channel Red",//62
								 "Convert mana into red mana."],
								["Channel Blue",//63
								 "Convert mana into blue mana."],
								["Channel Green",//64
								 "Convert mana into green mana."],
								["Channel Yellow",//65
								 "Convert mana into yellow mana."],
								["Bless",//66
								 "Applies a blessing on the target unit, increasing their battle skill."],
								["Shield Charge",//67
								 "Deals damage to the target unit, and gives this unit bonus armor."],
								["Avalanche",//68
								 "Deals little damage and stuns all enemy units."],
								["Exploit Weakness",//69
								 "Deals damage to the target unit based on it's higher stat."],
								["Berserker's Rage",//70
								 "Set this unit alight and gives it a strength bonus."],
								["Dark Bolt",//71
								 "A bolt of dark energy that deals damage and causes the target unit to bleed."],
								["Raise the Dead",//72
								 "Turns random gems into skulls."],
								["Siege",//73
								 "Destroys a random 3x3 row of gems."],
								["Charge",//74
								 "Destroys the target row of gems."],
								["Slash",//75
								 "A swift attack that can cause the target to bleed."],
								["Goblin Toss",//76
								 "Toss a goblin that destroys a random gem and grants you the mana."],
								["Reinforce",//77
								 "Increases the unit's armor by 25%."],
								["Body Slam",//78
								 "A powerful attack that increases the caster's armor for a long duration."],
								["Iron Fist",//79
								 "A powerful attack that increases the caster's battle for a long duration."],
								["Fire Bomb",//80
								 "Destroys the target gem and gives you the mana of that gem."],
								["Shield Order",//81
								 "Commands it's familiars to form a defensive formation, granting the caster bonus armor."],
								["Attack Order",//82
								 "Commands it's familiars to form a offensive formation, granting the caster bonus battle."],
								["Grave Digger",//83
								 "Turns green gems on the board into skulls."],
								["Bone Armor",//84
								 "Destroys all gems on the board and turns them in armor for this unit."],
								["Burrial",//85
								 "Destroys all skulls on the board. You do not gain the effect of the skulls destroyed."],
								["Fire Elemental",//86
								 "Creates a strong fire elemental that damages the target unit and sets it on fire."],
								["Water Elemental",//87
								 "Creates a strong water elemental that damages the target unit."],
								["Earth Elemental",//88
								 "Creates a strong earth elemental that damages the target unit and stunns the target."],
								["Wind Elemental",//89
								 "Creates a strong wind elemental that damages the target unit and silences it."],
								["Blood Offering",//90
								 "Gains bonus battle for every red mana you have, draining all your mana."],
								["Soul Offering",//91
								 "Gains bonus magic for every blue mana you have, draining all your mana."],
								["Body Offering",//92
								 "Gains bonus armor for every green mana you have, draining all your mana."],
								["Mind Offering",//93
								 "Gains bonus damage for every yellow mana you have, draining all your mana."],
								["Thunder",//94
								 "Destroy the target row of gems."],
								["Fortify",//95
								 "Doubles thus unit's armor, but halves it's attack."],
								["Shadow Wave",//96
								 "Applies Stun for 1 turn, Fear for 2 Turns and Silence for 3 Turns."],
								["Axe Throw",//97
								 "Deals damage based on the number of skulls in play."],
								["Life Leech",//98
								 "Damages the target unit and restores half of the damage dealt to this unit."],
								["Life Steal",//99
								 "Damages the target unit and restores half of the damage dealt to this unit."],
								["Swarm Strike",//100
								 "Summons a swarm of creatures, dealing random damage to the target unit."],
								["Savage Roar",//101
								 "Gives all friendly units a boost in damage for this turn only."],
								["Freeze",//102
								 "Freezes a target unit, stunning it and giving it a huge bonus in armor and magic."],
								["Sand Attack",//103
								 "Throws sand at the target unit, dealing little damage."],
								["Weakness",//104
								 "Inflicts sleep on the target unit for 3 turns, but also inflicts disease for this turn, reducing the unit's armor."],								 
								["Slumber",//105
								 "Puts this unit in a deep sleep. If awakened, it will have a huge bonus battle for a few turns."],											 
								["Plague Cloud",//106
								 "Deals damage to all diseased units based on the strength of their disease."],								  
								["Poison Cloud",//107
								 "Deals damage to all poisoned units based on the strength of their poison."],							  
								["Deadly Stinger",//108
								 "Inflicts a powerful poison that doubles in power if the target is already poisoned."],								 						  
								["Harpy's Kiss",//109
								 "A powerful attack that deals double damage if the target is diseasesd."],							  
								["Pestilience",//110
								 "Inflicts a powerful poison that only lasts 1 turn."],					  
								["Contaminate",//111
								 "Deals damage and inflicts disease on the target unit."],						  
								["Toxin",//112
								 "Deals damage and inflicts poison on the target unit."],					  
								["Rupture",//113
								 "Deals damage and causes the target unit to bleed."],	  
								["Hemorrhage",//114
								 "Makes the target unit bleed. The bleeding damage is doubled if the target is already bleeding."],	
								["Hex",//115
								 "Curses and Silences the target unit for the next turn."],	
								 ]
								
		public static var ArmyNames:Array = 
		[	//Attack	armor		magic
		 	["Citadel","Fort","Academy"],
		 	["Skeleton","Wight","Fallen"],
		 	["Vampire Bat","Plague Bat","Shrike"],
		 	["Harvestman","Webspinner","Venomancer"],
		 	["Goblin Warrior","Hobgoblin","Goblin Shaman"],
		 	["Rabid Rat","Giant Rat","Vermin"],
		 	["Dire Wolf","Ironforest Wolf","Warg"],
		 	["Frostwolf","Icebeast","Cursed Icebeast"],
		 	["Hornet","Wasp","Cyclone Bee"],
		 	["Fallen","Zombie","Plague Bearer"],
		 	["Devourer","Basilisk Worm","Earthfling"],
		 	["Scorpion","Husk","Deathmite"],
		 	["Imp","Deciever","Trickster"],
		 	["Troll Brawler","Gargoyle","Troll Shaman"],
		 	["Assassin","Bandit","Spellthief"],
		 	["Orc Brawler","Orc Figher","Orc Monk"],
		 	["Orc Warrior","Orc Defender","Orc Warlock"],
		 	["Orc Raider","Orc Wolf Rider","Orc Chief"],			
			["Dark Smith","Dark AxeDwarf","Hell Forger"],
		 	["Dwarf Berserker","Dwarf Guard","Runelord"],
		 	["Windfurry Harpy","Iron Crone","Harpy Hag"],
		 	["High Elf Warrior","IceGuard","Cleric"],
		 	["Pikeman","Pallace Guard","Anti-Mage"],
		 	["Ogre Fighter","Ogre Guard","Ogre Brawler"],
		 	["Manhunter","Dark Archer","Arcane Archer"],
		 	["Battlemage","Vindicator","Archmage"],
		 	["Ogre Battlemage","Ogre Cleric","Ogre Mage"],
		 	["Minotaur Warrior","Minotaur Guard","Minotaur Shaman"],
		 	["Infernal","Archdemon","Fire Demon"],
		 	["Plague Priest","Necromancer","Liche"],
		 	["Sprite","Pixie","Ice Maiden"],
		 	["Hill Giant","Rock Giant","Runic Giant"],			
		 	["Trebuchet","Gobshooter","Catapult"],
		 	["Centaur Marauder","Centaur Warrior","Centaur Druid"],
		 	["Slayer Knight","Doom Knight","Death Knight"],
		 	["War Griffon","Royal Griffon","Hippogryph"],
		 	["Desolator","Liche","Deathbringer"],
		 	["Paladin","Cavalier","Crusader"],
		 	["Battle Golem","Iron Golem","Mana Golem"],
		 	["Blood Priestess","Spider Priestess","Death Priestess"],
		 	["Brigand","Highwayman","Spellblade"],
		 	["Wyvern","Ironscale Wyrm","Storm Wyrm"],
		 	["Beastmaster","Forestguard","Gladewarden"],
		 	["Ogre Warrior","Ogre Guardian","Ogre Warlock"],
		 	["Minotaur Warrior","Minotaur Defender","Axe of Sartek"],
		 	["Minotaur Berserker","Minotaur Brute","Minotaur Warlock"],
		 	["Fire Giant","Magma Giant","Infernal Giant"],
		 	["Frost Giant","Ice Giant","Storm Giant"],
		 	["Archliche","Archliche","Archliche"],
		 	["Frost Dragon","Frost Dragon","Frost Dragon"],
		 	["Fire Dragon","Fire Dragon","Fire Dragon"],
		 	["Swamp Dragon","Swamp Dragon","Swamp Dragon"],
		 	["Undead Dragon","Undead Dragon","Undead Dragon"],
		 	["Hydra","Hydra","Hydra"],
		 	["Minotaur Skeleton","Minotaur Skeleton","Minotaur Skeleton"],
		 	["Elven Queen","Ice Maiden","Mana Priestess"]
		 ]; 
		 
		public static var PlayerName:String = "Lord Bane";
		 
		public static var VictoryScreen:Array = ["\n                            VICTORY\n\n\n","          GOLD:\n\n","             XP:\n"," has leveled up."," has learned new abilities."," has joined your cause.","DEFEAT\n\n\n\n\n\n\n","Your enemies have stolen "," Gold from you.","The game has been saved!","A new unit"];
		public static var MMText:Array = ["Menu","Mute","End Turn","Surrender","Quit"];
		public static var BarracksButtons:Array = ["Swap","Exit","Abilities","Swapping"];
		public static var BRKText:Array = ["Everything","Retinue","Barracks"];
		public static var InfoUIText:Array = ["Battle\n\nThe amount of damage this unit deals with basic attacks.",
											  "Armor\n\nThe amount of damage reduced when taking physical damage.",
											  "Spellcasting\n\nAffects the power and duration of the unit's spells."];											  
											  
		public static var MainMenuText:Array = ["NEW GAME","Completion: ","Delete"];		
		
		public static var BarracksText:Array = ["\nLevel ","\nXP to Level Up: ","\n\nAttack:","\nArmor:","\nSpecial:",
												"Abilities:\n","\n","Retinue","Barracks","Nothing","Innate: ",
												"Costs ","You have "," Gold","\nDeaths: ","\nMax Level Reached"];
												
		public static var ErrorData:Array = ["This spell can only target allied units.",
											 "This spell can only target enemy units.",
											 "This spell can only target living units.",
											 "This spell cannot target units past the first row",
											 "This spell can only target gems.",
											 "This spell can only target red gems.",
											 "This spell can only target blue gems.",
											 "This spell can only target green gems.",
											 "This spell can only target yellow gems.",
											 "This spell can only target skulls.",
											 "This spell can only target morale stars.",
											 "This spell can only target units.",
											 "You do not have enough mana.",
											 "This unit cannot act.",
											 "You cannot act during the enemy's turn!"];
											 
		public static var datModifierSilenced="Silence"
		public static var datModifierStunned="Stun"
		public static var datModifierAfterAct="Exhaust"
		public static var datModifierCursed="Curse"
		public static var datModifierBlessed="Bless"
		public static var datModifierFear="Fear"
		public static var datModifierPoison="Poison"
		public static var datModifierDisease="Disease"
		public static var datModifierRegen="Regeneration"
		public static var datModifierRejuvenation="Rejuvenation"
		public static var datModifierBerserk="Berserker"
		public static var datModifierSleep="Sleep"
		public static var datModifierStrength="Strength"
		public static var datModifierSneak="Hide"
		public static var datModifierSwift="Swift"
		public static var datModifierWeaken="Weak"
		public static var datModifierArmor="Armor"
		public static var datModifierBleed="Bleed"
		public static var datModifierBurn="Burn"
		public static var datModifierEnchanted="Enchant"
		public static var datModifierVulnerable="Vulnerable"
							
		public static var PassiveText:Array = [
							   	["Spellblade",
								 "Basic attacks deal magic damage."],
								["Thoughness",
								 "Refunds the cost of Defend."],
								["Tower Shield",
								 "10 Bonus Armor while defending."],
								["Arcane Shield",
								 "10 Bonus Magic while defending."],
								["Revenge",
								 "On death, deal damage equal to the army's level to the killer."],
								["Tempered",
								 "Gain bonus Attack when low on life."],
								["Stoneskin",
								 "Gain bonus armor when low on life."],
								["Blood Rite",
								 "Gain bonus Magic when low on life."],
								["Vampirism",
								 "Recover 20% of the physical damage dealt."],
								["Soul Stealer",
								 "Recover 20% of the magical damage dealt."],
								["Retaliate",
								 "Retaliate 20% of attack damage to attackers."],
								["Regeneration",
								 "Recover a small amount of health upon taking damage."],
								["Overpowered",
								 "This unit's spells are so powerful, it cannot act another turn after casting"],
								["Bash",
								 "This unit's attacks are so powerful, it cannot act another turn after casting"],
								["Arcanist",
								 "This unit's spells are so powerful, it cannot cast another turn after casting"],
								["Command",
								 "This unit grants extra morale"],
								["Execute",
								 "Execute minions with low health upon dealing damage."],
								["Aggressive",
								 "This unit has more battle and less armor."],
								["Enchanted",
								 "This unit has more magic and less armor."],
								["Feral",
								 "This unit has more battle and less special."],
								["Defensive",
								 "This unit has more armor and less battle."],
								["Wimpy",
								 "This unit has more magic and less battle."],
								["Primitive",
								 "This unit has more armor and less special."],
								["Heavy Hitter",
								 "Attacks consume one extra skull and deal extra damage."],
								["Stubborn",
								 "Death blows reduce this army's life to 1."],
								["Immortal",
								 "Death blows stun this army."],
								["Immune to curses."],
								["Immunity",
								 "Immune to poison and disease."],
								["Horde",
								 "Deals bonus damage when you have high Morale."],
								["Loner",
								 "Deals bonus damage when you have low Morale."],
								["Double Edge",
								 "Attacks deal bonus damage to the attacker and the target."],
								["Bedeviled",
								 "Curses and blessings have opposite effects on this army."],
								["Blunt",
								 "Attacks have a chance to stun."],
								["Cursed",
								 "Attacks have a chance to curse."],
								["Inspire Fear",
								 "Attacks have a chance to fear."],
								["Poison Claw",
								 "Poisons the attackers."],
								["Poison Sting",
								 "Attacks have a chance to poison."],
								["Poison Sting",
								 "Attacks set the targets on fire."],
								["Scavange",
								 "When attacking, a random gem on the board will be destroyed."],
								["Critical Strike",
								 "Chance to deal double damage with attacks."],
								["Plague",
								 "Inflicts Disease upon attackers and when landing attacks."],
								["Horrendous",
								 "This unit does not provide morale."],
								["Degenerate",
								 "This unit degenerates health."],
								["Fragile",
								 "This unit takes damage when attacking."],
								["Channel Fire",
								 "This unit generates red mana every turn."],
								["Channel Water",
								 "This unit generates blue mana every turn."],
								["Channel Earth",
								 "This unit generates green mana every turn."],
								["Channel Air",
								 "This unit generates yellow mana every turn."],
								["Manahoar",
								 "This unit generates random mana every turn."],
								["Blood Synergy",
								 "Gain bonus damage where there are "+GlobalVariables.ArmyPassiveTresspass+"or more red gems in play"],
								["Soul Synergy",
								 "Gain bonus special where there are "+GlobalVariables.ArmyPassiveTresspass+"or more blue gems in play"],
								["Body Synergy",
								 "Gain bonus armor where there are "+GlobalVariables.ArmyPassiveTresspass+"or more green gems in play"],
								["Mind Synergy",
								 "Generate Skulls where there are "+GlobalVariables.ArmyPassiveTresspass+"or more yellow gems in play"],
							]	
		 }
	
}
