﻿package  {
	
	public class DisplayItemArmy {

		public var Name;
		public var Type;
		public var Life;
		public var MaxLife;
		public var Defender;
		public var Attack;
		public var Special;
		public var Abilities;
		public var Modifiers;
		public var Armor;
		public var ID;
		public var realID;
		public var PlayerOwner;
		public var Passive;
		public var Level;
		
		public function CanAct(game:MainGame):Boolean
		{		
			return (AmIDead(game)==false && HasModifier(Language.datModifierStunned,game.Memory["CurrentTurn"])==false && HasModifier(Language.datModifierSleep,game.Memory["CurrentTurn"])==false && HasModifier(Language.datModifierAfterAct,game.Memory["CurrentTurn"])==false);			
		}
		
		public function AmIDead(game:MainGame):Boolean
		{		
			 if (Life>0)
			{
				return false;
			}
			else if (Passive=="Synergy")					
			{
				var iA=0;
				var iB=0;
				for each (var Zim:DisplayItemArmy in game.Memory["DisplayItemArmies"][PlayerOwner])
				{
					if (Zim.Passive=="Synergy")
					{
						if (Zim.Life<=0)
						{
							iA++;
						}
						iB++;
					}
				}
				return (iA>=iB);
			}
			else
			{
				return true;
			}
		}
		
		public function CanCast(game:MainGame):Boolean
		{		
			return (CanAct(game)==true && HasModifier(Language.datModifierSilenced,game.Memory["CurrentTurn"])==false);
		}
		
		public function GetMorale(game:MainGame):Number
		{		
			var Value=1
			if (CanAct(game)==false)
			{
				Value = 0;
			}
			if (Passive=="BonusMorale" && AmIDead(game)==false)
			{
				Value+=1;
			}
			if (HasModifier(Language.datModifierFear,game.Memory["CurrentTurn"])==true)
			{
				Value-=1;
			}
			else if (Passive=="Nomorale")
			{
				Value=0;
			}
			Value=Math.max(0,Value);
			return Value;
		}
		
		public function GetAttack(game:MainGame):Number
		{
			var Value=Attack;		
			if (Life<=MaxLife*0.5 && Passive=="bonusatkonlowhp")
			{
				Value+=5;
				if (Life<=MaxLife*0.25)
				{
					Value+=10;
				}		
			}		
			if (Passive=="selfstunafterattack")
			{
				Value*=1.3;
			}		
			else if (Passive=="highatklowdefense")
			{
				Value*=1.1;
			}		
			else if (Passive=="highatklowspec")
			{
				Value*=1.1;
			}		
			else if (Passive=="highdeflowatk")
			{
				Value*=0.9;
			}		
			else if (Passive=="highspclowatk")
			{
				Value*=0.9;
			}		
			else if (Passive=="loner")
			{
				Value+=(5*GlobalVariables.MaxArmyStack-5*game.Memory["PlayerMana"][PlayerOwner][GlobalVariables.GEM_PURPLE]);
			}		
			else if (Passive=="horde")
			{
				Value+=(5*game.Memory["PlayerMana"][PlayerOwner][GlobalVariables.GEM_PURPLE]);
			}		
			else if (Passive=="HabitatRed")
			{
				if (game.Memory["BoardMana"][GlobalVariables.GEM_RED]>GlobalVariables.ArmyPassiveTresspass)
				{
					Value*=1.1;
				}
			}		
				Value+=GetModifierValue(Language.datModifierBerserk,true,game.Memory["CurrentTurn"]);
				Value+=GetModifierValue(Language.datModifierStrength,true,game.Memory["CurrentTurn"]);
				Value-=GetModifierValue(Language.datModifierWeaken,true,game.Memory["CurrentTurn"]);
				
				if (HasModifier(Language.datModifierSneak,game.Memory["CurrentTurn"]))
					{
						Value*=1.5;
					}
			
			if (Passive=="bedeviled")
			{
				Value+=GetModifierValue(Language.datModifierCursed,true,game.Memory["CurrentTurn"]);
				Value-=GetModifierValue(Language.datModifierBlessed,true,game.Memory["CurrentTurn"]);
			}		
			else
			{
				Value-=GetModifierValue(Language.datModifierCursed,true,game.Memory["CurrentTurn"]);
				Value+=GetModifierValue(Language.datModifierBlessed,true,game.Memory["CurrentTurn"]);
			}
			
			Value=Math.max(1,Value);
			return Value;
		}
		
		public function GetDefense(game:MainGame):Number
		{
			var Value=Armor;		
			if (Defender==true && Passive=="bonusarmordefense")
			{
				Value+=10;
			}	
			if (Life<=MaxLife*0.5 && Passive=="bonusdefonlowhp")
			{
				Value+=10;
				if (Life<=MaxLife*0.25)
				{
					Value+=10;
				}		
			}		
			else if (Passive=="highdeflowatk")
			{
				Value*=1.1;
			}		
			else if (Passive=="highdeflowspc")
			{
				Value*=1.1;
			}		
			else if (Passive=="highatklowdefense")
			{
				Value*=0.9;
			}		
			else if (Passive=="highspclowdefense")
			{
				Value*=0.9;
			}			
			else if (Passive=="HabitatGreen")
			{
				if (game.Memory["BoardMana"][GlobalVariables.GEM_GREEN]>GlobalVariables.ArmyPassiveTresspass)
				{
					Value*=1.1;
				}
			}			
			
				Value+=GetModifierValue(Language.datModifierArmor,true,game.Memory["CurrentTurn"]);
				Value-=GetModifierValue(Language.datModifierDisease,true,game.Memory["CurrentTurn"]);
				Value+=GetModifierValue(Language.datModifierStrength,true,game.Memory["CurrentTurn"]);
			
			Value=Math.max(1,Value);
			return Value;
		}
		
		public function GetSpecial(game:MainGame):Number
		{
			var Value=Special;		
			if (Defender==true && Passive=="bonusspecialdefense")
			{
				Value+=10;
			}	
			if (Life<=MaxLife*0.5 && Passive=="bonusspconlowhp")
			{
				Value+=10;
				if (Life<=MaxLife*0.25)
				{
					Value+=10;
				}		
			}	
			if (Passive=="selfstunafterspell")
			{
				Value*=1.3;
			}		
			else if (Passive=="highspclowatk")
			{
				Value*=1.1;
			}		
			else if (Passive=="highspclowdefense")
			{
				Value*=1.1;
			}		
			else if (Passive=="highatklowspec")
			{
				Value*=0.9;
			}		
			else if (Passive=="highdeflowspc")
			{
				Value*=0.9;
			}		
			else if (Passive=="HabitatBlue")
			{
				if (game.Memory["BoardMana"][GlobalVariables.GEM_BLUE]>GlobalVariables.ArmyPassiveTresspass)
				{
					Value*=1.1;
				}
			}		
				Value+=GetModifierValue(Language.datModifierEnchanted,true,game.Memory["CurrentTurn"]);
			Value=Math.max(1,Value);
			return Value;
		}
						
		public function AddModifier(Name:String,Data:Number,Duration:Number,TurnAt:Number,Multiple:Boolean)
		{
			if (((Name==Language.datModifierDisease || Name==Language.datModifierPoison) && Passive!="immunity") || (Name==Language.datModifierCursed && Passive!="unbeliever") || (Name!=Language.datModifierCursed && Name!=Language.datModifierDisease && Name!=Language.datModifierPoison))
			{
				if (HasModifier(Name,TurnAt)==false || Multiple==true)
				{
					Modifiers.push([Name,Data,Duration+TurnAt]);
				}
			}
		}
						
		public function HasModifier(Name:String,TurnAt:Number)
		{
			var Bool=false;
			for (var Zim=0; Zim<Modifiers.length; Zim++)
			{
				if (Modifiers[Zim][0]==Name && Modifiers[Zim][2]>TurnAt)
				{
					Bool= true;
				}
			}
			return Bool;
		}
						
		public function RemoveModifier(Name:String)
		{
			for (var Zim=0; Zim<Modifiers.length; Zim++)
			{
				if (Modifiers[Zim][0]==Name)
				{
					Modifiers[Zim][2]=0;
				}
			}
		}
						
		public function GetModifierValue(Name:String,Additive:Boolean,TurnAt:Number)
		{
			var Gir=0;
			for (var Zim=0; Zim<Modifiers.length; Zim++)
			{
				if (Modifiers[Zim][0]==Name && Modifiers[Zim][2]>TurnAt)
				{
					if (Additive==true)
					{
						Gir+=Modifiers[Zim][1];
					}
					else if (Modifiers[Zim][1]>Gir)
					{
						Gir=Modifiers[Zim][1];
					}
				}
			}
			return Gir;
		}
						
		public function HandleTroop(game:MainGame,TurnAt:Number)
		{
			for each  (var Zim:Array in Modifiers)
			{					
				if (Zim[2]<TurnAt)
				{
					Modifiers.splice(Modifiers.indexOf(Zim),1);
				}
				else
				{
					if (Zim[0]==Language.datModifierRegen)
					{
						Heal(game,this,Zim[1],true,true);
					}
					if (Zim[0]==Language.datModifierPoison || Zim[0]==Language.datModifierBleed || Zim[0]==Language.datModifierBurn)
					{
						TakeDamage(game,this,this,Zim[1],true);
					}
					if (Zim[0]==Language.datModifierRejuvenation)
					{
						Heal(game,this,Zim[1],true,true);
					}
				}
			}
			if (Life>MaxLife)
			{
				var DMG=Math.max(1,Life-1);
				TakeDamage(game,this,this,DMG,true);
			}	
			if (Passive=="Degeneration")
			{
				TakeDamage(game,this,this,10,true);
			}	
			else if (Passive=="HabitatYellow" || Passive=="GenerateRed" || Passive=="GenerateBlue" || Passive=="GenerateGreen")
			{
				if (Passive=="HabitatYellow")
				{
					if (game.Memory["BoardMana"][GlobalVariables.GEM_YELLOW]>GlobalVariables.ArmyPassiveTresspass && Math.random()*100<GlobalVariables.ManaRegenChance)
					{
						HandleMana.AddMana(game,PlayerOwner,1,GlobalVariables.GEM_SKULL,true);
					}
				}
				else if (Passive=="GenerateRed")
				{
					if (Math.random()*100<GlobalVariables.ManaRegenChance)
					{
						HandleMana.AddMana(game,PlayerOwner,1,GlobalVariables.GEM_RED,true);
					}
				}	
				else if (Passive=="GenerateBlue")
				{
					if (Math.random()*100<GlobalVariables.ManaRegenChance)
					{
						HandleMana.AddMana(game,PlayerOwner,1,GlobalVariables.GEM_BLUE,true);
					}
				}	
				else if (Passive=="GenerateGreen")
				{
					if (Math.random()*100<GlobalVariables.ManaRegenChance)
					{
						HandleMana.AddMana(game,PlayerOwner,1,GlobalVariables.GEM_GREEN,true);
					}
				}	
				else if (Passive=="GenerateYellow")
				{
					if (Math.random()*100<GlobalVariables.ManaRegenChance)
					{
						HandleMana.AddMana(game,PlayerOwner,1,GlobalVariables.GEM_YELLOW,true);
					}
				}	
				else if (Passive=="GenerateRandom")
				{
					if (Math.random()*100<GlobalVariables.ManaRegenChance)
					{
						HandleMana.AddMana(game,PlayerOwner,1,Math.round(Math.random()*GlobalVariables.GEM_YELLOW),true);
					}
				}	
			}
		}
						
		public function GetModifiers(game:MainGame)
		{
			var Mods:String = "";
			for each  (var Zim:Array in Modifiers)
			{
				
					Mods+=(Zim[0]+"("+Math.round(Math.ceil(Zim[2]-game.Memory["CurrentTurn"])/2)+") ");
			}
			return Mods;
		}

		public static function DealDamagePhysical(game:MainGame,Attacker:DisplayItemArmy,Army:DisplayItemArmy,Base:Number,Multiplier:Number,Display:Boolean=true):Number
		{			
			var Damage=Base+Multiplier*Attacker.GetAttack(game);
			
			var Protection=Army.GetDefense(game);
			/*if (Protection >= Damage*4)
			{
				Damage /= 5;
			}
			else if (Protection >= Damage*3)
			{
				Damage /= 4;
			}
			else if (Protection >= Damage*2)
			{
				Damage /= 3;
			}
			else if (Protection >= Damage)
			{
				Damage /= 2;
			}
			else
			{
				Damage = Damage - Protection/2;
			}*/						
			trace(Damage,Protection/(Protection+GlobalVariables.Protection*Army.Level))
			Damage = Damage * (1-Protection/(Protection+GlobalVariables.Protection*Army.Level));
			
			Damage+=Damage*Math.random()*0.15;
				
			Damage=Math.max(0,Math.min(Damage,Army.Life));
			
			if (Attacker.Passive=="execute" && Army.Life<Attacker.Attack)
			{
				Damage*=2;
			}
			
			if (Army.Life>0)
			{
				Damage=TakeDamage(game,Attacker,Army,Damage,Display)
				
				if (Attacker.Passive=="vampirism")
				{
					Heal(game,Attacker,Damage*0.2,false,Display)
				}			
			}
			
			return Damage;
		}		
		
		public static function TakeDamage(game:MainGame,Attacker:DisplayItemArmy,Army:DisplayItemArmy,Damage:Number,Display:Boolean=true):Number
		{
			if (Damage>0)
			{
				if (Army.Passive=="stopdamageover1")
				{
					if (Army.Life>1 && Damage>=Army.Life)
					{
						Damage=Army.Life-1;
					}
				}
				else if (Army.Passive=="invulnerable" && Damage>=Army.Life && Army.HasModifier(Language.datModifierVulnerable,game.Memory["CurrentTurn"])==false)
				{
						Damage=0;
						Army.AddModifier(Language.datModifierVulnerable,0,3,game.Memory["CurrentTurn"],false);
				}
				else
				{
					Damage=Math.min(Army.Life,Damage);
				}				
				
				if (Army.Passive=="regenerate" && Damage>0 && Army.Life>=1)
				{				
					Army.AddModifier(Language.datModifierRegen,Damage/20,20,game.Memory["CurrentTurn"],true);
				}
			}
			
				Army.Life=Army.Life-Damage;			
				if (Display==true)
				{
					game.Memory["Display"]["PlayerLife"][Army.PlayerOwner][Army.ID]-=Damage;
				}
				Army.RemoveModifier(Language.datModifierSleep);
				Army.RemoveModifier(Language.datModifierSneak);
				Army.RemoveModifier(Language.datModifierRejuvenation);
			
			if (Army.Life<=0 && Army.Passive=="retaliateondeath")
			{
				DealDamageMagical(game,Army,Attacker,0,0.25,Display)
			}
			if (Army.Life<=0)
			{				
					Army.RemoveModifier(Language.datModifierRegen)
			}
			
			game.Memory["PlayerLife"][Army.PlayerOwner]=Math.max(0,game.Memory["PlayerLife"][Army.PlayerOwner]-Damage);			
			game.CheckPlayersLife();
			
			return Damage;
		}
		
		public static function DealDamageMagical(game:MainGame,Attacker:DisplayItemArmy,Army:DisplayItemArmy,Base:Number,Multiplier:Number,Display:Boolean=true):Number
		{
			var Damage=(Base+Attacker.GetSpecial(game)*Multiplier)//*Attacker.GetSpecial(game)/Army.GetSpecial(game);
			
			var Protection=Army.GetSpecial(game);
			Damage = Damage * (1-Protection/(Protection+GlobalVariables.Protection*Army.GetSpecial(game)/Attacker.GetSpecial(game)));
			
			Damage+=Damage*Math.random()*0.15;
				
			Damage=Math.max(0,Math.min(Damage,Army.Life));
			
			if (Attacker.Passive=="execute" && Army.Life<Attacker.Attack)
			{
				Damage*=2;
			}
			if (Army.Life>0)
			{
				Damage=TakeDamage(game,Attacker,Army,Damage,Display)
				
				if (Attacker.Passive=="spellvamp")
				{
					Heal(game,Attacker,Damage*0.2,true,Display)
				}
			}
			
			return Damage;
		}
			
		
		public static function Heal(game:MainGame,Army:DisplayItemArmy,Damage:Number,GoOver:Boolean,Display:Boolean):Number
		{
			var Protection=Army.GetSpecial(game);
			
			if (Army.Life>Army.MaxLife && GoOver==true)
			{
				Damage/=2;
			}		
			else if (Army.Life>=Army.MaxLife*2)
			{
				Damage=0;
			}			
			else if (Army.Life<=Army.MaxLife && Army.Life+Damage>Army.MaxLife)
			{
				if (GoOver==true)
				{
					 if (Army.Life+Damage>Army.MaxLife*2)
					{
						Damage=Army.MaxLife*2-Army.Life;
					}			
					else 
					{
						Damage=(Army.Life+Damage+Army.MaxLife)/2-Army.Life;
					}
				}
				else
				{
					Damage=Army.MaxLife-Army.Life;
				}
			}			
			
			TakeDamage(game,null,Army,0-Damage,Display)
			Damage+=Damage*Math.random()*0.15;
			
			return Damage;
		}

	}	
}
