﻿package  {
	import flash.display.MovieClip;
	
	public class InGameButtons {

		public static function HandleInGameButtons(game:MainGame,X:Number,Y:Number) {
			if (GetMouseGem(game,X,Y)==null || game.Memory["PopUpMenuOpen"]==true)
			{
				HandleButtons(game,X,Y)
			}
			else if (game.Memory["GemsMoving"]==false)
			{
				MouseOverBoard(game,X,Y)
			}
		}
		
		public static function getMouseButton(game:MainGame,X:Number,Y:Number):MovieClip
		{
			var Return=null;
			for (var Zim=0; Zim<game.Memory["InfoUI"].length; Zim++)
			{
				var Btn=game.Memory["InfoUI"][Zim];					
				
				if (Btn!=null && Btn.alpha>0 && X>=Btn.x-Btn.width/2+Btn.parent.x && X<=Btn.x+Btn.width/2+Btn.parent.x && Y>=Btn.y-Btn.height/2+Btn.parent.y && Y<=Btn.y+Btn.height/2+Btn.parent.y)
				{
					Return=Btn;
				}
			}
			return Return;
		}
		
		public static function HandleButtons(game:MainGame,X:Number,Y:Number)
		{
			var T=game.Memory["HWH"].Memory["GameTime"]
			var Meny=false;
			if (T.GetTime()>T.ClickTimer && game.Memory["Buttons"]!=null)
			{
				for (var Zim=0; Zim<game.Memory["Buttons"].length; Zim++)
				{
					var Btn=game.Memory["Buttons"][Zim];
						
					
					if (Btn!=null && Btn.active==true && X>=Btn.x-Btn.width/2+Btn.parent.x && X<=Btn.x+Btn.width/2+Btn.parent.x && Y>=Btn.y-Btn.height/2+Btn.parent.y && Y<=Btn.y+Btn.height/2+Btn.parent.y)
					{
						if (game.Memory["PopUpMenuOpen"]!=true)
						{
							T.ClickTimer=T.GetTime()+5
							if (Btn.data=="Army")
							{
								if (game.Memory["SpellBeingCast"]==null)
								{
									if (game.Memory["ButtonSelection"][Btn.Owner]==Btn)
									{
										game.Memory["ButtonSelection"][Btn.Owner]=null;
									}
									else
									{
										game.Memory["ButtonSelection"][Btn.Owner]=Btn;
																
										InGameUI.DrawAbilityUI(game,Btn.Owner,Btn.Army)
									}
								}
								else if (cSpellEffect.DoITargetArmies(game)==true)
								{
									if (Btn.Owner==game.Memory["SpellBeingCast"][2] && cSpellEffect.DoITargetAllies(game)==true && game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army].Life>0)
									{
										cSpellEffect.CastSpellOnArmyTarget(game,game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army]);
									}
									else if (Btn.Owner!=game.Memory["SpellBeingCast"][2] && cSpellEffect.AmIValidTarget(game,game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army])==true && 
											 cSpellEffect.DoITargetEnemies(game)==true && game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army].Life>0)
									{
										cSpellEffect.CastSpellOnArmyTarget(game,game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army]);
									}
									else if (Btn.Owner==game.Memory["SpellBeingCast"][2] && game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army].ID==game.Memory["SpellBeingCast"][3])
									{
										game.Memory["SpellBeingCast"]=null;
										if (game.Memory["Channel"]!=null)
										{
											game.Memory["Channel"].animation="Death";
											game.Memory["Channel"]=null;
										}
									}
									else if (Btn.Owner==game.Memory["SpellBeingCast"][2] && cSpellEffect.DoITargetAllies(game)==false)
									{
										DrawInfoOverlay.DisplayErrorOverlay(game,0);
									}
									else if (Btn.Owner!=game.Memory["SpellBeingCast"][2] && cSpellEffect.DoITargetEnemies(game)==false)
									{
										DrawInfoOverlay.DisplayErrorOverlay(game,1);
									}
									else if (game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army].Life<=0)
									{
										DrawInfoOverlay.DisplayErrorOverlay(game,2);
									}								
									else if (Btn.Owner!=game.Memory["SpellBeingCast"][2] && cSpellEffect.AmIValidTarget(game,game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army])==false)
									{
										DrawInfoOverlay.DisplayErrorOverlay(game,3);
									}
								}
									else if (Btn.Owner==game.Memory["SpellBeingCast"][2] && game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army].ID==game.Memory["SpellBeingCast"][3])
									{
										game.Memory["SpellBeingCast"]=null;
										if (game.Memory["Channel"]!=null)
										{
											game.Memory["Channel"].animation="Death";
											game.Memory["Channel"]=null;
										}
									}
								else 
								{
									DrawInfoOverlay.DisplayErrorOverlay(game,4);
								}
								break;
							}
							else if (Btn.data=="Attack")
							{
								cSpellEffect.PrepareSpell(game,SpellData.cSpellAttack,Btn.Owner,game.Memory["DisplayItemArmies"][Btn.Owner][game.Memory["ButtonSelection"][Btn.Owner].Army].ID);
							}
							else if (Btn.data=="Defend")
							{
								cSpellEffect.PrepareSpell(game,SpellData.cSpellDefender,Btn.Owner,game.Memory["DisplayItemArmies"][Btn.Owner][game.Memory["ButtonSelection"][Btn.Owner].Army].ID);
							}
							else if (Btn.data=="Spell")
							{
								cSpellEffect.PrepareSpell(game,Btn.SpellData,Btn.Owner,game.Memory["DisplayItemArmies"][Btn.Owner][game.Memory["ButtonSelection"][Btn.Owner].Army].ID);
							}
							else if (Btn.data=="PopUpMenu")
							{
								game.Memory["PopUpMenuOpen"]=true;
								Meny=true;
							}
						}
						else
						{
							if (Btn.data=="mute")
							{							
								game.Memory["SoundManager"].MuteSounds();
							}
							else if (Btn.data=="endturn")
							{														
							game.DoVictory()
									if (game.Memory["PlayerTurn"]==0 && HandleMana.DoIHaveEnoughMana(game,0,[0,0,0,0,0,1],false))
									{
										HandleMana.RemoveMana(game,0,999,GlobalVariables.GEM_PURPLE);
									}
							}
							else if (Btn.data=="surrender")
							{														
									HubWorld.Draw(game.Memory["HWH"]);
									DataMissions.TakeGold(game.Memory["HWH"],game.Memory["Rewards"][0]*GlobalVariables.LossPenalty,true);
									HubWorld.DisplayDefeatScreen(game.Memory["HWH"],game.Memory["Rewards"]);
									game.Memory["HWH"].Memory["Data"].saveData();
									game.Close();
							}
							else if (Btn.data=="quit")
							{			
									MainMenus.DrawMainMenu(game.Memory["HWH"],false);											
									DataMissions.TakeGold(game.Memory["HWH"],game.Memory["Rewards"][0],true);
									game.Memory["HWH"].Memory["Data"].saveData();
									game.Close();
							}				
							
						}
					}
				}
			}			
			if (Meny==false)
			{				
								game.Memory["PopUpMenuOpen"]=false;
			}
		}
		
		public static function GetArmyDisplay(game:MainGame,Owner:Number,Army:Number,Forced:Boolean=true)
		{
			var Button=null;
			for (var Zim=0; Zim<game.Memory["Buttons"].length; Zim++)
			{
				var Btn=game.Memory["Buttons"][Zim];
					
				
				if (Btn!=null && (Btn.active==true || Forced==true) && Btn.data=="Army" && Btn.Owner==Owner && Btn.Army==Army)
				{
					
					Button=Btn;
				}
			}	
			return Button;
		}
		
		public static function UpdateButtons(game:MainGame)
		{
			for (var Zim=0; Zim<game.Memory["Buttons"].length; Zim++)
			{
				var Btn=game.Memory["Buttons"][Zim];
				if (Btn!=null)
				{
					if (Btn.data=="UI")
					{
						for (var I=0; I<Btn.ManaDisplays.length; I++)
						{
							if (Btn.ManaDisplays[I]!=null)
							{
								Btn.ManaDisplays[I].text=Math.floor(game.Memory["Display"]["PlayerMana"][Btn.Owner][I]);	
								Btn.ManaDisplays[I].setTextFormat(GlobalVariables.TextARMYNAME);
							}
						}
						if (game.Memory["ButtonSelection"][Btn.Owner]!=null)
						{
							var Army:DisplayItemArmy = game.Memory["DisplayItemArmies"][Btn.Owner][game.Memory["ButtonSelection"][Btn.Owner].Army]
							if (Army.AmIDead(game)==true)
							{
								game.Memory["ButtonSelection"][Btn.Owner]=null;
							}
							
							for (I=0; I<Btn.Icons.length; I++)
							{
								if (Btn.Icons[I]!=null)
								{
									Btn.Icons[I].alpha=Math.min(1,Btn.Icons[I].alpha+0.3);
								}
							}
							Btn.TextA.text=Math.round(Army.GetAttack(game));
							Btn.TextA.alpha=Math.min(1,Btn.TextA.alpha+0.3);
							
							Btn.TextB.text=Math.round(Army.GetDefense(game));
							Btn.TextB.alpha=Math.min(1,Btn.TextB.alpha+0.3);
							
							Btn.TextC.text=Math.round(Army.GetSpecial(game));
							Btn.TextC.alpha=Math.min(1,Btn.TextC.alpha+0.3);
							
							Btn.TextA.setTextFormat(GlobalVariables.TextARMYUI);
							Btn.TextB.setTextFormat(GlobalVariables.TextARMYUI);
							Btn.TextC.setTextFormat(GlobalVariables.TextARMYUI);
						}
						else
						{
							for (I=0; I<Btn.Icons.length; I++)
							{
								if (Btn.Icons[I]!=null)
								{
									Btn.Icons[I].alpha=Math.max(0,Btn.Icons[I].alpha-0.3);
								}
							}
							Btn.TextA.alpha=Math.max(0,Btn.TextA.alpha-0.3);
							Btn.TextB.alpha=Math.max(0,Btn.TextB.alpha-0.3);
							Btn.TextC.alpha=Math.max(0,Btn.TextC.alpha-0.3);
						}
					}
					else if (Btn.data=="Attack" || Btn.data=="Defend")
					{
						if (game.Memory["ButtonSelection"][Btn.Owner]==null)
						{
							Btn.alpha=Math.max(0,Btn.alpha-0.3);
							Btn.active=false;
						}
						else
						{
							Btn.alpha=Math.min(1,Btn.alpha+0.3);
							Btn.active=true;
						}
						var SpellID=0;
						if (Btn.data=="Defend")
						{
							SpellID=1;
						}
						Btn.BG.gotoAndStop(3)
							var ManaReq=0;
							for (var Gir=0; Gir<6; Gir++)
							{
								if (SpellData.SpellBook[SpellID][1][Gir]==0 || 
									SpellData.SpellBook[SpellID][1][Gir]<=game.Memory["PlayerMana"][Btn.Owner][Gir])
									{
										ManaReq++
									}
								}											
							if (ManaReq>=GlobalVariables.xk_MAX_MANA_TYPES)
							{
								Btn.BG.gotoAndStop(2)
							}
					}
					else if (Btn.data=="Army")
					{						
						if (game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army].Defender==false)
						{
							Btn.Shield.alpha=0;
						}
						else
						{
							Btn.Shield.alpha=1;
						}
						if (game.Memory["ButtonSelection"][Btn.Owner]==Btn)
						{
							Btn.dy=130;
							Btn.active=true;
						}
						else if (game.Memory["ButtonSelection"][Btn.Owner]!=null && game.Memory["ButtonSelection"][Btn.Owner]!=Btn)
						{
							Btn.alpha=Math.max(0,Btn.alpha-0.3);
							Btn.active=false;
								
						}
						else if (game.Memory["ButtonSelection"][Btn.Owner]==null)
						{
							Btn.dy=130+45*Btn.Army;
							Btn.alpha=Math.min(1,Btn.alpha+0.3);
							Btn.active=true;							
						}				
						
						if (game.Memory["SpellBeingCast"]!=null && game.Memory["SpellBeingCast"][2]==Btn.Owner && game.Memory["SpellBeingCast"][3]==Btn.Army)
						{
							Btn.dx=Btn.ox+10*Btn.F;
						}
						/*else if (game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army].CanAct(game.Memory["CurrentTurn"])==false)
						{
							Btn.dx=Btn.ox-5*Btn.F;
						}*/
						else
						{
							Btn.dx=Btn.ox;
						}
						
						if (Btn.LifeBar!=null)
						{
							if (game.Memory["Display"]["PlayerLife"][Btn.Owner][Btn.Army]!=game.Memory["Display"]["PlayerLifeUpdateCheck"][Btn.Owner][Btn.Army])
							{
								if (Math.round(game.Memory["Display"]["PlayerLifeUpdateCheck"][Btn.Owner][Btn.Army]-game.Memory["Display"]["PlayerLife"][Btn.Owner][Btn.Army])!=0)
								{
									SpecialEffects.MakeSpecialEffectTextOnArmy(game,""+Math.round(game.Memory["Display"]["PlayerLifeUpdateCheck"][Btn.Owner][Btn.Army]-game.Memory["Display"]["PlayerLife"][Btn.Owner][Btn.Army]),game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army],0)
								}
								game.Memory["Display"]["PlayerLifeUpdateCheck"][Btn.Owner][Btn.Army]=game.Memory["Display"]["PlayerLife"][Btn.Owner][Btn.Army]
							}
							
							Btn.LifeText.text=Math.max(0,Math.round(game.Memory["Display"]["PlayerLife"][Btn.Owner][Btn.Army]))+"/"+Math.ceil(game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army].MaxLife);								
							Btn.LifeText.setTextFormat(GlobalVariables.TextLIFEBAR);
						
							Btn.LifeBar.scaleX=Math.min((game.Memory["Display"]["PlayerLife"][Btn.Owner][Btn.Army])/(game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army].MaxLife)*0.463768115942029,0.463768115942029);							
							Btn.LifeBar.x=Btn.LifeBar.width/2+43*Btn.LifeBar.F-43;
						}
						
						
						if (game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army].AmIDead(game)==true && game.Memory["Display"]["PlayerLife"][Btn.Owner][Btn.Army]<=0)
						{
							Btn.dx=Btn.ox-300*Btn.F;
							Btn.active=false;							
						}		
						
					}
					else if (Btn.data=="Spell" || Btn.data=="SpellEnemy")
					{
						if (Btn.active==true && game.Memory["ButtonSelection"][Btn.Owner]==null)
						{
							Btn.active=false;
						}
						if (Btn.active==true)
						{
							Btn.alpha=Math.min(1,Btn.alpha+0.1);
						}
						else
						{							
							Btn.alpha=Math.max(0,Btn.alpha-0.1);
						}
						Btn.BG.gotoAndStop(3)
						if (Btn.SpellData!=null)
						{
							var ManaReq=0;
							for (var Gir=0; Gir<Btn.ManaDisplays.length; Gir++)
							{
								if (SpellData.SpellBook[Btn.SpellData][1][Gir]==0 || 
									SpellData.SpellBook[Btn.SpellData][1][Gir]<=game.Memory["PlayerMana"][Btn.Owner][Gir])
									{
										ManaReq++
									}
								}											
							if (ManaReq>=GlobalVariables.xk_MAX_MANA_TYPES)
							{
								Btn.BG.gotoAndStop(2)
							}
						}
					}
					else if (Btn.data=="MainMenu")
					{
						if (game.Memory["PopUpMenuOpen"]!=true)
						{
							Btn.dy=game.Memory["Stage"].stageHeight+126;
						}
						else
						{
							Btn.dy=game.Memory["Stage"].stageHeight-80;
						}
						
					}
					
					if (Btn.y<Btn.dy)
					{
						Btn.y=Math.min(Btn.dy,Btn.y+GlobalVariables.GemSpeed)
					}
					if (Btn.y>Btn.dy)
					{
						Btn.y=Math.max(Btn.dy,Btn.y-GlobalVariables.GemSpeed)
					}
					
					if (Btn.x<Btn.dx)
					{
						Btn.x=Math.min(Btn.dx,Btn.x+GlobalVariables.GemSpeed/3)
					}
					if (Btn.x>Btn.dx)
					{
						Btn.x=Math.max(Btn.dx,Btn.x-GlobalVariables.GemSpeed/3)
					}
				}				
			}			
		}
		
		public static function MouseOverBoard(game:MainGame,X:Number,Y:Number)
		{
			var Gem=GetMouseGem(game,X,Y);
				
				if (game.Memory["SpellBeingCast"]==null)
				{
					if (game.Memory["GemSelection"]==null || game.Memory["GemSelection"]==undefined)
					{
								game.Memory["GemSelection"]=Gem;
								game.Memory["Selection"].x=Gem.Clip.parent.x+Gem.Clip.x;
								game.Memory["Selection"].y=Gem.Clip.parent.y+Gem.Clip.y;
								game.Memory["Selection"].active=true;
								game.Memory["Selection"].gotoAndStop(1);
					}
					else
					{
						if (game.Memory["PlayerTurn"]==0 && 
							((game.Memory["GemSelection"].X==Gem.X && game.Memory["GemSelection"].Y==Gem.Y-1) ||
							(game.Memory["GemSelection"].X==Gem.X && game.Memory["GemSelection"].Y==Gem.Y+1) ||
							(game.Memory["GemSelection"].X==Gem.X-1 && game.Memory["GemSelection"].Y==Gem.Y) ||
							(game.Memory["GemSelection"].X==Gem.X+1 && game.Memory["GemSelection"].Y==Gem.Y)))
							{
								GemManager.PlayerMoveGems(game,0,Gem,game.Memory["GemSelection"]);
								
								game.Memory["GemSelection"]=null;
								game.Memory["Selection"].active=false;
							}
							else if (game.Memory["GemSelection"]==GemManager.GetGemAt(game,Gem.X,Gem.Y))
									 {
								
								game.Memory["GemSelection"]=null;
								game.Memory["Selection"].active=false;
									 }
									 else
							{								
								game.Memory["GemSelection"]=Gem;
								game.Memory["Selection"].x=Gem.Clip.parent.x+Gem.Clip.x;
								game.Memory["Selection"].y=Gem.Clip.parent.y+Gem.Clip.y;
								game.Memory["Selection"].active=true;
								game.Memory["Selection"].gotoAndStop(1);
							}
					}
				}
				else if (cSpellEffect.DoITargetArmies(game)==false)
				{
					if (cSpellEffect.AmITheRightGem(game,Gem)==true)
						{
							cSpellEffect.CastSpellOnGemTarget(game,Gem);
						}
						else if (game.Memory["SpellBeingCast"][1]==cSpellEffect.targeting_GemRed)
						{
							DrawInfoOverlay.DisplayErrorOverlay(game,5);
						}
						else if (game.Memory["SpellBeingCast"][1]==cSpellEffect.targeting_GemBlue)
						{
							DrawInfoOverlay.DisplayErrorOverlay(game,6);
						}
						else if (game.Memory["SpellBeingCast"][1]==cSpellEffect.targeting_GemGreen)
						{
							DrawInfoOverlay.DisplayErrorOverlay(game,7);
						}
						else if (game.Memory["SpellBeingCast"][1]==cSpellEffect.targeting_GemYellow)
						{
							DrawInfoOverlay.DisplayErrorOverlay(game,8);
						}
						else if (game.Memory["SpellBeingCast"][1]==cSpellEffect.targeting_GemWhite)
						{
							DrawInfoOverlay.DisplayErrorOverlay(game,9);
						}
						else if (game.Memory["SpellBeingCast"][1]==cSpellEffect.targeting_GemPurple)
						{
							DrawInfoOverlay.DisplayErrorOverlay(game,10);
						}						
				}
						else if (cSpellEffect.DoITargetArmies(game)==true)
						{
							DrawInfoOverlay.DisplayErrorOverlay(game,11);
						}
		}
		
		public static function GetMouseGem(game:MainGame,X:Number,Y:Number):Object {			
			if (X<=game.Memory["GemLayer"].x || Y<=game.Memory["GemLayer"].y || X>=game.Memory["GemLayer"].x+game.Memory["BoardSize"]*GlobalVariables.GemSize || Y>=game.Memory["GemLayer"].y+game.Memory["BoardSize"]*GlobalVariables.GemSize || game.Memory["GemsMoving"]==true)
			{
				return null;
			}
			else
			{
				X=Math.ceil((X-game.Memory["GemLayer"].x)/GlobalVariables.GemSize);
				Y=Math.ceil((Y-game.Memory["GemLayer"].y)/GlobalVariables.GemSize);
				
				return GemManager.GetGemAt(game,X,Y);
			}
		}
		public static function UpdateUI(game:MainGame){
			
			for (var Player=0; Player<game.Memory["Display"]["PlayerMana"].length; Player++)
			{
				for (var Zim=0; Zim<game.Memory["Display"]["PlayerMana"][Player].length; Zim++)
				{
					if (game.Memory["Display"]["PlayerMana"][Player][Zim]!=game.Memory["PlayerMana"][Player][Zim])
					{
						game.Memory["Display"]["PlayerMana"][Player][Zim]=game.Memory["PlayerMana"][Player][Zim];
					}
				}
				for (var Gir=0; Gir<game.Memory["DisplayItemArmies"][Player].length; Gir++)
				{
					if (game.Memory["Display"]["PlayerLife"][Player][Gir]!=game.Memory["DisplayItemArmies"][Player][Gir].Life)
					{
						game.Memory["Display"]["PlayerLife"][Player][Gir]=game.Memory["DisplayItemArmies"][Player][Gir].Life;
					}
				}
			}
		}
	}
	
}
