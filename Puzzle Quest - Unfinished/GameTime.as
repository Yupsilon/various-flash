﻿package  {	
	import flash.events.*;
	import flash.display.*;

	public class GameTime extends MovieClip {

		private var T=1;
		public var ClickTimer=1;

		public function init() {
			stage.addEventListener(Event.ENTER_FRAME,Tick);
			T=0;
			ClickTimer=5;
		}
		private function Tick(evt:Event)
		{
			T=T+1;			
		}
		public function GetTime():int{
			return T;			
		}

	}
	
}
