﻿package  {
	import flash.display.*;
	import flash.events.*;
	
	public class HubWorldHandler extends MovieClip {

		/*public var GTime:GameTime=null;
		public var Stage=stage;
		public var Data:GameData=null;
		public var Sounds:SoundManager;
		
		//World Map
		public var map:MovieClip=null;
		public var world:MovieClip=null;
		public var InfoUI:MovieClip=null;
		
		public var BarracksUI:MovieClip=null;
		public var BarracksCategory:String="";
		public var BarracksMaX:Number=0;		
		public var BarracksSwapping:Boolean=false;		
		public var BBGUI:MovieClip=null;
		
		public var MapLocations:Array=null;
		public var Buttons:Array=null;
		public var PlayerParty:Array=null;
		public var MM=null;*/
		
		public var Memory:Function;

		function HubWorldHandler() {
			init();
			MainMenus.DrawMainMenu(this);
		}
		/*
		function DrawMainMenus(Intro:Boolean=true) {
			
			Memory["MM"]= new MainMenus();
			this.addChild(Memory["MM"]);
			Memory["MM"].DrawMainMenu(this,Intro);
		}*/
		
		function init() {
			Memory=new Function();
			Memory["Stage"]=stage;
			
			for( var i:int = 0; i<stage.numChildren - 1; i++ ) {
    			if( stage.getChildAt(i) is GameTime ) {
					Memory["GameTime"]=stage.getChildAt(i) as GameTime;
				}
			}	
			if (Memory["GameTime"]==null)
			{
				Memory["GameTime"]=new GameTime();
				stage.addChild(Memory["GameTime"]);
				Memory["GameTime"].init();
			}
			
			Memory["Effects"]=new Array();
			Memory["SoundManager"]=new SoundManager();
			this.addChild(Memory["SoundManager"]);
			
			stage.addEventListener(Event.ENTER_FRAME,Tick);
			stage.addEventListener(MouseEvent.MOUSE_UP,MouseDn);
		}
		
		public function StartGame(SaveFile:Number) {
			
			Memory["Data"]=GameData.loadData(SaveFile)
			Memory["Difficulty"]=GlobalVariables.Difficulty[SaveFile];		
			if (Memory["Data"]==null)
			{
				var SaveData:GameData=new GameData();
				SaveData.Name="Save "+(SaveFile+1)+"\n";
				SaveData.ID=SaveFile;				
				SaveData.saveData()
				SaveData.PlayerData[0]=MapData.StartLocation;			
				Memory["Data"]=SaveData;
			}			
			if (Memory["MM"]!=null && Memory["MM"].parent==this)
			{
				this.removeChild(Memory["MM"])
			}
			
			//StartTheGame
			HubWorld.Draw(this);
			
		}
		
		public function Tick(evt:Event)
		{
			if (Memory["HubWorld"]!=null){
			HubWorld.CameraBorders(this,stage.mouseX,stage.mouseY)
			}
			HubWorld.UpdateVictoryScreen(this)
			SpecialEffects.HandleSpecialEffects(this)
		}
		
		public function MouseDn(evt:MouseEvent)
		{
			if (Memory["HubWorld"]!=null){
				HubWorld.UiButtons(this,evt)
			}
			if (Memory["MM"]!=null)
			{
				MainMenus.HandleButtons(this,evt)
			}
			if (Memory["BarracksUI"]!=null)
			{
				DataBarracks.HandleButtons(this,evt)
			}
		}

	}
	
}
