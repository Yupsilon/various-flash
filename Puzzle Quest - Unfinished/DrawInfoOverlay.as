﻿package  {
	
		import flash.text.*;
		import flash.display.*;
		import flash.events.*;

	public class DrawInfoOverlay {		
	
			public static function PrepareInfoOverlay(game:MainGame)
			{				
				if (game.Memory["InfoOverlay"]==null)
				{
					
					
					var Zim:MovieClip=new MovieClip()
					game.Memory["LayerLayer"].addChild(Zim);
					Zim.x=0;
					Zim.y=0;
					Zim.alpha=0;
					game.Memory["InfoOverlay"]=Zim;
					
					var BackGround:InfoOverlay=new InfoOverlay()
					Zim.addChild(BackGround);
					BackGround.x=0;
					BackGround.y=0;
					BackGround.width=GlobalVariables.OverlayWidth;
					BackGround.height=GlobalVariables.OverlayWidth;
					game.Memory["InfoOverlay"].BG=BackGround;
										
					var DIAGTEXT:TextField=new TextField()	
					game.Memory["InfoOverlay"].addChild(DIAGTEXT);
					DIAGTEXT.text="aaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaaaaaaaa";
					DIAGTEXT.selectable=false;
					DIAGTEXT.wordWrap=true;	
					var format=GlobalVariables.TextARMYUI;
					DIAGTEXT.setTextFormat(format);
					DIAGTEXT.width=BackGround.width-8;	
					DIAGTEXT.height=Math.max(DIAGTEXT.height+format.size,DIAGTEXT.textHeight+format.size)-6;
					BackGround.height=Math.max(DIAGTEXT.height+format.size,BackGround.height);
					DIAGTEXT.x=0-DIAGTEXT.width/2+3;
					DIAGTEXT.y=0+3;
					
					game.Memory["InfoOverlay"].Diag=DIAGTEXT;
				}
				
			}
			
			public static function GetText(game:MainGame,Btn:MovieClip):String
			{			
				if (Btn!=null)
				{
						if (Btn.UIdata=="Army")
						{
							var Strink:String=game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army].Name+Language.BarracksText[0]+Math.floor(game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army].Level)+"\n"+Language.BarracksText[10]+Language.PassiveText[DataItem.Passives.indexOf(game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army].Passive)][0];
							if (game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army].Modifiers.length>0)
							{
								Strink+=("\n\n"+game.Memory["DisplayItemArmies"][Btn.Owner][Btn.Army].GetModifiers(game));
							}
							return Strink;
						}
						else if (Btn.UIdata=="Attack")
						{
							return Language.InfoUIText[0];
						}
						else if (Btn.UIdata=="Armor")
						{
							return Language.InfoUIText[1];
						}
						else if (Btn.UIdata=="Special")
						{
							return Language.InfoUIText[2];
						}
						else if (Btn.UIdata=="AttackSpell")
						{
							return Language.SpellText[0][0]+"\n\n"+Language.SpellText[0][1];
						}
						else if (Btn.UIdata=="DefenderSpell")
						{
							return Language.SpellText[1][0]+"\n\n"+Language.SpellText[1][1];
						}
						else if (Btn.UIdata=="SpellData")
						{
							return Language.SpellText[Btn.SpellData][0]+"\n\n"+Language.SpellText[Btn.SpellData][1];
						}
						else
						{
							return "";
						}
				}
					else
					{
						return "null";
					}
			}
		
			public static function FrameEvent(game:MainGame)
			{
				var MoC:MovieClip=InGameButtons.getMouseButton(game,game.Memory["Stage"].mouseX,game.Memory["Stage"].mouseY);
				
				if (game.Memory["InfoOverlay"].Btn!=MoC)
				{
					game.Memory["InfoOverlay"].Time=game.Memory["HWH"].Memory["GameTime"].GetTime()+30;
					game.Memory["InfoOverlay"].Btn=MoC
					if (MoC==null)
					{
						game.Memory["InfoOverlay"].Time=-1;
					}
					game.Memory["InfoOverlay"].alpha=0;
					game.Memory["InfoOverlay"].Diag.text=GetText(game,game.Memory["InfoOverlay"].Btn);
					game.Memory["InfoOverlay"].Diag.setTextFormat(GlobalVariables.TextARMYUI);
					game.Memory["InfoOverlay"].BG.height=game.Memory["InfoOverlay"].Diag.textHeight+GlobalVariables.TextARMYUI.size;
					game.Memory["InfoOverlay"].BG.y=0+(game.Memory["InfoOverlay"].BG.height)/2
				}
				
				if (game.Memory["InfoOverlay"].Time>=0)
				{
					if (game.Memory["InfoOverlay"].Time<game.Memory["HWH"].Memory["GameTime"].GetTime())
					{						
						game.Memory["InfoOverlay"].x=Math.max(0,Math.min(game.Memory["Stage"].mouseX+game.Memory["InfoOverlay"].BG.width/2,GlobalVariables.stageWidth-game.Memory["InfoOverlay"].BG.width/2));
						game.Memory["InfoOverlay"].y=Math.max(0,Math.min(game.Memory["Stage"].mouseY,GlobalVariables.stageHeight-game.Memory["InfoOverlay"].BG.height));
						game.Memory["InfoOverlay"].alpha=Math.min(1,game.Memory["InfoOverlay"].alpha+0.2);
					}
				}
			}	
			public static function DisplayErrorOverlay(game:MainGame,Data:Number)
			{
				game.Memory["InfoOverlay"].Time=0;
				game.Memory["InfoOverlay"].Btn=InGameButtons.getMouseButton(game,game.Memory["Stage"].mouseX,game.Memory["Stage"].mouseY);
				game.Memory["InfoOverlay"].alpha=0;
				game.Memory["InfoOverlay"].Diag.text=Language.ErrorData[Data];
				game.Memory["InfoOverlay"].Diag.setTextFormat(GlobalVariables.TextARMYUI);
				game.Memory["InfoOverlay"].BG.height=game.Memory["InfoOverlay"].Diag.textHeight+GlobalVariables.TextARMYUI.size;
				game.Memory["InfoOverlay"].BG.y=0+(game.Memory["InfoOverlay"].BG.height)/2
			}
	}	
}
