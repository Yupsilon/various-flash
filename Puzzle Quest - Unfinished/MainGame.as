﻿package  {
	import flash.display.*;
	import flash.events.*;
	
	public class MainGame extends MovieClip {

		var Memory:Function;
		
		/*Todo
		
		MAP
		XP GAIN
		MAP ICONS
		MANA PARTICLES
		BARRACKSBUTTONS
		SPECIALEFFECTTEXT MODIFIERS
		SPECIALEFFECTTEXT
		EFFECTS
		MAIN MENU
		REBALANCE ABILITIES
		FPS DROP
		GEM BOARD GENERATION
		
		*/
		
		/*var LayerLayer:MovieClip;
		var EffectLayer:MovieClip;
		var UiLayer:MovieClip;
		var GemLayer:MovieClip;
		var BGLayer:MovieClip;
		var STAGE;
		var Buttons:Array;
		var ButtonSelection:Array;
		
		var gamePaused=false;
		var PlayerTurn;
		var MouseDown:Boolean;
		
		var SpellBeingCast:Array;
		
		var BoardSize;
		var BoardUpdate;
		var Board:Array;
		
		var GemsMoving;
		var GemSelection;
		var PrevGemColor;
		var Gems:Array;
				
		public var PlayerNames:Array=["Panty","Stocking"];				
		public var DataItemArmies:Array;
		public var Rewards:Array;
		public var HWH:HubWorldHandler;
		
		public var PlayerMana:Array=[[0,0,0,0,0,0],[0,0,0,0,0,0]];	
		var DisplayItemArmies:Array;
		
		public var GameOver:Boolean;
		public var PlayerLife:Array;*/

		/*public function MainGame() {
			// constructor code		
			BetaFunction()
			init();
		}*/
		
		function init(game:HubWorldHandler) {											
		
			Memory=new Function();		
			Memory["HWH"]=game;
			Memory["Stage"]=game.Memory["Stage"];
			game.Memory["game"]=this;
			Memory["Gems"]=new Array();
			Memory["Buttons"]=new Array();
			Memory["BoardSize"]=Math.ceil((GlobalVariables.stageHeight*0.8)/GlobalVariables.GemSize);
			Memory["GemsMoving"]=false;
			Memory["PlayerTurn"]=-1;
			Memory["MouseDown"]=false;
			Memory["DisplayItemArmies"]=new Array();
			Memory["ButtonSelection"]=[null,null];
			Memory["GameOver"]=false;			
			Memory["Effects"]=new Array();
			
			Memory["gamePaused"]=false;		
			Memory["SpellBeingCast"]= null		
			Memory["BoardUpdate"]=false;		
			Memory["GemSelection"]=null;
			Memory["PrevGemColor"]=-1;
			Memory["CurrentTurn"]=0;
			
			Memory["Display"]=new Array();
			
			Memory["BoardMana"]=[0,0,0,0,0,0]
			Memory["PlayerMana"]=[[0,0,0,0,0,0],[0,0,0,0,0,0]];	
			Memory["Display"]["PlayerMana"]=[[0,0,0,0,0,0],[0,0,0,0,0,0]];
			Memory["Display"]["PlayerLife"]=[[0,0,0,0,0],[0,0,0,0,0]];
			Memory["Display"]["PlayerLifeUpdateCheck"]=[[0,0,0,0,0],[0,0,0,0,0]];
		}
		
			
		function fin() {							
			DataItem.PrepareTheArmies(this);
			InGameUI.DrawTheUI(this);	
			DrawInfoOverlay.PrepareInfoOverlay(this);
			GemManager.DrawTheBoard(this);
			this.addEventListener(Event.ENTER_FRAME,FrameEvent);			
			Memory["Stage"].addEventListener(MouseEvent.MOUSE_DOWN,OnMouseDown);
			Memory["Stage"].addEventListener(MouseEvent.MOUSE_UP,OnMouseUp);
		}
		
		function FrameEvent(evt:Event) {
				InGameButtons.UpdateButtons(this);	
			if (Memory["gamePaused"]==false && Memory["VictoryT"]==null && Memory["DefeatT"]==null)
			{
				GemManager.HandleGems(this);
				DrawInfoOverlay.FrameEvent(this);	
				AiPlayer.HandleTurn(this);	
			}
			else if (Memory["VictoryT"]<Memory["HWH"].Memory["GameTime"].GetTime())
			{
					DoVictory()
			}
			else if (Memory["DefeatT"]<Memory["HWH"].Memory["GameTime"].GetTime())
			{
					DoDefeat()
			}
			
			if (Memory["HWH"].Memory["Effects"].length==0)
			{
				InGameButtons.UpdateUI(this);
			}
		}
			
		public function CheckPlayersLife()
			{
				if (Memory["PlayerLife"][0]>1/2 && Memory["PlayerLife"][1]<=1/2)
				{
					Memory["VictoryT"]=Memory["HWH"].Memory["GameTime"].GetTime()+GlobalVariables.IdleTime;
				}
				else if (Memory["PlayerLife"][1]>1/2 && Memory["PlayerLife"][0]<=1/2)
				{
					Memory["DefeatT"]=Memory["HWH"].Memory["GameTime"].GetTime()+GlobalVariables.IdleTime;
				}
			}	
		
		
		public function OnMouseUp(evt:MouseEvent) {
			InGameButtons.HandleInGameButtons(this,evt.stageX,evt.stageY);
			Memory["MouseDown"]=false;
			}
		public function OnMouseDown(evt:MouseEvent) {
			Memory["MouseDown"]=true;
			}
			
		public function Close()
		{
			this.removeEventListener(Event.ENTER_FRAME,FrameEvent);			
			Memory["Stage"].removeEventListener(MouseEvent.MOUSE_DOWN,OnMouseDown);
			Memory["Stage"].removeEventListener(MouseEvent.MOUSE_UP,OnMouseUp);
			Memory["LayerLayer"].parent.removeChild(Memory["LayerLayer"]);
			Memory=null;
		}		
			
		public function DoVictory()
		{
			Memory["HWH"].Memory["Data"].PlayerData[1]+=Memory["Rewards"][0];					
					var VictoryData:Array=new Array();
					
					if (Memory["Rewards"][1]>0)
					{
						for each (var Zim:DisplayItemArmy in Memory["DisplayItemArmies"][0])
						{
							if (Zim!=null && Zim.realID>=0)
							{
								VictoryData.push(Memory["HWH"].Memory["Data"].Armies[Zim.realID].AddXP(Memory["Rewards"][1],Memory["DataItemArmies"][1][0].Level*Memory["DataItemArmies"][1].length));
								
								if (Zim.AmIDead(this)==true)
								{
									Memory["HWH"].Memory["Data"].Armies[Zim.realID].Deaths+=1;
									if (Memory["Difficulty"]>=GlobalVariables.Difficulty[2])
									{
										Memory["HWH"].Memory["Data"].Graveyard.push(Memory["HWH"].Memory["Data"].Armies[Zim.realID])
										Memory["HWH"].Memory["Data"].Armies.splice(Memory["HWH"].Memory["Data"].Armies.indexOf(Memory["HWH"].Memory["Data"].Armies[Zim.realID]),1)
									}
								}
							}
						}
					}
					if (Memory["Rewards"][2]!=null)
					{
						DataItem.GivePlayerArmy(Memory["HWH"],DataItemArmy.MakeNewArmy(Memory["Rewards"][2]));
					}
					if (Memory["Rewards"][3]!=null)
					{
						Memory["HWH"].Memory["Data"].updateProgress(Memory["Rewards"][3])
					}
					
					HubWorld.Draw(Memory["HWH"]);
					HubWorld.DisplayVictoryScreen(Memory["HWH"],Memory["Rewards"],VictoryData);
					Memory["HWH"].Memory["Data"].saveData();
					Close();
		}
			
		public function DoDefeat()
		{	
						for each (var Zim:DisplayItemArmy in Memory["DisplayItemArmies"][0])
						{
							if (Zim.AmIDead(this)==true && Zim.realID>=0)
							{
								Memory["HWH"].Memory["Data"].Armies[Zim.realID].Deaths+=1;
									if (Memory["Difficulty"]>=GlobalVariables.Difficulty[2])
									{
										Memory["HWH"].Memory["Data"].Graveyard.push(Memory["HWH"].Memory["Data"].Armies[Zim.realID])
										Memory["HWH"].Memory["Data"].Armies.splice(Memory["HWH"].Memory["Data"].Armies.indexOf(Memory["HWH"].Memory["Data"].Armies[Zim.realID]),1)
									}
							}
						}
					HubWorld.Draw(Memory["HWH"]);
					DataMissions.TakeGold(Memory["HWH"],Memory["Rewards"][0],true);
					HubWorld.DisplayDefeatScreen(Memory["HWH"],Memory["Rewards"]);
					Memory["HWH"].Memory["Data"].saveData();
					Close();
		}
			
		public function ForwardTurn()
		{			
			Memory["CurrentTurn"]+=1;
			for (var Player=0; Player<Memory["DisplayItemArmies"].length; Player++)
			{
				for (var Zim=0; Zim<Memory["DisplayItemArmies"][Player].length; Zim++)
				{
					Memory["DisplayItemArmies"][Player][Zim].HandleTroop(this,Memory["CurrentTurn"])
				}				
			}
			Memory["AiPlayer"]["SpellsCastThisTurn"]=null;
			Memory["AiPlayer"]["IdleTime"]=Memory["HWH"].Memory["GameTime"].GetTime()+GlobalVariables.AiIdleTime;
			
			Memory["Player"+Memory["PlayerTurn"]+"Turn"].alpha=1;
			Memory["Player"+(1-Memory["PlayerTurn"])+"Turn"].alpha=0;
		}
	}
	
}
