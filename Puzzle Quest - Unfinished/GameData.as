﻿package  {
	import flash.net.SharedObject;
	
	public class GameData {

		var Name;
		var ID=0;
		var Armies=new Array();
		var BenchedArmies=new Array();
		var Graveyard=new Array();
		var ProgressMade=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];		
		
		//0-tutorial
		//1-mainstory
		//2-wolves
		
		var PlayerData=[0,0];	//First one is Player Location, second one is player gold

		public function saveData():void{		
			
			var saveDataObject = SharedObject.getLocal("PzlQueF");
						 			 			 
			 var StoreArmies:Array=new Array();			 
			 for (var Zim=0; Zim<Armies.length; Zim++)
			 {
				 if (Armies[Zim]!=null)
				 {
					 StoreArmies.push(Armies[Zim].Wrap())
				 }
			 }
			 			 
			 var StoreGarrison:Array=new Array();
			 for (Zim=0; Zim<BenchedArmies.length; Zim++)
			 {
				 if (BenchedArmies[Zim]!=null)
				 {
					 StoreGarrison.push(BenchedArmies[Zim].Wrap())
				 }
			 }
			 			 
			 var StoreGraveyard:Array=new Array();
			 for (Zim=0; Zim<Graveyard.length; Zim++)
			 {
				 if (Graveyard[Zim]!=null)
				 {
					 StoreGraveyard.push(Graveyard[Zim].Wrap())
				 }
			 }
			 			 
			 var SaveData:Array=[Name,StoreArmies,StoreGarrison,StoreGraveyard,ProgressMade,PlayerData];
			 saveDataObject.data.SaveData[ID]=SaveData;
			 saveDataObject.flush(); 
		}
		
		public static function eraseSave(ID:Number):void{		
			
			var saveDataObject = SharedObject.getLocal("PzlQueF");
									 			 
			
			 saveDataObject.data.SaveData[ID]=null;
			 saveDataObject.flush(); 
		}
		
		public function updateProgress(Data:Array):void{		
			
			for each (var Zim:Array in Data)
			{
				if (Zim[2]=="=")
				{
					ProgressMade[Zim[0]]=Zim[1];
				}
				else if (Zim[2]=="+")
				{
					ProgressMade[Zim[0]]+=Zim[1];
				}
				else if (Zim[2]=="-")
				{
					ProgressMade[Zim[0]]-=Zim[1];
				}
			}
			
			saveData(); 
		}
		
		public function GetCompletionPercent():Number
		{
			var Completion=[0,0];
			for (var Zim=0; Zim<MapData.ProgressCompletion.length; Zim++)
			{
				if (MapData.ProgressCompletion[Zim]!=0)
				{
					if (ProgressMade[Zim]!=null){
					Completion[0]+=Math.min(ProgressMade[Zim],MapData.ProgressCompletion[Zim])}
					Completion[1]+=MapData.ProgressCompletion[Zim]
				}
			}
			return Completion[0]/Completion[1]*100;
		}
		 
		public static function loadData(SaveFile:Number):GameData
		{	
			var saveDataObject = SharedObject.getLocal("PzlQueF");
			if (saveDataObject.data.SaveData!=null)
			{
				if (saveDataObject.data.SaveData[SaveFile]!=null)
				{
					var SaveData:GameData=new GameData();
					SaveData.Name=saveDataObject.data.SaveData[SaveFile][0]
					
					SaveData.Armies=new Array();
					 for (var Zim=0; Zim<saveDataObject.data.SaveData[SaveFile][1].length; Zim++)
					 {
						 if (saveDataObject.data.SaveData[SaveFile][1][Zim]!=null)
						 {
							 SaveData.Armies.push(DataItemArmy.UnWrap(saveDataObject.data.SaveData[SaveFile][1][Zim]))
						 }
					 }
					 
					 SaveData.Graveyard=new Array();
					 for (Zim=0; Zim<saveDataObject.data.SaveData[SaveFile][2].length; Zim++)
					 {
						 if (saveDataObject.data.SaveData[SaveFile][2][Zim]!=null)
						 {
							 SaveData.BenchedArmies.push(DataItemArmy.UnWrap(saveDataObject.data.SaveData[SaveFile][2][Zim]))
						 }
					 }
					 
					 SaveData.Graveyard=new Array();
					 for (Zim=0; Zim<saveDataObject.data.SaveData[SaveFile][3].length; Zim++)
					 {
						 if (saveDataObject.data.SaveData[SaveFile][3][Zim]!=null)
						 {
							 SaveData.Graveyard.push(DataItemArmy.UnWrap(saveDataObject.data.SaveData[SaveFile][3][Zim]))
						 }
					 }
					
					if (saveDataObject.data.SaveData[SaveFile][4]!=null)
					{
						SaveData.ProgressMade=new Array(MapData.ProgressCompletion.length)
						for (var Zim=0; Zim<MapData.ProgressCompletion.length; Zim++)
						{
							if (saveDataObject.data.SaveData[SaveFile][4]!=null)
							{
								SaveData.ProgressMade[Zim]=saveDataObject.data.SaveData[SaveFile][4][Zim];
							}
							else
							{
								SaveData.ProgressMade[Zim]=0;
							}
						}
					}
					if (saveDataObject.data.SaveData[SaveFile][5]!=null)
					{
						SaveData.PlayerData=saveDataObject.data.SaveData[SaveFile][5]
					}
					return SaveData;
				}
				else
				{
					return null;
				}
			}
			else
			{
				saveDataObject.data.SaveData=[null,null,null];
				saveDataObject.flush(); 
				return null;
			}
		}

	}
	
}
