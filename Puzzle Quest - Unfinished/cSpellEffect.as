﻿package  {
	
	public class cSpellEffect {

		public static var targeting_BasicAttack=0;		
		public static var targeting_UnitFriendly=1;	
		public static var targeting_UnitEnemy=2;
		public static var targeting_UnitBoth=3;
		public static var targeting_NullTarget=4;
		public static var targeting_AllFriendlies=5;
		public static var targeting_AllEnemies=6;
		public static var targeting_AllArmies=7;
		public static var targeting_GemAny=8;
		public static var targeting_GemRed=9;
		public static var targeting_GemBlue=10;
		public static var targeting_GemGreen=11;
		public static var targeting_GemYellow=12;
		public static var targeting_GemWhite=13;
		public static var targeting_GemPurple=14;		
		public static var targeting_EntireBoard=15;		
				
		public static function PrepareSpell(game:MainGame,SpellID:Number,PlayerOwner:Number,ArmyCaster:Number)
		{
			if (game.Memory["PlayerTurn"]==PlayerOwner)
			{
			var Spell = SpellData.SpellBook[SpellID];
			if (HandleMana.DoIHaveEnoughMana(game,PlayerOwner,Spell[1],false)==true && 
				(game.Memory["DisplayItemArmies"][PlayerOwner][ArmyCaster].CanCast(game)==true || (SpellID<=SpellData.cSpellDefender && game.Memory["DisplayItemArmies"][PlayerOwner][ArmyCaster].CanAct(game)==true)))
				{						
					if (	Spell[1][GlobalVariables.GEM_SKULL]>Spell[1][GlobalVariables.GEM_RED] && 
							 Spell[1][GlobalVariables.GEM_SKULL]>Spell[1][GlobalVariables.GEM_BLUE] && 
							 Spell[1][GlobalVariables.GEM_SKULL]>Spell[1][GlobalVariables.GEM_GREEN] && 
							 Spell[1][GlobalVariables.GEM_SKULL]>Spell[1][GlobalVariables.GEM_YELLOW])
					{
						game.Memory["Channel"]=SpecialEffects.MakeSpecialEffectChannel(game.Memory["HWH"],game.Memory["DisplayItemArmies"][PlayerOwner][ArmyCaster],game.Memory["EffectLayer"],0);
					}
					else if (Spell[1][GlobalVariables.GEM_RED]>=Spell[1][GlobalVariables.GEM_SKULL] && 
							 Spell[1][GlobalVariables.GEM_RED]>=Spell[1][GlobalVariables.GEM_BLUE] && 
							 Spell[1][GlobalVariables.GEM_RED]>=Spell[1][GlobalVariables.GEM_GREEN] && 
							 Spell[1][GlobalVariables.GEM_RED]>=Spell[1][GlobalVariables.GEM_YELLOW])
					{
						game.Memory["Channel"]=SpecialEffects.MakeSpecialEffectChannel(game.Memory["HWH"],game.Memory["DisplayItemArmies"][PlayerOwner][ArmyCaster],game.Memory["EffectLayer"],1,5);
					}
					else if (Spell[1][GlobalVariables.GEM_BLUE]>=Spell[1][GlobalVariables.GEM_SKULL] && 
							 Spell[1][GlobalVariables.GEM_BLUE]>=Spell[1][GlobalVariables.GEM_RED] && 
							 Spell[1][GlobalVariables.GEM_BLUE]>=Spell[1][GlobalVariables.GEM_GREEN] && 
							 Spell[1][GlobalVariables.GEM_BLUE]>=Spell[1][GlobalVariables.GEM_YELLOW])
					{
						game.Memory["Channel"]=SpecialEffects.MakeSpecialEffectChannel(game.Memory["HWH"],game.Memory["DisplayItemArmies"][PlayerOwner][ArmyCaster],game.Memory["EffectLayer"],2,5);
					}
					else if (Spell[1][GlobalVariables.GEM_GREEN]>=Spell[1][GlobalVariables.GEM_SKULL] && 
							 Spell[1][GlobalVariables.GEM_GREEN]>=Spell[1][GlobalVariables.GEM_RED] && 
							 Spell[1][GlobalVariables.GEM_GREEN]>=Spell[1][GlobalVariables.GEM_BLUE] && 
							 Spell[1][GlobalVariables.GEM_GREEN]>=Spell[1][GlobalVariables.GEM_YELLOW])
					{
						game.Memory["Channel"]=SpecialEffects.MakeSpecialEffectChannel(game.Memory["HWH"],game.Memory["DisplayItemArmies"][PlayerOwner][ArmyCaster],game.Memory["EffectLayer"],3,5);
					}
					else if (Spell[1][GlobalVariables.GEM_YELLOW]>=Spell[1][GlobalVariables.GEM_SKULL] && 
							 Spell[1][GlobalVariables.GEM_YELLOW]>=Spell[1][GlobalVariables.GEM_RED] && 
							 Spell[1][GlobalVariables.GEM_YELLOW]>=Spell[1][GlobalVariables.GEM_BLUE] && 
							 Spell[1][GlobalVariables.GEM_YELLOW]>=Spell[1][GlobalVariables.GEM_GREEN])
					{
						game.Memory["Channel"]=SpecialEffects.MakeSpecialEffectChannel(game.Memory["HWH"],game.Memory["DisplayItemArmies"][PlayerOwner][ArmyCaster],game.Memory["EffectLayer"],4,5);
					}
					else
					{
						game.Memory["Channel"]=SpecialEffects.MakeSpecialEffectChannel(game.Memory["HWH"],game.Memory["DisplayItemArmies"][PlayerOwner][ArmyCaster],game.Memory["EffectLayer"],5,5);
					}
					
					if (PlayerOwner==0)
					{
						game.Memory["ButtonSelection"][0]=null;
						game.Memory["ButtonSelection"][1]=null;				
					}
					game.Memory["SpellBeingCast"]=[SpellID,Spell[0],PlayerOwner,ArmyCaster];				
					
					if (game.Memory["SpellBeingCast"][1]==targeting_NullTarget)
					{
						CastSpellNoTarget(game);
					}
					else if (game.Memory["SpellBeingCast"][1]==targeting_EntireBoard)
					{
						CastSpellOnBoard(game);
					}
					else if (game.Memory["SpellBeingCast"][1]==targeting_AllFriendlies)
					{
						CastSpellOnArmies(game,0);
					}
					else if (game.Memory["SpellBeingCast"][1]==targeting_AllEnemies)
					{
						CastSpellOnArmies(game,1);
					}
					else if (game.Memory["SpellBeingCast"][1]==targeting_AllArmies)
					{
						CastSpellOnArmies(game,-1);
					}
				}
				else if (game.Memory["DisplayItemArmies"][PlayerOwner][ArmyCaster].CanAct(game)!=true)
				{
					DrawInfoOverlay.DisplayErrorOverlay(game,13);
				}
			}
			else
			{			
					DrawInfoOverlay.DisplayErrorOverlay(game,14);
			}
		}		
		
		public static function HandleAfterSpell(game:MainGame,Army:DisplayItemArmy,StunArmyCaster:Boolean,SpellID:Number)
		{			
			if (game.Memory["Channel"]!=null)
			{
				game.Memory["Channel"].animation="Death";
				game.Memory["Channel"]=null;
			}
			if (SpellID!=SpellData.cSpellAttack)
			{
				if (Army.Passive=="selfstunafterspell")
				{
					Army.AddModifier(Language.datModifierStunned,0,3,game.Memory["CurrentTurn"],false);
				}
				else if (Army.Passive=="selfsilenceafterspell")
				{
					Army.AddModifier(Language.datModifierSilenced,0,1,game.Memory["CurrentTurn"],false);
				}
				else if (StunArmyCaster==true)
				{
					if (Army.HasModifier(Language.datModifierSwift,game.Memory["CurrentTurn"]))
					{
						Army.RemoveModifier(Language.datModifierSwift);
					}
					else
					{
						Army.AddModifier(Language.datModifierAfterAct,0,1,game.Memory["CurrentTurn"],false);
					}
				}
				if (SpellID!=SpellData.cSpellDefender)
				{
					SpecialEffects.MakeSpecialEffectTextOnArmy(game,Language.SpellText[SpellID][0],Army,10)
				}
			}
			else 
			{		
				if (StunArmyCaster==true && Army!=null)
				{
					if (Army.HasModifier(Language.datModifierSwift,game.Memory["CurrentTurn"]))
					{
						Army.RemoveModifier(Language.datModifierSwift);
					}
					else
					{
						Army.AddModifier(Language.datModifierAfterAct,0,1,game.Memory["CurrentTurn"],false);
					}
				}
			}
		}
		
		public static function CastSpellNoTarget(game:MainGame)
		{
			var SpellMessage:DataItemSpell=new DataItemSpell;
			SpellMessage.spelldata=SpellData.SpellBook[game.Memory["SpellBeingCast"][0]];
			SpellMessage.caster=game.Memory["DisplayItemArmies"][game.Memory["SpellBeingCast"][2]][game.Memory["SpellBeingCast"][3]];					
			HandleAfterSpell(game,SpellMessage.caster,SpellMessage.spelldata[1][GlobalVariables.xk_MAX_MANA_TYPES]>0,game.Memory["SpellBeingCast"][0])
			
			if (HandleMana.DoIHaveEnoughMana(game,game.Memory["SpellBeingCast"][2],SpellMessage.spelldata[1])==true)
				{					
					SpellMessage.spelldata[2](game,SpellMessage);	
					game.Memory["SpellBeingCast"]=null;
				}
					SpellMessage=null;
		}
		
		public static function CastSpellOnArmyTarget(game:MainGame,ArmyTarget:DisplayItemArmy)
		{
			var SpellMessage:DataItemSpell=new DataItemSpell;
			SpellMessage.spelldata=SpellData.SpellBook[game.Memory["SpellBeingCast"][0]];
			SpellMessage.caster=game.Memory["DisplayItemArmies"][game.Memory["SpellBeingCast"][2]][game.Memory["SpellBeingCast"][3]];				
			HandleAfterSpell(game,SpellMessage.caster,SpellMessage.spelldata[1][GlobalVariables.xk_MAX_MANA_TYPES]>0,game.Memory["SpellBeingCast"][0])						
			
			if (ArmyTarget.AmIDead(game)==false && HandleMana.DoIHaveEnoughMana(game,game.Memory["SpellBeingCast"][2],SpellMessage.spelldata[1])==true)
				{	
					game.Memory["SpellBeingCast"]=null;
					SpellMessage.armytarget=ArmyTarget;
					SpellMessage.spelldata[2](game,SpellMessage);	
				}
		}
				
		public static function CastSpellOnArmies(game:MainGame,ArmyTarget:Number)
		{
			var SpellMessage:DataItemSpell=new DataItemSpell;
			SpellMessage.spelldata=SpellData.SpellBook[game.Memory["SpellBeingCast"][0]];
			SpellMessage.caster=game.Memory["DisplayItemArmies"][game.Memory["SpellBeingCast"][2]][game.Memory["SpellBeingCast"][3]];			
			HandleAfterSpell(game,SpellMessage.caster,SpellMessage.spelldata[1][GlobalVariables.xk_MAX_MANA_TYPES]>0,game.Memory["SpellBeingCast"][0])						
			SpellMessage.armytargets=new Array();
			for (var Zim=0; Zim<game.Memory["DisplayItemArmies"][0].length; Zim++)
			{
				if (((ArmyTarget==0 || ArmyTarget==-1) && SpellMessage.caster.PlayerOwner==0)
				|| ((ArmyTarget==1 || ArmyTarget==-1) && SpellMessage.caster.PlayerOwner==1))
				{
					SpellMessage.armytargets.push(game.Memory["DisplayItemArmies"][0][Zim]);			
				}
			}	
			for (var Zim=0; Zim<game.Memory["DisplayItemArmies"][1].length; Zim++)
			{
				if (((ArmyTarget==0 || ArmyTarget==-1) && SpellMessage.caster.PlayerOwner==1)
				|| ((ArmyTarget==1 || ArmyTarget==-1) && SpellMessage.caster.PlayerOwner==0))
				{
					SpellMessage.armytargets.push(game.Memory["DisplayItemArmies"][1][Zim]);			
				}
			}	
			
			
			if (HandleMana.DoIHaveEnoughMana(game,game.Memory["SpellBeingCast"][2],SpellMessage.spelldata[1])==true)
				{				
					SpellMessage.CastInstance=true;
					game.Memory["SpellBeingCast"]=null;
					for (Zim=0; Zim<SpellMessage.armytargets.length; Zim++)
					{
						if (SpellMessage.armytargets[Zim]!=null && SpellMessage.armytargets[Zim].AmIDead(game)==false)
						{
							SpellMessage.armytarget=SpellMessage.armytargets[Zim];
							SpellMessage.spelldata[2](game,SpellMessage);	
							SpellMessage.CastInstance=false;
						}
					}
					SpellMessage=null;
				}
		}
		
		
		public static function CastSpellOnGemTarget(game:MainGame,Target:DataItemGem)
		{
			var SpellMessage:DataItemSpell=new DataItemSpell;
			SpellMessage.spelldata=SpellData.SpellBook[game.Memory["SpellBeingCast"][0]];
			SpellMessage.caster=game.Memory["DisplayItemArmies"][game.Memory["SpellBeingCast"][2]][game.Memory["SpellBeingCast"][3]];								
			HandleAfterSpell(game,SpellMessage.caster,SpellMessage.spelldata[1][GlobalVariables.xk_MAX_MANA_TYPES]>0,game.Memory["SpellBeingCast"][0])								
			
			if (HandleMana.DoIHaveEnoughMana(game,game.Memory["SpellBeingCast"][2],SpellMessage.spelldata[1])==true)
				{	
					game.Memory["SpellBeingCast"]=null;
					SpellMessage.gemtarget=Target;		
					SpellMessage.spelldata[2](game,SpellMessage);	
					SpellMessage=null;
				}
		}
				
		public static function CastSpellOnBoard(game:MainGame)
		{
			var SpellMessage:DataItemSpell=new DataItemSpell;
			SpellMessage.spelldata=SpellData.SpellBook[game.Memory["SpellBeingCast"][0]];
			SpellMessage.caster=game.Memory["DisplayItemArmies"][game.Memory["SpellBeingCast"][2]][game.Memory["SpellBeingCast"][3]];			
			HandleAfterSpell(game,SpellMessage.caster,SpellMessage.spelldata[1][GlobalVariables.xk_MAX_MANA_TYPES]>0,game.Memory["SpellBeingCast"][0])
			SpellMessage.gemtargets=new Array();
			
			for each (var Zim in game.Memory["Gems"])
			{
				if (Zim!=null && Zim.dead==false)
				{
					SpellMessage.gemtargets.push(Zim);			
				}
			}	
			
			if (HandleMana.DoIHaveEnoughMana(game,game.Memory["SpellBeingCast"][2],SpellMessage.spelldata[1])==true)
				{				
					SpellMessage.spelldata[2](game,SpellMessage);
					SpellMessage=null;
					game.Memory["SpellBeingCast"]=null;
				}
		}
		
		public static function DoITargetArmies(game:MainGame):Boolean
		{
			return (game.Memory["SpellBeingCast"][1]==targeting_BasicAttack || game.Memory["SpellBeingCast"][1]==targeting_UnitEnemy || game.Memory["SpellBeingCast"][1]==targeting_UnitFriendly);
		}	
		
		public static function DoITargetAllies(game:MainGame):Boolean
		{
			return (game.Memory["SpellBeingCast"][1]==targeting_UnitFriendly);
		}		
		
		public static function DoITargetEnemies(game:MainGame):Boolean
		{
			return (game.Memory["SpellBeingCast"][1]==targeting_BasicAttack || game.Memory["SpellBeingCast"][1]==targeting_UnitEnemy);
		}
		
		public static function AmITheRightGem(game:MainGame,Gem:DataItemGem):Boolean
		{
			return (game.Memory["SpellBeingCast"][1]==targeting_GemAny || 
					(game.Memory["SpellBeingCast"][1]==targeting_GemRed && Gem.Type-1==GlobalVariables.GEM_RED) || 
					(game.Memory["SpellBeingCast"][1]==targeting_GemBlue && Gem.Type-1==GlobalVariables.GEM_BLUE) || 
					(game.Memory["SpellBeingCast"][1]==targeting_GemGreen && Gem.Type-1==GlobalVariables.GEM_GREEN) || 
					(game.Memory["SpellBeingCast"][1]==targeting_GemYellow && Gem.Type-1==GlobalVariables.GEM_YELLOW) || 
					(game.Memory["SpellBeingCast"][1]==targeting_GemPurple && Gem.Type-1==GlobalVariables.GEM_PURPLE) || 
					(game.Memory["SpellBeingCast"][1]==targeting_GemWhite && Gem.Type-1==GlobalVariables.GEM_SKULL));
		}	
		
		public static function AmIValidTarget(game:MainGame,Army:DisplayItemArmy):Boolean
		{
			if (game.Memory["SpellBeingCast"]==null || Army==null)
			{
				return false;
			}
			else if (game.Memory["SpellBeingCast"][1]!=targeting_BasicAttack || Army.Defender==true)
			{				
				return (Army.AmIDead(game)==false);
			}
			else
			{
				var Clear=(Army.AmIDead(game)==false);				
				for (var Zim=0; Zim<game.Memory["DisplayItemArmies"][Army.PlayerOwner].length; Zim++)
				{
					if (game.Memory["DisplayItemArmies"][Army.PlayerOwner][Zim]!=null && game.Memory["DisplayItemArmies"][Army.PlayerOwner][Zim]!=Army && 
						game.Memory["DisplayItemArmies"][Army.PlayerOwner][Zim].Defender==true && game.Memory["DisplayItemArmies"][Army.PlayerOwner][Zim].AmIDead(game)!=true)
						{
							Clear=false;
						}
				}
				return Clear;
			}
			
		}
		
	}
	
}
