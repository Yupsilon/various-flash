﻿package  {
	import flash.events.*;
	import flash.media.Sound;
	import flash.media.SoundTransform;
	import flash.display.MovieClip;
	
	public class SoundManager extends MovieClip {

		var MusicTime:Number=0;
		var MusicDuration:Number=0;
		var Music:Sound=null;
		var mutesound:Boolean=false;
		var Sounds:Array;
		
		function TurnOnTheMusic()
		{			
			Sounds=new Array();
			this.addEventListener(Event.ENTER_FRAME,PlayMusic);
		}
		
		function PlaySound(ID)
		{
			
			if (mutesound==false)
			{		
			var Random=Math.random()*2
			var mySound = null
			
			/*if (ID==1)
			{
				 mySound = new SFX1();
			}
			else if (ID==2)
			{
				 mySound = new SFX2();
			}
			else if (ID==3)
			{
				 mySound = new SFX3();
			}
			else if (ID==4)
			{
				 mySound = new SFX4();
			}
			
			var Sound = mySound;	
			Sound.Duration=Time+(Sound.length/100)
			Sound.Channel=Sound.play();
			Sounds.push(Sound)*/
			}
		}
		
		function MuteSounds()
		{
			mutesound=(mutesound==false);
			MusicTime=0;
			if (mutesound==true)
			{
				ClearSounds();
			}
		}
		
		function ClearSounds()
		{
			if (Sounds!=null)
			{
				for (var s=0; s<Sounds.length;s++)
				{
					var Sound=Sounds[s]
					if (Sound!=null && Sound.Channel!=null)
					{
						Sound.Channel.stop()
					}
				}
				Sounds.length=0;
			}
			MusicTime=0;
			MusicDuration=1;
			Music=null;
			this.removeEventListener(Event.ENTER_FRAME,PlayMusic);
		}
		
		function PlayMusic(evt:Event)
		{
			MusicTime=MusicTime+1
			
			if (MusicTime>MusicDuration)
			{
				if (mutesound==false)
				{
				/*Music = new G_Music;					
				MusicDuration = MusicTime+5650
				var trans:SoundTransform=new SoundTransform(0.1, -1);
				Music.Channel=Music.play(0,1,trans);
				Sounds.push(Music)
				Music.volume=1/4*/
				
				}
			}
		}

	}
	
}
