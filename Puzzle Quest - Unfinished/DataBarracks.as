﻿package  {
	import flash.events.*;
	import flash.display.*;
	import flash.geom.*;
	import flash.text.*;
	import flash.utils.*;
	
	public class DataBarracks extends MovieClip {
		
		
		
		public static function DrawBarracksMenu(game:HubWorldHandler):void {
			game.Memory["Buttons"]=new Array();
			
			game.Memory["BarracksUI"]=new MovieClip();
			game.stage.addChild(game.Memory["BarracksUI"]);
			game.Memory["BarracksUI"].x=0;
			game.Memory["BarracksUI"].y=0;	
			
			var BGO:BarracksBG=new BarracksBG();
			game.Memory["BarracksUI"].addChild(BGO);
			BGO.width=GlobalVariables.stageWidth;
			BGO.height=GlobalVariables.stageHeight;
			BGO.x=GlobalVariables.stageWidth/2;
			BGO.y=GlobalVariables.stageHeight/2;
			
			game.Memory["BarracksUI"]["BG"]=new MovieClip();
			game.Memory["BarracksUI"].addChild(game.Memory["BarracksUI"]["BG"]);
			game.Memory["BarracksUI"]["BG"].x=0;
			game.Memory["BarracksUI"]["BG"].y=0;	
			
			var Overlay:OverheadUI=new OverheadUI();
			game.Memory["BarracksUI"].addChild(Overlay);
			Overlay.width=GlobalVariables.stageWidth;
			Overlay.height=100*1;
			Overlay.x=GlobalVariables.stageWidth/2;
			Overlay.y=100*1/2;
			
			var Data=["Arrange","Swap","exit"];
			
			for (var Zim=0; Zim<Data.length; Zim++)
			{			
				var BTNNews:InfoOverlay=new InfoOverlay();
				game.Memory["BarracksUI"].addChild(BTNNews);
				BTNNews.width=100;
				BTNNews.height=35;
				BTNNews.x=100+120*Zim;
				BTNNews.y=30;
				game.Memory["Buttons"].push(BTNNews);
				BTNNews.data=Data[Zim];
				BTNNews.Active=true;
				BTNNews.gotoAndStop(Zim);							
					
				var DIAGTEXT:TextField=new TextField()	
				game.Memory["BarracksUI"].addChild(DIAGTEXT);
				DIAGTEXT.text=Language.BarracksButtons[Math.max(0,Zim-1)];
				DIAGTEXT.wordWrap=true;	
				DIAGTEXT.width=BTNNews.width;	
				DIAGTEXT.height=BTNNews.height;
				DIAGTEXT.x=BTNNews.x-BTNNews.width/2+5;
				DIAGTEXT.y=BTNNews.y-BTNNews.height/2+5;
				DIAGTEXT.setTextFormat(GlobalVariables.TextBARRACKS);
				if ( Zim==0)
				{
					game.Memory["BarracksUI"]["CatTXT"]=DIAGTEXT;
				}
				if ( Zim==1)
				{
					game.Memory["BarracksUI"]["SwapTXT"]=DIAGTEXT;
				}
			}
			
			game.Memory["BarracksUI"]["CurrCat"]=-1;
			game.Memory["BarracksUI"]["Swap"]=false;
					
			DrawArmyList(game)		
		}
		
		public static function HandleButtons(game:HubWorldHandler,evt:MouseEvent)
		{			
		var T=game.Memory["GameTime"].GetTime()
		if (game.Memory["Buttons"]!=null)
		{
				var X=evt.stageX
				var Y=evt.stageY
				var erase=false;
				trace(X,Y)
				
				for (var b=0; b<game.Memory["Buttons"].length; b++)
				{	
					if (game.Memory["Buttons"][b]!=null && game.Memory["Buttons"][b].parent!=null)				
					{
						trace(X>=game.Memory["Buttons"][b].x-game.Memory["Buttons"][b].width/2+game.Memory["Buttons"][b].parent.x , X<=game.Memory["Buttons"][b].x+game.Memory["Buttons"][b].width/2+game.Memory["Buttons"][b].parent.x,
							 Y>=game.Memory["Buttons"][b].y-game.Memory["Buttons"][b].height/2+game.Memory["Buttons"][b].parent.y , Y<=game.Memory["Buttons"][b].y+game.Memory["Buttons"][b].height/2+game.Memory["Buttons"][b].parent.y)
						/*if (X>=game.Memory["Buttons"][b].x-game.Memory["Buttons"][b].width/2+game.Memory["Buttons"][b].parent.x && X<=game.Memory["Buttons"][b].x+game.Memory["Buttons"][b].width/2+game.Memory["Buttons"][b].parent.x
							&& Y>=game.Memory["Buttons"][b].y-game.Memory["Buttons"][b].height/2+game.Memory["Buttons"][b].parent.y && Y<=game.Memory["Buttons"][b].y+game.Memory["Buttons"][b].height/2+game.Memory["Buttons"][b].parent.y)*/
							if (X>=game.Memory["Buttons"][b].x-game.Memory["Buttons"][b].width/2 && X<=game.Memory["Buttons"][b].x+game.Memory["Buttons"][b].width/2
							&& Y>=game.Memory["Buttons"][b].y-game.Memory["Buttons"][b].height/2 && Y<=game.Memory["Buttons"][b].y+game.Memory["Buttons"][b].height/2)
							{
									trace(game.Memory["Buttons"][b].data)
								if (game.Memory["Buttons"][b].data=="Move1")
								{
									game.Memory["BarracksUI"]["BG"].x=Math.min(game.Memory["BarracksUI"]["BG"].x+100,Math.max(0,game.Memory["BarracksUI"]["BarracksMaX"]))
								}
								else if (game.Memory["Buttons"][b].data=="Move2")
								{
									game.Memory["BarracksUI"]["BG"].x=Math.max(game.Memory["BarracksUI"]["BG"].x-100,Math.min(0,game.Memory["BarracksUI"]["BarracksMaX"]));
								}
								else if (game.Memory["Buttons"][b].data=="exit")
								{
									Close(game);
								}	
								else if (game.Memory["Buttons"][b].data=="spelllearned")
								{
									game.Memory["Buttons"][b].unit.Abilities.splice(game.Memory["Buttons"][b].unit.Abilities.indexOf(game.Memory["Buttons"][b].ID),1);
									OpenTaskMenu(game,game.Memory["Buttons"][b].unit);
								}	
								else if (game.Memory["Buttons"][b].data=="spellbook")
								{
									if (game.Memory["Buttons"][b].unit.Abilities.length<GlobalVariables.MaxArmySpells)
									{
									game.Memory["Buttons"][b].unit.Abilities.push(game.Memory["Buttons"][b].ID);
									OpenTaskMenu(game,game.Memory["Buttons"][b].unit);
									}
								}	
								else if (game.Memory["Buttons"][b].data=="learnspells")
								{
									OpenTaskMenu(game,game.Memory["Buttons"][b].unit);
								}	
								else if (game.Memory["Buttons"][b].data=="cancelselection")
								{
									ListPeople(game,game.Memory["BarracksUI"]["CurrCat"]);
								}	
								else if (game.Memory["Buttons"][b].data=="Arrange")
								{
									if (game.Memory["BarracksUI"]["CurrCat"]==Language.BRKText[0])
									{
										ListPeople(game,Language.BRKText[1]);
									}
									else if (game.Memory["BarracksUI"]["CurrCat"]==Language.BRKText[1])
									{
										ListPeople(game,Language.BRKText[2]);
									}
									else
									{
										ListPeople(game,Language.BRKText[0]);
									}
								}	
								else if (game.Memory["Buttons"][b].data=="Swap")
								{
									game.Memory["BarracksUI"]["Swap"]=(game.Memory["BarracksUI"]["Swap"]==false);
									if (game.Memory["BarracksUI"]["Swap"]==false)
									{
										game.Memory["BarracksUI"]["SwapTXT"]=Language.BarracksButtons[0];
									}
									else
									{
										game.Memory["BarracksUI"]["SwapTXT"]=Language.BarracksButtons[3];
									}
								}	
								else if (game.Memory["Buttons"][b].data=="barracks" || game.Memory["Buttons"][b].data=="retinue")
								{
									if (game.Memory["BarracksUI"]["Swap"]==false)
									{
										DrawUnitMenu(game,game.Memory["Buttons"][b].unit,game.Memory["Buttons"][b].data=="retinue");
									}
									else
									{
										if (game.Memory["Buttons"][b].data=="retinue")
										{
											if (game.Memory["Data"].Armies.indexOf(game.Memory["Buttons"][b].unit)!=-1)
											{
												game.Memory["Data"].BenchedArmies.push(game.Memory["Buttons"][b].unit);
												game.Memory["Data"].Armies.splice(game.Memory["Data"].Armies.indexOf(game.Memory["Buttons"][b].unit),1)
												ListPeople(game,game.Memory["BarracksUI"]["CurrCat"]);
											}
										}
										else
										{
											if (game.Memory["Data"].BenchedArmies.indexOf(game.Memory["Buttons"][b].unit)!=-1 && game.Memory["Data"].Armies.length<GlobalVariables.MaxArmyStack)
											{											
												game.Memory["Data"].Armies.push(game.Memory["Buttons"][b].unit);
												game.Memory["Data"].BenchedArmies.splice(game.Memory["Data"].BenchedArmies.indexOf(game.Memory["Buttons"][b].unit),1)
												ListPeople(game,game.Memory["BarracksUI"]["CurrCat"]);
											}
										}
									}
								}			
								if (game.Memory["Buttons"][b].data!=null)
								{									
									game.Memory["GameTime"].ClickTimer=T+5;
									break;
								}	
							}
					}
				}			
			}
		}
		
		public static function OpenTaskMenu(game:HubWorldHandler,This:DataItemArmy)
		{
			if (This!=null)
			{
				ClearTempLists(game);
			
				var BGX:BarracksBG=new BarracksBG();
				game.Memory["BarracksUI"]["BG"].addChild(BGX);
				BGX.scaleX=GlobalVariables.stageWidth/BGX.width;
				BGX.scaleY=BGX.scaleX;
				BGX.x=GlobalVariables.stageWidth/2;
				BGX.y=GlobalVariables.stageHeight/2;
				game.Memory["Buttons"].push(BGX);
				
				var BTNClose:BarracksBTN=new BarracksBTN();
				game.Memory["BarracksUI"]["BG"].addChild(BTNClose);
				BTNClose.width=150;
				BTNClose.height=50;
				BTNClose.x=600;
				BTNClose.y=300;
				BTNClose.data="cancelselection";
				BTNClose.unit=This;
				BTNClose.Active=true;
				BTNClose.gotoAndStop(1);
				game.Memory["Buttons"].push(BTNClose);
					
				var TEXTY:TextField=new TextField()	
				game.Memory["BarracksUI"]["BG"].addChild(TEXTY);
				TEXTY.text=Language.BarracksButtons[1];
				TEXTY.wordWrap=true;	
						TEXTY.width=100;	
						TEXTY.height=100;
						TEXTY.x=BTNClose.x-50;
						TEXTY.y=BTNClose.y-12;
						TEXTY.setTextFormat(GlobalVariables.TextINGAMEMENU);
					BTNClose.T=TEXTY;		
				
				for (var Zim=0; Zim<This.Abilities.length; Zim++)
				{
					var ActionBTN:BarracksBTNSpell=new BarracksBTNSpell();
					game.Memory["BarracksUI"]["BG"].addChild(ActionBTN);
					ActionBTN.width=150;
					ActionBTN.height=40;
					ActionBTN.x=600;
					ActionBTN.y=(40*Zim+130)*1;
					ActionBTN.data="spelllearned";
					ActionBTN.unit=This;
					ActionBTN.ID=This.Abilities[Zim];
					ActionBTN.Active=true;
					ActionBTN.gotoAndStop(1);
					game.Memory["Buttons"].push(ActionBTN);
					
					var DIAGTEXT:TextField=new TextField()	
					game.Memory["BarracksUI"]["BG"].addChild(DIAGTEXT);
					DIAGTEXT.text=Language.SpellText[This.Abilities[Zim]][0];
					DIAGTEXT.wordWrap=true;	
					DIAGTEXT.width=ActionBTN.width;	
					DIAGTEXT.height=ActionBTN.height;
					DIAGTEXT.x=ActionBTN.x-ActionBTN.width/2+5;
					DIAGTEXT.y=ActionBTN.y-ActionBTN.height/2;
					DIAGTEXT.setTextFormat(GlobalVariables.TextBARRACKS);
					ActionBTN.T=DIAGTEXT;		
				}
							
				var aX=0;
				var aY=0;
				var ArmyAbilities=This.GetAbilities();
				for (var act=0; act<ArmyAbilities.length; act++)
				{
					if (This.Abilities.indexOf(ArmyAbilities[act]==-1))
					{
						var ActionListBTN:HubWorldUIBTN=new HubWorldUIBTN();
						game.Memory["BarracksUI"]["BG"].addChild(ActionListBTN);
						ActionListBTN.width=100;
						ActionListBTN.height=40;
						ActionListBTN.x=120+102*aX;
						ActionListBTN.y=140+45*aY;
						ActionListBTN.data="spellbook";
						ActionListBTN.unit=This;
						ActionListBTN.ID=ArmyAbilities[act];
						ActionListBTN.Active=true;
						ActionListBTN.gotoAndStop(1);
						game.Memory["Buttons"].push(ActionListBTN);
						aX=aX+1;
						if (aX>=4)
						{					
							aX=0;
							aY=aY+1;
						}						
					
						var BTNTXT:TextField=new TextField()	
						game.Memory["BarracksUI"]["BG"].addChild(BTNTXT);
						BTNTXT.text=Language.SpellText[ArmyAbilities[act]][0];
						BTNTXT.wordWrap=true;	
						BTNTXT.width=ActionListBTN.width;	
						BTNTXT.height=ActionListBTN.height;
						BTNTXT.x=ActionListBTN.x-ActionListBTN.width/2+5;
						BTNTXT.y=ActionListBTN.y-ActionListBTN.height/2;
						BTNTXT.setTextFormat(GlobalVariables.TextBARRACKS);
						ActionListBTN.T=BTNTXT;		
					}
				}
			}			
		}

		public static function DrawUnitMenu(game:HubWorldHandler,This:DataItemArmy,Retinue:Boolean)
		{
			if (This!=null)
			{
				ClearTempLists(game);
			
			var BGX:BarracksBG=new BarracksBG();
			game.Memory["BarracksUI"]["BG"].addChild(BGX);
			BGX.width=GlobalVariables.stageWidth;
			BGX.height=GlobalVariables.stageHeight;
			BGX.x=GlobalVariables.stageWidth/2;
			BGX.y=GlobalVariables.stageHeight/2;
			game.Memory["Buttons"].push(BGX);
								
			var TempBTN:BarracksArmyBG=new BarracksArmyBG();
			game.Memory["BarracksUI"]["BG"].addChild(TempBTN);
			TempBTN.width=140;
			TempBTN.height=140;
			TempBTN.x=150;
			TempBTN.y=170;
			TempBTN.gotoAndStop(1);
					
			var ArmyIcon=new Bitmap(new ArmyData.Display[This.Type](null, null));
			game.Memory["BarracksUI"]["BG"].addChild(ArmyIcon);
			ArmyIcon.scaleY=TempBTN.height/ArmyIcon.height;
			ArmyIcon.scaleX=ArmyIcon.scaleY;
			ArmyIcon.x=TempBTN.x-(TempBTN.width-ArmyIcon.width)/2-TempBTN.width/2+1;
			ArmyIcon.y=TempBTN.y-TempBTN.height/2-1;
			
			var Text:String=""
			if (This.LevelUpXP>This.XP)
			{
				var XP=Math.floor(This.LevelUpXP-This.XP);
				Text+=(This.Name+Language.BarracksText[0]+This.Level+" "+This.GetMyName()+Language.BarracksText[1]+XP+Language.BarracksText[2]+Math.round(This.Attack)+Language.BarracksText[3]+Math.round(This.Armor)+Language.BarracksText[4]+Math.round(This.Special)+Language.BarracksText[14]+This.Deaths);
			}
			else
			{
				Text+=(This.Name+Language.BarracksText[0]+This.Level+" "+This.GetMyName()+Language.BarracksText[15]+Language.BarracksText[2]+Math.round(This.Attack)+Language.BarracksText[3]+Math.round(This.Armor)+Language.BarracksText[4]+Math.round(This.Special)+Language.BarracksText[14]+This.Deaths);
			}
			
						
				var HMTEXT:TextField=new TextField()	
				game.Memory["BarracksUI"]["BG"].addChild(HMTEXT);
				HMTEXT.text=Text;
				HMTEXT.wordWrap=true;	
				HMTEXT.width=200;	
				HMTEXT.height=2000;
				HMTEXT.x=80;
				HMTEXT.y=245;
				HMTEXT.setTextFormat(GlobalVariables.TextBARRACKS);		
				
				Text=Language.BarracksText[5]+Language.BarracksText[10]+Language.PassiveText[DataItem.Passives.indexOf(This.Passive)][0]+Language.BarracksText[6]+Language.PassiveText[DataItem.Passives.indexOf(This.Passive)][1]+Language.BarracksText[6];
			for (var Zim=0; Zim<This.Abilities.length; Zim++)
			{
				Text=Text+Language.BarracksText[6]+Language.SpellText[This.Abilities[Zim]][0]+Language.BarracksText[6]+Language.SpellText[This.Abilities[Zim]][1];
			}			
			
				var KNTEXT:TextField=new TextField()	
				game.Memory["BarracksUI"]["BG"].addChild(KNTEXT);
				KNTEXT.text=Text;
				KNTEXT.wordWrap=true;	
				KNTEXT.width=300;	
				KNTEXT.height=2000;
				KNTEXT.x=250;
				KNTEXT.y=116;
				KNTEXT.setTextFormat(GlobalVariables.TextBARRACKS);
				
						var BTNAct:BarracksBTN=new BarracksBTN();
						game.Memory["BarracksUI"]["BG"].addChild(BTNAct);
						BTNAct.width=130;
						BTNAct.height=40;
						BTNAct.x=630;
						BTNAct.y=255;
						BTNAct.data="learnspells";
						BTNAct.unit=This;
						BTNAct.Active=true;
						BTNAct.gotoAndStop(1);
						game.Memory["Buttons"].push(BTNAct);
						
						var MyText:TextField=new TextField()	
						game.Memory["BarracksUI"]["BG"].addChild(MyText);
						MyText.text=Language.BarracksButtons[2];
						MyText.wordWrap=true;	
						MyText.width=100;	
						MyText.height=100;
						MyText.x=BTNAct.x-50;
						MyText.y=BTNAct.y-12;
						MyText.setTextFormat(GlobalVariables.TextINGAMEMENU);
					
						var BTNClose:BarracksBTN=new BarracksBTN();
						game.Memory["BarracksUI"]["BG"].addChild(BTNClose);
						BTNClose.width=130;
						BTNClose.height=40;
						BTNClose.x=630;
						BTNClose.y=300;
						BTNClose.data="cancelselection";
						BTNClose.Active=true;
						BTNClose.gotoAndStop(1);
						game.Memory["Buttons"].push(BTNClose);
						
						var MyTextTwo:TextField=new TextField()	
						game.Memory["BarracksUI"]["BG"].addChild(MyTextTwo);
						MyTextTwo.text=Language.BarracksButtons[1];
						MyTextTwo.wordWrap=true;	
						MyTextTwo.width=100;	
						MyTextTwo.height=100;
						MyTextTwo.x=BTNClose.x-50;
						MyTextTwo.y=BTNClose.y-12;
						MyTextTwo.setTextFormat(GlobalVariables.TextINGAMEMENU);
			}
		}
		
		public static function DrawArmyList(game:HubWorldHandler)
		{
			game.Memory["BarracksUI"]["BG"].x=0;
			game.Memory["BarracksUI"]["BG"].y=0;
			
			var backBTN:BarracksBTNninety=new BarracksBTNninety();
			game.Memory["BarracksUI"].addChild(backBTN);
			backBTN.width=35//GlobalVariables.stageWidth/20;
			backBTN.height=35//GlobalVariables.stageHeight*1/3//-100*1; 
			backBTN.x=GlobalVariables.stageWidth/40;
			backBTN.y=GlobalVariables.stageHeight/2+50*1;
				backBTN.Active=true;
				backBTN.gotoAndStop(1);
			backBTN.data="Move1";
			game.Memory["Buttons"].push(backBTN);
			
			var frontBTN:BarracksBTNninety=new BarracksBTNninety();
			game.Memory["BarracksUI"].addChild(frontBTN);
			frontBTN.width=35//GlobalVariables.stageWidth/20;
			frontBTN.height=35//GlobalVariables.stageHeight*1/3//-100*1;
			frontBTN.x=GlobalVariables.stageWidth-GlobalVariables.stageWidth/40;
				frontBTN.Active=true;
			frontBTN.y=GlobalVariables.stageHeight/2+50*1;;
			frontBTN.data="Move2";
				frontBTN.gotoAndStop(2);
			game.Memory["Buttons"].push(frontBTN);										
				
			ListPeople(game,Language.BRKText[0]);
		}
		
		public static function ClearTempLists(game:HubWorldHandler)
		{
			while (game.Memory["BarracksUI"]["BG"].numChildren>0)
			{
				game.Memory["BarracksUI"]["BG"].removeChildAt(0);
			}
			game.Memory["BarracksUI"]["BarracksMaX"]=0;
			game.Memory["BarracksUI"]["BG"].x=0;
			game.Memory["BarracksUI"]["BG"].y=0;
		}
		
		public static function Close(game:HubWorldHandler)
		{
			while (game.Memory["BarracksUI"].numChildren>0)
			{
				game.Memory["BarracksUI"].removeChildAt(0);
			}
			game.Memory["BarracksUI"].parent.removeChild(game.Memory["BarracksUI"]);
			game.Memory["BarracksUI"]=null;
			HubWorld.Draw(game);
		}
		
		public static function ListPeople(game:HubWorldHandler,Category:String)
		{
			ClearTempLists(game);
			game.Memory["BarracksUI"]["CurrCat"]=Category;
			game.Memory["BarracksUI"]["CatTXT"].text=Category;
			game.Memory["BarracksUI"]["CatTXT"].setTextFormat(GlobalVariables.TextBARRACKS);
			
			var Retinue:Array=new Array();
			var Barracks:Array=new Array();
			if (game.Memory["BarracksUI"]["CurrCat"]==Language.BRKText[1])
			{
				for (var i=0; i<game.Memory["Data"].Armies.length; i++)
				{
					Retinue.push(game.Memory["Data"].Armies[i]);
				}
			}
			else if (game.Memory["BarracksUI"]["CurrCat"]==Language.BRKText[2])
			{
				for (var i=0; i<game.Memory["Data"].BenchedArmies.length; i++)
				{
					Barracks.push(game.Memory["Data"].BenchedArmies[i]);
				}
			}
			else
			{
				for (var i=0; i<game.Memory["Data"].Armies.length; i++)
				{
					Retinue.push(game.Memory["Data"].Armies[i]);
				}
				for (var i=0; i<game.Memory["Data"].BenchedArmies.length; i++)
				{
					Barracks.push(game.Memory["Data"].BenchedArmies[i]);
				}
			}
						
			var iT=0;
			i=0;
			if (Retinue.length>0)
			{
				var HMTEXT:TextField=new TextField()	
				game.Memory["BarracksUI"]["BG"].addChild(HMTEXT);
				HMTEXT.text=Language.BarracksText[7];
				HMTEXT.wordWrap=true;	
				HMTEXT.width=125;	
				HMTEXT.height=150;
				HMTEXT.x=60;
				HMTEXT.y=110;
				HMTEXT.setTextFormat(GlobalVariables.TextBARRACKS);
				
				for (i=0; i<Retinue.length; i++)
				{
					if (i-iT*4>3)
					{
						iT+=1;
					}
					var TempBTN:BarracksArmyBG=new BarracksArmyBG();
					game.Memory["BarracksUI"]["BG"].addChild(TempBTN);
					TempBTN.width=50;
					TempBTN.height=50;
					TempBTN.x=(90+iT*60);
					TempBTN.y=(160+60*(i-iT*4));
					TempBTN.data="retinue";
					TempBTN.unit=Retinue[i];
					game.Memory["Buttons"].push(TempBTN);
					TempBTN.gotoAndStop(1);
					TempBTN.Active=true;
					
					var ArmyIcon=new Bitmap(new ArmyData.Display[Retinue[i].Type](null, null));
					game.Memory["BarracksUI"]["BG"].addChild(ArmyIcon);
					ArmyIcon.scaleY=(TempBTN.height-2)/ArmyIcon.height;
					ArmyIcon.scaleX=ArmyIcon.scaleY;					
					ArmyIcon.x=TempBTN.x-TempBTN.width/2+1;
					ArmyIcon.y=TempBTN.y-TempBTN.height/2+1;
					
				}
					iT=Math.ceil(i/4);
					i=iT*4;
				
			}
			var iT2=0;
			var ii=0
			if (Barracks.length>0)
			{
				var HMTEXT:TextField=new TextField()	
				game.Memory["BarracksUI"]["BG"].addChild(HMTEXT);
				HMTEXT.text=Language.BarracksText[8];
				HMTEXT.wordWrap=true;	
				HMTEXT.width=125;	
				HMTEXT.height=150;
				HMTEXT.x=60+(iT+iT2)*60;
				HMTEXT.y=110;
				HMTEXT.setTextFormat(GlobalVariables.TextBARRACKS);	
				
				for (ii=0; ii<Barracks.length; ii++)
				{
					if (ii-iT2*4>3)
					{
						iT2+=1;
					}
					TempBTN=new BarracksArmyBG();
					game.Memory["BarracksUI"]["BG"].addChild(TempBTN);
					TempBTN.width=50;
					TempBTN.height=50;
					TempBTN.x=(90+(iT+iT2)*60);
					TempBTN.y=(160+60*(ii+i-(iT+iT2)*4));
					TempBTN.data="barracks";
					TempBTN.unit=Barracks[ii];
					game.Memory["Buttons"].push(TempBTN);
					TempBTN.Active=true;
					TempBTN.gotoAndStop(1);
					
					var ArmyIcon=new Bitmap(new ArmyData.Display[Barracks[ii].Type](null, null));
					game.Memory["BarracksUI"]["BG"].addChild(ArmyIcon);
					ArmyIcon.scaleY=(TempBTN.height-2)/ArmyIcon.height;
					ArmyIcon.scaleX=ArmyIcon.scaleY;					
					ArmyIcon.x=TempBTN.x-TempBTN.width/2+1;
					ArmyIcon.y=TempBTN.y-TempBTN.height/2+1;
				}
			}
			if (Barracks.length==0 && Retinue.length==0)
			{
				var HMTEXT:TextField=new TextField()	
				game.Memory["BarracksUI"]["BG"].addChild(HMTEXT);
				HMTEXT.text=Language.BarracksText[9];
				HMTEXT.wordWrap=true;	
				HMTEXT.width=125;	
				HMTEXT.height=150;
				HMTEXT.x=60;
				HMTEXT.y=120;
				HMTEXT.setTextFormat(GlobalVariables.TextBARRACKS);	
			}
			game.Memory["BarracksUI"]["BarracksMaX"]=0-Math.max(11,iT+iT2)*60+GlobalVariables.stageWidth-GlobalVariables.stageWidth/10;
		}
	}
	
}

