﻿package  {
	
	public class SpellData {

		public static var cSpellAttack=0;
		public static var cSpellDefender=1;
		public static var cSpellFireBlast=2;
		public static var cSpellIceBlast=3;
		public static var cSpellBombardment=4;
		public static var cSpellWindBlast=5;
		public static var cSpellEatSkull=6;
		public static var cSpellScare=7;
		public static var cSpellStealManaBlue=8;
		public static var cSpellUnholyRage=9;
		public static var cSpellShieldBash=10;
		public static var cSpellDrainSkulls=11;
		public static var cSpellSwoop=12;
		public static var cSpellEcho=13;
		public static var cSpellSwarmCall=14;
		public static var cSpellVenom=15;
		public static var cSpellInjectPoison=16;
		public static var cSpellWeb=17;
		public static var cSpellRainPoison=18;
		public static var cSpellSneak=19;
		public static var cSpellScavenge=20;
		public static var cSpellIgnite=21;
		public static var cSpellReplenish=22;
		public static var cSpellZap=23;
		public static var cSpellRabidBite=24;
		public static var cSpellPlague=25;
		public static var cSpellRevenge=26;
		public static var cSpellSwiftness=27;
		public static var cSpellScratch=28;
		public static var cSpellHowl=29;
		public static var cSpellTaunt=30;
		public static var cSpellIceFloe=31;
		public static var cSpellGust=32;
		public static var cSpellMeatShield=33;
		public static var cSpellRegenerate=34;
		public static var cSpellEatStar=35;
		public static var cSpellBlight=36;
		public static var cSpellBurrow=37;
		public static var cSpellDig=38;
		public static var cSpellTrickMana=39;
		public static var cSpellFireBall=40;
		public static var cSpellSkullBash=41;
		public static var cSpellDrinkAle=42;
		public static var cSpellStoneSkin=43;
		public static var cSpellRainFire=44;
		public static var cSpellHeavyBlow=45;
		public static var cSpellTowerShield=46;
		public static var cSpellBackStab=47;
		public static var cSpellEatFire=48;
		public static var cSpellGemSmash=49;
		public static var cSpellThrowStone=50;
		public static var cSpellThrowBoulder=51;
		public static var cSpellEatEarth=52;
		public static var cSpellDecieve=53;
		public static var cSpellBloodRite=54;
		public static var cSpellStrength=55;
		public static var cSpellGodlyStrength=56;
		public static var cSpellRest=57;
		public static var cSpellCurse=58;
		public static var cSpellSilence=59;
		public static var cSpellEntangle=60;
		public static var cSpellChannel=61;
		public static var cSpellChannelRed=62;
		public static var cSpellChannelBlue=63;
		public static var cSpellChannelGreen=64;
		public static var cSpellChannelYellow=65;
		public static var cSpellBless=66;
		public static var cSpellShieldCharge=67;
		public static var cSpellAvalanche=68;
		public static var cSpellTrickShot=69;
		public static var cSpellBerserker=70;
		public static var cSpellDarkBolt=71;
		public static var cSpellRaiseDead=72;
		public static var cSpellSiege=73;
		public static var cSpellCharge=74;
		public static var cSpellWound=75;
		public static var cSpellGoblinToss=76;		
		public static var cSpellReinforce=77;
		public static var cSpellBodySlam=78;
		public static var cSpellIronFist=79;
		public static var cSpellFireBomb=80;
		public static var cSpellSwarmAttack=81;
		public static var cSpellSwarmDeffend=82;
		public static var cSpellGraveDigger=83;
		public static var cSpellClearSkulls=84;
		public static var cSpellBurrial=85;
		public static var cSpellFireElly=86;
		public static var cSpellWaterElly=87;
		public static var cSpellEarthElly=88;
		public static var cSpellWindElly=89;		
		public static var cSpellFireAvatar=90;
		public static var cSpellWaterAvatar=91;
		public static var cSpellEarthAvatar=92;
		public static var cSpellWindAvatar=93;
		public static var cSpellThunder=94;
		public static var cSpellIronWall=95;
		public static var cSpellUltimate=96;
		public static var cSpellThrowAxe=97;
		public static var cSpellTapLife=98;
		public static var cSpellStealLife=99;
		public static var cSpellSwarmStrike=100;
		public static var cSpellSavageRoar=101;
		public static var cSpellFreeze=102;
		public static var cSpellSandAttack=103;
		public static var cSpellSleep=104;
		public static var cSpellHybernate=105;
		public static var cSpellPlagueExplosion=106;
		public static var cSpellPoisonExplosion=107;
		public static var cSpellToxic=108;
		public static var cSpellNecroPlague=109;
		public static var cSpellAnthragsKiss=110;
		public static var cSpellPlagueBlade=111;
		public static var cSpellPoisonBlade=112;
		public static var cSpellBloodBlade=113;
		public static var cSpellRupture=114;
		public static var cSpellHex=115;
		public static var cSpellStoneForm=116;
		public static var cSpellLesserHeal=117;
		public static var cSpellGreaterHeal=118;
		public static var cSpellInnerFire=119;
		public static var cSpellCleansingFlame=120;
		public static var cSpellRainBlood=121;
		public static var cSpellWallOfFire=122;
		public static var cSpellGrow=123;
		public static var cSpellChannelWhite=124;
		public static var cSpellNightmare=125;
		public static var cSpellSleepPowder=126;
		public static var cSpellTriadOfSkulls=127;
		public static var cSpellSandStorm=128;
		
		public static var SpellBook:Array = [//	***	SPELLBOOK	***
							   	[// --- ATTACK ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [0,0,0,0,1,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  var nDamage=1;
									  if (Keys.caster.Passive=="attackasmagic")
									  {
										   nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,0,1,false)
									  }
									  else
									  {
									  
											if (Keys.caster.Passive=="selfstunafterattack")
											{
												Keys.armytarget.AddModifier(Language.datModifierStunned,0,1,game.Memory["CurrentTurn"],false);
											}										  
											 else if (Keys.armytarget.Passive=="retaliate")
											{
												 DisplayItemArmy.DealDamagePhysical(game,Keys.armytarget,Keys.caster,0,0.2,true)
											}							  
											 else if (Keys.caster.Passive=="heavyhitter" && HandleMana.GetMana(game,Keys.caster.PlayerOwner,GlobalVariables.GEM_SKULL)>=1)
											{
												HandleMana.RemoveMana(game,Keys.caster.PlayerOwner,1,GlobalVariables.GEM_SKULL)
										  		nDamage+=DisplayItemArmy.DealDamagePhysical(game,Keys.caster,Keys.armytarget,0,1,false)
											}								  
											 else if (Keys.caster.Passive=="horde")
											{
												nDamage*=HandleMana.GetMana(game,Keys.caster.PlayerOwner,GlobalVariables.GEM_PURPLE)/20+1
											}							  
											 else if (Keys.caster.Passive=="loner")
											{
												nDamage/=HandleMana.GetMana(game,Keys.caster.PlayerOwner,GlobalVariables.GEM_PURPLE)/20+2/3
											}				  
											 else if (Keys.caster.Passive=="blunt")
											{
												if (Math.random()*100<(25+Keys.caster.Level)/2)
												{
													Keys.armytarget.AddModifier(Language.datModifierStunned,0,3,game.Memory["CurrentTurn"],false);
												}
											}		  
											 else if (Keys.caster.Passive=="bonusmgonatk")
											{
												 DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,0,1/2,false);
											}		  
											 else if (Keys.caster.Passive=="cursed")
											{
												if (Math.random()*100<(100+Keys.caster.Level)/2)
												{
													Keys.armytarget.AddModifier(Language.datModifierCursed,(10+Keys.caster.Level)/2,5,game.Memory["CurrentTurn"],true);
												}
											}  
											 else if (Keys.caster.Passive=="fearfull")
											{
												if (Math.random()*100<(50+Keys.caster.Level)/2)
												{
													Keys.armytarget.AddModifier(Language.datModifierFear,0,3,game.Memory["CurrentTurn"],false);
												}
											}
											 else if (Keys.caster.Passive=="poisonclaw")
											{
												if (Math.random()*100>(75-Keys.caster.Level)/2)
												{
													Keys.armytarget.AddModifier(Language.datModifierPoison,(1+Keys.caster.Level)/3,5,game.Memory["CurrentTurn"],true);
												}
											}
											 else if (Keys.caster.Passive=="fireatk")
											{
												Keys.armytarget.AddModifier(Language.datModifierBurn,(2+Keys.caster.Level)/3,1,game.Memory["CurrentTurn"],true);
											}
											 else if (Keys.caster.Passive=="crit")
											{												
												if (Math.random()*100<25)
												{
													nDamage*=2;
												}
											}
											 else if (Keys.caster.Passive=="plaguetouch")
											{												
												if (Math.random()*100<(50+Keys.caster.Level)/2)
												{
													Keys.armytarget.AddModifier(Language.datModifierDisease,(5+Keys.caster.Level)/3,5,game.Memory["CurrentTurn"],true);
												}
											}
											
											if (Keys.armytarget.Passive=="plaguetouch")
											{												
												if (Math.random()*100<(50+Keys.armytarget.Level)/2)
												{
													Keys.caster.AddModifier(Language.datModifierDisease,(5+Keys.armytarget.Level)/3,5,game.Memory["CurrentTurn"],true);
												}
											}
											if (Keys.armytarget.Passive=="poisontouch")
											{
												//if (Math.random()*100<(50+Keys.caster.Level)/2)
												{
													Keys.caster.AddModifier(Language.datModifierPoison,(1+Keys.armytarget.Level)/3,3,game.Memory["CurrentTurn"],true);
												}
											}
											if (Keys.caster.Passive=="destroyrandomgem")
											{
												GemManager.DestroyGem(game,Math.round(Math.random()*game.Memory["BoardSize"]),Math.round(Math.random()*game.Memory["BoardSize"]),null);
											}
											
											if (Keys.caster.Passive=="bonusmgonatk")
											{
												DisplayItemArmy.TakeDamage(game,Keys.caster,Keys.caster,10,false);
											}
											
										   nDamage=DisplayItemArmy.DealDamagePhysical(game,Keys.caster,Keys.armytarget,0,1,false)
										   SpecialEffects.GenerateProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],0,Keys.caster,Keys.armytarget,nDamage,1);
									  }
								  }
							   	],
								[// --- DEFEND ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,0,0,0,1,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  Keys.caster.Defender=(Keys.caster.Defender==false)
									   if (Keys.caster.Passive=="freedefend")
									   {
										   HandleMana.AddMana(game,Keys.caster.PlayerOwner,1,GlobalVariables.GEM_PURPLE,true);
									   }
									   if (Keys.caster.Passive=="Synergy")
									   {
										   HandleMana.AddMana(game,Keys.caster.PlayerOwner,1,GlobalVariables.GEM_PURPLE,true);
									  	   Keys.caster.Defender=false
									   }
								  }
							   	],
								[// --- FIREBOLT ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [3,0,0,0,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=5;
									  var nDamageB=Keys.caster.GetSpecial(game)/5*0.4;
									  									  
									  DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,1);
									  Keys.armytarget.AddModifier(Language.datModifierBurn,nDamageB,5,game.Memory["CurrentTurn"],true);		
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,11,1,Keys.caster);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,5,1,Keys.armytarget);
								  }
							   	],
								[// --- ICEBLAST ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,3,0,0,0,1,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=5;									  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,5,1,false);									  
									  SpecialEffects.GenerateParticleProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],7,22,Keys.caster,Keys.armytarget,nDamage,5,1);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,13,1,Keys.caster);
								  }
							   	],
								[// --- BOMBARDMENT ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,0,3,0,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=5;
									  									  
									 Keys.armytarget.AddModifier(Language.datModifierStunned,0,2,game.Memory["CurrentTurn"],false);								  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,1,false);		
									  SpecialEffects.GenerateParticleProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],20,6,Keys.caster,Keys.armytarget,nDamage,1,1);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,14,1,Keys.caster);
								  }
							   	],
								[// --- WINDBLAST ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,0,0,3,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=5;			
									  									  
									 Keys.armytarget.AddModifier(Language.datModifierSilenced,0,2,game.Memory["CurrentTurn"],false);									  									  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,1,false);									  
									  SpecialEffects.GenerateParticleProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],8,8,Keys.caster,Keys.armytarget,nDamage,5,1);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,15,1,Keys.caster);
								  }
							   	],
								[// --- EAT SKULL ---
								 cSpellEffect.targeting_GemWhite,//Targeting
								  [2,0,0,0,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  Keys.gemtarget.dead=true;
									  var nHeal=10+Keys.caster.GetSpecial(game)/2;
									  									  
									  DisplayItemArmy.Heal(game,Keys.caster,nHeal,true,true);									  
									   SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,3,1,Keys.caster);
								  }
							   	],
								[// --- SCARE ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,3,0,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDuration=Math.floor(Keys.caster.GetSpecial(game)/30);
									  									  
									 Keys.armytarget.AddModifier(Language.datModifierFear,0,2+nDuration,game.Memory["CurrentTurn"],false);							  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,10,1,Keys.armytarget);
								  }
							   	],
								[// --- STEAL MANA: BLUE ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [1,0,1,1,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=1+Math.floor(Keys.caster.GetSpecial(game)/25);
									  				
									if (HandleMana.GetMana(game,1-Keys.caster.PlayerOwner,GlobalVariables.GEM_BLUE)>=nDamage)
										{
										  HandleMana.AddMana(game,Keys.caster.PlayerOwner,nDamage,GlobalVariables.GEM_BLUE,true);
										  HandleMana.RemoveMana(game,1-Keys.caster.PlayerOwner,nDamage,GlobalVariables.GEM_BLUE);			  
										  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,13,1,Keys.caster);						  
										}									  
								  }
							   	],
								[// --- UNHOLY RAGE ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [2,0,0,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									  
									   DisplayItemArmy.TakeDamage(game,Keys.caster,Keys.caster,5,true)
									 Keys.caster.AddModifier(Language.datModifierBerserk,10,3,game.Memory["CurrentTurn"],true);			
									   
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,9,1,Keys.caster);						  
								  }
							   	],
								[// --- SHIELDBASH ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,0,2,0,1,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=5+Keys.caster.GetDefense(game)/2;									  									  
									  									  
									 Keys.armytarget.AddModifier(Language.datModifierStunned,0,2,game.Memory["CurrentTurn"],false);								  
									  nDamage=DisplayItemArmy.DealDamagePhysical(game,Keys.caster,Keys.armytarget,nDamage,0);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,6,1,Keys.armytarget);
								  }
							   	],
								[// --- DRAIN MANA: SKULLS ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [1,3,0,0,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=1+Math.floor(Keys.caster.GetSpecial(game)/30);
									  
									  if (HandleMana.GetMana(game,1-Keys.caster.PlayerOwner,GlobalVariables.GEM_SKULL)>=nDamage)
										{
									 	 HandleMana.RemoveMana(game,1-Keys.caster.PlayerOwner,nDamage,GlobalVariables.GEM_SKULL);		
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,16,1,Keys.caster);			  
										}
								  }
							   	],
								[// --- SWOOP ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [0,0,0,2,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  SpellBook[cSpellAttack][2](game,Keys);
									 Keys.caster.AddModifier(Language.datModifierWeaken,Keys.caster.GetAttack(game)/2,1,game.Memory["CurrentTurn"],true);			
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,15,1,Keys.caster);			
								  }
							   	],
								[// --- ECHO ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [1,0,0,2,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=4;
									  									  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,1/2);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,10,1,Keys.armytarget);
								  }
							   	],
								[// --- SWARMCALL ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,0,1,1,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=1+Math.floor(Keys.caster.GetSpecial(game)/50)*2;						
									 Keys.caster.AddModifier(Language.datModifierBerserk,25-HandleMana.GetMana(game,Keys.caster.PlayerOwner,GlobalVariables.GEM_PURPLE)*5,nDamage,game.Memory["CurrentTurn"],true);											  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,2,1,Keys.armytarget);

								  }
							   	],
								[// --- VENOM ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [0,0,1,0,1,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									  
									  var nDamage=Keys.caster.GetAttack(game)/5;
									  									  
									 if (Math.random()*2>1)
										 {
											 Keys.armytarget.AddModifier(Language.datModifierPoison,nDamage,3,game.Memory["CurrentTurn"],true);								  
										 }
									  DisplayItemArmy.DealDamagePhysical(game,Keys.caster,Keys.armytarget,8,0.2);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,4,1,Keys.armytarget);
								  }
							   	],
								[// --- INJECT POISON ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,0,2,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=Math.ceil(Keys.caster.GetSpecial(game)/10);
									  
									Keys.armytarget.AddModifier(Language.datModifierPoison,5,nDamage,game.Memory["CurrentTurn"],true);								  										
									 SpecialEffects.GenerateParticleProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],4,3,Keys.caster,Keys.armytarget,0,5,1/2);

								  }
							   	],
								[// --- WEB ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,0,0,2,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  Keys.armytarget.AddModifier(Language.datModifierStunned,0,2,game.Memory["CurrentTurn"],false);							  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,6,1,Keys.armytarget);
								  }
							   	],
								[// --- POISON RAIN ---
								 cSpellEffect.targeting_AllEnemies,//Targeting
								  [0,0,4,0,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=Math.ceil(Keys.caster.GetSpecial(game)/5);
									  
									  if (Math.random()*3>2)
									  {
											Keys.armytarget.AddModifier(Language.datModifierPoison,nDamage,5,game.Memory["CurrentTurn"],true);								  
									  }
									  else
									  {
										  Keys.armytarget.AddModifier(Language.datModifierPoison,nDamage,3,game.Memory["CurrentTurn"],true);		
									  }
										 									  
									 SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,4,1,Keys.armytarget);

								  }
							   	],
								[// --- SNEAK ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,0,4,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  Keys.caster.AddModifier(Language.datModifierSneak,0,10,game.Memory["CurrentTurn"],false);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,14,1,Keys.caster);
								  }
							   	],
								[// --- SCAVENGE ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,2,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						  
									  var nDamage=Math.ceil(Keys.caster.GetSpecial(game)/20);
									  GemManager.DestroyGem(game,Math.round(Math.random()*game.Memory["BoardSize"]),Math.round(Math.random()*game.Memory["BoardSize"]),null);
									   Keys.caster.AddModifier(Language.datModifierBerserk,3,nDamage,game.Memory["CurrentTurn"],true);									
									   SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,2,1,Keys.caster);
								  }
							   	],
								[// --- IGNITE ---
								 cSpellEffect.targeting_EntireBoard,//Targeting
								  [2,0,0,1,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  var NR=2+Math.floor(Keys.caster.GetSpecial(game)/25);
									  var Targets=PickRandomNrOfGems(Keys.gemtargets,NR,[GlobalVariables.GEM_GREEN,GlobalVariables.GEM_YELLOW])
									   for each (var Zim in Targets)
									  {
										  GemManager.ChangeGemColor(game,Keys.gemtargets[Zim],GlobalVariables.GEM_RED);
									  }							
									   SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,5,1,Keys.caster);
								  }
							   	],
								[// --- REPLENISH ---
								 cSpellEffect.targeting_AllFriendlies,//Targeting
								  [0,0,1,2,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function													  
									  var nHeal=Math.ceil(Keys.caster.GetSpecial(game)/4);
									   DisplayItemArmy.Heal(game,Keys.armytarget,nHeal,true,true);	
									   SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,3,1,Keys.armytarget);
								  }
							   	],
								[// --- ZAP ---
								 cSpellEffect.targeting_UnitBoth,//Targeting
								  [1,0,0,0,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									
									  var nDamage=1+Math.floor(Keys.caster.GetSpecial(game)/5*game.Memory["BoardMana"][GlobalVariables.GEM_RED]);
									  									  
									 if ( Keys.armytarget.PlayerOwner== Keys.caster.PlayerOwner)
									 {
									   DisplayItemArmy.Heal(game,Keys.armytarget,nDamage,true,true);										
									  SpecialEffects.GenerateParticleProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],3,3,Keys.caster,Keys.armytarget,0,1,1);	
									 }
									 else
									 {
									   	DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,0);
										  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,1,1,Keys.armytarget);
									 }

								  }
							   	],
								[// --- RABID BITE ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [0,1,0,0,1,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=Keys.caster.GetAttack(game)/2;  
									  									  
									 if (Math.random()*2>1)
										 {
											 Keys.armytarget.AddModifier(Language.datModifierDisease,nDamage,3,game.Memory["CurrentTurn"],true);								  
										 }
									  DisplayItemArmy.DealDamagePhysical(game,Keys.caster,Keys.armytarget,8,0.2);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,2,1,Keys.armytarget);								  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,4,3/4,Keys.armytarget);

								  }
							   	],
								[// --- PLAGUE ---
								 cSpellEffect.targeting_AllEnemies,//Targeting
								  [0,0,0,3,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=Math.ceil(Keys.caster.GetSpecial(game)/20)*5;
									  
									  if (Math.random()*15>5)
									  {
											Keys.armytarget.AddModifier(Language.datModifierDisease,nDamage,5,game.Memory["CurrentTurn"],true);								  
									  }
									  else
									  {
										  Keys.armytarget.AddModifier(Language.datModifierDisease,nDamage,1,game.Memory["CurrentTurn"],true);		
									  }										 									
									  SpecialEffects.GenerateParticleProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],3,4,Keys.caster,Keys.armytarget,0,6,1);	
								  }
							   	],
								[// --- REVENGE ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [2,0,0,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									  
									   var nData=Keys.caster.Life/Keys.caster.MaxLife*Keys.caster.GetAttack(game);
									 Keys.caster.AddModifier(Language.datModifierBerserk,nData,3,game.Memory["CurrentTurn"],true);			
									   
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,9,1,Keys.caster);						  
								  }
							   	],
								[// --- SWIFT ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,1,2,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									
									 Keys.caster.AddModifier(Language.datModifierSwift,0,1,game.Memory["CurrentTurn"],false);			
									   
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,14,1,Keys.caster);						  
								  }
							   	],
								[// --- SCRATCH ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [1,0,0,0,1,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=5;  
									  									  
									 if (Math.random()*1>2/3)
										 {
											 nDamage*=2;
										 }
									  DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,2/3);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,2,1,Keys.armytarget);

								  }
							   	],
								[// --- HOWL ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,1,0,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  Keys.caster.AddModifier(Language.datModifierBerserk,10,5,game.Memory["CurrentTurn"],true);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,10,1,Keys.caster);
								  }
							   	],
								[// --- TAUNT ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [1,0,0,0,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamageA=2+Math.ceil(Keys.caster.GetDefense(game)/20);  
									  
									Keys.armytarget.AddModifier(Language.datModifierSilenced,0,nDamageA,game.Memory["CurrentTurn"],false);	
									  DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamageA,0);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,9,1,Keys.armytarget);

								  }
							   	],
								[// --- ICEFLOE ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,2,0,0,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=3;
									  									  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,1/2);									 									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,7,1,Keys.armytarget);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,12,1,Keys.caster);										
								  }
							   	],
								[// --- GUST ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,0,0,2,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=3;										  									  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,1/2);									  								 									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,8,1/2,Keys.armytarget);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,14,1,Keys.caster);					
								  }
							   	],
								[// --- MEATSHIELD ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,1,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  Keys.caster.AddModifier(Language.datModifierArmor,10,5,game.Memory["CurrentTurn"],true);
									  Keys.caster.AddModifier(Language.datModifierWeaken,5,5,game.Memory["CurrentTurn"],true);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,4,1,Keys.caster);
								  }
							   	],
								[// --- REGENERATE ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,2,0,1,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  var nHeal=Math.ceil(Keys.caster.MaxLife/25)
									  Keys.caster.AddModifier(Language.datModifierRegen,nHeal,5,game.Memory["CurrentTurn"],true);
									  Keys.caster.AddModifier(Language.datModifierWeaken,10,3,game.Memory["CurrentTurn"],true);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,15,1,Keys.caster);
								  }
							   	],
								[// --- EAT STAR ---
								 cSpellEffect.targeting_GemPurple,//Targeting
								  [0,0,2,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  Keys.caster.AddModifier(Language.datModifierStrength,3,75,game.Memory["CurrentTurn"],true);
									  var nHeal=Math.ceil(Keys.caster.MaxLife/25)
									  Keys.gemtarget.dead=true;
									  
									  DisplayItemArmy.Heal(game,Keys.caster,nHeal,true,true);									  
									   SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,9,1,Keys.caster);
								  }
							   	],
								[// --- BLIGHT ---
								 cSpellEffect.targeting_AllEnemies,//Targeting
								  [1,0,1,0,0,1,1],//ManaCost
								   function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamageA=Math.ceil(Keys.caster.GetSpecial(game)/2);
									  var nDamageB=Math.ceil(Keys.caster.GetSpecial(game)/3);
									  
									 Keys.armytarget.AddModifier(Language.datModifierDisease,nDamageA,3,game.Memory["CurrentTurn"],true);		
									 Keys.armytarget.AddModifier(Language.datModifierPoison,nDamageB,3,game.Memory["CurrentTurn"],true);	
									 
									 if (Keys.CastInstance==true)
										 {
									 		DisplayItemArmy.TakeDamage(game,Keys.caster,Keys.caster,Keys.caster.Life/2);  
									 		SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,2,1,Keys.caster);
										 }				 									
								  }
							   	],
								[// --- BURROW ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,2,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  Keys.caster.AddModifier(Language.datModifierArmor,1000,2,game.Memory["CurrentTurn"],true);
									  Keys.caster.AddModifier(Language.datModifierEnchanted,1000,2,game.Memory["CurrentTurn"],true);
									  Keys.caster.AddModifier(Language.datModifierStunned,0,2,game.Memory["CurrentTurn"],true);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,6,1,Keys.caster);
								  }
							   	],
								[// --- DIG ---
								 cSpellEffect.targeting_EntireBoard,//Targeting
								  [0,0,3,0,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  var NR=3+Math.floor(Keys.caster.GetSpecial(game)/25);
									  var Targets=PickRandomNrOfGems(Keys.gemtargets,NR,[GlobalVariables.GEM_GREEN])
									
									   for each (var Zim in Targets)
									  {
										  Keys.gemtargets[Targets[Zim]].dead=true;	
									  }
									  Keys.caster.AddModifier(Language.datModifierArmor,Targets.length*5,10,game.Memory["CurrentTurn"],true);	
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,6,1,Keys.caster);			
								  }
							   	],
								[// --- STEAL MANA: RANDOM ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [1,0,1,0,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=2+Math.floor(Keys.caster.GetSpecial(game)/40);
									  var cMana=Math.round(Math.random()*GlobalVariables.GEM_YELLOW);
									  
									  if (HandleMana.GetMana(game,1-Keys.caster.PlayerOwner,cMana)>=nDamage)
										{
									 	 HandleMana.RemoveMana(game,1-Keys.caster.PlayerOwner,nDamage,cMana);	
										 HandleMana.AddMana(game,Keys.caster.PlayerOwner,nDamage,cMana,true);
										}
								  }
							   	],
								[// --- FIREBALL ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [2,0,0,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=3;
									  var nDamageB=Keys.caster.GetSpecial(game)/5*0.4;
									  									  
									  DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,1/2);
									  Keys.armytarget.AddModifier(Language.datModifierBurn,nDamageB,2,game.Memory["CurrentTurn"],true);		
									  SpecialEffects.GenerateParticleProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],22,2,Keys.caster,Keys.armytarget,nDamage,5,1);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,2,1,Keys.armytarget);
								  }
							   	],
								[// --- SKULLBASH ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [1,0,0,0,1,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=5;									  									  
									  									  
									 Keys.armytarget.AddModifier(Language.datModifierStunned,0,2,game.Memory["CurrentTurn"],false);								  
									  nDamage=DisplayItemArmy.DealDamagePhysical(game,Keys.caster,Keys.armytarget,nDamage,1/2);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,2,1,Keys.armytarget);
								  }
							   	],
								[// --- SIP ALE ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,0,2,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  var nHeal=10+Math.ceil(Keys.caster.GetSpecial(game)/2)
									  
									  DisplayItemArmy.Heal(game,Keys.caster,nHeal,true,true);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,3,1,Keys.caster);
								  }
							   	],
								[// --- STONESKIN ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,3,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function			
									  var nDamage=15+Math.ceil(Keys.caster.GetSpecial(game)/2)			
									  Keys.caster.AddModifier(Language.datModifierArmor,nDamage,2,game.Memory["CurrentTurn"],true);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,13,1,Keys.caster);
								  }
							   	],
								[// --- RAIN OF FIRE ---
								 cSpellEffect.targeting_AllEnemies,//Targeting
								  [4,0,0,0,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=0;								
									  var nDamageB=Math.ceil(Keys.caster.GetSpecial(game)/25*0.5);								
									  									  
									  DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,1/5);
									  Keys.armytarget.AddModifier(Language.datModifierBurn,nDamageB,3,game.Memory["CurrentTurn"],true);	
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,5,1,Keys.armytarget);
									  
									  
									 if (Keys.CastInstance==true)
										 {
									 		SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,11,1,Keys.caster);
										 }		
								  }
							   	],
								[// --- HEAVY BLOW ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [4,0,0,0,1,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									var nDamageA=0;  
									var nDamageB=Keys.caster.GetAttack(game)/2;
									
									Keys.caster.AddModifier(Language.datModifierWeaken,nDamageB,5,game.Memory["CurrentTurn"],true);
										 
									DisplayItemArmy.DealDamagePhysical(game,Keys.caster,Keys.armytarget,nDamageA,2);									  
									SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,9,1,Keys.armytarget);

								  }
							   	],
								[// --- TOWERSHIELD ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,2,2,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  Keys.caster.AddModifier(Language.datModifierArmor,40,3,game.Memory["CurrentTurn"],true);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,11,1,Keys.caster);
									  Keys.caster.Defender=true
								  }
							   	],
								[// --- BACKSTAB ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [0,0,0,0,1,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									
																			 
									DisplayItemArmy.DealDamagePhysical(game,Keys.caster,Keys.armytarget,10,0.1);									  
									SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,6,1,Keys.armytarget);

								  }
							   	],
								[// --- EATFIRE ---
								 cSpellEffect.targeting_EntireBoard,//Targeting
								  [0,1,0,0,0,1,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  var Targets=new Array();
									  var NR=3;
									  
									  var Targets=PickRandomNrOfGems(Keys.gemtargets,NR,[GlobalVariables.GEM_RED])
									  for each (var Zim in Targets)
									  {
										  Keys.gemtargets[Zim].dead=true;
									  }
									  if (Targets.length==3)
									  {
											HandleMana.AddMana(game,Keys.caster.PlayerOwner,1,GlobalVariables.GEM_RED,true);						  
											SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,5,1,Keys.caster);
									  }
								  }
							   	],
								[// --- GEM SMASH ---
								 cSpellEffect.targeting_GemAny,//Targeting
								  [0,0,0,0,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  Keys.gemtarget.dead=true;						  
									SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,10,1,Keys.armytarget);
								  }
							   	],
								[// --- THROW STONE ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,0,2,0,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=1;
									  									  
									 Keys.armytarget.AddModifier(Language.datModifierStunned,0,2,game.Memory["CurrentTurn"],false);								  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,1/2,false);									  
									  
									  SpecialEffects.GenerateProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],2,Keys.caster,Keys.armytarget,nDamage,1/4);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,14,1,Keys.armytarget);
								  }
							   	],
								[// --- BOULDER TOSS ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,0,4,0,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=5;
									  									  
									 Keys.armytarget.AddModifier(Language.datModifierStunned,0,5,game.Memory["CurrentTurn"],false);								  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,1/2,false);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,7,1,Keys.armytarget);
									  
									  SpecialEffects.GenerateParticleProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],21,6,Keys.caster,Keys.armytarget,nDamage,5,1);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,14,1,Keys.caster);
								  }
							   	],
								[// --- EATSTONE ---
								 cSpellEffect.targeting_EntireBoard,//Targeting
								  [0,0,0,1,0,1,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  var NR=Math.min(game.Memory["BoardMana"][GlobalVariables.GEM_GREEN],3);									  
									  var Targets=PickRandomNrOfGems(Keys.gemtargets,NR,[GlobalVariables.GEM_GREEN])
									    for each (var Zim in Targets)
									  {
										  Keys.gemtargets[Zim].dead=true;
									  }
									  if (Targets.length==3)
									  {
											HandleMana.AddMana(game,Keys.caster.PlayerOwner,1,GlobalVariables.GEM_GREEN,true);
									  		SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,14,1,Keys.caster);
									  }
								  }
							   	],
								[// --- DECIEVE ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [1,0,0,0,1,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=Keys.armytarget.GetAttack(game);									  															  
									  nDamage=DisplayItemArmy.DealDamagePhysical(game,Keys.caster,Keys.armytarget,nDamage,1/5);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,2,1,Keys.armytarget);
								  }
							   	],
								[// --- BLOOD RITE ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [2,0,0,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									  
									  Keys.caster.AddModifier(Language.datModifierBleed,3,5,game.Memory["CurrentTurn"],true);			
									 Keys.caster.AddModifier(Language.datModifierStrength,15,5,game.Memory["CurrentTurn"],true);			
									   
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,9,1,Keys.caster);						  
								  }
							   	],
								[// --- STRENGTH ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,1,2,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									
									 Keys.caster.AddModifier(Language.datModifierStrength,10,5,game.Memory["CurrentTurn"],true);			
									   
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,6,1,Keys.caster);						  
								  }
							   	],
								[// --- GODLY STRENGTH ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,1,4,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									
									 Keys.caster.AddModifier(Language.datModifierStrength,20,5,game.Memory["CurrentTurn"],true);			
									   
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,6,1,Keys.caster);						  
								  }
							   	],
								[// --- REST ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,1,1,1,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  var nHeal=5+Math.floor(Keys.caster.GetSpecial(game)/4)
									  Keys.caster.AddModifier(Language.datModifierRejuvenation,nHeal,3,game.Memory["CurrentTurn"],true);
									  Keys.caster.AddModifier(Language.datModifierSleep,0,3,game.Memory["CurrentTurn"],true);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,10,1,Keys.caster);
									  Keys.caster.Defender=false
								  }
							   	],
								[// --- CURSE ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [1,0,0,1,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamageA=Math.ceil(Keys.caster.GetSpecial(game)/2);  
									  
									  Keys.armytarget.AddModifier(Language.datModifierCursed,nDamageA,5,game.Memory["CurrentTurn"],true);								  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,4,1,Keys.armytarget);

								  }
							   	],
								[// --- SILENCE ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,1,0,1,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamageA=2+Math.ceil(Keys.caster.GetSpecial(game)/25);  
									  
									  Keys.armytarget.AddModifier(Language.datModifierCursed,0,nDamageA,game.Memory["CurrentTurn"],false);								  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,10,1,Keys.armytarget);

								  }
							   	],
								[// --- ENTANGLE ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,0,2,0,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									  
									 Keys.armytarget.AddModifier(Language.datModifierStunned,0,3,game.Memory["CurrentTurn"],false);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,13,1,Keys.caster);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,13,1,Keys.armytarget);
								  }
							   	],
								[// --- CHANNEL PURPLE ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [1,1,1,1,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){									  
									HandleMana.AddMana(game,Keys.caster.PlayerOwner,3,GlobalVariables.GEM_PURPLE,true);			
									SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,1,1,Keys.caster);
								  }
							   	],
								[// --- CHANNEL RED ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,1,1,1,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){									  
									HandleMana.AddMana(game,Keys.caster.PlayerOwner,2,GlobalVariables.GEM_RED,true);	
									SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,11,1,Keys.caster);								   
								  }
							   	],
								[// --- CHANNEL BLUE ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [1,0,1,1,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){									  
									HandleMana.AddMana(game,Keys.caster.PlayerOwner,2,GlobalVariables.GEM_BLUE,true);	
									SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,12,1,Keys.caster);								   
								  }
							   	],
								[// --- CHANNEL GREEN ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [1,1,0,1,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){									  
									HandleMana.AddMana(game,Keys.caster.PlayerOwner,2,GlobalVariables.GEM_GREEN,true);	
									SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,13,1,Keys.caster);								   
								  }
							   	],
								[// --- CHANNEL YELLOW ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [1,1,1,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){									  
									HandleMana.AddMana(game,Keys.caster.PlayerOwner,2,GlobalVariables.GEM_YELLOW,true);
									SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,14,1,Keys.caster);									   
								  }
							   	],
								[// --- BLESS ---
								 cSpellEffect.targeting_UnitFriendly,//Targeting
								  [1,0,0,0,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function																		 			  
									  var nDamageA=Math.ceil(Keys.caster.GetSpecial(game)/2);  
									  
									  Keys.armytarget.AddModifier(Language.datModifierBlessed,nDamageA,5,game.Memory["CurrentTurn"],true);								  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,3,1,Keys.armytarget);

								  }
							   	],
								[// --- SHIELDCHARGE ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [3,0,1,0,1,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=Keys.caster.GetDefense(game)/2;									  									  
									  									  
									 Keys.caster.AddModifier(Language.datModifierArmor,20,5,game.Memory["CurrentTurn"],true);								  
									  nDamage=DisplayItemArmy.DealDamagePhysical(game,Keys.caster,Keys.armytarget,nDamage,0);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,2,1,Keys.armytarget);
								  }
							   	],
								[// --- AVALANCHE ---
								 cSpellEffect.targeting_AllEnemies,//Targeting
								  [2,0,0,4,0,2,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=10;									
									  									  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,1/10,false);
									  Keys.armytarget.AddModifier(Language.datModifierStunned,0,2,game.Memory["CurrentTurn"],false);	
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,5,1,Keys.armytarget);
									  SpecialEffects.GenerateProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],2,Keys.caster,Keys.armytarget,nDamage,1);
								  }
							   	],
								[// --- TRICK SHOT ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [3,0,0,0,1,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=Math.max(Keys.armytarget.GetAttack(game),Keys.armytarget.GetDefense(game),Keys.armytarget.GetSpecial(game));									  															  
									  nDamage=DisplayItemArmy.DealDamagePhysical(game,Keys.caster,Keys.armytarget,nDamage,0);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,6,1,Keys.armytarget);
								  }
							   	],
								[// --- BERSERKER ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [2,0,0,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									  
									  Keys.caster.AddModifier(Language.datModifierBurn,4,5,game.Memory["CurrentTurn"],true);			
									 Keys.caster.AddModifier(Language.datModifierBerserk,30,5,game.Memory["CurrentTurn"],true);			
									   
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,9,1,Keys.caster);						  
								  }
							   	],
								[// --- DARKBOLT ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,1,0,2,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=Keys.caster.GetSpecial(game)/3+3;									  									  
									  
									  Keys.caster.AddModifier(Language.datModifierBleed,nDamage,2,game.Memory["CurrentTurn"],true);		
									  nDamage=DisplayItemArmy.TakeDamage(game,Keys.caster,Keys.armytarget,nDamage*3,false);
									  SpecialEffects.GenerateProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],3,Keys.caster,Keys.armytarget,nDamage,1);
								  }
							   	],
								[// --- RAISE THE DEAD ---
								 cSpellEffect.targeting_EntireBoard,//Targeting
								  [0,0,1,2,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  var NR=2+Math.floor(Keys.caster.GetSpecial(game)/25);
									  
									  var Targets=PickRandomNrOfGems(Keys.gemtargets,NR,[GlobalVariables.GEM_RED,GlobalVariables.GEM_BLUE,GlobalVariables.GEM_GREEN,,GlobalVariables.GEM_YELLOW,,GlobalVariables.GEM_PURPLE])	 
									 
									   for each (var Zim in Targets)
									  {
										  GemManager.ChangeGemColor(game,Keys.gemtargets[Zim],GlobalVariables.GEM_SKULL);
									  }
										SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,15,1,Keys.caster);
								  }
							   	],								
								[// --- SIEGE ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,1,0,1,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						  
									  var X=Math.round(2+Math.random()*game.Memory["BoardSize"]-3);
									  var Y=Math.round(2+Math.random()*game.Memory["BoardSize"]-3);									  
									  									 
									  GemManager.DestroyGem(game,X,Y,null);									   
									  GemManager.DestroyGem(game,X+1,Y,null);									   
									  GemManager.DestroyGem(game,X-1,Y,null);										 
									  GemManager.DestroyGem(game,X,Y+1,null);									   
									  GemManager.DestroyGem(game,X,Y-1,null);									   
									  GemManager.DestroyGem(game,X+1,Y+1,null);										 
									  GemManager.DestroyGem(game,X-1,Y+1,null);									   
									  GemManager.DestroyGem(game,X+1,Y-1,null);									   
									  GemManager.DestroyGem(game,X+1,Y-1,null);									   
								  }
							   	],
								[// --- CHARGE ---
								 cSpellEffect.targeting_GemAny,//Targeting
								  [0,0,1,2,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  
									  for (var Zim=0; Zim<game.Memory["BoardSize"]; Zim++)
									  {
									  	GemManager.DestroyGem(game,Zim,Keys.gemtarget.Y,null)
									  }									  
								  }
							   	],
								[// --- WOUND ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [1,0,0,0,1,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									 
									  var nDamageB=1+Keys.caster.GetAttack(game)/5;
									  									  
									 if (Math.random()>1/2)
										 {
											 Keys.armytarget.AddModifier(Language.datModifierBleed,nDamageB,3,game.Memory["CurrentTurn"],true);								  
										 }
									  DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,20,0.1);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,6,1,Keys.armytarget);

								  }
							   	],
								[// --- GOBLINTOSS ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,0,0,1,1,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						  
									  var nDamage=Math.ceil(Keys.caster.GetSpecial(game)/20);
									  
									  var Gem=GemManager.GetGemAt(game,Math.round(Math.random()*game.Memory["BoardSize"]),Math.round(Math.random()*game.Memory["BoardSize"]))
									  if (Gem!=null)
									  {
										  HandleMana.AddMana(game,Keys.caster.PlayerOwner,1,Gem.Type,true);
									  	  GemManager.DestroyGem(game,-1,-1,Gem);  
									  }
								  }
							   	],
								[// --- REINFORCE ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,1,2,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									
									 Keys.caster.AddModifier(Language.datModifierArmor,Keys.caster.GetDefense(game)*0.25,5,game.Memory["CurrentTurn"],true);			
									   
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,9,1,Keys.caster);						  
								  }
							   	],
								[// --- BODY SLAM ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [0,0,0,2,1,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=Keys.caster.GetDefense(game);
									  									  
									 if (Math.random()>1/2)
										 {
											 Keys.caster.AddModifier(Language.datModifierArmor,3,100,game.Memory["CurrentTurn"],true);								  
										 }
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,0);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,6,1,Keys.armytarget);

								  }
							   	],
								[// --- IRON FIST ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [0,0,0,2,1,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=5;
									  									  
									 if (Math.random()>1/2)
										 {
											 Keys.caster.AddModifier(Language.datModifierBerserk,3,100,game.Memory["CurrentTurn"],true);								  
										 }
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,1);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,9,1,Keys.armytarget);

								  }
							   	],
								[// --- FIREBOMB ---
								  cSpellEffect.targeting_GemAny,//Targeting
								  [4,0,0,0,0,1,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						  
									  if (Keys.gemtarget!=null)
									  {
										  HandleMana.AddMana(game,Keys.caster.PlayerOwner,1,Keys.gemtarget.Type,true);
									  	  GemManager.DestroyGem(game,-1,-1,Keys.gemtarget);  
									  }
								  }
							   	],
								[// --- SHIELDORDER ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,0,0,1,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									
									 Keys.caster.AddModifier(Language.datModifierArmor,Keys.caster.GetSpecial(game),7,game.Memory["CurrentTurn"],true);			
									 Keys.caster.AddModifier(Language.datModifierSilenced,0,1,game.Memory["CurrentTurn"],true);			
									   
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,4,1,Keys.caster);						  
								  }
							   	],
								[// --- ATTACKDORDER ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,0,0,1,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									
									 Keys.caster.AddModifier(Language.datModifierArmor,Keys.caster.GetSpecial(game),7,game.Memory["CurrentTurn"],true);			
									 Keys.caster.AddModifier(Language.datModifierSilenced,0,1,game.Memory["CurrentTurn"],true);						
									   
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,4,1,Keys.caster);						  
								  }
							   	],
								[// --- GRAVEDIGGER ---
								 cSpellEffect.targeting_EntireBoard,//Targeting
								  [0,0,0,2,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  var NR=Math.ceil(Keys.caster.GetSpecial(game)/5);
									  var Targets=PickRandomNrOfGems(Keys.gemtargets,NR,[GlobalVariables.GEM_GREEN])
									   for each (var Zim in Targets)
									  {
										  GemManager.ChangeGemColor(game,Keys.gemtargets[Zim],GlobalVariables.GEM_SKULL);
									  }								  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,13,1,Keys.caster);
								  }
							   	],
								[// --- CLEARSKULLS ---
								 cSpellEffect.targeting_EntireBoard,//Targeting
								  [0,4,0,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  var NR=2+Math.ceil(Keys.caster.GetSpecial(game)/25);
									  var Targets=PickRandomNrOfGems(Keys.gemtargets,NR,[GlobalVariables.GEM_SKULL])
									 Keys.caster.AddModifier(Language.datModifierArmor,Targets.length*5,5,game.Memory["CurrentTurn"],true);	
									   for each (var Zim in Targets)
									  {
										  Targets[Zim].dead=true;
									  }
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,15,1,Keys.caster);
								  }
							   	],
								[// --- BURIAL ---
								 cSpellEffect.targeting_EntireBoard,//Targeting
								  [0,3,0,3,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  var NR=2+Math.ceil(Keys.caster.GetSpecial(game)/25);
									  var Targets=PickRandomNrOfGems(Keys.gemtargets,NR,[GlobalVariables.GEM_SKULL])
									  for each (var Zim in Targets)
									  {
										  GemManager.ChangeGemColor(game,Keys.gemtargets[Zim],GlobalVariables.GEM_PURPLE);
									  }
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,15,1,Keys.caster);
								  }
							   	],
								[// --- FIREELLY ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [4,0,0,0,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=Keys.caster.GetSpecial(game)/5*game.Memory["BoardMana"][GlobalVariables.GEM_RED];
									  var nDamageB=Keys.caster.GetSpecial(game)*1/30;
									  									  
									  DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,0);
									  Keys.armytarget.AddModifier(Language.datModifierBurn,nDamageB,5,game.Memory["CurrentTurn"],true);		
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,11,1,Keys.caster);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,16,1,Keys.armytarget);
								  }
							   	],
								[// --- WATERELLY ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,4,0,0,0,1,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=Keys.caster.GetSpecial(game)/5*game.Memory["BoardMana"][GlobalVariables.GEM_BLUE];								  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,0);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,12,1,Keys.caster);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,17,1,Keys.armytarget);
								  }
							   	],
								[// --- EARTHELLY ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,0,4,0,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=Keys.caster.GetSpecial(game)/5*game.Memory["BoardMana"][GlobalVariables.GEM_GREEN];		
									  									  
									 Keys.armytarget.AddModifier(Language.datModifierStunned,0,2,game.Memory["CurrentTurn"],false);								  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,0);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,13,1,Keys.caster);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,18,1,Keys.armytarget);
								  }
							   	],
								[// --- WINDELLY ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,0,0,4,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=Keys.caster.GetSpecial(game)/5*game.Memory["BoardMana"][GlobalVariables.GEM_YELLOW];		
									  									  
									 Keys.armytarget.AddModifier(Language.datModifierSilenced,0,2,game.Memory["CurrentTurn"],false);									  									  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,0);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,14,1,Keys.caster);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,19,1,Keys.armytarget);
								  }
							   	],
								[// --- FIREAVATAR ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,1,1,1,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									
									 Keys.caster.AddModifier(Language.datModifierBerserk,HandleMana.GetMana(game,Keys.caster.PlayerOwner,GlobalVariables.GEM_RED),15,game.Memory["CurrentTurn"],true);			
									HandleMana.RemoveMana(game,Keys.caster.PlayerOwner,HandleMana.GetMana(game,Keys.caster.PlayerOwner,GlobalVariables.GEM_RED), GlobalVariables.GEM_RED)  					  									
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,11,1,Keys.caster);
								  }
							   	],
								[// --- WATERAVATAR ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [1,0,1,1,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									
									 Keys.caster.AddModifier(Language.datModifierEnchanted,HandleMana.GetMana(game,Keys.caster.PlayerOwner,GlobalVariables.GEM_BLUE),15,game.Memory["CurrentTurn"],true);			
									HandleMana.RemoveMana(game,Keys.caster.PlayerOwner,HandleMana.GetMana(game,Keys.caster.PlayerOwner,GlobalVariables.GEM_BLUE), GlobalVariables.GEM_BLUE)  				  									
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,12,1,Keys.caster);
								  }
							   	],
								[// --- EARTHAVATAR ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [1,1,0,1,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									
									 Keys.caster.AddModifier(Language.datModifierArmor,HandleMana.GetMana(game,Keys.caster.PlayerOwner,GlobalVariables.GEM_GREEN),15,game.Memory["CurrentTurn"],true);			
									HandleMana.RemoveMana(game,Keys.caster.PlayerOwner,HandleMana.GetMana(game,Keys.caster.PlayerOwner,GlobalVariables.GEM_GREEN), GlobalVariables.GEM_GREEN)  	
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,13,1,Keys.caster);				  
								  }
							   	],
								[// --- WINDAVATAR ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [1,1,1,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									
									 Keys.caster.AddModifier(Language.datModifierStrength,HandleMana.GetMana(game,Keys.caster.PlayerOwner,GlobalVariables.GEM_YELLOW),7,game.Memory["CurrentTurn"],true);			
									HandleMana.RemoveMana(game,Keys.caster.PlayerOwner,HandleMana.GetMana(game,Keys.caster.PlayerOwner,GlobalVariables.GEM_YELLOW), GlobalVariables.GEM_YELLOW)  					  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,14,1,Keys.caster);
								  }
							   	],
								[// --- THUNDER ---
								 cSpellEffect.targeting_GemWhite,//Targeting
								  [2,1,0,0,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  
									  for (var Zim=0; Zim<game.Memory["BoardSize"]; Zim++)
									  {
									  	GemManager.DestroyGem(game,Keys.gemtarget.X,Zim,null)
									  }
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,1,1,Keys.caster);
								  }
							   	],
								[// --- IRON WALL ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,4,0,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									
									 Keys.caster.AddModifier(Language.datModifierArmor,Keys.caster.GetDefense(game)/2,5,game.Memory["CurrentTurn"],false);			
									 Keys.caster.AddModifier(Language.datModifierWeaken,Keys.caster.GetAttack()/2,5,game.Memory["CurrentTurn"],false);			
									   
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,6,1,Keys.caster);						  
								  }
							   	],
								[// --- ULTIMATE ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [1,1,1,1,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									  									  
									  Keys.armytarget.AddModifier(Language.datModifierStunned,0,2,game.Memory["CurrentTurn"],true)
									  Keys.armytarget.AddModifier(Language.datModifierSilenced,0,6,game.Memory["CurrentTurn"],true)
									  Keys.armytarget.AddModifier(Language.datModifierFear,0,4,game.Memory["CurrentTurn"],true)
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,6,1,Keys.armytarget);
								  }
							   	],
								[// --- THROW AXE ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,0,0,0,2,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=Keys.caster.GetAttack(game)/5*game.Memory["BoardMana"][GlobalVariables.GEM_SKULL];
									  									  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,1/2,false);
									
									SpecialEffects.GenerateProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],0,Keys.caster,Keys.armytarget,nDamage,1/2);
									  
								  }
							   	],
								[// --- STEAL LIFE ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [2,1,0,0,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=2;
									  									  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,1/2,true);
									   nDamage=DisplayItemArmy.Heal(game,Keys.caster,nDamage/2,true,false);			
									  
									SpecialEffects.GenerateProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],3,Keys.armytarget,Keys.caster,0-nDamage,1/4);								
								  }
							   	],
								[// --- STEAL LIFE GREATER ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [4,2,0,0,0,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=5;
									  									  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,1,true);
									   nDamage=DisplayItemArmy.Heal(game,Keys.caster,nDamage/2,true,false);				
									  
									SpecialEffects.GenerateProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],3,Keys.armytarget,Keys.caster,0-nDamage,1/2);							
								  }
							   	],
								[// --- SWARM STRIKE ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [2,0,0,0,1,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function
									  var nDamage=Keys.caster.GetSpecial(game)/3;
									  nDamage*=Math.round(Math.random()*3)+2
									  									  
									  nDamage=DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,0);	
									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,10,1,Keys.armytarget);							
								  }
							   	],
								[// --- SAVAGE ROAR ---
								 cSpellEffect.targeting_AllFriendlies,//Targeting
								  [2,0,0,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function													  
									  var nBonus=9+Math.ceil(Keys.caster.GetSpecial(game)/10);
									  Keys.armytarget.AddModifier(Language.datModifierBerserk,nBonus,1,game.Memory["CurrentTurn"],false)
									   SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,9,1,Keys.armytarget);
								  }
							   	],
								[// --- FREEZE ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,2,0,1,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									  
									  Keys.armytarget.AddModifier(Language.datModifierStunned,0,3,game.Memory["CurrentTurn"],true);		
									  Keys.armytarget.AddModifier(Language.datModifierArmor,100,3,game.Memory["CurrentTurn"],true);		
									  Keys.armytarget.AddModifier(Language.datModifierEnchanted,100,3,game.Memory["CurrentTurn"],true);		
									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,12,1,Keys.caster);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,7,1,Keys.armytarget);
								  }
							   	],
								[// --- SAND ATTACK ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [0,1,0,1,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									
																			 
									DisplayItemArmy.DealDamagePhysical(game,Keys.caster,Keys.armytarget,10,0.2);									  
									SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,6,1,Keys.armytarget);

								  }
							   	],
								[// --- SLEEP ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,0,2,0,1,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									 
									  var nDamage=1+Keys.caster.GetSpecial(game)/2;
									  
									  Keys.armytarget.AddModifier(Language.datModifierDisease,nDamage,1,game.Memory["CurrentTurn"],true);
									  Keys.armytarget.AddModifier(Language.datModifierSleep,0,3,game.Memory["CurrentTurn"],true);
									  							  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,4,1,Keys.armytarget);

								  }
							   	],
								[// --- SLUMBER ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,1,2,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									 
									  var nDamage=Keys.caster.GetAttack(game)/2;
									  
									  Keys.caster.AddModifier(Language.datModifierBerserk,nDamage,5,game.Memory["CurrentTurn"],true);
									  Keys.caster.AddModifier(Language.datModifierSleep,0,5,game.Memory["CurrentTurn"],true);
									  							  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,10,1,Keys.caster);

								  }
							   	],
								[// --- PLAGUE EXPLOSION ---
								 cSpellEffect.targeting_AllEnemies,//Targeting
								  [1,0,0,4,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=Math.ceil(Keys.armytarget.GetModifierValue(Language.datModifierDisease,true,game.Memory["CurrentTurn"]));									  									 
									  DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,0.1);						 													  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,4,1,Keys.armytarget);
								  }
							   	],
								[// --- POISON EXPLOSION ---
								 cSpellEffect.targeting_AllEnemies,//Targeting
								  [0,0,2,3,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=Math.ceil(Keys.armytarget.GetModifierValue(Language.datModifierPoison,true,game.Memory["CurrentTurn"]));											 
									  DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,0.1);													  									  				 									 													  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,4,1,Keys.armytarget);
								  }
							   	],
								[// --- TOXIC ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,0,2,0,1,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=Keys.caster.GetSpecial(game)/10;
									  if (Keys.armytarget.HasModifier(Language.datModifierPoison,game.Memory["CurrentTurn"])==true)
									  {
										  nDamage=Math.ceil(nDamage*2);
									  }									  
									Keys.armytarget.AddModifier(Language.datModifierPoison,nDamage,10,game.Memory["CurrentTurn"],true);								  										
									  SpecialEffects.GenerateParticleProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],4,9,Keys.caster,Keys.armytarget,0,5,1);
								  }
							   	],
								[// --- NECRO PLAGUE ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [1,1,0,0,1,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=Keys.caster.GetAttack(game);
									  if (Keys.armytarget.HasModifier(Language.datModifierPoison,game.Memory["CurrentTurn"])==true)
									  {
										  nDamage=Math.ceil(nDamage*2);
									  }									  
									  DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,0);	
									  SpecialEffects.GenerateParticleProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],4,9,Keys.caster,Keys.armytarget,0,5,1);
								  }
							   	],
								[// --- ANTHRAG KISS ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [1,1,0,2,1,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  									  
									  Keys.armytarget.AddModifier(Language.datModifierDisease,100,1,game.Memory["CurrentTurn"],true);					
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,4,5/4,Keys.armytarget);

								  }
							   	],
								[// --- PLAGUE BLADE ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [0,2,0,0,1,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamageA=Keys.caster.GetAttack(game);  
									  var nDamageB=1+Keys.caster.GetSpecial(game);
									  Keys.armytarget.AddModifier(Language.datModifierDisease,nDamageB,3,game.Memory["CurrentTurn"],true);								  
									  DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamageA,0);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,2,1,Keys.armytarget);								  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,4,3/4,Keys.armytarget);

								  }
							   	],
								[// --- POISON BLADE ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [0,0,2,0,1,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamageA=Keys.caster.GetAttack(game);  
									  var nDamageB=1+Keys.caster.GetSpecial(game)/3;
										Keys.armytarget.AddModifier(Language.datModifierPoison,nDamageB,3,game.Memory["CurrentTurn"],true);								  
									  DisplayItemArmy.DealDamagePhysical(game,Keys.caster,Keys.armytarget,nDamageA,0);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,4,1,Keys.armytarget);

								  }
							   	],
								[// --- BLEED ---
								 cSpellEffect.targeting_BasicAttack,//Targeting
								  [2,0,0,0,1,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamageA=2;  
									  var nDamageB=1+Keys.caster.GetSpecial(game)/3;
										Keys.armytarget.AddModifier(Language.datModifierBleed,nDamageB,3,game.Memory["CurrentTurn"],true);								  
									  DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamageA,1);									  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,6,1,Keys.armytarget);

								  }
							   	],
								[// --- RUPTURE ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,0,2,0,1,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=Keys.caster.GetSpecial(game)/5;
									  if (Keys.armytarget.HasModifier(Language.datModifierBleed,game.Memory["CurrentTurn"])==true)
									  {
										  nDamage=Math.ceil(nDamage*2);
									  }									  
									Keys.armytarget.AddModifier(Language.datModifierBleed,nDamage,5,game.Memory["CurrentTurn"],true);								  													  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,6,1,Keys.armytarget);
								  }
							   	],
								[// --- HEX ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,3,0,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamageA=Math.ceil(Keys.caster.GetSpecial(game)/2);  
									  
									  Keys.armytarget.AddModifier(Language.datModifierCursed,nDamageA,2,game.Memory["CurrentTurn"],true);	
									  Keys.armytarget.AddModifier(Language.datModifierSilenced,0,2,game.Memory["CurrentTurn"],true);								  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,10,1,Keys.armytarget);

								  }
							   	],
								[// --- STONEFORM ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,3,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  var nHeal=Math.ceil(Keys.caster.MaxLife/10)
									  Keys.caster.AddModifier(Language.datModifierRegen,nHeal,3,game.Memory["CurrentTurn"],true);
									  Keys.caster.AddModifier(Language.datModifierStunned,0,3,game.Memory["CurrentTurn"],true);
									  Keys.caster.AddModifier(Language.datModifierArmor,25,3,game.Memory["CurrentTurn"],true);
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,15,1,Keys.caster);
								  }
							   	],
								[// --- LESSER HEAL ---
								 cSpellEffect.targeting_UnitFriendly,//Targeting
								  [0,0,2,0,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									
									  var nDamage=4+Math.floor(Keys.caster.GetSpecial(game)/2);									  									  									
									   DisplayItemArmy.Heal(game,Keys.armytarget,nDamage,true,true);										
									  SpecialEffects.GenerateParticleProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],3,3,Keys.caster,Keys.armytarget,0,3,1/2);										 
								  }
							   	],
								[// --- GREATER HEAL ---
								 cSpellEffect.targeting_UnitFriendly,//Targeting
								  [0,0,4,0,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									
									  var nDamage=6+Math.floor(Keys.caster.GetSpecial(game));									  									  									
									   DisplayItemArmy.Heal(game,Keys.armytarget,nDamage,true,true);										
									  SpecialEffects.GenerateParticleProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],3,3,Keys.caster,Keys.armytarget,0,1,1/2);										 
								  }
							   	],
								[// --- BLESS ---
								 cSpellEffect.targeting_UnitFriendly,//Targeting
								  [0,2,2,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function																		 			  
									  var nDamageA=Math.ceil(Keys.caster.GetSpecial(game)/2);  
									  
									  var nDamage=Math.floor(Keys.caster.GetSpecial(game));									  									  									
									   DisplayItemArmy.Heal(game,Keys.armytarget,nDamage,true,true);																				
									   
									  Keys.armytarget.AddModifier(Language.datModifierBlessed,nDamageA,5,game.Memory["CurrentTurn"],true);								  
									  SpecialEffects.GenerateParticleProjectileArmyArmy(game.Memory["HWH"],game.Memory["EffectLayer"],3,0,Keys.caster,Keys.armytarget,0,1,1);

								  }
							   	],
								[// --- SEARING FLAME ---
								 cSpellEffect.targeting_UnitBoth,//Targeting
								  [3,0,0,2,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									
									  var nDamage=1+Math.floor(Keys.caster.GetSpecial(game));
									  
									   	DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage*2,0);
										Keys.armytarget.AddModifier(Language.datModifierRegen,nDamage/5,5,game.Memory["CurrentTurn"],true);
										  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,1,1,Keys.armytarget);
									 	 
									}
							   	],
								[// --- BLOOD RAIN ---
								 cSpellEffect.targeting_AllEnemies,//Targeting
								  [2,2,0,0,0,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=Math.ceil(Keys.caster.GetSpecial(game)/5);
									  
									  if (Math.random()*3>2)
									  {
											Keys.armytarget.AddModifier(Language.datModifierBleed,nDamage,5,game.Memory["CurrentTurn"],true);								  
									  }
									  else
									  {
										  Keys.armytarget.AddModifier(Language.datModifierBleed,nDamage,3,game.Memory["CurrentTurn"],true);		
									  }
										 									  
									 SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,4,1,Keys.armytarget);

								  }
							   	],
								[// --- WALL OF FIRE ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [1,1,0,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						  
									  var nHeal=Math.ceil(Keys.caster.GetMaxLife(game));
									  
									  nHeal=DisplayItemArmy.Heal(game,Keys.caster,nHeal,false,true)/10;
									  Keys.caster.AddModifier(Language.datModifierBurn,nHeal,10,game.Memory["CurrentTurn"],true);		
									  
									   SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,11,1,Keys.caster);
								  }
							   	],
								[// --- GROW ---
								 cSpellEffect.targeting_UnitFriendly,//Targeting
								  [0,1,3,0,1,0,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						  
									  var nHeal=Math.ceil(Keys.armytarget.GetMaxLife(game)*0.25);
									  
									  nHeal=DisplayItemArmy.Heal(game,Keys.armytarget,nHeal,false,true);
									  Keys.armytarget.AddModifier(Language.datModifierBurn,nHeal,10,game.Memory["CurrentTurn"],true);
									  
									  Keys.armytarget.AddModifier(Language.datModifierStrength,10,5,game.Memory["CurrentTurn"],true);		
									  Keys.armytarget.AddModifier(Language.datModifierStrength,10,3,game.Memory["CurrentTurn"],true);		
									  Keys.armytarget.AddModifier(Language.datModifierStrength,10,1,game.Memory["CurrentTurn"],true);		
									  
									   SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,11,1,Keys.caster);
								  }
							   	],
								[// --- CHANNEL SKULL ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [0,0,0,0,0,1,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){									  
									HandleMana.AddMana(game,Keys.caster.PlayerOwner,1,GlobalVariables.GEM_SKULL,true);			
									SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,1,1,Keys.caster);
								  }
							   	],
								[// --- NIGHTMARE ---
								 cSpellEffect.targeting_UnitEnemy,//Targeting
								  [0,4,0,1,1,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=Math.ceil(Keys.caster.GetAttack(game));
									  if (Keys.armytarget.HasModifier(Language.datModifierSleep,game.Memory["CurrentTurn"])==true)
									  {
										  nDamage=nDamage*2;
									  }									  
									  
									   	DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,0);						  													  
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,1,1,Keys.armytarget);
								  }
							   	],
								[// --- VEIL OF NIGHTMARES ---
								 cSpellEffect.targeting_AllEnemies,//Targeting
								  [0,5,0,0,2,1,1],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function									  
									  var nDamage=Math.ceil(Keys.caster.GetAttack(game));
									  
									  if (Keys.armytarget.HasModifier(Language.datModifierSleep,game.Memory["CurrentTurn"])==true)
									  {
									   	DisplayItemArmy.DealDamageMagical(game,Keys.caster,Keys.armytarget,nDamage,0);		  													  
									  	SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,1,1,Keys.armytarget);						  
									  }
										  
									Keys.armytarget.AddModifier(Language.datModifierSleep,0,2,game.Memory["CurrentTurn"],true);		

								  }
							   	],
								[// --- CHANNEL 3 SKULL ---
								 cSpellEffect.targeting_NullTarget,//Targeting
								  [3,0,0,0,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){									  
									HandleMana.AddMana(game,Keys.caster.PlayerOwner,3,GlobalVariables.GEM_SKULL,true);			
									SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,1,1,Keys.caster);
								  }
							   	],
								[// --- SANDSTORM ---
								 cSpellEffect.targeting_EntireBoard,//Targeting
								  [0,0,2,1,0,0,0],//ManaCost
								  function(game:MainGame,Keys:DataItemSpell){
									  //Spell function						
									  var NR=3+Math.ceil(Keys.caster.GetSpecial(game)/25);
									  var Targets=PickRandomNrOfGems(Keys.gemtargets,NR,[GlobalVariables.GEM_RED,GlobalVariables.GEM_BLUE,GlobalVariables.GEM_GREEN])
									  for each (var Zim in Targets)
									  {
										  GemManager.ChangeGemColor(game,Keys.gemtargets[Zim],GlobalVariables.GEM_YELLOW);
									  }
									  SpecialEffects.MakeSpecialEffectExplosionOnArmy(game,15,1,Keys.caster);
								  }
							   	],
							]
								
								
								
								
								
								
								public static function PickRandomNrOfGems(GemArray:Array,NR:Number,GemColors:Array)
								 {															  
									  var Targets=new Array();
									  for (var X=0; X<GemArray.length;X++)
									  {
										  if (GemArray[X]!=null)
										  {
											   var False=false;
											  for each (var Zim in GemColors)
											  {
												  if ( GemArray[X].Type==Zim+1)
												  {
													  False=true;
												  }
											  }
											  if (GemArray[X]!=null && GemArray[X]!=undefined && GemArray[X].dead!=true && False==true)
											 {
												 Targets.push(GemArray[X])
										 	}
										  }
									  }
									  NR=Math.min(Targets.length,NR)									  
									  var FinalTargets=new Array();
									  while (FinalTargets.length<NR)
									  {										  
										  var Zim=Math.round(Math.random()*(Targets.length-1));
										//  if (FinalTargets.indexOf(Targets[Zim])==-1)
										  {
											  FinalTargets.push(GemArray.indexOf(Targets[Zim]));
											  Targets.splice(Zim,1);
										  }
									  }
									  return FinalTargets;
								 }
/*
   <Text tag="[SPELL_S1ST_NAME]">Song of Terror</Text>
   <Text tag="[SPELL_S1ST_DESC]">Inflicts Terror on enemies (doing damage every turn equal to half the Purple Stars in play) for 3 turns,</Text>
   <Text tag="[SPELL_S1ST_DETL]">plus 1 turn for every 8 Yellow Mana</Text> 
   <Text tag="[SPELL_S1AG_NAME]">Song of Ages</Text>
   <Text tag="[SPELL_S1AG_DESC]">Extends status effects on caster for 5 turns,</Text>
   <Text tag="[SPELL_S1AG_DETL]">plus 1 turn for every 8 Yellow Mana</Text>   
   <Text tag="[SPELL_S1SO_NAME]">Song of Sorrow</Text>
   <Text tag="[SPELL_S1SO_DESC]">Extends Status Effects on enemy for 3 turns,</Text>
   <Text tag="[SPELL_S1SO_DETL]">plus 1 turn for every 8 Yellow Mana</Text>
   
   ------------		WARLORD SPELLS		------------
   
   <Text tag="[SPELL_S1AR_NAME]">Arrow of Slaying</Text>
   <Text tag="[SPELL_S1AR_DESC]">Has a percentage chance equal to half your Green Mana to reduce an enemy&apos;s Life Points to 1 (max 50%)</Text>
   <Text tag="[SPELL_S1AR_DETL]">There is no effect if the Arrow misses</Text>     
   <Text tag="[SPELL_S1FA_NAME]">Flame Arrow</Text>
   <Text tag="[SPELL_S1FA_DESC]">Destroys all Red Gems</Text>
   <Text tag="[SPELL_S1FA_DETL]">Adds +1 to Red Mana for every 2 Red gems destroyed</Text>
   <Text tag="[SPELL_S1IA_NAME]">Ice Arrow</Text>
   <Text tag="[SPELL_S1IA_DESC]">Destroys all Blue Gems</Text>
   <Text tag="[SPELL_S1IA_DETL]">Adds +1 to Blue Mana for every 2 Blue gems destroyed</Text>
   <Text tag="[SPELL_S1SA_NAME]">Storm Arrow</Text>
   <Text tag="[SPELL_S1SA_DESC]">Destroys all Yellow Gems</Text>
   <Text tag="[SPELL_S1SA_DETL]">Adds +1 to Yellow Mana for every 2 Yellow gems destroyed</Text>
   <Text tag="[SPELL_SCHL_NAME]">Challenge</Text>
   <Text tag="[SPELL_SCHL_DESC]">Adds +50% to damage on both players for 6 turns</Text>
   <Text tag="[SPELL_SCHL_DETL]">Your turn does not end if Yellow Mana is 15+</Text>      
   <Text tag="[SPELL_SCHM_NAME]">Charm</Text>
   <Text tag="[SPELL_SCHM_DESC]">All Skulls in play are destroyed</Text>
   <Text tag="[SPELL_SCHM_DETL]">Adds +1 to Life Points for every Skull destroyed</Text>
   <Text tag="[SPELL_SCMA_NAME]">Consume Mana</Text>
   <Text tag="[SPELL_SCMA_DESC]">Destroys all Mana Gems of a random color</Text>
   <Text tag="[SPELL_SCMA_DETL]">You gain the full effects for all gems destroyed</Text>
   <Text tag="[SPELL_SCON_NAME]">Conflagration</Text>
   <Text tag="[SPELL_SCON_DESC]">Turns all gems of the selected type into Red Gems</Text>
   <Text tag="[SPELL_SCON_DETL]">Any type can be selected, including Skulls or Gold</Text>   
   <Text tag="[SPELL_SKLO_NAME]">Knight Lord</Text>
   <Text tag="[SPELL_SKLO_DESC]">Doubles the number of Purple Stars in play</Text>
   <Text tag="[SPELL_SKLO_DETL]">After gems are transformed, the turn ends</Text>   
   <Text tag="[SPELL_SSBM_NAME]">Submerge</Text>
   <Text tag="[SPELL_SSBM_DESC]">Doubles your Blue Mana</Text>
   <Text tag="[SPELL_SSBM_DETL]">Reduces your enemy&apos;s Yellow Mana by half</Text>   
   <Text tag="[SPELL_SSGZ_NAME]">Stone Gaze</Text>
   <Text tag="[SPELL_SSGZ_DESC]">Turns Green Gems into Skulls</Text>
   <Text tag="[SPELL_SSGZ_DETL]">Your turn does not end if Green Mana is 15+</Text>  
   <Text tag="[SPELL_S1DC_NAME]">Discord</Text>
   <Text tag="[SPELL_S1DC_DESC]">Destroys all Purple Stars, and all gems around them</Text>
   <Text tag="[SPELL_S1DC_DETL]">You gain the full effects for all gems destroyed</Text>
   <Text tag="[SPELL_S1DI_NAME]">Disguise</Text>
   <Text tag="[SPELL_S1DI_DESC]">Turns all gems of the selected type into Blue Gems</Text>
   <Text tag="[SPELL_S1DI_DETL]">Any type can be selected, including Skulls or Gold</Text>
*/
	}
	
}
