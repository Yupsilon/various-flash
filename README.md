# Flash Games #

Ths project contains three flash games. The technology is outdated, but they are still playable.

### Extra Terminator

This is a 2D side scroller shooting game (think Contra or Metal Slug). It features 8 levels and multiple enemies.

Play the release version at: kongregate.com/games/Wisakejak/extra-terminator

### Prism

Topdown shoother whith randomly generated waves and bosses.

Play the release version at: kongregate.com/games/Wisakejak/prism

### Puzzle Quest

This is a work-in-progress fan recreation of the game Puzzle Quest, in Flash.

This game was never completed or released.

# Installation

To access the game code and data, opening the respective '.fla' file with a stable version of Adobe Flash is required.